webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/animations/animation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return fadeInAnimation; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_animations__ = __webpack_require__("../../../animations/esm5/animations.js");

var fadeInAnimation = Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["i" /* trigger */])('fadeInAnimation', [
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["h" /* transition */])(':enter', [
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* style */])({ opacity: 0 }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["e" /* animate */])('.150s', Object(__WEBPACK_IMPORTED_MODULE_0__angular_animations__["g" /* style */])({ opacity: 1 }))
    ]),
]);


/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-loading></app-loading>\r\n<app-navbar></app-navbar>\r\n<app-alerts></app-alerts>\r\n<router-outlet></router-outlet>\r\n<br><br>\r\n<section class=\"footer-section\">\r\n    <footer class=\"footer\">\r\n        <div class=\"container\">\r\n            <div class=\"col-lg-1 float-left\"><img id=\"img-footer\" src=\"../assets/img/logo-footer.svg\"></div>\r\n            <div class=\"col-lg-11 float-left\">\r\n                <p class=\"footer-block\">Use of this Web site constitutes acceptance of <a [routerLink]=\"['/terms-and-conditions']\">EXCHANGELOANS terms of Use for Trading</a> on This Site and the <a [routerLink]=\"['/trading-terms']\">Website Usage Terms & Conditions</a><br> 8190 E.Kaiser Blvd, Anaheim Hills, CA 92808 | Phone (800) 931-2424\r\n                    | CA Broker DRE #01759687<br> Copyright 2018 EXCHANGELOANS. All Rights Reserved.</p>\r\n            </div>\r\n        </div>\r\n    </footer>\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_Forms__ = __webpack_require__("../../../Forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_select2__ = __webpack_require__("../../../../ng2-select2/ng2-select2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_select2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_select2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__structExports_components__ = __webpack_require__("../../../../../src/app/structExports/components.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__routings_routing__ = __webpack_require__("../../../../../src/app/routings/routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_terms_and_conditions_terms_and_conditions_component__ = __webpack_require__("../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_trading_terms_trading_terms_component__ = __webpack_require__("../../../../../src/app/components/trading-terms/trading-terms.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_how_to_buy_notes_how_to_buy_notes_component__ = __webpack_require__("../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_how_to_sell_notes_how_to_sell_notes_component__ = __webpack_require__("../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// this for all ours services 

// this for all ours components 







var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["q" /* buyNotesComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["t" /* loginComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["g" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["r" /* loadingComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["p" /* alertsComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["u" /* myExchangeComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["s" /* loanNoteDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["m" /* SubmitOfferComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["i" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["a" /* AboutUsComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["b" /* ContactUsComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["l" /* ServicesComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["c" /* FaqComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["v" /* transactionComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["k" /* SellnoteComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_terms_and_conditions_terms_and_conditions_component__["a" /* TermsAndConditionsComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_trading_terms_trading_terms_component__["a" /* TradingTermsComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_how_to_buy_notes_how_to_buy_notes_component__["a" /* HowToBuyNotesComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_how_to_sell_notes_how_to_sell_notes_component__["a" /* HowToSellNotesComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["h" /* PaymentComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["d" /* ForgotPasswordComponent */],
                __WEBPACK_IMPORTED_MODULE_9__structExports_components__["j" /* ResetPasswordComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__routings_routing__["a" /* AppRouting */],
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7_ng2_select2__["Select2Module"]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["f" /* buyNotesService */],
                __WEBPACK_IMPORTED_MODULE_11__guards_auth_guard__["a" /* authGuard */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["e" /* authentificationService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["b" /* NavbarService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["i" /* loadingService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["l" /* pagerService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["d" /* alertService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["a" /* ExcelService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["k" /* myExchangeService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["j" /* loanNoteDetailService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["o" /* transactionService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["c" /* SellnoteService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["m" /* paymentService */],
                __WEBPACK_IMPORTED_MODULE_8__structExports_services__["g" /* contactUsService */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_8__structExports_services__["h" /* httpServiceToken */],
                    useFactory: function (backend, options) {
                        return new __WEBPACK_IMPORTED_MODULE_8__structExports_services__["h" /* httpServiceToken */](backend, options);
                    },
                    deps: [__WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* XHRBackend */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]]
                }
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/about-us/about-us.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/about-us/about-us.component.html":
/***/ (function(module, exports) {

module.exports = "\n<section style=\"margin-top: 20px;\">\n  <div class=\"container-fluid FAQ-page\">\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n        <section>\n          <div class=\"col-lg-12 bg-light\">\n            <h4 class=\"infoclient\">Pub</h4>\n          </div>\n        </section>\n        <section style=\"margin-top: 20px;\">\n          <div class=\"col-lg-12 doc-space\"></div>\n        </section>\n      </div>\n      <div class=\"col-lg-10 bg-light\">\n        <div class=\"col-lg-12\">\n          <h2 class=\"infoclient-aboutus\">Welcome</h2>\n        </div>\n        <div class=\"col-lg-7 float-left\">\n          <span class=\"text-bold\">I want to personally welcome you to EXCHANGELOANS.</span>\n          <p class=\"text-one aboutus-page\"><a href=\"\">FCI Lender Services</a> is the sister company of EXCHANGELOANS and has been deeply involved in loan servicing, default, and the buying and selling of notes and REO properties for 30 years. As a servicer with $1.7 Billion under management we understand the importance of buying and selling Notes and REOs accurately and compliantly. Our expert staff regularly assists clients in the resolution of issues resulting from improper loan trades, including serious disclosure and compliance issues. <span class=\"text-bold\">We knew there had to be a better way.</span></p>\n\n          <p class=\"text-one aboutus-page\">EXCHANGELOANS decided it was time to use our expertise to offer a solution to the market. EXCHANGELOANS is a sophisticated and efficient loan trading platform where Buyers and Sellers can trade privately with the experts at EXCHANGELOANS closely monitoring all aspects of the transaction. We help Sellers put together compliant full disclosure packages for sale that encourage and give confidence to Buyers. We provide Buyers current accurate data, including never before seen live servicing data, to use for their investment decisions. Both Buyers and Sellers can feel confident the transaction will proceed and close compliantly through the interactive step-by-step Transaction Summary, and then move smoothly into servicing. EXCHANGELOANS will also be there after the sale to assist with any unforeseen issues.</p>\n        </div>\n        <div class=\"col-lg-5 float-left\">\n          <img class=\"img-about-us text-center\" src=\"assets/img/img-aboutus.jpg\">\n          <p class=\"text-center text-img\">Michael Griffith, Chairman</p><br>\n        </div>\n        <div class=\"col-lg-12\">\n          <p class=\"text-one aboutus-page\">This is <span class=\"text-bold underline\">not</span> an auction site demanding immediate participation, forcing rushed decisions and possible missteps. You will also see none of the hype, product or services pushing, fluffed ads, pricing games or blind product listing seen on some other sites. We know there is much more to successfully buying and selling Notes and Properties than just listing them on some internet site.</p><br>\n\n          <p class=\"text-one aboutus-page\">To help our clients, we put many user friendly tools on FCI EXCHANGE including My Product Alerts where Buyers can be notified of just the product for sale that meets their investment criteria. There is also a sophisticated Loan Investment Strategy Model that calculates the best retention or liquidation scenarios for a particular purchase. We are here to help anyone from Novice to Expert Buy Notes and Sell Notes with confidence, quickly and compliantly.</p><br>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/about-us/about-us.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutUsComponent = (function () {
    function AboutUsComponent(nav) {
        this.nav = nav;
        this.nav.show();
    }
    AboutUsComponent.prototype.ngOnInit = function () {
    };
    AboutUsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-about-us',
            template: __webpack_require__("../../../../../src/app/components/about-us/about-us.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/about-us/about-us.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */]])
    ], AboutUsComponent);
    return AboutUsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/alerts/alerts.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".allAlerts{\r\n\tmargin-top: 1%;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/alerts/alerts.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n\t<div *ngFor=\"let alert of alerts\" class=\"{{ cssClass(alert) }} alert-dismissable text-center allAlerts\">\r\n\t    <strong><i class=\"fa {{ fontClass(alert)}}\" aria-hidden=\"true\"></i></strong>&nbsp; {{alert.message}}\r\n\t     <a class=\"close\" (click)=\"removeAlert(alert)\">&times;</a>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/alerts/alerts.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return alertsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_models__ = __webpack_require__("../../../../../src/app/structExports/models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__animations_animation__ = __webpack_require__("../../../../../src/app/animations/animation.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var alertsComponent = (function () {
    function alertsComponent(alertService) {
        this.alertService = alertService;
        this.alerts = [];
    }
    alertsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getAlert().subscribe(function (alert) {
            if (!alert) {
                _this.alerts = [];
                return;
            }
            _this.alerts.push(alert);
        });
    };
    alertsComponent.prototype.removeAlert = function (alert) {
        this.alerts = this.alerts.filter(function (x) { return x !== alert; });
    };
    alertsComponent.prototype.cssClass = function (alert) {
        if (!alert) {
            return;
        }
        switch (alert.type) {
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Success:
                return 'alert alert-success';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Error:
                return 'alert alert-danger';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Info:
                return 'alert alert-info';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Warning:
                return 'alert alert-warning';
        }
    };
    alertsComponent.prototype.fontClass = function (alert) {
        if (!alert) {
            return;
        }
        switch (alert.type) {
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Success:
                return 'fa-check-circle';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Error:
                return 'fa-exclamation-circle';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Info:
                return 'fa-info-circle';
            case __WEBPACK_IMPORTED_MODULE_1__structExports_models__["a" /* AlertType */].Warning:
                return 'fa-exclamation';
        }
    };
    alertsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-alerts',
            template: __webpack_require__("../../../../../src/app/components/alerts/alerts.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/alerts/alerts.component.css")],
            animations: [__WEBPACK_IMPORTED_MODULE_3__animations_animation__["a" /* fadeInAnimation */]],
            host: { '[@fadeInAnimation]': '' }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__structExports_services__["d" /* alertService */]])
    ], alertsComponent);
    return alertsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/buyNotes/buyNotes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/buyNotes/buyNotes.component.html":
/***/ (function(module, exports) {

module.exports = "<section *ngIf=\"IsViewSwitch;else ViewTwo\">\n    <div class=\"container-fluid\">\n\n        <div class=\"col-lg-2 col-filter\">\n            <h4 class=\"my-4\">Notes for Sales</h4>\n            <form novalidate #formFilter=\"ngForm\" (ngSubmit)=\"onFilter()\">\n                <div class=\"form-group\">\n                    <label class=\"text-label\" for=\"UPB\">Product ID</label>\n                    <input class=\"form-control\" type=\"text\" name=\"UPB\" [(ngModel)]=\"filterFiledUpb\" id=\"UPB\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"State\">City</label>\n                    <input type=\"text\" class=\"form-control\" name=\"State\" [(ngModel)]=\"filterFiledState\" id=\"State\" placeholder=\"State\">\n                </div>\n                <div class=\"form-group\">\n                    <button class=\"btn btn-primary btn-block\" type=\"submit\"><i aria-hidden=\"true\" class=\"fa fa-search\"></i> SEARCH</button>\n                    <button *ngIf=\"resetFilter\" class=\"btn btn-success btn-block\" (click)=\"onResetFilter()\"><i class=\"fa fa-window-restore\" aria-hidden=\"true\"></i>&nbsp;RESET</button>\n\n                </div>\n            </form>\n        </div>\n\n\n\n        <div class=\"col-lg-10 col-all-items\">\n\n            <div class=\"row\">\n                <div class=\"col-sm-12 pagination-padding\">\n                    <nav aria-label=\"Page navigation example\">\n                        <ul class=\"pagination justify-content-end float-right\">\n                            <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                <a class=\"page-link\" (click)=\"setPage(1)\">First</a>\n                            </li>\n\n                            <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                <a class=\"page-link\" (click)=\"setPage(pager.currentPage - 1)\">Previous</a>\n                            </li>\n                            <li class=\"page-item\" *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                                <a class=\"page-link\" (click)=\"setPage(page)\">{{page}}</a>\n                            </li>\n                            <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                <a class=\"page-link\" (click)=\"setPage(pager.currentPage + 1)\">Next</a>\n                            </li>\n                            <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                <a class=\"page-link\" (click)=\"setPage(pager.totalPages)\">Last</a>\n                            </li>\n                        </ul>\n                        <p class=\"text-pagination float-left\">Results - <span>Viewing ltems    {{pager.startIndex }}-{{pager.endIndex +1}} of {{pager.totalItems}}</span></p>\n                    </nav>\n                </div>\n            </div>\n\n\n\n            <hr class=\"sort-by\">\n\n            <div class=\"row\">\n                <div class=\"col-sm-6 pagination-padding\">\n                    <ul class=\"nav nav-pills Sort-by marg-top\">\n                        <li style=\"list-style: none; display: inline\">\n                            <p class=\"text-filter\">Sort by :</p>\n                        </li>\n                        <li class=\"nav-item \">\n                            <a class=\"nav-link pointer\" [ngClass]=\"{active:sortFiledUpb === true}\" (click)=\"onSortFiledUpb(sortFiledUpb)\">UPB</a>\n                        </li>\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link pointer\" [ngClass]=\"{active:sortFiledState === true}\" (click)=\"onSortFiledCity(sortFiledState)\">City</a>\n                        </li>\n\n                    </ul>\n                </div>\n\n                <div class=\"col-lg-6\">\n                    <div aria-label=\"Basic example\" class=\"btn-group pull-right\" role=\"group\" style=\"margin-top: 0px\">\n                        <button class=\"btn btn-success active\" type=\"button\" (click)=\"onViewOne()\"><i aria-hidden=\"true\" class=\"fa fa-th-list\"></i></button>\n                        <button class=\"btn btn-success\" type=\"button\" (click)=\"onViewTow()\"><i aria-hidden=\"true\" class=\"fa fa-table\"></i></button>\n                    </div>\n                    <button *ngIf=\"role == 1\" class=\"btn btn-primary btn-sm float-right btn-filter-published\" type=\"button\" (click)=\"getAllLoanNotes()\">Published</button>\n                    <button *ngIf=\"role == 1\" class=\"btn btn-secondary btn-sm float-right btn-filter-published\" type=\"button\" (click)=\"getNonPublished()\">Not Published</button>\n                </div>\n\n\n            </div>\n            <hr class=\"sort-by\">\n\n\n        </div>\n        <div class=\"col-lg-10 col-all-items\">\n\n            <div class=\"row\" style=\"margin-bottom: 12px\" *ngFor=\"let item of pagedItems \">\n                <div class=\"col-lg-12\">\n                    <div class=\"card text-center\">\n\n                        <div class=\"card-header\">\n                            <p class=\"pull-left address\">{{item?.property?.address}},{{item?.property?.city}},{{item?.property?.zip}} </p>\n                            <p class=\"pull-right product-seller-id\">Product ID: <a [routerLink]=\"['/buy-note', item.loanNoteID]\">{{item.loanNoteID}}</a>\n                            </p>\n\n                        </div>\n\n\n\n                        <div class=\"card-body\">\n\n                            <div class=\"col-sm-3 float-left\">\n                                <img class=\"img-card \" src=\"assets/img/Art-1.png\" width=\"100%\">\n                            </div>\n\n                            <div class=\"col-sm-3 float-left zone-text\">\n                                <p class=\"text-left text-card\">Loan Status <span class=\"pull-right spane-text\"> {{item.loanStatus?.name}}</span></p>\n\n                                <p class=\"text-left text-card padding-btm\">Lien Position\n                                    <span *ngIf=\"loanNote?.deedMortgageStatus; else elseBlock\" class=\"pull-right spane-text\">1<sup>st</sup></span>\n                                    <ng-template #elseBlock>\n                                        <span class=\"pull-right spane-text\" *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}}</span>\n                                        <ng-template #elseBlock2>\n                                            <span class=\"pull-right spane-text\">2<sup>nd</sup></span>\n                                        </ng-template>\n                                    </ng-template><br></p>\n\n\n                                <p class=\"text-left text-card\">Original Blance <span class=\"pull-right spane-text\">$00.00</span></p>\n                                <p class=\"text-left text-card\">Principal Blanace <span class=\"pull-right spane-text\">${{item.principaleBalance}}</span></p>\n                                <p class=\"text-left text-card\">Note Rate <span class=\"pull-right spane-text\">{{item.noteInterestRate}}%</span></p>\n                            </div>\n\n\n                            <div class=\"col-lg-3 float-left zone-text border-befor\">\n                                <p class=\"text-center texte-simple\">Minimum Asking Price</p>\n                                <p class=\"text-center texte-bold\">${{item.askingPrice}} <br>Of {{item.askingPrice / item.principaleBalance | number : '1.2-2'}} %\n                                    <p *ngIf=\"item?.loanServicer != undefined\" class=\"text-center texte-simple\">Servicer: {{item?.loanServicer?.name}}</p>\n                            </div>\n                            <div class=\"col-lg-3 float-left text-center zone-text\">\n                                <button class=\"btn btn-success text-center btn-Submit btn-index\" style=\"width: 100%;\" type=\"button\" (click)=\"SubmitOffer(item)\">Submit an Indicative Offer</button>\n                                <button [style.background-color]=\"getStyle(item.isPublished)\" *ngIf=\"role == 1 && item.isPublished === true\" (click)=\"changePulishedStatus(item.loanNoteID); item.isPublished = !item.isPublished;\" class=\"btn btn-success text-center btn-Submit btn-index\"\n                                    style=\"width: 100%;\" type=\"button\"> {{item.isPublished === true ? unPublishedtext : publishedtext}} </button>\n                                <button [style.background-color]=\"getStyle(item.isPublished)\" *ngIf=\"role == 1 && item.isPublished === false\" (click)=\"changePulishedStatus(item.loanNoteID); item.isPublished = !item.isPublished;\" class=\"btn btn-success text-center btn-Submit btn-index\"\n                                    style=\"width: 100%;\" type=\"button\"> {{item.isPublished === true ? unPublishedtext : publishedtext}} </button>\n                                <button *ngIf=\"role == 1\" class=\"btn btn-success text-center btn-Submit btn-index\" style=\"width: 100%;\" type=\"button\">Update</button>\n                                <button [routerLink]=\"['/buy-note', item.loanNoteID]\" class=\"btn btn-outline-success text-center btn-Submit btn-index\" style=\"width: 100%;\" type=\"button\">More Details</button>\n                            </div>\n\n\n                        </div>\n\n\n                        <div class=\"card-footer\">\n                            <small class=\"text-muted pull-left\"><a>Investment Strategy Model</a></small>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n\n    </div>\n</section>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!--Template view 2-->\n\n<ng-template #ViewTwo>\n    <section style=\"margin-top: 20px;\">\n        <div class=\"container-fluid\">\n            <div class=\"row\">\n                <div class=\"col-lg-3\">\n                    <nav class=\"nav-filter-page-excel\">\n                        <ul>\n                            <li>\n                                <p>Sort by :</p>\n                            </li>\n                            <li> <a class=\"nav-link\" [ngClass]=\"{active:sortFiledUpb === true}\" (click)=\"onSortFiledUpb(sortFiledUpb)\">UPB</a></li>\n                            <li> <a class=\"nav-link\" [ngClass]=\"{active:sortFiledState === true}\" (click)=\"onSortFiledCity(sortFiledState)\">City</a></li>\n                        </ul>\n                    </nav>\n                </div>\n\n                <div class=\"col-lg-9\">\n                    <form novalidate #formFilter=\"ngForm\" (ngSubmit)=\"onFilter()\" class=\"form-row\">\n                        <div class=\"col-lg-2 mb-3\">\n                            <input type=\"text\" class=\"form-control\" name=\"UPB\" [(ngModel)]=\"filterFiledUpb\" id=\"UPB\" placeholder=\"UPB\">\n                        </div>\n                        <div class=\"col-lg-2 mb-3\">\n                            <input type=\"text\" class=\"form-control\" name=\"State\" [(ngModel)]=\"filterFiledState\" id=\"State\" placeholder=\"State\">\n                        </div>\n                        <div class=\"col-lg-2 mb-3\">\n                            <button type=\"button\" type=\"submit\" class=\"btn btn-primary  btn-block btn-filter-excel\"><i aria-hidden=\"true\" class=\"fa fa-search\"></i> SEARCH</button>\n                        </div>\n                        <div class=\"col-lg-2 mb-3\">\n                            <button *ngIf=\"resetFilter\" type=\"button\" class=\"btn btn-success  btn-block btn-filter-excel\" (click)=\"onResetFilter()\"><i aria-hidden=\"true\" class=\"fa fa-window-restore\"></i> RESET</button>\n                        </div>\n                        <div class=\"col-lg-4\">\n                            <div aria-label=\"Basic example\" class=\"btn-group pull-right\" role=\"group\" style=\"margin-top: 7px\">\n                                <button class=\"btn btn-success\" type=\"button\" (click)=\"onViewOne()\"><i aria-hidden=\"true\" class=\"fa fa-th-list\"></i></button>\n                                <button class=\"btn btn-success active\" type=\"button\" (click)=\"onViewTow()\"><i aria-hidden=\"true\" class=\"fa fa-table\"></i></button>\n                            </div>\n                            <div aria-label=\"Basic example\" class=\"btn-group pull-right\" role=\"group\" style=\"margin-top: 6px\">\n                                <button *ngIf=\"role == 1\" type=\"button\" class=\"btn btn-primary  float-right btn-filter-published\" (click)=\"getAllLoanNotes()\">Published</button>\n                                <button *ngIf=\"role == 1\" type=\"button\" class=\"btn btn-secondary float-right btn-filter-published\" (click)=\"getNonPublished()\">Not Published</button>\n                                <button type=\"button\" class=\"btn btn-outline-primary btn-filter-published\" (click)=\"exportLiveTape()\"><i class=\"fa fa-download\" aria-hidden=\"true\" ></i> Get Live Tape</button>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </section>\n\n    <section>\n        <div class=\"container-fluid excel-page\">\n            <div class=\"row\">\n                <div class=\"col-lg-12 table-responsive\">\n                    <table class=\"table table-condensed table-mycolor table-hover\">\n                        <thead>\n\n                            <tr>\n                                <th scope=\"col\">Loan Status</th>\n                                <th scope=\"col\">Lien Position</th>\n                                <th scope=\"col\">Original Blance</th>\n                                <th scope=\"col\">Note Rate</th>\n                                <th scope=\"col\">UPB</th>\n                                <th scope=\"col\">Asking Price</th>\n                                <th scope=\"col\">Product ID</th>\n                                <th scope=\"col\">Address</th>\n                                <th scope=\"col\">Submit Offer</th>\n                                <th scope=\"col\">View Detail</th>\n\n\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let item of pagedItems\">\n                                <td>{{item?.loanStatus?.name}}</td>\n                                <td> <span *ngIf=\"loanNote?.deedMortgageStatus; else elseBlock\">1<sup>st</sup></span>\n                                    <ng-template #elseBlock>\n                                        <span *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}}</span>\n                                        <ng-template #elseBlock2>\n                                            <span>2<sup>nd</sup></span>\n                                        </ng-template>\n                                    </ng-template>\n                                </td>\n                                <td>{{item?.principaleBalance}}</td>\n                                <td>{{item?.noteInterestRate}}</td>\n                                <td>{{item?.unPaidPrincipalBalance}}</td>\n                                <td>${{item?.askingPrice}}</td>\n                                <td>{{item.loanNoteID}}</td>\n                                <td>{{item?.property?.address}},{{item?.property?.city}}, {{item?.property?.zip}}</td>\n                                <td class=\"\"><button type=\"button\" class=\"btn btn-success btn-sm\" (click)='SubmitOffer(item)'>Submit</button></td>\n                                <td class=\"icone-view\"><a [routerLink]=\"['/buy-note', item.loanNoteID]\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></a></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                    <div class=\"col-sm-12 pagination-padding\">\n                        <nav aria-label=\"Page navigation example\">\n                            <ul class=\"pagination justify-content-end float-right\">\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a class=\"page-link\" (click)=\"setPage(1)\">First</a>\n                                </li>\n\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.currentPage - 1)\">Previous</a>\n                                </li>\n                                <li class=\"page-item\" *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                                    <a class=\"page-link\" (click)=\"setPage(page)\">{{page}}</a>\n                                </li>\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.currentPage + 1)\">Next</a>\n                                </li>\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.totalPages)\">Last</a>\n                                </li>\n                            </ul>\n                            <p class=\"text-pagination float-left\">Results - <span>Viewing ltems    {{pager.startIndex }}-{{pager.endIndex +1}} of {{pager.totalItems}}</span></p>\n                        </nav>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!-- \t\t<div class=\"container-fluid\">\n                    <div class=\"row\">\n                    </div>\n                </div> -->\n    </section><br><br>\n\n    <!-- <section style=\"margin-top: 20px;\">\n        <div class=\"container-fluid\">\n            <div class=\"row\">\n\n                <div class=\"col-lg-3\">\n\n                    <form novalidate #formFilter=\"ngForm\" (ngSubmit)=\"onFilter()\">\n                        <div class=\"form-row\">\n\n                            <div class=\"col-md-4 mb-3\">\n                                <input type=\"text\" class=\"form-control\" name=\"UPB\" [(ngModel)]=\"filterFiledUpb\" id=\"UPB\" placeholder=\"UPB\">\n                            </div>\n                            <div class=\"col-md-4 mb-3\">\n                                <input type=\"text\" class=\"form-control\" name=\"State\" [(ngModel)]=\"filterFiledState\" id=\"State\" placeholder=\"State\">\n                            </div>\n                            <div class=\"col-md-2 mb-3\">\n                                <button type=\"button\" type=\"submit\" class=\"btn btn-primary btn-lg btn-block btn-filter-excel\"><i aria-hidden=\"true\" class=\"fa fa-search\"></i> SEARCH</button>\n                            </div>\n                            <div class=\"col-md-2 mb-3\">\n                                <button *ngIf=\"resetFilter\" type=\"button\" class=\"btn btn-success btn-lg btn-block btn-filter-excel\" (click)=\"onResetFilter()\"><i aria-hidden=\"true\" class=\"fa fa-window-restore\"></i> RESET</button>\n                            </div>\n                        </div>\n                    </form>\n                </div>\n                <div class=\"col-lg-2\">\n                    <nav class=\"nav-filter-page-excel\">\n                        <ul>\n                            <li>\n                                <p>Sort by :</p>\n                            </li>\n                            <li class=\"nav-item \">\n                                <a class=\"nav-link\" [ngClass]=\"{active:sortFiledUpb === true}\" (click)=\"onSortFiledUpb(sortFiledUpb)\">UPB</a>\n                            </li>\n                            <li class=\"nav-item\">\n                                <a class=\"nav-link\" [ngClass]=\"{active:sortFiledState === true}\" (click)=\"onSortFiledCity(sortFiledState)\">City</a>\n                            </li>\n                        </ul>\n                    </nav>\n                </div>\n                <div class=\"col-lg-2\">\n                    <button *ngIf=\"role == 1\" type=\"button\" class=\"btn btn-primary btn-sm float-right btn-filter-published\" (click)=\"getAllLoanNotes()\">Published</button>\n                    <button *ngIf=\"role == 1\" type=\"button\" class=\"btn btn-secondary btn-sm float-right btn-filter-published\" (click)=\"getNonPublished()\">Not Published</button>\n\n                    <div class=\"btn-group pull-right\" style=\"margin-top: 15px\" role=\"group\" aria-label=\"Basic example\">\n                        <button type=\"button\" class=\"btn btn-success\" (click)=\"onViewOne()\"><i class=\"fa fa-th-list\" aria-hidden=\"true\"></i></button>\n                        <button type=\"button\" class=\"btn btn-success active\" (click)=\"onViewTow()\"><i class=\"fa fa-table\" aria-hidden=\"true\"></i></button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </section>\n    <section>\n        <div class=\"container-fluid excel-page\">\n            <div class=\"row\">\n                <div class=\"col-lg-12 table-responsive\">\n                    <table class=\"table table-condensed table-mycolor table-hover\">\n                        <thead>\n                            <tr>\n                                <th scope=\"col\">Loan Status</th>\n                                <th scope=\"col\">Lien Position</th>\n                                <th scope=\"col\">Original Blance</th>\n                                <th scope=\"col\">Note Rate</th>\n                                <th scope=\"col\">UPB</th>\n                                <th scope=\"col\">Asking Price</th>\n                                <th scope=\"col\">Product ID</th>\n                                <th scope=\"col\">Address</th>\n                                <th scope=\"col\">Submit Offer</th>\n                                <th scope=\"col\">View Detail</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let item of pagedItems\">\n                                <td>{{item?.loanStatus?.name}}</td>\n                                <td> <span *ngIf=\"loanNote?.deedMortgageStatus; else elseBlock\">1<sup>st</sup></span>\n                                    <ng-template #elseBlock>\n                                        <span *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}}</span>\n                                        <ng-template #elseBlock2>\n                                            <span>2<sup>nd</sup></span>\n                                        </ng-template>\n                                    </ng-template>\n                                </td>\n                                <td>{{item?.principaleBalance}}</td>\n                                <td>{{item?.noteInterestRate}}</td>\n                                <td>{{item?.unPaidPrincipalBalance}}</td>\n                                <td>${{item?.askingPrice}}</td>\n                                <td>{{item.loanNoteID}}</td>\n                                <td>{{item?.property?.address}},{{item?.property?.city}}, {{item?.property?.zip}}</td>\n                                <td class=\"\"><button type=\"button\" class=\"btn btn-success btn-sm\" (click)='SubmitOffer(item)'>Submit</button></td>\n                                <td class=\"icone-view\"><a [routerLink]=\"['/buy-note', item.loanNoteID]\"><i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i></a></td>\n                            </tr>\n                        </tbody>\n                    </table>\n                    <div class=\"col-sm-12 pagination-padding\">\n                        <nav aria-label=\"Page navigation example\">\n                            <ul class=\"pagination justify-content-end float-right\">\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a class=\"page-link\" (click)=\"setPage(1)\">First</a>\n                                </li>\n\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.currentPage - 1)\">Previous</a>\n                                </li>\n                                <li class=\"page-item\" *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                                    <a class=\"page-link\" (click)=\"setPage(page)\">{{page}}</a>\n                                </li>\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.currentPage + 1)\">Next</a>\n                                </li>\n                                <li class=\"page-item\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a class=\"page-link\" (click)=\"setPage(pager.totalPages)\">Last</a>\n                                </li>\n                            </ul>\n                            <p class=\"text-pagination float-left\">Results - <span>Viewing ltems    {{pager.startIndex }}-{{pager.endIndex +1}} of {{pager.totalItems}}</span></p>\n                        </nav>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </section> -->\n\n\n\n</ng-template>"

/***/ }),

/***/ "../../../../../src/app/components/buyNotes/buyNotes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return buyNotesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_takeWhile__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/takeWhile.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var buyNotesComponent = (function () {
    function buyNotesComponent(dashboardServ, nav, loading, pagerServ, alertService, _router, ExcelService) {
        this.dashboardServ = dashboardServ;
        this.nav = nav;
        this.loading = loading;
        this.pagerServ = pagerServ;
        this.alertService = alertService;
        this._router = _router;
        this.ExcelService = ExcelService;
        this.pager = {};
        this.sortFiledUpb = false;
        this.sortFiledState = false;
        this.resetFilter = false;
        this.alive = true;
        this.publishedtext = "Publish";
        this.unPublishedtext = "UnPublish";
        this.checkP = true;
        this.checkUnP = true;
        this.IsViewSwitch = true;
        this.array = [];
        this.alertService.clear();
        this.filterFiledUpb = "";
        this.filterFiledState = "";
        this.role = JSON.parse(localStorage.currentuser).role;
    }
    buyNotesComponent.prototype.ngOnInit = function () {
        this.nav.show();
        this.getAllLoanNotes();
    };
    buyNotesComponent.prototype.getAllLoanNotes = function () {
        var _this = this;
        this.loading.show();
        this.dashboardServ.getAllLoanNotes().takeWhile(function () { return _this.alive; })
            .subscribe(function (res) {
            _this.allLoansList = res;
            _this.setPage(1);
            console.log(_this.allLoansList);
            setTimeout(function () {
                _this.loading.hide();
            }, 500);
        });
    };
    buyNotesComponent.prototype.setPage = function (page) {
        var _this = this;
        this.loading.show();
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagerServ.getPager(this.allLoansList.length, page);
        this.pagedItems = this.allLoansList.slice(this.pager.startIndex, this.pager.endIndex + 1);
        setTimeout(function () {
            _this.loading.hide();
        }, 500);
    };
    buyNotesComponent.prototype.onFilter = function () {
        var _this = this;
        if (!this.isEmpty(this.filterFiledUpb) || !this.isEmpty(this.filterFiledState)) {
            this.loading.show();
            var loansFilteredList = [];
            loansFilteredList = this.allLoansList.filter(function (loans) { return (!_this.isEmpty(_this.filterFiledUpb)) ? loans.unPaidPrincipalBalance == _this.filterFiledUpb : ' '; });
            loansFilteredList = loansFilteredList.filter(function (loans) { return (!_this.isEmpty(_this.filterFiledState)) ? loans.property.city == _this.filterFiledState : ' '; });
            if (!__WEBPACK_IMPORTED_MODULE_2_lodash__["isEmpty"](loansFilteredList)) {
                this.allLoansList = loansFilteredList;
                this.setPage(1);
                setTimeout(function () {
                    _this.loading.hide();
                }, 500);
                this.resetFilter = true;
                this.alertService.clear();
            }
            else {
                setTimeout(function () {
                    _this.loading.hide();
                }, 200);
                setTimeout(function () {
                    _this.alertService.clear();
                    _this.alertService.info('No loans with this filter you can try with other.');
                }, 300);
            }
        }
        else {
            this.alertService.clear();
            this.alertService.info('Fill in the fields so that you can filter.');
            return;
        }
    };
    buyNotesComponent.prototype.onResetFilter = function () {
        this.resetFilter = false;
        this.filterFiledState = "";
        this.filterFiledUpb = "";
        this.getAllLoanNotes();
    };
    // get diagnostic() { return JSON.stringify(this.allLoansList); }
    buyNotesComponent.prototype.onSortFiledUpb = function (sortFiledUpb) {
        this.sortFiledUpb = !sortFiledUpb;
        this.sortFiledState = false;
        if (this.sortFiledUpb == true) {
            this.allLoansList = __WEBPACK_IMPORTED_MODULE_2_lodash__["sortBy"](this.allLoansList, function (loans) { return loans.unPaidPrincipalBalance; });
            this.allLoansList = this.allLoansList.reverse();
            this.setPage(1);
        }
        else {
            this.allLoansList = __WEBPACK_IMPORTED_MODULE_2_lodash__["sortBy"](this.allLoansList, function (loans) { return loans.unPaidPrincipalBalance; });
            this.setPage(1);
        }
    };
    buyNotesComponent.prototype.onSortFiledCity = function (sortFiledState) {
        this.sortFiledState = !sortFiledState;
        this.sortFiledUpb = false;
        if (this.sortFiledState == true) {
            this.allLoansList = __WEBPACK_IMPORTED_MODULE_2_lodash__["sortBy"](this.allLoansList, function (loans) { return loans.property.city; });
            this.allLoansList = this.allLoansList.reverse();
            this.setPage(1);
        }
        else {
            this.allLoansList = __WEBPACK_IMPORTED_MODULE_2_lodash__["sortBy"](this.allLoansList, function (loans) { return loans.property.city; });
            this.setPage(1);
        }
    };
    buyNotesComponent.prototype.isEmpty = function (value) {
        return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
    };
    buyNotesComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    buyNotesComponent.prototype.changePulishedStatus = function (id) {
        var _this = this;
        this.dashboardServ.changePulishedLoanNotes(id).subscribe(function (res) {
            console.log(res);
            _this.checkP = !_this.checkP;
            _this.checkUnP = !_this.checkUnP;
        });
    };
    buyNotesComponent.prototype.getNonPublished = function () {
        var _this = this;
        this.loading.show();
        this.dashboardServ.getNonPublishedLoanNotes().subscribe(function (res) {
            _this.allLoansList = res;
            _this.setPage(1);
            console.log(_this.allLoansList);
            setTimeout(function () {
                _this.loading.hide();
            }, 500);
        });
    };
    buyNotesComponent.prototype.onViewOne = function () {
        this.IsViewSwitch = true;
    };
    buyNotesComponent.prototype.onViewTow = function () {
        this.IsViewSwitch = false;
    };
    buyNotesComponent.prototype.getStyle = function (status) {
        if (status) {
            return "#868e96";
        }
        else {
            return "#007bff";
        }
    };
    buyNotesComponent.prototype.SubmitOffer = function (item) {
        this._router.navigate(["SubmitOffer", item.loanNoteID]);
    };
    buyNotesComponent.prototype.getSafe = function (arr, propt, subpropt) {
        try {
            return arr[propt][subpropt];
        }
        catch (e) {
            return undefined;
        }
    };
    buyNotesComponent.prototype.exportLiveTape = function () {
        console.log(this.allLoansList);
        for (var j = 0; j < this.allLoansList.length; j++) {
            this.array.push({
                "Product ID": this.allLoansList[j].loanNoteID,
                "Seller ID": this.allLoansList[j].userID,
                "Serviced By": "None",
                "Payment Status": "None",
                "Status": "",
                "Lien Position": "",
                "Address": this.getSafe(this.allLoansList[j], "property", "address"),
                "City": this.getSafe(this.allLoansList[j], "property", "city"),
                "State": this.getSafe(this.allLoansList[j], "property", "stateID"),
                "Zip": this.getSafe(this.allLoansList[j], "property", "zip"),
                "Paid to Date": this.allLoansList[j].paidToDate,
                "Loan Maturity": this.allLoansList[j].noteMaturityDate,
                "Original Balance": this.allLoansList[j].originalLoanAmount,
                "Principle Balance": this.allLoansList[j].principaleBalance,
                "Note Rate": this.allLoansList[j].noteInterestRate,
                "Estimated Market Value": this.getSafe(this.allLoansList[j], "property", "propertyMarketValue"),
                "Submit Offer": "Submit Offer",
                "Asking Price": this.allLoansList[j].askingPrice,
                "% of UPB": this.allLoansList[j].principaleBalance != null && this.allLoansList[j].principaleBalance != 0 ? this.allLoansList[j].askingPrice / this.allLoansList[j].principaleBalance : "None",
                "% of Value": "None",
                "View Payment History": "None",
                "Payment Amount": this.allLoansList[j].notePaymentAmount,
                "Sold Rate": this.allLoansList[j].soldInterestRate,
                "Modified?": "None",
                "First Payment Date": this.allLoansList[j].firstPayementDate
            });
        }
        this.ExcelService.exportLiveTape(this.array, 'Live Tape');
    };
    buyNotesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-buyNotes',
            template: __webpack_require__("../../../../../src/app/components/buyNotes/buyNotes.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/buyNotes/buyNotes.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__structExports_services__["f" /* buyNotesService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["b" /* NavbarService */],
            __WEBPACK_IMPORTED_MODULE_4__structExports_services__["i" /* loadingService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["l" /* pagerService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["a" /* ExcelService */]])
    ], buyNotesComponent);
    return buyNotesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/contact-us/contact-us.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/contact-us/contact-us.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <form novalidate [formGroup]=\"formcontactus\" (ngSubmit)=\"onSubmitContactus()\" (ngReset)=\"onresetcontactus()\" class=\"page-register\">\n      <div class=\"container-fluid\">\n          <div class=\"row\">\n              <div class=\"col-lg-2\">\n                  <section>\n                      <div class=\"col-lg-12 bg-light\">\n                          <h4 class=\"infoclient\">Pub</h4>\n                      </div>\n                  </section>\n                  <section style=\"margin-top: 20px;\">\n                      <div class=\"col-lg-12 doc-space\"></div>\n                  </section>\n              </div>\n              <div class=\"col-lg-10\">\n                  <div class=\"row\">\n                      <div class=\"col-md-9 offset-md-1 alert alert-warning\" role=\"alert\">\n                          <div class=\"col-lg-1 float-left icone-contact-page\">\n                              <i aria-hidden=\"true\" class=\"fa fa-info-circle text-center\"></i>\n                          </div>\n                          <div class=\"col-lg-11 float-left text-alert-contact-page\">\n                              <p>Use this form to request more information about FCI EXCHANGE and the services we provide.</p>\n                              <p><span>Potential Clients:</span> please fill in the required fields, add your request, and click \"submit\". Feel free to call us at (800) 931-2424 x750.</p>\n                              <p><span>Registered Clients:</span> if you used the login to enter the site, your information is auto filled; simply type in your request.</p>\n                          </div>\n                      </div>\n                  </div><br>\n                  <div class=\"row\" data-select2-id=\"4\">\n                      <div class=\"col-lg-4 float-left\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': name.invalid && (name.dirty || name.touched),'has-success': name.valid && (name.dirty || name.touched)}\">Name(First,Last) : <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"name\">\n                          <div class=\"form-control-feedback\" *ngIf=\"name.errors && (name.dirty || name.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"name.errors.required\">name is required</p>\n                          </div>\n                      </div>\n                      <div class=\"col-lg-4 float-left\">\n                          <label for=\"inputPassword6\">Company :</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"company\">\n                      </div>\n                      <div class=\"col-lg-4 float-left\">\n                          <label for=\"inputPassword6\">Address :</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"address\">\n                      </div>\n                      <div class=\"col-lg-4 float-left\">\n                          <label for=\"inputPassword6\">City :</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"city\">\n                      </div>\n                      <!-- <div class=\"col-lg-3\">\n                        <label for=\"inputPassword6\">State:</label>\n                        <div class=\"dropdown\">\n                            <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"state\">\n                                <option value=null disabled selected>--Select--</option>\n                                <option  *ngFor=\" let type of _States\" [value]=\"type.typeName\">{{type.typeName}}</option>\n                              </select>\n                        </div>\n                      </div> -->\n                      <div class=\"col-lg-4 float-left\">\n                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': state.invalid && (state.dirty || state.touched),'has-success': state.valid && (state.dirty || state.touched)}\">State : <em>*</em></label> \n                        <select class=\"form-control\" style=\"width: 100%\" formControlName=\"state\">\n                          <option value=\"ALABAMA\">\n                            ALABAMA\n                          </option>\n                          <option value=\"ALASKA\">\n                            ALASKA\n                          </option>\n                          <option value=\"CALIFORNIA\">\n                            CALIFORNIA\n                          </option>\n                          <option value=\"ARIZONA\">\n                            ARIZONA\n                          </option>\n                          <option value=\"COLORADO\">\n                            COLORADO\n                          </option>\n                          <option value=\"FLORIDA\">\n                            FLORIDA\n                          </option>\n                          <option value=\"NewYork\">\n                            NEW YORK\n                          </option>\n                         \n                        </select>\n                        <div class=\"form-control-feedback\" *ngIf=\"state.errors && (state.dirty || state.touched)\">\n                          <p class=\"text-danger\" *ngIf=\"state.errors.required\">state is required </p>\n                          <p class=\"text-danger\" *ngIf=\"state.errors.pattern\">state is not valid ,exp <i>181 384 120 66</i> </p>\n                      </div>\n                      </div>\n          \n                      <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\">Zip :</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\"formControlName=\"zip\">\n                      </div>\n                      <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger':telephone.invalid && (telephone.dirty || telephone.touched),'has-success': telephone.valid && (telephone.dirty || telephone.touched)}\">Telephone : <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"telephone\">\n                          <div class=\"form-control-feedback\" *ngIf=\"telephone.errors && (telephone.dirty || telephone.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"telephone.errors.required\">telephone is required </p>\n                              <p class=\"text-danger\" *ngIf=\"telephone.errors.pattern\">telephone is not valid ,exp <i>181 384 120 66</i> </p>\n                          </div>\n                      </div>\n                      <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">Extension :</label>\n                           <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"extension\">\n                      </div>\n                      <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">Fax :</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"fax\">\n                      </div>\n                      <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger':email.invalid && (email.dirty || email.touched),'has-success': email.valid && (email.dirty || email.touched)}\">Email : <em>*</em></label>\n                           <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"email\">\n                           <div class=\"form-control-feedback\" *ngIf=\"email.errors && (email.dirty || email.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"email.errors.required\">Email is required </p>\n                              <p class=\"text-danger\" *ngIf=\"email.errors.pattern\">Email is not valid ,exp <i>contact@adelphatech.com</i> </p>\n                          </div>\n                      </div>\n                      <div class=\"col-md-8\">\n                          <label for=\"inputPassword6\">Remember to enter your request here : </label> <textarea class=\"form-control\" formControlName=\"message\" id=\"comment\" placeholder=\"\" rows=\"3\"></textarea><br>\n                      </div>\n                      <div class=\"col-md-6 offset-md-3 text-center\">\n                        <button type=\"submit\" class=\"btn btn-success btn-lg btn-block\"  class=\"btn btn-success float-left\" style=\"width: 45% ;margin: 10px;\">Submit</button>\n                        <button type=\"Reset\" class=\"btn btn-success btn-lg btn-block\"  class=\"btn btn-outline-success float-left\" style=\"width: 45% ;margin: 10px;\">Clear</button>\n                      </div>\n                  </div>\n              </div>\n          </div>\n      </div>\n      <!-- <pre>{{formcontactus.value | json}}</pre> -->\n  </form>\n</section>\n\n\n    \n"

/***/ }),

/***/ "../../../../../src/app/components/contact-us/contact-us.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_Forms__ = __webpack_require__("../../../Forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactUsComponent = (function () {
    function ContactUsComponent(nav, loading, alertService, contactUsServ) {
        this.nav = nav;
        this.loading = loading;
        this.alertService = alertService;
        this.contactUsServ = contactUsServ;
        // this.contactUsServ.getDropDownData(this._urlStates).subscribe(data => {this._States = data});
    }
    ContactUsComponent.prototype.ngOnInit = function () {
        this.getUserInfo();
        this.onCreateFormControls();
        this.onCreateForm();
        this.nav.show();
    };
    ContactUsComponent.prototype.getUserInfo = function () {
        var _this = this;
        this.contactUsServ.getUserInfo().subscribe(function (cdata) {
            console.log(cdata);
            _this.userInfo = cdata;
            _this.formcontactus.controls['name'].setValue(_this.userInfo.firstName + " " + _this.userInfo.lastName);
            _this.formcontactus.controls['telephone'].setValue(_this.userInfo.PhoneNumber);
            _this.formcontactus.controls['email'].setValue(_this.userInfo.UserName);
            _this.formcontactus.controls['address'].setValue(_this.userInfo.address);
            _this.formcontactus.controls['company'].setValue(_this.userInfo.company);
            _this.formcontactus.controls['city'].setValue(_this.userInfo.city);
            _this.formcontactus.controls['state'].setValue(_this.userInfo.state);
            _this.formcontactus.controls['zip'].setValue(_this.userInfo.zip);
            _this.formcontactus.controls['fax'].setValue(_this.userInfo.fax);
            //  address   city
        });
    };
    // private _rootUrl:string = 'https://localhost:44305/api';
    // private _urlStates:string = `${this._rootUrl}/types/states`;
    // private _States:type[];
    ContactUsComponent.prototype.onCreateFormControls = function () {
        this.name = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.state = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.telephone = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].pattern("^[0-9]+$")]);
        this.email = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].pattern("[^ @]*@[^ @]*")]);
        this.address = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.city = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.company = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.extension = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.message = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.zip = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.fax = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
    };
    ContactUsComponent.prototype.onCreateForm = function () {
        this.formcontactus = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["b" /* FormGroup */]({
            name: this.name,
            company: this.company,
            address: this.address,
            city: this.city,
            state: this.state,
            zip: this.zip,
            telephone: this.telephone,
            extension: this.extension,
            fax: this.fax,
            email: this.email,
            message: this.message
        });
    };
    ContactUsComponent.prototype.onresetcontactus = function () {
        this.formcontactus.reset();
    };
    ContactUsComponent.prototype.onSubmitContactus = function () {
        var _this = this;
        this.loading.show();
        if (this.formcontactus.valid) {
            this.contactUsServ.contactUs(this.formcontactus.value).subscribe(function (res) {
                _this.alertService.clear();
                _this.loading.hide();
                var mail = _this.email.value;
                _this.formcontactus.reset();
                _this.alertService.success('Your Request has been successfully Send ,thank you', true);
            });
        }
        else {
            this.validateAllFormFields(this.formcontactus);
            setTimeout(function () {
                _this.loading.hide();
                window.scrollTo(0, 0);
                _this.alertService.clear();
                _this.alertService.error('You must complete all required fields');
            }, 300);
        }
    };
    ContactUsComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["b" /* FormGroup */]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    ContactUsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contact-us',
            template: __webpack_require__("../../../../../src/app/components/contact-us/contact-us.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/contact-us/contact-us.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["i" /* loadingService */],
            __WEBPACK_IMPORTED_MODULE_1__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["g" /* contactUsService */]])
    ], ContactUsComponent);
    return ContactUsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/faq/faq.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/faq/faq.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\r\n    <div class=\"container-fluid FAQ-page\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-2\">\r\n                <div class=\"col-lg-12 bg-light\">\r\n                    <h4 class=\"infoclient\">Pub</h4>\r\n                </div>\r\n                <section style=\"margin-top: 20px;\">\r\n                    <div class=\"col-lg-12 doc-space\"></div>\r\n                </section>\r\n            </div>\r\n            <div class=\"col-lg-10\">\r\n                <div class=\"col-lg-12 bg-light\">\r\n                    <h5 class=\"infoclient\">General Questions</h5>\r\n                </div><br>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse1\">1. What is FCI EXCHANGE?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse1\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>FCI EXCHANGE is a trading platform where Buyers and Sellers of mortgage Notes can get together and have the entire Buy/Sell and Closing process supervised by a major servicer who understands compliant trades.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse2\">2. Who can use FCI EXCHANGE?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse2\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>Anybody is welcome to use FCI EXCHANGE; it’s a very user friendly website with easy to follow detailed instructions.  FCI EXCHANGE is here to help anyone from a novice to an expert Buy<br> and Sell Notes  with confidence, quickly and compliantly.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse3\">3. What types of Notes are listed?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse3\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>FCI EXCHANGE is a trading platform where Buyers and Sellers of mortgage Notes can get together and have the entire Buy/Sell and Closing process supervised by a major servicer who understands compliant trades.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12 bg-light\">\r\n                    <h5 class=\"infoclient\">Buyer Questions</h5>\r\n                </div><br>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a [routerLink]=\"['how-to-sell-notes']\">1. How do I buy Notes in ExchangeLoans?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse4\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>The only way to contact the seller is through the website; dialogue is opened when you place an offer.  There is a comment box on the Place an Offer page <br>where you may leave comments and questions which are sent to the seller upon approval of your proof of funds.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse4\">2. Can I contact the seller to get more information?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse4\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>The only way to contact the seller is through the website; dialogue is opened when you place an offer.  There is a comment box on the Place an Offer page <br>where you may leave comments and questions which are sent to the seller upon approval of your proof of funds.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse5\">3. How do I know if the seller is serious and legitimate?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse5\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>FCI EXCHANGE has a detailed Note listing and Quality Control process in place where each loan listed must give relevant information and attached copies of relevant documents including Notes, Deeds or Mortgages, and Assignments.  Each listing is reviewed for accurate information and documentation.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse6\">4. How can I verify that a property actually exists?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse6\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>It is recommended that a buyer perform their own due diligence on the property before placing an offer on the Note; also attachments of a copy of the Note and Deed are standard on most loans.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12 bg-light\">\r\n                    <h5 class=\"infoclient\">Seller Questions</h5>\r\n                </div><br>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a [routerLink]=\"['how-to-sell-notes']\" >1. How do I Sell Notes in ExchangeLoans?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse7\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>FCI EXCHANGE requires that buyer’s first register, then submit Proof of Funds in the form of a bank statement,<br> current financial institution statement, or a Letter of Credit from a financial institution showing funds available that cover the amount they are offering on the Note.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse7\">2. How do I know if a buyer is serious and legitimate?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse7\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>FCI EXCHANGE requires that buyer’s first register, then submit Proof of Funds in the form of a bank statement,<br> current financial institution statement, or a Letter of Credit from a financial institution showing funds available that cover the amount they are offering on the Note.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse8\">3. Do you list REO’s?</a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse8\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>No, FCI EXCHANGE does not list REO’s; the site is strictly for trading Notes. In the event a Note listed individually or in a Loan Pool becomes an REO before a trade is completed, the Note must be removed from the website. FCI EXCHANGE is a Note trading platform only. As an additional service, we offer REO Management and Sales with FCI Lenders Services at www.trustfci.com or call 714-282-2424, ext 226 for more information.</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-lg-12\">\r\n                    <div class=\"panel-group\">\r\n                        <div class=\"panel panel-default\">\r\n                            <div class=\"panel-heading\">\r\n                                <h6 class=\"panel-title\"><a data-toggle=\"collapse\" href=\"#collapse9\">4. Do you have investors who are interested in Notes in my particular region? </a></h6>\r\n                            </div><br>\r\n                            <div class=\"panel-collapse collapse\" id=\"collapse9\">\r\n                                <div class=\"panel-body\">\r\n                                    <p>We serve as a platform that brings buyers and sellers together from anywhere in the country.  We have visitors and registered users from all over the United States, and Notes listed in almost every stat</p><br>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section><br>\r\n<br>"

/***/ }),

/***/ "../../../../../src/app/components/faq/faq.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaqComponent = (function () {
    function FaqComponent(nav) {
        this.nav = nav;
    }
    FaqComponent.prototype.ngOnInit = function () {
        this.nav.show();
    };
    FaqComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-faq',
            template: __webpack_require__("../../../../../src/app/components/faq/faq.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/faq/faq.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */]])
    ], FaqComponent);
    return FaqComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/forgotPassword/forgotPassword.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { loanNote } from '../../models/loanNotes';




var ForgotPasswordComponent = (function () {
    function ForgotPasswordComponent(_http, _alert, router) {
        this._http = _http;
        this._alert = _alert;
        this.router = router;
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        this.emailforgot = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* Validators */].required);
        this.forgotpassword = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({ Email: this.emailforgot, });
    };
    ForgotPasswordComponent.prototype.onForgotPassword = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        this._http.post(__WEBPACK_IMPORTED_MODULE_5__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Account/ForgotPassword", "Email=" + this.emailforgot.value, options).subscribe(function (res) {
            _this._alert.success("Please Check your Email to reset your password", true);
            _this.router.navigate(['login']);
            console.log(res);
        });
        console.log(this.emailforgot.value);
    };
    ForgotPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__("../../../../../src/app/components/forgotPassword/forgotpassword.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/forgotPassword/forgotpassword.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/forgotPassword/forgotpassword.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/forgotPassword/forgotpassword.component.html":
/***/ (function(module, exports) {

module.exports = "<section>\n\n<div class=\"container icones-container\">\n    <div class=\"row icones\">\n      <div class=\"col-sm-12 logo-zone\"><img src=\"assets/img/logo.png\"></div>\t\n    </div>\n  </div>\n\n\n\n<div class=\"container page-login-container\">\n    <div class=\"row\">\n      <div class=\"col-md-8 offset-md-2\">\n        <div class=\"card card-signup\">\n                          <h2 class=\"card-title text-center card-title-login\" style=\"color:#818181\">Forgot Your <span style=\"color: #acd589\">Password?</span></h2>\n                          <h6 class=\"card-title text-center\">Enter your email address below and we'll get you back on track.</h6>\n          <div class=\"row\">\n            <div class=\"col-md-10 offset-md-1 inputs\">\n              <form class=\"form\" [formGroup]=\"forgotpassword\" (ngSubmit)=\"onForgotPassword()\">\n                <div class=\"card-content\">\n                  <div class=\"form-group\">\n                    <label for=\"formGroupExampleInput\">Email</label> <input class=\"form-control\" formControlName=\"Email\" id=\"formGroupExampleInput\" placeholder=\"Email\" type=\"text\">\n                  </div>\n                  <div class=\"form-group col-sm-4 offset-4 text-center\">\n                                          <button type=\"submit\" class=\"btn btn-success btn-lg\">Send</button>\n                                          <a [routerLink]=\"['/login']\" class=\"Back_to_sign_in\">Back to sign in</a>\n                                      </div>\n                </div>\n                <!-- <pre>{{ forgotpassword.value | json}} </pre> -->\n              </form>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section><br><br>"

/***/ }),

/***/ "../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-12 bg-light\">\n        <h4 class=\"infoclient\">How To Buy Notes</h4>\n      </div>\n    </div>\n        <div class=\"col-sm-12 Page-buy-Notes\">\n          <div class=\"row \">\n\n            <div class=\"col-lg\">\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img class=\"img-responsive\" src=\"assets/img/Indicative-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-lg-1 icone-next text-center img-next\"><img class=\"img-responsive\" src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img class=\"img-responsive\" src=\"assets/img/Due-Diligence-Period.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-lg-1 icone-next text-center img-next\"><img class=\"img-responsive\" src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img class=\"img-responsive\" src=\"assets/img/Final-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n              \n            <div class=\"col-lg-1 icone-next text-center img-next\"><img class=\"img-responsive\" src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img class=\"img-responsive\" src=\"assets/img/PSA.svg\" width=\"70px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n              \n            <div class=\"col-lg-1 icone-next text-center img-next\"><img class=\"img-responsive\" src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img class=\"img-responsive\" src=\"assets/img/Wrap-Up.svg\" width=\"80px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                    <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n          </div>\n\n        </div><br><br>\n  </div>\n</section>\n<section>\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-lg-8 col-all-items how-page\">\n        <p class=\"text-one\">Buying a Loan at <a href=\"www.ExchangeLoans.com\">www.ExchangeLoans.com</a> is a simple 5 step process. Once you have located a Loan that fits your personal investment criteria, click “Submit an Indicative Offer” which begins the non-binding transaction process. Once the offer is sent you can begin discussions directly with the Seller of the Note through Exchange Loan’s chat system inside the transaction.</p><br>\n        <small class=\"text-one-small\">Exchange Loans does not own any of the notes on the platform.</small><br><br>\n\n\n        <h4>Step 1: INDICATIVE OFFER</h4>\n        <p class=\"text-one\">Submit your non-binding Indicative Offer to the Seller. The Seller may, have a minimum offer threshold that will auto reject your offer if it is too low. The Seller will set what type of closing they will allow on the transaction. The Seller may accept, reject or counter your offer then the Buyer can then accept, reject or counter the Seller’s counter offer. Once both parties have agreed on an indicative offer price, the type of closing, and who pays which fees, the transaction moves to Step 2.</p><br>\n\n        <p class=\"text-one\">Selecting the Closing Service through Exchange Loans is the preferred closing method as the transaction will go through a licensed escrow closing agent. You have the option to do a direct closing with the Seller, but closing arrangements and money will need to be transferred between the parties at Buyer and Seller’s own risk.</p><br>\n\n\n\n        <h4>Step 2: DUE DILIGENCE PERIOD</h4>\n        <p class=\"text-one\">You will have a minimum of 3 business days to complete Due Diligence. However, you can extend your Due Diligence Period, as long as the Seller approves the extention. Once the transaction enters Step 2 the Due Diligence Documents uploaded by the Seller will be available for your review. You will also be able to order a 3rd Party BPO and O&E Report through ExchangeLoans.com during this time.</p><br>\n\n        <p class=\"text-one\">Buyers are also able to sign up for the “Star Buyer” Program. For $50 a month the Star Buyer program lets you receive a 3rd Party BPO and a O&E Report on every Star Loan that is currently uploaded to Exchange Loans. Also “Star Buyers” can set unlimited Loan Alerts, that notify you when a loan that matches your criteria has been uploaded or updated, thus giving you an advantage to find the deal first.</p><br>\n        \n\n        <h4>Step 3: FINAL OFFER</h4>\n        <p class=\"text-one\">This is where you can resubmit your offer from Step 1 or lower your offer based on what was found in Due Diligence. Once you and the Seller agree to a Final Offer the transaction will move to Step 4.</p><br>\n\n\n        <h4>Step 4: PURCHASE AND SALE AGREEMENT</h4>\n        <p class=\"text-one\">A Purchase and Sale agreement (PSA) will be generated and will need to be signed electronically by Buyer and Seller. Once the Purchase and Sale agreement is signed the transaction becomes Binding.</p><br>\n        \n\n        <h4>Step 5: WRAP UP</h4>\n        <p class=\"text-one\">Based on the type of closing selected in Step 3, fees will be paid and the transfer email will be sent out. Once this step is completed the transaction is completed on <a href=\"\">ExchangeLoans.com</a>.</p><br>\n\n        \n        <p class=\"text-one\">Additional Information on Offers:</p>\n          <p class=\"text-one before-text\">Offer amounts, even at the Full Asking Price, are nonbinding until a Purchase and Sale Agreement has been signed.</p>\n          <p class=\"text-one before-text\">Following acceptance of an Indicative Offer, Buyer is given a Due Diligence Period, but Seller still has the right to respond to any other Offer.</p>\n          <p class=\"text-one before-text\">Following the Due Diligence Period and the Final Offer acceptance, the transaction is a binding obligation when the Purchase and Sale Agreement has been executed by both parties.</p>\n          <p class=\"text-one before-text\">Buyer and Seller have the right to cancel the transaction at any time prior to the execution of the Purchase and Sale Agreement by both parties.</p>\n          <p class=\"text-one before-text\">FCI Exchange is not an agent, fiduciary, broker or representative for either party in connection with any transaction on this site and cannot enforce any obligations the parties they may have in performing, cooperating, or otherwise with the other party in the transactions contemplated on this site.</p>\n          <p class=\"text-one before-text\">Please note that Exchange Loans has no involvement in, or control over, a Seller's asking price or a Buyer's offering price. To assist in fair disclosure, Sellers are required to put down an asking price, and if a Seller counters a Buyer offer differently, the asking price will be adjusted accordingly.</p>\n          <p class=\"text-one before-text\">No fees shall be assessed unless outlined in the <a href=\"\"> Terms of Use for Trading on This Site</a>. Exchange Loans uses this performance based fee structure to ensure all Parties that FCI Exchange’s income is in completed transactions and not simply Listing Loans.</p>\n\n      </div>\n      <div class=\"col-lg-4 col-filter info\">\n        <div class=\"col-lg-2 float-left text-center icone-zone\">\n          <i aria-hidden=\"true\" class=\"fa fa-info-circle\"></i>\n        </div>\n        <div class=\"col-lg-10 float-left\">\n          <p class=\"text-description-of-exchange\">ExchangeLoans is not an agent or representative for either party in connection with any transaction on this site and cannot enforce any obligations the parties they may have in performing, cooperating, or otherwise with the other party in the transactions contemplated on this site</p>\n        </div>\n      </div>\n    </div>\n  </div>\n</section><br>\n<br>"

/***/ }),

/***/ "../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HowToBuyNotesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HowToBuyNotesComponent = (function () {
    function HowToBuyNotesComponent(nav) {
        this.nav = nav;
    }
    HowToBuyNotesComponent.prototype.ngOnInit = function () {
        this.nav.show();
    };
    HowToBuyNotesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-how-to-buy-notes',
            template: __webpack_require__("../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */]])
    ], HowToBuyNotesComponent);
    return HowToBuyNotesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n        <section>\n          <div class=\"col-lg-12 bg-light\">\n            <h4 class=\"infoclient\">Pub</h4>\n          </div>\n        </section>\n        <section style=\"margin-top: 20px;\">\n          <div class=\"col-lg-12 doc-space\"></div>\n        </section>\n      </div>\n      <div class=\"col-lg-10\">\n        <div class=\"col-lg-12 bg-light\">\n          <h4 class=\"infoclient\">HOW TO SELL NOTES</h4>\n        </div>\n        <div class=\"col-sm-8 offset-sm-2 Page-Sell-Notes\">\n          <div class=\"row \">\n\n            <div class=\"col-lg\">\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img src=\"assets/img/Indicative-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-lg-1 icone-next text-center img-next\"><img src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img src=\"assets/img/Indicative-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n\n            <div class=\"col-lg-1 icone-next text-center img-next\"><img src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img src=\"assets/img/Indicative-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n              \n            <div class=\"col-lg-1 icone-next text-center img-next\"><img src=\"assets/img/icone/Next.svg\"></div>\n\n            <div class=\"col-lg\">\t\n              <div class=\"card text-center icone-top border rounded\">\n                <div class=\"card-block\">\n                  <p class=\"card-text text-center\"><img src=\"assets/img/Indicative-Offer.svg\" width=\"56px\"></p>\n                </div>\n                <div class=\"card-footer text-muted\">\n                  <p>Search For Notes</p>\n                </div>\n              </div>\n            </div>\n\n          </div>\n\n        </div><br><br>\n        <div class=\"row\">\n          <div class=\"col-lg-12\">\n            <p class=\"text-one\">To sell Newly Originated Notes, Performing Notes, Non Performing Notes, and Loan Pools on the FCI EXCHANGE trading platform, simply follow the 3 Steps below. If you are listing more than 10 Notes ,or a Loan Pool, we will email you an Excel Spread Sheet to complete and email back. For assistance, please email your request to admin@fciexchange.com or call our customer service department (800) 931-2424 x750.</p><br>\n            <p class=\"text-one\">Provide as much information about the product for sale as you can. Remember that buyers respond faster and will often pay more when more information is supplied. Consider adding a number of photos to your listing, as well as a current independent BPO or an Ownership & Encumbrance Report, both available from FCI under Due Diligence Options..</p><br>\n\n            <p class=\"text-one\">Please note that FCI EXCHANGE is a supervised Note trading platform only and has no involvement in, or control over, a Seller's asking price or a Buyer's offering price. To assist in fair disclosure, Sellers are required to put down an asking price. In the event a Note listed individually or in a Loan Pool becomes an REO before a trade is completed, the Note must be removed from the website or reclassified as an REO. As an additional service, we offer REO Management and Sales with FCI Lenders Services at www.trustfci.com or call 714-282-2424, ext 226 for more information.</p><br>\n\n            <h5 class=\"title-condition-page\">\"Diamond\" Program</h5>\n\n            <p class=\"text-one\">The Diamond Program is an industry first that provides the most information and credibility to Buyers for the Notes and Loan Pools being sold.</p><br>\n\n            <p class=\"text-one\">Notes are put into loan servicing with FCI Lender Services (just a $45 set up fee). FCI will \"scrub\" the notes by going through each document to help create a compliant and complete loan package for sale. There is a live data feed between FCI Lender Services and FCI EXCHANGE that will allow qualified buyers to view live account activity, loan history and related documents. Buyers will have the most up to date information ever offered on Notes For Sale. Informed Buyers are more comfortable making offers and typically make higher offers. For more information about FCI Lender Services, click on www.trustfci.com</p><br>\n\n            <p class=\"text-one\">There are no fees to use this site until, and unless, a transaction closes. FCI EXCHANGE uses this performance based fee structure to ensure Sellers get their money and Buyers get their Product before the site earns its Seller Sale Processing Fee.</p><br>\n\n            <p class=\"text-one\">To update information on an existing Note or Note Pool For Sale go to My Notes For Sale.The previously filled out information will appear, simply make any changes and click \"Submit\"</p><br>\n\n            <p class=\"text-one before-text\">1. Make sure you are Registered. There is no charge.</p>\n\n            <p class=\"text-one before-text\">2. LIST PRODUCT FOR SALE (Click on each item for related forms and instructions) </p>\n\n            <!--<p class=\"text-one before-text padding\">CHOOSE AND SELECT NOTE CATEGORY </p>\n            <p class=\"text-one before-text padding-su-title\"><a href=\"#\">Newly Originated Notes For Sale (click here)</a></p>\n            <p class=\"text-one before-text padding-su-title\"><a href=\"#\">Performing Notes For Sale (click here)</a></p>\n            <p class=\"text-one before-text padding-su-title\"><a href=\"#\">Non Performing Notes For Sale (click here)</a></p>\n            <p class=\"text-one before-text padding-su-title\"><a href=\"#\">Loan Pools for Sale (click here)</a></p>-->\n\n            <p class=\"text-one before-text\">3. After notes are entered, and approval email is received (usually 4-8 business hours), go to <a href=\"#\"> My Notes For Sale</a> to view status, offers, and to start the Closing Process and complete the Transaction.</p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section><br>\n<br>"

/***/ }),

/***/ "../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HowToSellNotesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HowToSellNotesComponent = (function () {
    function HowToSellNotesComponent(nav) {
        this.nav = nav;
    }
    HowToSellNotesComponent.prototype.ngOnInit = function () {
        this.nav.show();
    };
    HowToSellNotesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-how-to-sell-notes',
            template: __webpack_require__("../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */]])
    ], HowToSellNotesComponent);
    return HowToSellNotesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/loading/loading.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Absolute Center Spinner */\r\n\r\n.loading {\r\n    position: fixed;\r\n    z-index: 999;\r\n    height: 2em;\r\n    width: 2em;\r\n    overflow: show;\r\n    margin: auto;\r\n    top: 0;\r\n    left: 0;\r\n    bottom: 0;\r\n    right: 0;\r\n}\r\n\r\n\r\n/* Transparent Overlay */\r\n\r\n.loading:before {\r\n    content: '';\r\n    display: block;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    width: 100%;\r\n    height: 100%;\r\n    background-color: rgba(0, 0, 0, 0.3);\r\n}\r\n\r\n\r\n/* :not(:required) hides these rules from IE9 and below */\r\n\r\n.loading:not(:required) {\r\n    /* hide \"loading...\" text */\r\n    font: 0/0 a;\r\n    color: transparent;\r\n    text-shadow: none;\r\n    background-color: transparent;\r\n    border: 0;\r\n}\r\n\r\n.loading:not(:required):after {\r\n    content: '';\r\n    display: block;\r\n    font-size: 10px;\r\n    width: 1em;\r\n    height: 1em;\r\n    margin-top: -0.5em;\r\n    -webkit-animation: spinner 1500ms infinite linear;\r\n    animation: spinner 1500ms infinite linear;\r\n    border-radius: 0.5em;\r\n    box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;\r\n}\r\n\r\n\r\n/* Animation */\r\n\r\n@-webkit-keyframes spinner {\r\n    0% {\r\n        -webkit-transform: rotate(0deg);\r\n        transform: rotate(0deg);\r\n    }\r\n    100% {\r\n        -webkit-transform: rotate(360deg);\r\n        transform: rotate(360deg);\r\n    }\r\n}\r\n\r\n@keyframes spinner {\r\n    0% {\r\n        -webkit-transform: rotate(0deg);\r\n        transform: rotate(0deg);\r\n    }\r\n    100% {\r\n        -webkit-transform: rotate(360deg);\r\n        transform: rotate(360deg);\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/loading/loading.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"loading.visible\">Loading&#8230;</div>"

/***/ }),

/***/ "../../../../../src/app/components/loading/loading.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var loadingComponent = (function () {
    function loadingComponent(loading) {
        this.loading = loading;
    }
    loadingComponent.prototype.ngOnInit = function () {
    };
    loadingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-loading',
            template: __webpack_require__("../../../../../src/app/components/loading/loading.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/loading/loading.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["i" /* loadingService */]])
    ], loadingComponent);
    return loadingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 50px;\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"col-sm-3 float-left\">\r\n                    <p class=\"text-center page-detail\">{{loanNote?.property?.address}},{{loanNote?.property?.city}}, {{loanNote?.property?.zip}}</p>\r\n                </div>\r\n                <div class=\"col-sm-1 float-left text-center\"><img class=\"img-borderr\" src=\"assets/img/border.png\"></div>\r\n                <div class=\"col-sm-4 float-left\">\r\n                    <p class=\"text-center page-detail-espace-tow\">Seller asking: <span>${{loanNote?.askingPrice}} ({{loanNote?.askingPrice / loanNote?.principaleBalance | number : '1.2-2' }}% UPB)</span><br>\r\n                        <small class=\"small-text\">Pricing may vary over time and is subject to Seller Approval</small></p>\r\n                </div>\r\n                <div class=\"col-sm-1 float-left text-center\"><img class=\"img-borderr\" src=\"assets/img/border.png\"></div>\r\n\r\n            </div>\r\n        </div>\r\n        <hr class=\"sort-by\">\r\n    </div>\r\n</section>\r\n<section style=\"margin-top: 20px;\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"col-sm-4 float-left\">\r\n                    <!--slideshow-->\r\n                    <div #flexslider class=\"flexslider\">\r\n                        <ul class=\"slides\">\r\n                            <li data-thumb=\"assets/img/Art-1.png\" rel=\"gallery1\"><img src=\"assets/img/Art-1.png\"></li>\r\n                            <li data-thumb=\"assets/img/Art-1.png\" rel=\"gallery1\"><img src=\"assets/img/Art-1.png\"></li>\r\n                            <li data-thumb=\"assets/img/Art-1.png\" rel=\"gallery1\"><img src=\"assets/img/Art-1.png\"></li>\r\n                            <li data-thumb=\"assets/img/Art-1.png\" rel=\"gallery1\"><img src=\"assets/img/Art-1.png\"></li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-4 float-left\">\r\n                    <h3 class=\"text-left text-img-page-detail title\">General Note Information</h3>\r\n\r\n                    <p class=\"text-left text-img-page-detail\">Product ID <span class=\"pull-right\">{{loanNote?.loanNoteID}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Lien Position <span *ngIf=\"loanNote?.deedMortgageStatus; else elseBlock\" class=\"pull-right\">1<sup>st</sup></span>\r\n\r\n                        <ng-template #elseBlock>\r\n                            <span class=\"pull-right\" *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}}</span>\r\n                            <ng-template #elseBlock2>\r\n                                <span>2<sup>nd</sup></span>\r\n                            </ng-template>\r\n                        </ng-template>\r\n\r\n                    </p>\r\n                    <p class=\"text-left text-img-page-detail\">Note Status <span class=\"pull-right\">{{loanNote?.loanStatus?.name}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Note type <span class=\"pull-right\">{{loanNote?.loanType?.nameLoanType}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">#of Payments Last 12 Months <span class=\"pull-right\">{{loanNote?.ofPayementsLast12}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Paid To <span class=\"pull-right\">{{loanNote?.paidToDate | date:'MM/dd/yyyy'}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Loan Maturity <span class=\"pull-right\">{{loanNote?.noteMaturityDate | date:'MM/dd/yyyy'}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Property Type <span class=\"pull-right\">{{PropertyType?.typeProperty?.name}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Original Balance <span class=\"pull-right\">none</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Principal Balance <span class=\"pull-right\">{{loanNote?.originalLoanAmount}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Est. Market Value <span class=\"pull-right\">{{loanNote?.propertyMarketValue}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Payment Amount <span class=\"pull-right\">none</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Note Rate <span class=\"pull-right\">{{loanNote?.noteInterestRate}}</span></p>\r\n                    <p class=\"text-left text-img-page-detail\">Sold Rate <span class=\"pull-right\">{{loanNote?.soldInterestRate}}</span></p>\r\n                </div>\r\n                <div class=\"col-sm-1 float-left text-center\"><img class=\"img-borderr\" height=\"410px;\" src=\"assets/img/border.png\" width=\"1px;\"></div>\r\n                <div class=\"col-sm-3 float-left\">\r\n                    <h3 class=\"text-left text-img-page-detail title\">Actions</h3><button class=\"btn btn-success btn-lg btn-block btn-action\" type=\"button\" (click)=\"SubmitOffer(loanNote)\">Submit an Indicative Offer</button><br>\r\n                    <br>\r\n                    <hr>\r\n                    <a href=\"#\" class=\"hrf-action\"><i aria-hidden=\"true\" class=\"fa fa-chevron-right\" id=\"icone-investment\"></i> Investment Strategy Model</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <hr class=\"sort-by\">\r\n    </div>\r\n</section>\r\n<section style=\"margin-top: 20px;\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <h3 class=\"text-img-page-detail title\">Similar listings</h3>\r\n            </div>\r\n        </div><br>\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div id=\"owl-demo\" #owldemo>\r\n                    <div class=\"item\" *ngFor=\"let Note of similarNotes\">\r\n                        <div class=\"card\">\r\n                            <div class=\"col-sm-12 card-body\">\r\n                                <div class=\"col-lg-6 float-left\"><img src=\"assets/img/Art-1.png\" width=\"100%\"></div>\r\n                                <div class=\"col-lg-6 float-left\">\r\n                                    <p class=\"card-text text-card\">Product ID <span>{{Note?.loanNoteID}}</span></p>\r\n                                    <p class=\"card-text text-card\"> <span *ngIf=\"Note?.deedMortgageStatus; else elseBlock\">1<sup>st</sup> Mortgage</span>\r\n                                        <ng-template #elseBlock>\r\n                                            <span *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}} Mortgage</span>\r\n                                            <ng-template #elseBlock2>\r\n                                                <span>2<sup>nd</sup> Mortgage</span>\r\n                                            </ng-template>\r\n                                        </ng-template>\r\n                                    </p>\r\n                                    <p class=\"card-text text-card\">UPB: <span>{{Note?.loanNoteID}}</span></p>\r\n                                    <p class=\"card-text text-card\">Not Rate: <span>{{Note?.loanNoteID}}</span></p>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"card-footer text-muted add-card text-left\">\r\n                                {{loanNote?.property?.address}},{{loanNote?.property?.city}}, {{loanNote?.property?.zip}}\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section><br>\r\n<br>\r\n<section style=\"margin-top: 20px;\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <h3 class=\"text-img-page-detail title\">Detailed Note Information</h3>\r\n            </div>\r\n        </div><br>\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-12 nav-pill-footer border\">\r\n                <ul class=\"nav nav-pills mb-5\" id=\"pills-tab\" role=\"tablist\">\r\n                    <li class=\"nav-item\">\r\n                        <a aria-controls=\"pills-home\" aria-selected=\"true\" class=\"nav-link active\" data-toggle=\"pill\" href=\"#pills-TERMS\" id=\"pills-TERMS-tab\" role=\"tab\">NOTE TERMS</a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a aria-controls=\"pills-profile\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Dates\" id=\"pills-Dates-tab\" role=\"tab\">Note Dates</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"bankruptcyChapter != null\">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Bankruptcy\" id=\"pills-Bankruptcy-tab\" role=\"tab\">Bankruptcy</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"loanNote?.escrowImpounds != undefined\">\r\n                        <a aria-controls=\"pills-profile\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Escrow\" id=\"pills-Escrow-tab\" role=\"tab\">Escrow Information</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"loanNote?.foreclosure != undefined\">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Foreclosure\" id=\"pills-Foreclosure-tab\" role=\"tab\">Foreclosure</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"loanNote?.drawPeriodStartDate != undefined || loanNote?.repaymentPeriodStartDate != undefined \">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Credit\" id=\"pills-Credit-tab\" role=\"tab\">Line Of Credit</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"loanNote?.indexName != undefined || loanNote?.margin != undefined  || loanNote?.ceiling != undefined  || loanNote?.floor != undefined  || loanNote?.nextAdjustement != undefined  || loanNote?.adjustementFrequency != undefined \">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-ARM\" id=\"pills-ARM-tab\" role=\"tab\">ARM</a>\r\n                    </li>\r\n                    <li class=\"nav-item\" *ngIf=\"loanNote?.property != undefined\">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-Property\" id=\"pills-Property-tab\" role=\"tab\">Property Information</a>\r\n                    </li>\r\n                </ul>\r\n                <hr>\r\n                <div class=\"tab-content\" id=\"pills-tabContent\">\r\n                    <div aria-labelledby=\"pills-TERMS-tab\" class=\"tab-pane fade show active\" id=\"pills-TERMS\" role=\"tabpanel\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Lien Position\r\n                                            <span *ngIf=\"loanNote?.deedMortgageStatus; else elseBlock\" class=\"pull-right\">1<sup>st</sup></span>\r\n                                            <ng-template #elseBlock>\r\n                                                <span class=\"pull-right\" *ngIf=\"Note?.deedMortgage === undefined; else elseBlock2\">{{Note?.deedMortgage?.lienPosition}}</span>\r\n                                                <ng-template #elseBlock2>\r\n                                                    <span>2<sup>nd</sup></span>\r\n                                                </ng-template>\r\n                                            </ng-template>\r\n                                        </p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Original Balance <span class=\"pull-right\">{{loanNote?.originalLoanAmount}} </span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Total Payment <span class=\"pull-right\">$427.08</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Total in Trust <span class=\"pull-right\">{{loanNote?.payementTrust}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Unpaid Charges <span class=\"pull-right\">$0.00</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Ballon payment <span class=\"pull-right\">{{loanNote?.ballonPayement}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">On Forbearance Plan <span class=\"pull-right\">{{loanNote?.onForbearancePlan}}</span></p>\r\n                                    </div>\r\n                                    <div class=\"col-sm-1 float-left text-center\"><img class=\"img-borderr footer-bord\" src=\"assets/img/border.png\" height=\"180px;\" width=\"1px;\"></div>\r\n                                    <div class=\"col-lg-4 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Amortization Type <span class=\"pull-right\">{{loanNote?.amortizationType?.name}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Current Blance <span class=\"pull-right\">$48,423.00</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Note Rate <span class=\"pull-right\">{{loanNote?.noteInterestRate}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Unpaid Interest <span class=\"pull-right\">{{loanNote?.UnpaidInterest}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Prepayment Penalty <span class=\"pull-right\">{{loanNote?.prePayPenalty}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Loan Terms Modified <span class=\"pull-right\">{{loanNote?.loanTermsModified}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">In Foreclosure <span class=\"pull-right\">{{loanNote?.foreclosure?.forceclosureStatus}}</span></p>\r\n                                    </div>\r\n                                    <div class=\"col-sm-1 float-left text-center\"><img class=\"img-borderr footer-bord\" src=\"assets/img/border.png\" height=\"180px;\" width=\"1px;\"></div>\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Loan Type <span class=\"pull-right\">0006200</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">PI <span class=\"pull-right\">{{loanNote?.PI}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Sold Rate <span class=\"pull-right\">{{loanNote?.soldInterestRate}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Late Charges is <span class=\"pull-right\">{{loanNote?.lateCharge}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Rate Type <span class=\"pull-right\">{{loanNote?.rateType?.name}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Registered w/MERS <span class=\"pull-right\">{{loanNote?.registerswMers}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">In Bankruptcy <span class=\"pull-right\">{{bankruptcyChapter?.bankruptcyStatus}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Dates-tab\" class=\"tab-pane fade\" id=\"pills-Dates\" role=\"tabpanel\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-5 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Origination Date <span class=\"pull-right\">{{loanNote?.originationDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">First Payment Date <span class=\"pull-right\">{{loanNote?.firstPayementDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Paid to Date <span class=\"pull-right\">{{loanNote?.paidToDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">nextPayementDate <span class=\"pull-right\">{{loanNote?.nextPayementDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Maturity Date <span class=\"pull-right\">{{loanNote?.noteMaturityDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Last Payment Received <span class=\"pull-right\">{{loanNote?.payersLastPayementMadeDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Bankruptcy-tab\" class=\"tab-pane fade\" id=\"pills-Bankruptcy\" role=\"tabpanel\" *ngIf=\"bankruptcyChapter != null\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-5 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Bk Discharge Date <span class=\"pull-right\">{{bankruptcyChapter?.bkDisChargeDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Bk Chapter <span class=\"pull-right\">{{bankruptcyChapter?.name}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Bk Dismissal Date <span class=\"pull-right\">{{bankruptcyChapter?.bkDismissalDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Bk Filling Date <span class=\"pull-right\">{{bankruptcyChapter?.bkFillingDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Escrow-tab\" class=\"tab-pane fade\" id=\"pills-Escrow\" role=\"tabpanel\" *ngIf=\"loanNote?.escrowImpounds != undefined\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Note Payment Amount<span class=\"pull-right\">{{loanNote?.escrowImpounds?.notePaymentAmount}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Tax Portion<span class=\"pull-right\">{{loanNote?.escrowImpounds?.taxPortion}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Insurance Portion <span class=\"pull-right\">{{loanNote?.escrowImpounds?.insurancePortion}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Trust Balance <span class=\"pull-right\">{{loanNote?.escrowImpounds?.trustBalance}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Foreclosure-tab\" class=\"tab-pane fade\" id=\"pills-Foreclosure\" role=\"tabpanel\" *ngIf=\"loanNote?.foreclosure != undefined\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Date Opened <span class=\"pull-right\">{{loanNote?.foreclosure?.dateOpened | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Schedule Sale Date <span class=\"pull-right\">{{loanNote?.foreclosure?.scheduledSaleDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Credit-tab\" class=\"tab-pane fade\" id=\"pills-Credit\" role=\"tabpanel\" *ngIf=\"loanNote?.drawPeriodStartDate != undefined || loanNote?.repaymentPeriodStartDate != undefined \">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Draw Period Start Date <span class=\"pull-right\">{{loanNote?.drawPeriodStartDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Repayment Period Start Date <span class=\"pull-right\">{{loanNote?.repaymentPeriodStartDate | date:'MM/dd/yyyy'}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                            \r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-ARM-tab\" class=\"tab-pane fade\" id=\"pills-ARM\" role=\"tabpanel\" *ngIf=\"loanNote?.indexName != undefined || loanNote?.margin != undefined  || loanNote?.ceiling != undefined  || loanNote?.floor != undefined  || loanNote?.nextAdjustement != undefined  || loanNote?.adjustementFrequency != undefined \">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Current Index Name<span class=\"pull-right\">{{loanNote?.indexName}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Margin <span class=\"pull-right\">{{loanNote?.margin}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Ceiling <span class=\"pull-right\">{{loanNote?.ceiling}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Floor <span class=\"pull-right\">{{loanNote?.floor}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Next Adjustment <span class=\"pull-right\">{{loanNote?.nextAdjustement}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Adjustment Frequency <span class=\"pull-right\">{{loanNote?.adjustementFrequency}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-Property-tab\" class=\"tab-pane fade\" id=\"pills-Property\" role=\"tabpanel\" *ngIf=\"loanNote?.property != undefined\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"row\" id=\"footer-nav-pills\">\r\n                                <div class=\"col-md-12\">\r\n                                    <div class=\"col-lg-3 float-left\">\r\n                                        <p class=\"text-left text-img-page-detail footer\">Property Address <span class=\"pull-right\">{{loanNote?.property?.address}} {{loanNote?.property?.city}} {{loanNote?.property?.zip}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Property Type <span class=\"pull-right\">{{PropertyType?.typeProperty?.name}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">City <span class=\"pull-right\">{{loanNote?.property?.city}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Market Value <span class=\"pull-right\">{{loanNote?.propertyMarketValue}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">LTV Ratio <span class=\"pull-right\">none</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Country <span class=\"pull-right\">{{loanNote?.property?.country}}</span></p>\r\n                                        <p class=\"text-left text-img-page-detail footer\">Valuation Date <span class=\"pull-right\">{{PropertyType?.valuationType?.name}}</span></p>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <br>\r\n                    <div class=\"col-sm-12\">\r\n                        <p class=\"text-img-page-detail title-footer\">Seller Comments</p>\r\n                    </div>\r\n                    <div class=\"col-sm-12\">\r\n                        <p class=\"text-img-page-detail title-footer-text\">{{loanNote?.comments}}.</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>  \r\n<br><br>\r\n<section>\r\n    <div class=\"container-fluid\">\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <div class=\"alert alert-success\" role=\"alert\">\r\n              <div class=\"col-lg-8 offset-lg-2\">\r\n                  <p class=\"text-center alert-contact\">\r\n                    If you have any questions or concerns please contact ExchangeLoans at support@exchangeloans.com or call (800)931-2424 x 750.\r\n                  </p>\r\n            </div>\r\n          </div>  \r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section><br>\r\n<br>"

/***/ }),

/***/ "../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loanNoteDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var loanNoteDetailComponent = (function () {
    function loanNoteDetailComponent(_loanNoteDet, nav, route, loading, _router) {
        var _this = this;
        this._loanNoteDet = _loanNoteDet;
        this.nav = nav;
        this.route = route;
        this.loading = loading;
        this._router = _router;
        this.id = 0;
        setTimeout(function () {
            /** Flex Slider **/
            $(_this.el.nativeElement).flexslider({
                animation: "slide",
                controlNav: "thumbnails",
                lightbox: true
            });
            /** owl Slider **/
            $(_this.elowl.nativeElement).owlCarousel({
                autoPlay: 4000,
                items: 3,
                itemsDesktop: [1200, 1],
                itemsDesktoplgall: [900, 1],
                itemsTablet: [650, 1],
                //Pagination
                pagination: false,
                paginationNumbers: false,
            });
        }, 1000);
    }
    loanNoteDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nav.show();
        this.loading.show();
        this.id = this.route.snapshot.params['id'];
        this.getAllNotes(this.id);
        setTimeout(function () {
            _this.loading.hide();
        }, 2000);
        this.getLoanNoteAllDetail(this.id);
    };
    loanNoteDetailComponent.prototype.getAllNotes = function (id) {
        var _this = this;
        this._loanNoteDet.getAllNotes(id).subscribe(function (cdata) {
            _this.similarNotes = cdata;
            _this.similarNotes = _this.similarNotes;
            console.log(_this.similarNotes);
        });
    };
    loanNoteDetailComponent.prototype.getLoanNoteAllDetail = function (id) {
        var _this = this;
        this._loanNoteDet.getLoanNoteAllDetail(id).subscribe(function (data) {
            console.log(data);
            _this.loanNote = data[0];
            _this.bankruptcyChapter = data[1];
            _this.PropertyType = data[2];
        });
    };
    loanNoteDetailComponent.prototype.SubmitOffer = function (item) {
        this._router.navigate(["SubmitOffer", item.loanNoteID]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('flexslider'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], loanNoteDetailComponent.prototype, "el", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('owldemo'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], loanNoteDetailComponent.prototype, "elowl", void 0);
    loanNoteDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-loan-note-detail',
            template: __webpack_require__("../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["j" /* loanNoteDetailService */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["i" /* loadingService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]])
    ], loanNoteDetailComponent);
    return loanNoteDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body,\r\nhtml {\r\n    height: 100%;\r\n    background-repeat: no-repeat;\r\n    background: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));\r\n}\r\n\r\n.has-error {\r\n    color: red;\r\n}\r\n\r\n.containerLogin {\r\n    margin-top: 10%;\r\n}\r\n\r\n.has-error input {\r\n    border-color: red;\r\n}\r\n\r\n.card-container.card {\r\n    max-width: 350px;\r\n    padding: 40px 40px;\r\n}\r\n\r\n.btn {\r\n    font-weight: 700;\r\n    height: 36px;\r\n    -moz-user-select: none;\r\n    -webkit-user-select: none;\r\n    -ms-user-select: none;\r\n        user-select: none;\r\n    cursor: default;\r\n}\r\n\r\n\r\n/*\r\n * Card component\r\n */\r\n\r\n.card {\r\n    background-color: #F7F7F7;\r\n    /* just in case there no content*/\r\n    padding: 20px 25px 30px;\r\n    margin: 0 auto 25px;\r\n    margin-top: 50px;\r\n    /* shadows and rounded borders */\r\n    border-radius: 2px;\r\n    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);\r\n}\r\n\r\n.profile-img-card {\r\n    width: 96px;\r\n    height: 96px;\r\n    margin: 0 auto 10px;\r\n    display: block;\r\n    border-radius: 50%;\r\n}\r\n\r\n\r\n/*\r\n * Form styles\r\n */\r\n\r\n.profile-name-card {\r\n    font-size: 16px;\r\n    font-weight: bold;\r\n    text-align: center;\r\n    margin: 10px 0 0;\r\n    min-height: 1em;\r\n}\r\n\r\n.reauth-email {\r\n    display: block;\r\n    color: #404040;\r\n    line-height: 2;\r\n    margin-bottom: 10px;\r\n    font-size: 14px;\r\n    text-align: center;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    white-space: nowrap;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.form-signin #inputEmail,\r\n.form-signin #inputPassword {\r\n    direction: ltr;\r\n    height: 44px;\r\n    font-size: 16px;\r\n}\r\n\r\n.form-signin input[type=email],\r\n.form-signin input[type=password],\r\n.form-signin input[type=text],\r\n.form-signin button {\r\n    width: 100%;\r\n    display: block;\r\n    margin-bottom: 10px;\r\n    z-index: 1;\r\n    position: relative;\r\n    box-sizing: border-box;\r\n}\r\n\r\n.form-signin .form-control:focus {\r\n    border-color: rgb(104, 145, 162);\r\n    outline: 0;\r\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgb(104, 145, 162);\r\n}\r\n\r\n.btn.btn-signin {\r\n    /*background-color: #4d90fe; */\r\n    background-color: rgb(104, 145, 162);\r\n    /* background-color: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/\r\n    padding: 0px;\r\n    font-weight: 700;\r\n    font-size: 14px;\r\n    height: 36px;\r\n    border-radius: 3px;\r\n    border: none;\r\n    transition: all 0.218s;\r\n}\r\n\r\n.btn.btn-signin:hover,\r\n.btn.btn-signin:active,\r\n.btn.btn-signin:focus {\r\n    background-color: rgb(12, 97, 33);\r\n}\r\n\r\n.forgot-password {\r\n    color: rgb(104, 145, 162);\r\n}\r\n\r\n.forgot-password:hover,\r\n.forgot-password:active,\r\n.forgot-password:focus {\r\n    color: rgb(12, 97, 33);\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<section>\r\n    <div class=\"container icones-container\">\r\n        <div class=\"row icones\">\r\n            <div class=\"col-sm-12 logo-zone\"><img src=\"../../assets/img/logo.png\"></div>\r\n        </div>\r\n    </div>\r\n    <div class=\"container page-login-container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-8 offset-md-2\">\r\n                <div class=\"card card-signup\">\r\n                    <h4 class=\"card-title text-center card-title-login icones\">\r\n                        <a [routerLink]=\"['/register']\" class=\"icone-a\"><i class=\"fa fa-user-plus\" aria-hidden=\"true\"> Register</i></a>\r\n                    </h4>\r\n                    <h2 class=\"card-title text-center card-title-login\" style=\"color:#818181\">LOG<span style=\"color: #acd589\">IN</span></h2>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-10 offset-md-1 inputs\">\r\n                            <form class=\"form-signin\" name=\"form\" (ngSubmit)=\"f.form.valid && onLogin()\" #f=\"ngForm\" novalidate>\r\n                                <div class=\"card-content\">\r\n                                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n                                        <input type=\"text\" placeholder=\"Email address\" id=\"inputEmail\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.username\" #username=\"ngModel\" required />\r\n                                        <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Username is required</div>\r\n                                    </div>\r\n                                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n                                        <input type=\"password\" id=\"inputPassword\" placeholder=\"Password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"user.password\" #password=\"ngModel\" required />\r\n                                        <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n                                    </div>\r\n                                    <div class=\"row checkbox-space flot-left\">\r\n                                            <div class=\"col-sm-6 text-left\">\r\n                                                    <input class=\"form-check-input\" type=\"checkbox\" (change)=\"rememberme = !rememberme\" >\r\n                                                    <p class=\"text-login\"><label class=\"form-check-label\" for=\"defaultCheck1\">Remember me</label></p>\r\n                                            </div>\r\n                                            <div class=\"col-sm-6 text-right\">\r\n                                                    <a [routerLink]=\"['/forgotpassword']\" class=\"Forgotpassword\">Forgot password?</a>\r\n                                            </div>\r\n                                   </div><br>\r\n\r\n\r\n                                    \r\n\r\n\r\n                                   <div class=\"col-lg-12\">\r\n                                        <div class=\"row checkbox-space flot-left\">\r\n                                                <input class=\"form-check-input\" id=\"defaultCheck2\" type=\"checkbox\" value=\"\">\r\n                                                <p class=\"text-login condition\"><label class=\"form-check-label\" for=\"defaultCheck2\">I hereby agree to the <a [routerLink]=\"['/terms-and-conditions']\">Terms of Use For Trading On This Site</a> agreement and the <a [routerLink]=\"['/trading-terms']\">Website Usage Terms and Conditions</a> agreement, and further hereby agree that Contact with the Property Owners, Borrowers, etc. is strictly Prohibited. In accordance with these agreements, you agree that you will not contact borrowers, property owners, or tenants/residents of the Loans or any Note listed on the Site directly, or indirectly, by yourself or through an affiliate or agent of yours. You agree that all contact will be facilitated through FCI EXCHANGE's website or directly with the Seller and/or Buyer.</label></p>\r\n                                            </div>\r\n                                   </div>\r\n                                </div>\r\n                                <div class=\"footer text-center\">\r\n                                    <button [disabled]=\"loading\" class=\"btn btn-primary btn-block btn-signin\"><img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\"\r\n                                        /> &nbsp; Sign in</button>\r\n                                </div>\r\n                                <div *ngIf=\"error.length > 0 \" class=\"alert alert-danger text-center\" role=\"alert\">\r\n                                    <strong><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i>&nbsp;Ooops!</strong>\r\n                                    <p>{{error}}</p>\r\n                                </div>\r\n                            </form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_takeWhile__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/takeWhile.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var loginComponent = (function () {
    function loginComponent(_authenServ, router, activeRoute, nav) {
        this._authenServ = _authenServ;
        this.router = router;
        this.activeRoute = activeRoute;
        this.nav = nav;
        this.user = {};
        this.loading = false;
        this.error = '';
        this.alive = true;
        this.rememberme = false;
    }
    loginComponent.prototype.ngOnInit = function () {
        this._authenServ.logout();
        this.nav.hide();
        this.user.username = this.activeRoute.snapshot.queryParamMap.get('email');
    };
    loginComponent.prototype.onLogin = function () {
        var _this = this;
        this.loading = true;
        this._authenServ.login(this.user.username, this.user.password, this.rememberme).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
            if (res === true) {
                _this.loading = false;
                _this.router.navigate(['about-us']);
            }
        }, function (err) {
            _this.error = 'User name or password is incorrect';
            _this.loading = false;
        }, function () { return console.log('Task Complete'); });
    };
    loginComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    loginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__structExports_services__["e" /* authentificationService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_2__structExports_services__["b" /* NavbarService */]])
    ], loginComponent);
    return loginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/myExchange/myExchange.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".table-hover {\r\n    cursor: pointer;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/myExchange/myExchange.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<section style=\"margin-top: 20px;\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-12 nav-pill-footer page-myexchange border\">\r\n                <ul class=\"nav nav-pills mb-5\" id=\"pills-tab\" role=\"tablist\">\r\n                    <li class=\"nav-item\">\r\n                        <a aria-controls=\"pills-home\" aria-selected=\"true\" class=\"nav-link active\" data-toggle=\"pill\" href=\"#pills-home\" id=\"pills-home-tab\" role=\"tab\">My Loans </a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a aria-controls=\"pills-profile\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-profile\" id=\"pills-profile-tab\" role=\"tab\">My Offers</a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a aria-controls=\"pills-contact\" aria-selected=\"false\" class=\"nav-link\" data-toggle=\"pill\" href=\"#pills-contact\" id=\"pills-contact-tab\" role=\"tab\">Profile</a>\r\n                    </li>\r\n                </ul>\r\n                <hr>\r\n                <div class=\"tab-content\" id=\"pills-tabContent\">\r\n                    <div aria-labelledby=\"pills-home-tab\" class=\"tab-pane fade show active\" id=\"pills-home\" role=\"tabpanel\">\r\n                        <div class=\"container-fluid\">\r\n                            <br>\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-12 text-right\">\r\n\r\n                                    <button id=\"b\" (click)=\"exportToExcel()\" class=\"btn btn-primary\">Export Template</button>\r\n                                    <input type=\"file\" (change)=\"onFileChange($event)\" name=\"file\" id=\"file\" class=\"input-file\">\r\n\r\n                                    <label for=\"file\" class=\"btn btn-tertiary js-labelFile\">\r\n                                                    <i class=\"icon fa fa-check\"></i>\r\n                                                    <span class=\"js-fileName\">Choose a Template</span>\r\n                                    </label>\r\n                                    <button id=\"b\" class=\"btn btn-primary\" [hidden]=\"!isExistTemplate\" (click)=\"SaveJsonAsResultLoans()\">Save</button>\r\n\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-12 bordern\">\r\n                                <br>\r\n\r\n                                <h3 class=\"text-img-page-detail title\">My Loans:</h3><br>\r\n                                <div class=\"table-responsive\">\r\n                                    <table class=\"table page-myexchange\">\r\n                                        <thead class=\"custom\">\r\n                                            <tr>\r\n                                                <th scope=\"col\">Loan ID</th>\r\n                                                <th scope=\"col\">UPB</th>\r\n                                                <th scope=\"col\">Asking Price</th>\r\n                                                <th scope=\"col\">Note Rate</th>\r\n                                                <th scope=\"col\">Status</th>\r\n                                                <th scope=\"col\">Last Modified</th>\r\n                                                <th scope=\"col\">Update</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngIf=\"pagedItemsLoans?.length  == 0\">\r\n                                                <td colspan=\"7\">\r\n                                                    <h5 class=\"text-center text-info\">No Loans Currently !</h5>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let item of pagedItemsLoans\" (click)=\"onViewOffersLoan(item.loanNoteID)\">\r\n                                                <td>{{item.loanNoteID}}</td>\r\n                                                <td>{{item.unPaidPrincipalBalance}}</td>\r\n                                                <td>{{item.askingPrice}}</td>\r\n                                                <td>{{item.noteInterestRate}}</td>\r\n                                                <td>----</td>\r\n                                                <td>----</td>\r\n                                                <td><button class=\"btn btn-primary\" type=\"button\">Update</button></td>\r\n                                            </tr>\r\n\r\n                                        </tbody>\r\n                                    </table>\r\n\r\n                                    <div class=\"col-sm-12 pagination-padding\">\r\n                                        <nav aria-label=\"Page navigation example\">\r\n                                            <ul class=\"pagination justify-content-end float-right\">\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerLoans.currentPage === 1}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageLoans(1)\">First</a>\r\n                                                </li>\r\n\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerLoans.currentPage === 1}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageLoans(pagerLoans.currentPage - 1)\">Previous</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" *ngFor=\"let page of pagerLoans.pages\" [ngClass]=\"{active:pagerLoans.currentPage === page}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageLoans(page)\">{{page}}</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerLoans.currentPage === pagerLoans.totalPages}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageLoans(pagerLoans.currentPage + 1)\">Next</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerLoans.currentPage === pagerLoans.totalPages}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageLoans(pagerLoans.totalPages)\">Last</a>\r\n                                                </li>\r\n                                            </ul>\r\n                                            <p class=\"text-pagination float-left\">Results - <span>Viewing ltems    {{pagerLoans.startIndex }}-{{pagerLoans.endIndex +1}} of {{pagerLoans.totalItems}}</span></p>\r\n                                        </nav>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"table-responsive\" *ngIf=\"isActivtedLoanOffers\">\r\n                                    <h3 class=\"text-img-page-detail text-info title\">Offers Détail:</h3><br>\r\n                                    <table class=\"table table-mycolor table-hover\">\r\n                                        <thead class=\"thead-Exchange\">\r\n                                            <tr>\r\n                                                <th scope=\"col\">Offer Id</th>\r\n                                                <th scope=\"col\">Amount</th>\r\n                                                <th scope=\"col\">Comment</th>\r\n                                                <th scope=\"col\">Statut</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngIf=\"listOfOffersLoan?.length  == 0\">\r\n                                                <td colspan=\"4\">\r\n                                                    <h5 class=\"text-center text-info\">No Offers Currently !</h5>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let item of listOfOffersLoan\" (click)=\"navigateToTransactionSeller(item.offer.offerID)\">\r\n                                                <td>{{item.offer.offerID}}</td>\r\n                                                <td>{{item.offer.OfferAmount}}</td>\r\n                                                <td>{{item.offer.Comment}}</td>\r\n                                                <td>-----------</td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                    <div aria-labelledby=\"pills-profile-tab\" class=\"tab-pane fade\" id=\"pills-profile\" role=\"tabpanel\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"col-lg-12 border\">\r\n                                <br>\r\n                                <h3 class=\"text-img-page-detail title\">My Offers:</h3><br>\r\n                                <div class=\"table-responsive\">\r\n                                    <table class=\"table table-hover page-myexchange myoffers\">\r\n                                        <thead class=\"custom\">\r\n                                            <tr>\r\n                                                <th scope=\"col\">Offres Id</th>\r\n                                                <th scope=\"col\">Amount</th>\r\n                                                <th scope=\"col\">Status</th>\r\n                                                <th scope=\"col\">Last Update</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr *ngIf=\"pagedItemsOffers?.length  == 0\">\r\n                                                <td colspan=\"6\">\r\n                                                    <h5 class=\"text-center text-info\">No Offers Currently !</h5>\r\n                                                </td>\r\n                                            </tr>\r\n                                            <tr *ngFor=\"let item of pagedItemsOffers\" (click)=\"navigateToTransaction(item.Offer.offerID)\">\r\n                                                <td>{{item.Offer.offerID}}</td>\r\n                                                <td>{{item.Offer.OfferAmount}}</td>\r\n                                                <td>{{item.OfferStatus?.OfferStatusName}}</td>\r\n                                                <td>{{item.OfferStatus?.OfferStatusByDate}}</td>\r\n\r\n                                            </tr>\r\n\r\n                                        </tbody>\r\n                                    </table>\r\n                                    <div class=\"col-sm-12 pagination-padding\">\r\n                                        <nav aria-label=\"Page navigation example\">\r\n                                            <ul class=\"pagination justify-content-end float-right\">\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerOffers.currentPage === 1}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageOffers(1)\">First</a>\r\n                                                </li>\r\n\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerOffers.currentPage === 1}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageOffers(pagerOffers.currentPage - 1)\">Previous</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" *ngFor=\"let page of pagerOffers.pages\" [ngClass]=\"{active:pagerOffers.currentPage === page}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageOffers(page)\">{{page}}</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerOffers.currentPage === pagerOffers.totalPages}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageOffers(pagerOffers.currentPage + 1)\">Next</a>\r\n                                                </li>\r\n                                                <li class=\"page-item\" [ngClass]=\"{disabled:pagerOffers.currentPage === pagerOffers.totalPages}\">\r\n                                                    <a class=\"page-link\" (click)=\"setPageOffers(pagerOffers.totalPages)\">Last</a>\r\n                                                </li>\r\n                                            </ul>\r\n                                            <p class=\"text-pagination float-left\">Results - <span>Viewing ltems    {{pagerOffers.startIndex }}-{{pagerOffers.endIndex +1}} of {{pagerOffers.totalItems}}</span></p>\r\n                                        </nav>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <!-- <hr class=\"sort-by\"> -->\r\n                        </div>\r\n                    </div>\r\n                   <div aria-labelledby=\"pills-contact-tab\" class=\"tab-pane fade\" id=\"pills-contact\" role=\"tabpanel\">\r\n                        <div class=\"container-fluid\">\r\n                            <div class=\"col-lg-12\"><h3 class=\"text-img-page-detail title\">My Profile</h3></div>\r\n                                <div class=\"col-sm-12\">\r\n\r\n                                    <form (ngSubmit)=\"ChangePassword()\" name=\"form\" #f=\"ngForm\"  novalidate>\r\n                                        <div class=\"container-fluid\">\r\n                                            <div class=\"row\">\r\n                                       \r\n                                        <div class=\"col-lg-12\">\r\n                                            <label><b>Username</b></label>\r\n                                            <input name=\"uname\" placeholder=\"{{cuser}}\" required=\"\" disabled=\"\" type=\"text\">\r\n                                        </div>\r\n                                        <div class=\"col-lg-4\"> \r\n                                            <label><b>Old Password</b></label>\r\n                                            <input name=\"oldpassword\" placeholder=\"Enter Password\" required=\"\" type=\"password\" name=\"oldpassword\" [(ngModel)]=\"user.oldpassword\" #oldpassword=\"ngModel\">\r\n                                        </div>\r\n                                        <div class=\"col-lg-4\"> \r\n                                            <label><b>New Password</b></label>\r\n                                            <input name=\"newpassword\" placeholder=\"Confirmer Password\" required=\"\" type=\"password\" name=\"newpassword\" [(ngModel)]=\"user.newpassword\" #newpassword=\"ngModel\">\r\n                                        </div>\r\n                                        <div class=\"col-lg-4\"> \r\n                                            <label><b>Confirmer Password</b></label>\r\n                                            <input name=\"confirmpassword\" placeholder=\"Confirmer Password\" required=\"\" type=\"password\" name=\"confirmpassword\" [(ngModel)]=\"user.confirmpassword\" #confirmpassword=\"ngModel\">\r\n                                        </div>\r\n                                        </div>\r\n                                    </div>\r\n                                         <div class=\"container-fluid\"> \r\n                                            <button class=\"btn-success pull-right\">Update</button>\r\n                                        </div>\r\n                                    </form>\r\n\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/myExchange/myExchange.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return myExchangeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx__ = __webpack_require__("../../../../xlsx/xlsx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_xlsx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_takeWhile__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/takeWhile.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__structExports_models__ = __webpack_require__("../../../../../src/app/structExports/models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var myExchangeComponent = (function () {
    function myExchangeComponent(nav, loading, pagerService, myExchangeSer, ExcelService, alertService, router) {
        this.nav = nav;
        this.loading = loading;
        this.pagerService = pagerService;
        this.myExchangeSer = myExchangeSer;
        this.ExcelService = ExcelService;
        this.alertService = alertService;
        this.router = router;
        this.pagerLoans = {};
        this.pagerOffers = {};
        this.alive = true;
        this.user = {};
        this.isActivtedLoanOffers = false;
        this.templateLoans = __WEBPACK_IMPORTED_MODULE_5__structExports_models__["f" /* headerLaons */];
        this.isExistTemplate = false;
        this.cuser = JSON.parse(localStorage.currentuser);
        this.cuser = this.cuser.username;
        this.role = JSON.parse(localStorage.currentuser).role;
    }
    myExchangeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nav.show();
        this.loading.show();
        this.getLoansUser();
        this.getOffersUser();
        setTimeout(function () {
            _this.loading.hide();
        }, 400);
    };
    myExchangeComponent.prototype.onViewOffersLoan = function (idLoan) {
        var _this = this;
        this.loading.show();
        this.myExchangeSer.getOffersByLoan(idLoan).subscribe(function (res) {
            _this.isActivtedLoanOffers = true;
            _this.listOfOffersLoan = res;
            //console.log(this.listOfOffersLoan);
        });
        setTimeout(function () {
            _this.loading.hide();
        }, 300);
    };
    myExchangeComponent.prototype.navigateToTransactionSeller = function (OffId) {
        this.router.navigate(['transaction'], { queryParams: { offerId: OffId } });
        localStorage.setItem('OSB0303', "OSB0313S");
    };
    myExchangeComponent.prototype.navigateToTransaction = function (OffId) {
        this.router.navigate(['transaction'], { queryParams: { offerId: OffId } });
        localStorage.setItem('OSB0303', "OSB0313B");
    };
    myExchangeComponent.prototype.exportToExcel = function (event) {
        this.ExcelService.exportAsExcelFile(this.templateLoans, 'Loans');
    };
    myExchangeComponent.prototype.onFileChange = function (evt) {
        var _this = this;
        var target = (evt.target);
        var reader = new FileReader();
        reader.onload = function (e) {
            var bstr = e.target.result;
            var wb = __WEBPACK_IMPORTED_MODULE_2_xlsx__["read"](bstr, { type: 'binary' });
            var wsname = wb.SheetNames[0];
            var ws = wb.Sheets[wsname];
            _this.arrayOfJsonResult = (__WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].sheet_to_json(ws, { raw: true }));
            _this.isExistTemplate = true;
        };
        reader.readAsBinaryString(target.files[0]);
    };
    myExchangeComponent.prototype.SaveJsonAsResultLoans = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_3_lodash__["isEmpty"](this.arrayOfJsonResult)) {
            this.alertService.clear();
            this.alertService.error('You have to download the template and fill in and upload !');
        }
        else {
            this.loading.show();
            this.alertService.clear();
            this.myExchangeSer.setLoansExel(this.arrayOfJsonResult).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                _this.arrayOfJsonResult = [];
                _this.isExistTemplate = false;
                _this.getLoansUser();
                setTimeout(function () {
                    _this.loading.hide();
                    _this.alertService.success('Your operation has been successfully completed');
                }, 400);
            });
        }
    };
    myExchangeComponent.prototype.setPageLoans = function (page) {
        var _this = this;
        this.loading.show();
        if (page < 1 || page > this.pagerLoans.totalPages) {
            return;
        }
        this.pagerLoans = this.pagerService.getPager(this.loansUser.length, page);
        this.pagedItemsLoans = this.loansUser.slice(this.pagerLoans.startIndex, this.pagerLoans.endIndex + 1);
        setTimeout(function () {
            _this.loading.hide();
        }, 400);
    };
    myExchangeComponent.prototype.setPageOffers = function (page) {
        var _this = this;
        this.loading.show();
        if (page < 1 || page > this.pagerOffers.totalPages) {
            return;
        }
        this.pagerOffers = this.pagerService.getPager(this.OffersUser.length, page);
        this.pagedItemsOffers = this.OffersUser.slice(this.pagerOffers.startIndex, this.pagerOffers.endIndex + 1);
        setTimeout(function () {
            _this.loading.hide();
        }, 400);
    };
    myExchangeComponent.prototype.getLoansUser = function () {
        var _this = this;
        this.myExchangeSer.getMyLoans().takeWhile(function () { return _this.alive; }).subscribe(function (res) {
            _this.loansUser = res;
            _this.setPageLoans(1);
        });
    };
    myExchangeComponent.prototype.getOffersUser = function () {
        var _this = this;
        this.myExchangeSer.getMyOffers().takeWhile(function () { return _this.alive; }).subscribe(function (res) {
            _this.OffersUser = res;
            _this.setPageOffers(1);
            console.log(_this.OffersUser);
        });
    };
    myExchangeComponent.prototype.ChangePassword = function () {
        var _this = this;
        this.myExchangeSer.changePassword(this.user.oldpassword, this.user.newpassword, this.user.confirmpassword).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
            setTimeout(function () {
                _this.alertService.clear();
                _this.alertService.success("Password Changed successfully");
            }, 300);
        }, function (err) {
            _this.alertService.clear();
            _this.alertService.error("Error detected when password Changed");
        });
    };
    myExchangeComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    myExchangeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-myExchange',
            template: __webpack_require__("../../../../../src/app/components/myExchange/myExchange.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/myExchange/myExchange.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_6__structExports_services__["i" /* loadingService */],
            __WEBPACK_IMPORTED_MODULE_6__structExports_services__["l" /* pagerService */], __WEBPACK_IMPORTED_MODULE_6__structExports_services__["k" /* myExchangeService */], __WEBPACK_IMPORTED_MODULE_6__structExports_services__["a" /* ExcelService */], __WEBPACK_IMPORTED_MODULE_6__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], myExchangeComponent);
    return myExchangeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#menu-top-active .active {\r\n    border-bottom: 1px solid #69b943 !important;\r\n    color: #69b943 !important;\r\n    text-decoration: underline !important;\r\n}\r\n\r\n#menu-top-active .nav-item a:hover {\r\n    border-bottom: 1px solid #69b943 !important;\r\n    color: #69b943 !important;\r\n    text-decoration: underline !important;\r\n}\r\n\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"nav.visible\">\r\n        <section class=\"menu-top-background\">\r\n    \r\n            <div id=\"top-bar\" class=\"container-fluid\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-lg-12\">\r\n                        <div class=\"menu-top\" id=\"menu-top-active\">\r\n                            <ul class=\"nav nav-pills float-right\"  id=\"menu-top\">\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['about-us']\">ABOUT US</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['services']\">SERVICES</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['contact-us']\">CONTACT US</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['faq']\">FAQ</a>\r\n                                </li>\r\n    \r\n    \r\n                                <li class=\"nav-item border-left\">\r\n                                    <a class=\"nav-link\">Welcome, {{authServ.getCurrentUser()}}</a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link\" (click)=\"onLogout()\">Logout</a>\r\n                                </li>\r\n                            </ul>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- Navigation -->\r\n            <nav class=\"navbar navbar-expand-lg navbar-dark bg-light\">\r\n                <div class=\"container-fluid\">\r\n                    <a class=\"navbar-brand\"><img src=\"assets/img/logo.png\"></a>\r\n                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n                          <span class=\"navbar-toggler-icon\"></span>\r\n                        </button>\r\n                    <div class=\"collapse navbar-collapse  menu-bottom\" id=\"navbarResponsive\">\r\n                        <ul class=\"navbar-nav ml-auto\"  id=\"menu-bottom\">\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link \">HOME</a>\r\n                            </li>\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['buy-notes']\">BUY NOTES<span class=\"caret\"></span></a>\r\n                            </li>\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['sellnote']\">SELL NOTES<span class=\"caret\"></span></a>\r\n                            </li>\r\n                            <li class=\"nav-item\">\r\n                                <a class=\"nav-link\" [routerLinkActive]=\"['active']\" [routerLink]=\"['my-exchange']\">MY EXCHANGE</a>\r\n                            </li>\r\n                        </ul>\r\n    \r\n                    </div>\r\n                </div>\r\n            </nav>\r\n        </section>\r\n    </div>"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = (function () {
    //username :string = "";
    function NavbarComponent(authServ, router, nav) {
        this.authServ = authServ;
        this.router = router;
        this.nav = nav;
        // let info = JSON.parse(localStorage.getItem('currentuser'));
        // //console.log(info);
        // if(localStorage.getItem('currentuser'))
        // {
        //   let info = JSON.parse(localStorage.getItem('currentuser'));
        //   this.username = info.username;
        // }  
    }
    NavbarComponent.prototype.onLogout = function () {
        this.authServ.logout();
        this.router.navigate(['login']);
    };
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__structExports_services__["e" /* authentificationService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__structExports_services__["b" /* NavbarService */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/payment/payment.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/payment/payment.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\" id=\"payment\">\n\t\t<div class=\"container-fluid\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t\t<section>\n\t\t\t\t\t\t<div class=\"col-lg-12 bg-light\">\n\t\t\t\t\t\t\t<h4 class=\"infoclient\">Pub</h4>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</section>\n\t\t\t\t\t<section style=\"margin-top: 20px;\">\n\t\t\t\t\t\t<div class=\"col-lg-12 doc-space\"></div>\n\t\t\t\t\t</section>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-10\">\n\t\t\t\t\t<section style=\"margin-top: 20px;\">\n\t\t\t\t\t\t<div class=\"container-fluid\">\n\t\t\t\t\t\t\t<div class=\"row page-Payment\">\n\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t\t\t<h3 class=\"text-center titleGreen\">{{modePayment}}</h3>\n\t\t\t\t\t\t\t\t\t<p class=\"text-center su-text-Green\">A Broker Price Opinion is a report on a specific property by a local real estate agent. It typically shows Comparable Sales, Comparable Listings, neighborhood data, estimated repairs, an opinion of value \"As Is\" and \"Repaired\", and photos of the subject property and area. It will be a \"drive by inspection\" without access to the interior</p><br>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-md-12 bg-light border information-client\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-md-6 float-left\">\n\t\t\t\t\t\t\t\t\t\t<p class=\"text-left\">First Name: <span>{{userInfo.firstName}}</span></p>\n\t\t\t\t\t\t\t\t\t\t<p class=\"text-left\">Last Name: <span> {{userInfo.lastName}}</span></p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-md-6 float-left\">\n\t\t\t\t\t\t\t\t\t\t<p class=\"text-left\">Phone: <span>{{userInfo.PhoneNumber}}</span></p>\n\t\t\t\t\t\t\t\t\t\t<p class=\"text-left\">Email: <span>{{userInfo.UserName}}</span></p>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div><br>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t\t\t<h5 class=\"text-center titleGreen\">Please Make Selection Below:</h5>\n\t\t\t\t\t\t\t\t\t<p class=\"text-center\">5-7 business Days Exterior Residential BPO</p><br>\n\t\t\t\t\t\t\t\t\t<p class=\"text-center text-call\">Please call for commercial and multifamily orders for a quote.</p>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div><br>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t\t\t<h4 class=\"text-img-page-detail title bg-light\">CREDIT CARD INFORMATION</h4><br>\n\t\t\t\t\t\t\t\t\t<br>\n                  <form novalidate [formGroup]=\"formPayment\" (ngSubmit)=\"onSubmitPayment()\"  class=\"page-register\">\n\n                      <div class=\"form-row\">\n                          <div class=\"col-lg-12 text-center\">\n                              <h6>Total Due : $<strong >{{amount}}</strong></h6><br><br>\n                          </div>\n                          <div class=\"col-lg-4\">                              \n                              <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': creditType.invalid && (creditType.dirty || creditType.touched),'has-success': creditType.valid && (creditType.dirty || creditType.touched)}\">Credit Card Type:<em>*</em></label> \n                              <select class=\"form-control\" id=\"selected\" formControlName=\"creditType\">\n                                <option value=\"0\">\n                                  Select Card Type\n                                </option>\n                                <option value=\"Mastercard\">\n                                    Mastercard\n                                </option>\n                                <option value=\"Visa\">\n                                    Visa\n                                </option>\n                                <option value=\"American Express\">\n                                    American Express\n                                </option>\n                                <option value=\"Other\">\n                                    Other\n                                </option>\n                              </select>\n                              <div class=\"form-control-feedback\" *ngIf=\"creditType.errors && (creditType.dirty || creditType.touched)\">\n                                  <p class=\"text-danger\" *ngIf=\"creditType.errors.required\">creditType is required </p>\n                                  \n                              </div>\n                      </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\"[ngClass]=\"{'has-danger': card.invalid && (card.dirty || card.touched),'has-success': card.valid && (card.dirty || card.touched)}\">Credit Card Number: <em>*</em></label>\n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"card\">\n                          <div class=\"form-control-feedback\" *ngIf=\"card.errors && (card.dirty || card.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"card.errors.required\">Credit Card Number is required</p>\n                              <p class=\"text-danger\" *ngIf=\"card.errors.pattern\">Credit Card Number is not valid ,exp <i>14************68</i> </p>\n                          </div>\n                        </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': expiration.invalid && (expiration.dirty || expiration.touched),'has-success': expiration.valid && (expiration.dirty || expiration.touched)}\">Credit Card Expiration: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"MM/YY\" type=\"text\" formControlName=\"expiration\">\n                          <div class=\"form-control-feedback\" *ngIf=\"expiration.errors && (expiration.dirty || expiration.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"expiration.errors.required\">Credit Card Expiration is required</p>\n                          </div>\n                        </div>\n                        \n                        \n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': Name.invalid && (Name.dirty || Name.touched),'has-success': Name.valid && (Name.dirty || Name.touched)}\">Name on Credit Card: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"Name\">\n                          <div class=\"form-control-feedback\" *ngIf=\"Name.errors && (Name.dirty || Name.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"Name.errors.required\">Name is required</p>\n                          </div>\n                        </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lastName.invalid && (lastName.dirty || lastName.touched),'has-success': lastName.valid && (lastName.dirty || lastName.touched)}\">Last Name: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"lastName\">\n                          <div class=\"form-control-feedback\" *ngIf=\"lastName.errors && (lastName.dirty || lastName.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"lastName.errors.required\">Last Name is required</p>\n                          </div>\n                        </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\"[ngClass]=\"{'has-danger': address.invalid && (address.dirty || address.touched),'has-success': address.valid && (address.dirty || address.touched)}\">Billing Address: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"address\">\n                          <div class=\"form-control-feedback\" *ngIf=\"address.errors && (address.dirty || address.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"address.errors.required\">Billing Address is required</p>\n                          </div>\n                        </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': city.invalid && (city.dirty || city.touched),'has-success': city.valid && (city.dirty || city.touched)}\">City: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"city\">\n                          <div class=\"form-control-feedback\" *ngIf=\"city.errors && (city.dirty || city.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"city.errors.required\">City is required</p>\n                          </div>\n                        </div>\n                        <div class=\"col-lg-4\">                              \n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': state.invalid && (state.dirty || state.touched),'has-success': state.valid && (state.dirty || state.touched)}\">State:<em>*</em></label> \n                            <select class=\"form-control\" id=\"selected\" formControlName=\"state\">\n                                <option value=\"0\">\n                                    All\n                                  </option>\n                                  <option value=\"ALABAMA\">\n                                    ALABAMA\n                                  </option>\n                                  <option value=\"ALASKA\">\n                                    ALASKA\n                                  </option>\n                                  <option value=\"ARIZONA\">\n                                    ARIZONA\n                                  </option>\n                                  <option value=\"COLORADO\">\n                                    COLORADO\n                                  </option>\n                                  <option value=\"FLORIDA\">\n                                    FLORIDA\n                                  </option>\n                                  <option value=\"NewYork\">\n                                    NEW YORK\n                                  </option>\n                            </select>\n                            <div class=\"form-control-feedback\" *ngIf=\"state.errors && (state.dirty || state.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"state.errors.required\">state is required </p>\n                            </div>\n                          </div>\n                        <div class=\"col-lg-4\">\n                          <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': zip.invalid && (zip.dirty || zip.touched),'has-success': zip.valid && (zip.dirty || zip.touched)}\">Zip: <em>*</em></label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"zip\">\n                          <div class=\"form-control-feedback\" *ngIf=\"zip.errors && (zip.dirty || zip.touched)\">\n                              <p class=\"text-danger\" *ngIf=\"zip.errors.required\">Zip is required</p>\n                             \n                          </div>\n                          <br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-12\">\n                          <h4 class=\"text-img-page-detail title bg-light\">USER INFORMATION</h4>\n                        </div><br>\n                        <br>\n                        <div class=\"col-lg-6 offset-lg-3\">\n                          <label for=\"inputPassword6\">Email:</label>\n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"userEmail\">\n                          <div class=\"form-control-feedback\" *ngIf=\"userEmail.errors && (userEmail.dirty || userEmail.touched)\">\n                            <p class=\"text-danger\" *ngIf=\"userEmail.errors.pattern\">Email is not valid ,exp <i>contact@adelphatech.com</i> </p>\n                        </div><br>\n                        <br>\n                        </div>\n                        <div class=\"col-lg-12\">\n                          <h4 class=\"text-img-page-detail title bg-light\">PROPERTY INFORMATION</h4><br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">FCI Product ID #:</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"FCI\"> \n                          <small>(Product information will autofill) OR Please complete below if Product is not listed on the site.)</small><br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">Borrower Name:</label>\n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"borrowerName\">\n                          <br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">City:</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"propertyCity\">\n                          <br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">Property Address:</label> \n                          <input class=\"form-control\" placeholder=\"\" type=\"text\"formControlName=\"propertyAddress\">\n                          <br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">                              \n                            <label for=\"inputPassword6\">State:</label> \n                            <select class=\"form-control\" id=\"selected\" formControlName=\"propertyState\">\n                                <option value=\"0\">\n                                    All\n                                  </option>\n                                  <option value=\"ALABAMA\">\n                                    ALABAMA\n                                  </option>\n                                  <option value=\"ALASKA\">\n                                    ALASKA\n                                  </option>\n                                  <option value=\"ARIZONA\">\n                                    ARIZONA\n                                  </option>\n                                  <option value=\"COLORADO\">\n                                    COLORADO\n                                  </option>\n                                  <option value=\"FLORIDA\">\n                                    FLORIDA\n                                  </option>\n                                  <option value=\"NewYork\">\n                                    NEW YORK\n                                  </option>\n                            </select>\n                          </div>\n                        <div class=\"col-lg-3\">\n                          <label for=\"inputPassword6\">Zip: </label>\n                          <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"propertyZip\"><br>\n                          <br>\n                        </div>\n                        <div class=\"col-lg-3\">                              \n                            <label for=\"inputPassword6\">Property Type:</label> \n                            <select class=\"form-control\" id=\"selected\" formControlName=\"propertyType\">\n                              <option value=\"Single Family Res\">\n                                Single Family Res\n                              </option>\n                              <option value=\"Two Family Res\">\n                                Two Family Res\n                              </option>\n                              <option value=\"Four Family Res\">\n                                Four Family Res\n                              </option>\n                            </select>\n                        </div>\n                        <div class=\"col-md-3 text-center div-btn-payment\">\n                            <button type=\"submit\"  class=\"btn btn-success btn-lg float-left\" id=\"btn-payment\">Submit</button>\n          \n                            \n                        </div>\n                      </div>\n                      <!-- <pre>{{formPayment.value | json}}</pre>  -->\n                    </form>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</section>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</section><br><br>"

/***/ }),

/***/ "../../../../../src/app/components/payment/payment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_Forms__ = __webpack_require__("../../../Forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaymentComponent = (function () {
    function PaymentComponent(nav, loading, alertService, paymentServ, activatedRoute) {
        this.nav = nav;
        this.loading = loading;
        this.alertService = alertService;
        this.paymentServ = paymentServ;
        this.activatedRoute = activatedRoute;
        this.modePayment = this.activatedRoute.snapshot.queryParamMap.get('TypePayement');
        // alert(this.modePayment);
        if (this.modePayment == "BPO") {
            this.amount = "115.00";
        }
        else if (this.modePayment == "Purchase O&E") {
            this.amount = "115.00";
        }
        else if (this.modePayment == "Due Diligence Report Package") {
            this.amount = "115.00";
        }
        else if (this.modePayment == "Assignment") {
            this.amount = "200.00";
        }
    }
    PaymentComponent.prototype.ngOnInit = function () {
        this.getUserInfo();
        this.onCreateFormControls();
        this.onCreateForm();
        this.nav.show();
    };
    PaymentComponent.prototype.getUserInfo = function () {
        var _this = this;
        this.paymentServ.getUserInfo().subscribe(function (cdata) {
            _this.userInfo = cdata;
            _this.formPayment.controls['userEmail'].setValue(_this.userInfo.UserName);
            _this.formPayment.controls['tell'].setValue(_this.userInfo.PhoneNumber);
        });
    };
    PaymentComponent.prototype.onCreateFormControls = function () {
        this.creditType = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.card = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].pattern("^[0-9]+$")]);
        this.zip = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.Name = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.lastName = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.address = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.city = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.state = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.expiration = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].required);
        this.userEmail = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_Forms__["e" /* Validators */].pattern("[^ @]*@[^ @]*")]);
        this.FCI = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.borrowerName = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.propertyCity = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.propertyAddress = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.propertyState = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.propertyZip = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.propertyType = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
        this.mode = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */](this.modePayment);
        this.tell = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]('');
    };
    PaymentComponent.prototype.onCreateForm = function () {
        this.formPayment = new __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["b" /* FormGroup */]({
            creditType: this.creditType,
            card: this.card,
            zip: this.zip,
            Name: this.Name,
            lastName: this.lastName,
            address: this.address,
            city: this.city,
            state: this.state,
            expiration: this.expiration,
            userEmail: this.userEmail,
            FCI: this.FCI,
            borrowerName: this.borrowerName,
            propertyCity: this.propertyCity,
            propertyAddress: this.propertyAddress,
            propertyState: this.propertyState,
            propertyZip: this.propertyZip,
            propertyType: this.propertyType,
            mode: this.mode,
            tell: this.tell,
        });
    };
    PaymentComponent.prototype.onSubmitPayment = function () {
        var _this = this;
        this.loading.show();
        if (this.formPayment.valid) {
            console.log(this.formPayment.value);
            this.paymentServ.Payment(this.formPayment.value).subscribe(function (res) {
                _this.alertService.clear();
                _this.loading.hide();
                _this.formPayment.reset();
                _this.alertService.success(res);
            });
        }
        else {
            this.validateAllFormFields(this.formPayment);
            setTimeout(function () {
                _this.loading.hide();
                window.scrollTo(0, 0);
                _this.alertService.clear();
                _this.alertService.error('You must complete all required fields');
            }, 300);
        }
    };
    PaymentComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["a" /* FormControl */]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_3__angular_Forms__["b" /* FormGroup */]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    PaymentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-payment',
            template: __webpack_require__("../../../../../src/app/components/payment/payment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/payment/payment.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_2__structExports_services__["i" /* loadingService */],
            __WEBPACK_IMPORTED_MODULE_2__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_2__structExports_services__["m" /* paymentService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], PaymentComponent);
    return PaymentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".has-danger {\r\n    color: red;\r\n    font-style: italic;\r\n}\r\n\r\n.has-success {\r\n    color: green;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\" class=\"page-register-span\">\n    <div class=\"container\">\n\n        <div class=\"row\">\n            <div class=\"col-lg-12\">\n                <h4 class=\"text-img-page-detail title bg-light\">Step 1 : Personal Information</h4><br>\n                <br>\n                <form novalidate [formGroup]=\"formRegister\" (ngSubmit)=\"onSubmitRegister()\" class=\"page-register\">\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': FirstName.invalid && (FirstName.dirty || FirstName.touched),'has-success': FirstName.valid && (FirstName.dirty || FirstName.touched)}\">First Name: <em>*</em></label>\n\n                            <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"FirstName\">\n                            <div class=\"form-control-feedback\" *ngIf=\"FirstName.errors && (FirstName.dirty || FirstName.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"FirstName.errors.required\">First name is required</p>\n                            </div>\n\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lastName.invalid && (lastName.dirty || lastName.touched),'has-success' : lastName.valid && (lastName.dirty || lastName.touched)}\">Last Name: <em>*</em></label>\n\n                            <input type=\"text\" class=\"form-control\" placeholder=\"\" formControlName=\"lastName\">\n                            <div class=\"form-control-feedback\" *ngIf=\"lastName.errors && (lastName.dirty || lastName.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"lastName.errors.required\">Last name is required</p>\n                            </div>\n\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': address.invalid && (address.dirty || address.touched),'has-success' : address.valid && (address.dirty || address.touched)}\">Address: <em>*</em></label> <input class=\"form-control\"\n                                placeholder=\"\" type=\"text\" formControlName=\"address\">\n                            <div class=\"form-control-feedback\" *ngIf=\"address.errors && (address.dirty || address.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"address.errors.required\">Address is required</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': city.invalid && (city.dirty || city.touched),'has-success' : city.valid && (city.dirty || city.touched)}\">City: <em>*</em></label>\n                            <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"city\">\n                            <div class=\"form-control-feedback\" *ngIf=\"city.errors && (city.dirty || city.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"city.errors.required\">city is required</p>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\">Zip:</label> <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"zip\">\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger':stateCheck,'has-success' : !stateCheck}\">State: <em>*</em></label>\n                            <select2 [data]=\"stateData\" class=\"form-control\" required (valueChanged)=\"changedState($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"stateCheck\">\n                                <p class=\"text-danger\">State is required </p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger':Email.invalid && (Email.dirty || Email.touched),'has-success': Email.valid && (Email.dirty || Email.touched)}\">Email: <em>*</em></label>\n                            <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"Email\">\n                            <div class=\"form-control-feedback\" *ngIf=\"Email.errors && (Email.dirty || Email.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"Email.errors.required\">Email is required </p>\n                                <p class=\"text-danger\" *ngIf=\"Email.errors.pattern\">Email is not valid ,exp <i>iraouf@adelphatech.com</i> </p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger':confirmEmail.invalid && (confirmEmail.dirty || \n                            confirmEmail.touched),'has-success': confirmEmail.valid && (confirmEmail.dirty || confirmEmail.touched)}\">Confirm Email: <em>*</em></label>\n                            <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"confirmEmail\">\n\n                            <div class=\"form-control-feedback\" *ngIf=\"confirmEmail.errors && (confirmEmail.dirty || confirmEmail.touched)\">\n                                <p class=\"text-danger\">Email mismatch</p>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': PhoneNumber.invalid && (PhoneNumber.dirty || PhoneNumber.touched),'has-success' : PhoneNumber.valid && (PhoneNumber.dirty || PhoneNumber.touched)}\">PhoneNumber: <em>*</em></label>                            <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"PhoneNumber\">\n                            <div class=\"form-control-feedback\" *ngIf=\"PhoneNumber.errors && (PhoneNumber.dirty || PhoneNumber.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"PhoneNumber.errors.required\"> PhoneNumber is required</p>\n                                <p class=\"text-danger\" *ngIf=\"PhoneNumber.errors.pattern\"> PhoneNumber is not valid</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': confirmPhoneNumber.invalid && (confirmPhoneNumber.dirty || confirmPhoneNumber.touched), 'has-success': confirmPhoneNumber.valid && (confirmPhoneNumber.dirty || confirmPhoneNumber.touched)}\">Confirm PhoneNumber: <em>*</em></label>\n                            <input class=\"form-control\" formControlName=\"confirmPhoneNumber\" placeholder=\"\" type=\"text\">\n                            <div class=\"form-control-feedback\" *ngIf=\"confirmPhoneNumber.errors && (confirmPhoneNumber.dirty || confirmPhoneNumber.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"confirmPhoneNumber.errors\"> PhoneNumber mismatch</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\">Fax:</label> <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"fax\">\n                        </div>\n                        <div class=\"col-lg-3\">\n                            <label for=\"inputPassword6\">Company:</label> <input class=\"form-control\" placeholder=\"\" type=\"text\" formControlName=\"company\">\n                        </div>\n                    </div><br>\n                    <br>\n                    <br>\n                    <h4 class=\"text-img-page-detail title bg-light\">Step 2 : Create User Profile </h4><br>\n                    <br>\n                    <div class=\"form-row\">\n                        <div class=\"col-lg-6\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': Password.invalid && (Password.dirty || Password.touched),'has-success': Password.valid && (Password.dirty && Password.touched)}\">Password: <em>*</em></label>\n                            <input class=\"form-control\" placeholder=\"\" type=\"Password\" formControlName=\"Password\">\n\n                            <div class=\"form-control-feedback\" *ngIf=\"Password.errors && (Password.dirty || Password.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"Password.errors.required\">Password is required</p>\n                                <p class=\"text-danger\" *ngIf=\"Password.errors.minlength\">Min length is 8 characters</p>\n                                <p class=\"text-danger\" *ngIf=\"Password.errors.maxlength\">Max length is 50 characters</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': confirmPassword.invalid && (confirmPassword.dirty || confirmPassword.touched),'has-success': confirmPassword.valid && (confirmPassword.dirty && confirmPassword.touched)}\">Confirm Password: <em>*</em></label>\n                            <input class=\"form-control\" placeholder=\"\" type=\"Password\" formControlName=\"confirmPassword\">\n                            <div class=\"form-control-feedback\" *ngIf=\"confirmPassword.errors && (confirmPassword.dirty || confirmPassword.touched)\">\n                                <p class=\"text-danger\">Password mismatch</p>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{'has-danger':broker.invalid && (broker.dirty || broker.touched) }\">Are you a Broker or a Principal? <em>*</em></h6>\n                        </div>\n                        <div class=\"col-sm-6\">\n                            <div class=\"col-lg-6 checkbox float-left\">\n                                <label><input type=\"radio\" value=\"false\" formControlName=\"broker\" name=\"broker\" (change)=\"IsPrincipal()\" checked/> Principal</label>\n                            </div>\n                            <div class=\"col-lg-6 checkbox float-left\">\n                                <label><input type=\"radio\" value=\"true\" formControlName=\"broker\" name=\"broker\" (change)=\"IsBroker()\"> Broker</label>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-6\">\n                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': licence.invalid && (licence.dirty || licence.touched),'has-success': licence.valid && (licence.dirty && licence.touched)}\">Please enter your License Number: <em>*</em></label>\n                            <input type=\"text\" class=\"form-control\" formControlName=\"licence\">\n                            <div class=\"form-control-feedback\" *ngIf=\"licence.errors && (licence.dirty || licence.touched)\">\n                                <p class=\"text-danger\" *ngIf=\"licence.errors.required\">Are you broker !?,if yes Number license is required</p>\n                            </div>\n                        </div>\n                        <div class=\"col-lg-12\">\n                            <p class=\"info\">If you are a Broker representing a principal on a particular transaction, please be prepared to have your client sign a Non-Disclosure Agreement and provide Proof of Funds before your indicative offer is forwarded to the Seller.</p>\n                        </div>\n                    </div><br>\n                    <br>\n                    <br>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{'has-danger': hearAboutUsCheck}\">How did you hear about us? <em>*</em></h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9 About-us-is-required\">\n                            <select2 [data]=\"hearAboutUsData\" [options]=\"options\" required (valueChanged)=\"changedhearAboutUs($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"hearAboutUsCheck\">\n                                <p class=\"text-danger\">About us is required</p>\n                            </div>\n                        </div>\n\n                        <div class=\"col-lg-9 float-left\">\n                            <input class=\"form-control\" placeholder=\"Other\" type=\"text\" formControlName=\"otherhearAboutUs\">\n                        </div>\n                    </div>\n\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\">Mailing</h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n                            <select2 [data]=\"millingData\" [options]=\"options\" required (valueChanged)=\"changedMilling($event)\"> </select2>\n\n                        </div>\n\n                    </div>\n\n                    <br>\n                    <br>\n                    <h4 class=\"text-img-page-detail title bg-light\">Step 3 : Preferences </h4><br>\n                    <br>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{ 'has-danger':propertyTypesCheck}\">What Property Types do you Buy or Sell? <em>*</em></h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n\n                            <select2 [data]=\"propertyTypesData\" [options]=\"options\" required (valueChanged)=\"changedPropertyTypes($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"propertyTypesCheck\">\n                                <p class=\"text-danger\">Property types is required</p>\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{'has-danger':CurrentCapitalAvailableCheck}\">Current Capital Available: <em>*</em></h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n\n                            <select2 [data]=\"CurrentCapitalAvailableData\" [options]=\"options\" required (valueChanged)=\"changedCurrentCapitalAvailable($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"CurrentCapitalAvailableCheck\">\n                                <p class=\"text-danger\">Current Capital Available is required</p>\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{'has-danger':idealPropertyValueCheck}\">Ideal Property Value? <em>*</em></h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n\n                            <select2 [data]=\"idealPropertyValueData\" [options]=\"options\" required (valueChanged)=\"changedIdealPropertyValue($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"idealPropertyValueCheck\">\n                                <p class=\"text-danger\">Ideal Property Value is required</p>\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\" [ngClass]=\"{'has-danger' : LoanTypesCheck}\">Loan Types <em>*</em></h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n\n                            <select2 [data]=\"LoanTypesData\" [options]=\"options\" required (valueChanged)=\"changedLoanTypes($event)\"> </select2>\n                            <div class=\"form-control-feedback\" *ngIf=\"LoanTypesCheck\">\n                                <p class=\"text-danger\"> Loan Type is required</p>\n                            </div>\n                        </div>\n\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\">How do you purchase your notes?</h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n                            <select2 [data]=\"purchaseYourNoteData\" [options]=\"options\" required (valueChanged)=\"changedpurchaseYourNote($event)\"> </select2>\n\n                        </div>\n\n                    </div>\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\">What Lien Position are you looking to buy or sell?</h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n                            <select2 [data]=\"LienPositionData\" [options]=\"options\" required (valueChanged)=\"changedLienPosition($event)\"> </select2>\n\n                        </div>\n\n                    </div>\n\n                    <div class=\"form-row align-items-center\">\n                        <div class=\"col-lg-12\">\n                            <h6 class=\"title-step\">Which state(s) are you looking to buy or sell in?</h6>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-9\">\n                            <select2 [data]=\"statesData\" [options]=\"options\" required (valueChanged)=\"changedStates($event)\"> </select2>\n\n                        </div>\n\n                    </div><br>\n\n                    <div class=\"row\">\n                        <div class=\"col-lg-2 offset-lg-5\"><button type=\"submit\" class=\"btn btn-success btn-lg btn-block\">Submit</button></div>\n                    </div>\n                    <!-- <pre>{{formRegister.value | json }}</pre> -->\n                </form>\n            </div>\n        </div>\n    </div>\n\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_Forms__ = __webpack_require__("../../../Forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__structExports_models__ = __webpack_require__("../../../../../src/app/structExports/models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterComponent = (function () {
    function RegisterComponent(nav, loading, alertService, registerServ, router) {
        this.nav = nav;
        this.loading = loading;
        this.alertService = alertService;
        this.registerServ = registerServ;
        this.router = router;
        this.hearAboutUsData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["e" /* aboutUsData */];
        this.millingData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["h" /* millingData */];
        this.propertyTypesData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["i" /* propertyTypesData */];
        this.CurrentCapitalAvailableData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["b" /* CurrentCapitalAvailableData */];
        this.idealPropertyValueData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["g" /* idealPropertyValueData */];
        this.LoanTypesData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["d" /* LoanTypesData */];
        this.purchaseYourNoteData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["j" /* purchaseYourNoteData */];
        this.LienPositionData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["c" /* LienPositionData */];
        this.statesData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["k" /* statesData */];
        this.stateData = __WEBPACK_IMPORTED_MODULE_4__structExports_models__["k" /* statesData */];
        this.isBroker = false;
        this.options = {
            multiple: true,
        };
        this.hearAboutUsCheck = false;
        this.propertyTypesCheck = false;
        this.CurrentCapitalAvailableCheck = false;
        this.idealPropertyValueCheck = false;
        this.LoanTypesCheck = false;
        this.stateCheck = false;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.onCreateFormControls();
        this.onCreateForm();
        this.formRegister.controls['purchaseYourNote'].setValue({
            "InBulk": "false",
            "Single": "false"
        });
        this.formRegister.controls['states'].setValue([]);
    };
    RegisterComponent.prototype.onCreateFormControls = function () {
        this.FirstName = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.lastName = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.address = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.city = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('  ', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.zip = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.state = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('DefaultItem', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.Email = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].pattern("[^ @]*@[^ @]*")]);
        this.confirmEmail = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, this.emailConfirming]);
        this.PhoneNumber = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].pattern("^[0-9]+$")]);
        this.confirmPhoneNumber = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, this.PhoneNumberConfirming]);
        this.fax = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.company = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.Password = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].maxLength(50), __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].minLength(8), __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required]);
        this.confirmPassword = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required, this.PasswordConfirming]);
        this.broker = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.licence = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]({ value: "", disabled: true }, __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.hearAboutUs = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.otherhearAboutUs = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.milling = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.propertyTypes = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.capitalAvailable = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.idealPropertyValue = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.LoanTypes = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
        this.purchaseYourNote = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.LienPosition = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.states = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
        this.state = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]('');
    };
    RegisterComponent.prototype.onCreateForm = function () {
        this.formRegister = new __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["b" /* FormGroup */]({
            FirstName: this.FirstName,
            lastName: this.lastName,
            address: this.address,
            city: this.city,
            zip: this.zip,
            state: this.state,
            Email: this.Email,
            confirmEmail: this.confirmEmail,
            PhoneNumber: this.PhoneNumber,
            confirmPhoneNumber: this.confirmPhoneNumber,
            fax: this.fax,
            company: this.company,
            Password: this.Password,
            confirmPassword: this.confirmPassword,
            broker: this.broker,
            licence: this.licence,
            hearAboutUs: this.hearAboutUs,
            otherhearAboutUs: this.otherhearAboutUs,
            milling: this.milling,
            propertyTypes: this.propertyTypes,
            capitalAvailable: this.capitalAvailable,
            idealPropertyValue: this.idealPropertyValue,
            LoanTypes: this.LoanTypes,
            purchaseYourNote: this.purchaseYourNote,
            LienPosition: this.LienPosition,
            states: this.states
        });
    };
    RegisterComponent.prototype.onSubmitRegister = function () {
        var _this = this;
        this.loading.show();
        if (this.formRegister.valid && this.formRegister.controls['state'].value != "") {
            this.registerServ.register(this.formRegister.value).subscribe(function (res) {
                _this.alertService.clear();
                _this.loading.hide();
                var mail = _this.Email.value;
                _this.formRegister.reset();
                _this.alertService.success('Your registration has been successfully completed ,Enter your password and enjoy', true);
                _this.router.navigate(['login'], { queryParams: { email: mail } });
            }, function (error) {
                _this.loading.hide();
                _this.alertService.clear();
                _this.alertService.error('Email : ' + _this.formRegister.controls['Email'].value + ' is already taken ');
                window.scrollTo(0, 0);
            });
        }
        else {
            this.validateAllFormFields(this.formRegister);
            setTimeout(function () {
                _this.loading.hide();
                window.scrollTo(0, 0);
                _this.alertService.clear();
                _this.alertService.error('You must complete all required fields');
            }, 300);
        }
    };
    RegisterComponent.prototype.changedMilling = function (event) {
        var objectMilling = this.onChangeResult(event);
        this.formRegister.controls['milling'].setValue(objectMilling);
    };
    RegisterComponent.prototype.changedState = function (event) {
        var objectstate = event.data[0].id;
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objectstate)) {
            this.stateCheck = true;
        }
        else {
            //  alert(objectstate);
            this.stateCheck = false;
            this.formRegister.controls['state'].setValue(objectstate);
        }
    };
    RegisterComponent.prototype.changedhearAboutUs = function (event) {
        var objecthearAboutUs = this.onChangeResult(event);
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objecthearAboutUs)) {
            this.hearAboutUsCheck = true;
        }
        else {
            this.hearAboutUsCheck = false;
            this.formRegister.controls['hearAboutUs'].setValue(objecthearAboutUs);
        }
    };
    RegisterComponent.prototype.changedPropertyTypes = function (event) {
        var objectpropertyTypes = this.onChangeResult(event);
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objectpropertyTypes)) {
            this.propertyTypesCheck = true;
        }
        else {
            this.propertyTypesCheck = false;
            this.formRegister.controls['propertyTypes'].setValue(objectpropertyTypes);
        }
    };
    RegisterComponent.prototype.changedCurrentCapitalAvailable = function (event) {
        var objectCurrentCapitalAvailable = this.onChangeResult(event);
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objectCurrentCapitalAvailable)) {
            this.CurrentCapitalAvailableCheck = true;
        }
        else {
            this.CurrentCapitalAvailableCheck = false;
            this.formRegister.controls['capitalAvailable'].setValue(objectCurrentCapitalAvailable);
        }
    };
    RegisterComponent.prototype.changedIdealPropertyValue = function (event) {
        var objectidealPropertyValue = this.onChangeResult(event);
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objectidealPropertyValue)) {
            this.idealPropertyValueCheck = true;
        }
        else {
            this.idealPropertyValueCheck = false;
            this.formRegister.controls['idealPropertyValue'].setValue(objectidealPropertyValue);
        }
    };
    RegisterComponent.prototype.changedLoanTypes = function (event) {
        var objectLoanTypes = this.onChangeResult(event);
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](objectLoanTypes)) {
            this.LoanTypesCheck = true;
        }
        else {
            this.LoanTypesCheck = false;
            this.formRegister.controls['LoanTypes'].setValue(objectLoanTypes);
        }
    };
    RegisterComponent.prototype.changedpurchaseYourNote = function (event) {
        var objectpurchaseYourNote = this.onChangeResult(event);
        this.formRegister.controls['purchaseYourNote'].setValue(objectpurchaseYourNote);
    };
    RegisterComponent.prototype.changedLienPosition = function (event) {
        var objectLienPosition = this.onChangeResult(event);
        this.formRegister.controls['LienPosition'].setValue(objectLienPosition);
    };
    RegisterComponent.prototype.changedStates = function (event) {
        if (event.data) {
            var itemObj = [];
            for (var _i = 0, _a = event.data; _i < _a.length; _i++) {
                var item = _a[_i];
                itemObj.push(item.id);
            }
        }
        var objectstates = itemObj;
        this.formRegister.controls['states'].setValue(objectstates);
    };
    RegisterComponent.prototype.IsBroker = function () {
        this.isBroker = true;
        this.licence.enable();
        this.licence.setValidators(__WEBPACK_IMPORTED_MODULE_2__angular_Forms__["e" /* Validators */].required);
    };
    RegisterComponent.prototype.IsPrincipal = function () {
        this.isBroker = false;
        this.licence.clearValidators();
        this.licence.reset();
        this.licence.disable();
    };
    RegisterComponent.prototype.onChangeResult = function (event) {
        if (event.data) {
            var itemObj = new Object();
            for (var _i = 0, _a = event.data; _i < _a.length; _i++) {
                var item = _a[_i];
                var valueName = item.id;
                itemObj[valueName] = "true";
            }
        }
        return itemObj;
    };
    RegisterComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["a" /* FormControl */]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_2__angular_Forms__["b" /* FormGroup */]) {
                _this.validateAllFormFields(control);
            }
        });
        this.hearAboutUsCheck = this.checkValdateSelect2("hearAboutUs");
        this.LoanTypesCheck = this.checkValdateSelect2("propertyTypes");
        this.CurrentCapitalAvailableCheck = this.checkValdateSelect2("capitalAvailable");
        this.idealPropertyValueCheck = this.checkValdateSelect2("idealPropertyValue");
        this.LoanTypesCheck = this.checkValdateSelect2("LoanTypes");
        this.stateCheck = this.checkValdateSelect2("state");
        this.propertyTypesCheck = this.checkValdateSelect2("propertyTypes");
    };
    RegisterComponent.prototype.checkValdateSelect2 = function (nameOfcontrol) {
        if (__WEBPACK_IMPORTED_MODULE_5_lodash__["isEmpty"](this.formRegister.controls[nameOfcontrol].value) || this.formRegister.controls[nameOfcontrol].value == "DefaultItem") {
            return true;
        }
    };
    RegisterComponent.prototype.emailConfirming = function (c) {
        if (!c.parent || !c)
            return;
        var pwd = c.parent.get('Email');
        var cpwd = c.parent.get('confirmEmail');
        if (!pwd || !cpwd)
            return;
        if (pwd.value !== cpwd.value) {
            return { invalid: true };
        }
    };
    RegisterComponent.prototype.PhoneNumberConfirming = function (c) {
        if (!c.parent || !c)
            return;
        var pwd = c.parent.get('PhoneNumber');
        var cpwd = c.parent.get('confirmPhoneNumber');
        if (!pwd || !cpwd)
            return;
        if (pwd.value !== cpwd.value) {
            return { invalid: true };
        }
    };
    RegisterComponent.prototype.PasswordConfirming = function (c) {
        if (!c.parent || !c)
            return;
        var pwd = c.parent.get('Password');
        var cpwd = c.parent.get('confirmPassword');
        if (!pwd || !cpwd)
            return;
        if (pwd.value !== cpwd.value) {
            return { invalid: true };
        }
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__structExports_services__["n" /* registerService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_3__structExports_services__["i" /* loadingService */],
            __WEBPACK_IMPORTED_MODULE_3__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_3__structExports_services__["n" /* registerService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/resetPassword/resetPassword.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/resetPassword/resetPassword.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n\n  <section>\n      <div class=\"container icones-container\">\n        <div class=\"row icones\">\n            <div class=\"col-sm-12 logo-zone\"><img src=\"assets/img/logo.png\"></div>\n        </div>\n      </div>\n      <div class=\"container page-login-container\">\n        <div class=\"row\">\n          <div class=\"col-md-8 offset-md-2\">\n            <div class=\"card card-signup\">\n                              <h2 class=\"card-title text-center card-title-login\" style=\"color:#818181\">Forgot Your <span style=\"color: #acd589\">Password?</span></h2>\n                              <h6 class=\"card-title text-center\">Enter your email address below and we'll get you back on track.</h6>\n              <div class=\"row\">\n                <div class=\"col-md-10 offset-md-1 inputs\">\n                  <form class=\"form\" [formGroup]=\"resetpassword\" (ngSubmit)=\"onResetPassword()\">\n                    <div class=\"card-content\">\n                      <div class=\"form-group\">\n                                              <label for=\"formGroupExampleInput\">Email</label> \n                                              <input class=\"form-control\" formControlName=\"Email\" id=\"formGroupExampleInput\" placeholder=\"Email\" type=\"text\">\n  \n                                              <label for=\"formGroupExampleInput\">Password</label> \n                                              <input class=\"form-control\" formControlName=\"password\" id=\"formGroupExampleInput\" placeholder=\"Password\" type=\"Password\">\n  \n                                              <label for=\"formGroupExampleInput\">Confirm Password</label> \n                                              <input class=\"form-control\" formControlName=\"confirmpassword\" id=\"formGroupExampleInput\" placeholder=\"Confirm Password\" type=\"Password\">\n                      </div>\n                      <div class=\"form-group col-sm-4 offset-4 text-center\">\n                                              <button type=\"submit\" class=\"btn btn-success btn-lg\">Reset</button>\n                                              <!-- <a href=\"./Login-Page.html\" class=\"Back_to_sign_in\">Back to sign in</a> -->\n                                          </div>\n                    </div>\n                    <!-- <pre>{{ resetpassword.value | json}} </pre> -->\n                  </form>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </section><br><br>"

/***/ }),

/***/ "../../../../../src/app/components/resetPassword/resetPassword.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(_http, route, _alert, router) {
        this._http = _http;
        this.route = route;
        this._alert = _alert;
        this.router = router;
        this.re = "+exch+";
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        var re = "+exch+";
        console.log(this.route.snapshot.params['code']);
        this.replacertoken = this.route.snapshot.params['code'];
        this.replacertoken = this.replacertoken.replace(re, "/");
        console.log(this.replacertoken.replace(re, "/"));
        this.yyyy = this.replacertoken.replace(re, "/");
        this.emailforgot = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* Validators */].required);
        this.password = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* Validators */].required);
        this.confirmpassword = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* Validators */].required);
        this.resetpassword = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            Email: this.emailforgot,
            password: this.password,
            confirmpassword: this.confirmpassword
        });
    };
    ResetPasswordComponent.prototype.onResetPassword = function () {
        // console.log(this.emailforgot.value);
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Email=" + this.emailforgot.value + "&Password=" + this.password.value + "&ConfirmPassword=" + this.confirmpassword.value + "&Code=" + this.replacertoken.replace("+exch+", "/");
        this._http.post(__WEBPACK_IMPORTED_MODULE_5__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Account/ResetPassword", body, options).subscribe(function (res) {
            _this._alert.success("Password Changed Successfully", true);
            _this.router.navigate(['login']);
            console.log(res);
        });
    };
    ResetPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-reset-password',
            template: __webpack_require__("../../../../../src/app/components/resetPassword/resetPassword.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/resetPassword/resetPassword.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_3__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sellnote/sellnote.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".has-danger {\r\n    color: red;\r\n    font-style: italic;\r\n}\r\n\r\n.has-success {\r\n    color: green;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sellnote/sellnote.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"sell-notes\" style=\"margin-top: 20px;\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-lg-2\">\n                <section>\n                    <div class=\"col-lg-12 bg-light\">\n                        <h4 class=\"infoclient\">Pub</h4>\n                    </div>\n                </section>\n                <section style=\"margin-top: 20px;\">\n                    <div class=\"col-lg-12 doc-space\"></div>\n                </section>\n            </div>\n            <div class=\"col-lg-10\">\n                <section style=\"margin-top: 20px;\">\n                    <div class=\"container-fluid\">\n                        <div class=\"tab\">\n                            <button class=\"tablinks active\" onclick=\"openCity(event, 'London')\">Step 1</button>\n                            <button class=\"tablinks\" onclick=\"openCity(event, 'Paris')\">Step 2</button>\n                            <button class=\"tablinks\" onclick=\"openCity(event, 'Tokyo')\">Step 3</button>\n                            <button class=\"tablinks\" onclick=\"openCity(event, 'Canada')\">Step 4</button>\n                        </div>\n                        <form class=\"page-register\" [formGroup]=\"sellNoteForm\" (ngSubmit)=\"onSubmitSellNote()\">\n                            <div class=\"tabcontent\" style=\"display:block\" id=\"London\">\n                                <div class=\"row\">\n                                    <div class=\"col-lg-12 bg-light\">\n                                        <h4 class=\"infoclient\">LOAN INFORMATION</h4>\n                                        <!-- <p class=\"pull-right requi\">Required Fields*</p> -->\n                                    </div><br>\n                                    <br>\n                                    <div class=\"col-lg-12\" id=\"sell-note-page1\" name=\"sell-note-page1\" style=\"margin-top: 20px;\">\n                                        <div class=\"form-row\">\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"exampleFormControlSelect1\" [ngClass]=\"{'has-danger': loanType.invalid && (loanType.dirty || loanType.touched),'has-success': loanType.valid && (loanType.dirty || loanType.touched)}\">Loan Type: <em>*</em></label>\n                                                <!--  (change)=\"modalSelectAddValidation(['drawPeriodStartDate','repaymentPeriodStartDate'],LoanType.options[LoanType.value].text,'selected')\" -->\n                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"loanType\" id=\"selectbox\">\n                                             <option value=null disabled selected>--Select--</option>\n                                             <option  *ngFor=\" let type of _LoanTypes\" [ngClass]=\"{'#consumergoods': type.typeName=='Line Of Credit'}\"  [value]=\"type.typeId\">{{type.typeName}}</option>\n                                         </select>\n\n                                                <div class=\"form-control-feedback\" *ngIf=\"loanType.errors && (loanType.dirty || loanType.touched)\">\n                                                    <p class=\"text-danger\" *ngIf=\"loanType.errors.required\">Loan Type is required</p>\n                                                </div>\n\n                                            </div>\n                                            <!---Modal-LoanTypelineOfCredit-->\n                                            <section>\n                                                <div class=\"modal fade\" data-backdrop=\"static\" data-target=\"#consumergoods\" id=\"consumergoods\">\n                                                    <div class=\"modal-dialog modal-lg\">\n                                                        <div class=\"modal-content\">\n                                                            <div class=\"modal-header\">\n                                                                <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"resetSelect('loanType')\">&times;</button>\n                                                                <h4 class=\"modal-title\"></h4>\n                                                            </div>\n                                                            <div class=\"modal-body\">\n                                                                <div class=\"container-fluid\">\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">Draw Period Start Date</label> <input formControlName=\"paidToDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\"\n                                                                                value=\"2011-08-19\">\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">Repayment Period Start Date</label> <input formControlName=\"repaymentPeriodStartDate\" class=\"form-control\" id=\"example-date-input\"\n                                                                                type=\"date\" value=\"2011-08-19\">\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"modal-footer\">\n                                                                <button class=\"btn btn-success\" data-dismiss=\"modal\" type=\"button\">Save</button>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!---Fin-Modal-LoanTypelineOfCredit-->\n\n                                            <section>\n                                                <div class=\"modal fade\" data-backdrop=\"static\" data-target=\"#consumergoods2\" id=\"consumergoods2\">\n                                                    <div class=\"modal-dialog modal-lg\">\n                                                        <div class=\"modal-content\">\n                                                            <div class=\"modal-header\">\n                                                                <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"resetSelect('rateType')\">&times;</button>\n                                                                <h4 class=\"modal-title\"></h4>\n                                                            </div>\n                                                            <div class=\"modal-body\">\n                                                                <div class=\"container-fluid\">\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">Next Adjustment</label> <input formControlName=\"nextAdjustement\" class=\"form-control\" id=\"example-date-input\" type=\"date\"\n                                                                                value=\"2011-08-19\">\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">Adj. Payment Change Date</label> <input formControlName=\"adjPayementChangeDate\" class=\"form-control\" id=\"example-date-input\"\n                                                                                type=\"date\" value=\"2011-08-19\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Adjusment Frecuency:</label> <input formControlName=\"adjustementFrequency\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Floor:</label> <input formControlName=\"floor\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Index Name</label> <input formControlName=\"indexName\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Margin: %</label> <input formControlName=\"margin\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-12\">\n                                                                            <label for=\"inputPassword6\">Ceiling:</label> <input formControlName=\"ceiling\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"modal-footer\">\n                                                                <button class=\"btn btn-success\" data-dismiss=\"modal\" type=\"button\">Save</button>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!---Fin-Modal-myModal-select-arm-->\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"exampleFormControlSelect1\" [ngClass]=\"{'has-danger': amortizationType.invalid && (amortizationType.dirty || amortizationType.touched),'has-success': amortizationType.valid && (amortizationType.dirty || amortizationType.touched)}\">Amortization Type: <em>*</em></label>\n\n                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"amortizationType\">\n                                           <option value=null disabled selected>--Select--</option>\n                                           <option  *ngFor=\" let type of _amortizationType\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                         </select>\n                                                <div class=\"form-control-feedback\" *ngIf=\"amortizationType.errors && (amortizationType.dirty || amortizationType.touched)\">\n                                                    <p class=\"text-danger\" *ngIf=\"amortizationType.errors.required\">Amortization Type is required</p>\n                                                </div>\n                                            </div>\n\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"exampleFormControlSelect1\" [ngClass]=\"{'has-danger': rateType.invalid && (rateType.dirty || rateType.touched),'has-success': rateType.valid && (rateType.dirty || rateType.touched)}\"> Rate Type: <em>*</em></label>\n\n                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"rateType\" id=\"selectbox2\">\n                                                <option value=null disabled selected>--Select--</option>\n                                                <option  *ngFor=\" let type of _rateType\" [ngClass]=\"{'#consumergoods2': type.typeName=='ARM'}\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                              </select>\n                                                <div class=\"form-control-feedback\" *ngIf=\"rateType.errors && (rateType.dirty || rateType.touched)\">\n                                                    <p class=\"text-danger\" *ngIf=\"rateType.errors.required\">Amortization Type is required</p>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"exampleFormControlSelect1\" [ngClass]=\"{'has-danger': loanStatus.invalid && (loanStatus.dirty || loanStatus.touched),'has-success': loanStatus.valid && (loanStatus.dirty || loanStatus.touched)}\">status Loan: <em>*</em></label>\n\n                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"loanStatus\">\n                                           <option value=null disabled selected>--Select--</option>\n                                           <option  *ngFor=\"let type of _loanStatus\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                         </select>\n                                                <div class=\"form-control-feedback\" *ngIf=\"loanStatus.errors && (loanStatus.dirty || loanStatus.touched)\">\n                                                    <p class=\"text-danger\" *ngIf=\"loanStatus.errors.required\">Loan Status is required</p>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-12\" style=\"margin-top: 30px;\">\n                                                <div class=\"form-group row\">\n                                                    <div class=\"col-lg-2 checkbox float-left\">\n                                                        <label><input formControlName=\"prePayPenalty\" type=\"checkbox\" value=\"true\"> Pre Pay Penalty</label>\n                                                    </div>\n                                                    <div class=\"col-lg-2 checkbox float-left\">\n                                                        <label><input formControlName=\"ballonPayement\" type=\"checkbox\" value=\"true\"> Balloon Payment</label>\n                                                    </div>\n                                                    <div class=\"col-lg-2 checkbox float-left\">\n                                                        <label><input formControlName=\"registerswMers\" type=\"checkbox\" value=\"true\"> Registered w/MERS</label>\n                                                    </div>\n                                                    <div class=\"col-lg-3 checkbox float-left\">\n                                                        <label><input formControlName=\"onForbearancePlan\" type=\"checkbox\" value=\"true\"> On Forbearance Plan</label>\n                                                    </div>\n                                                    <div class=\"col-lg-3 checkbox float-left\">\n                                                        <label><input formControlName=\"loanTermsModified\" type=\"checkbox\" value=\"true\"> Loan Terms Modified</label>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-12\">\n                                                <div class=\"form-group row\">\n                                                    <div class=\"col-lg-6 checkbox float-left\">\n                                                        <label>In Foreclosure <em>*</em> </label><input formControlName=\"foreclosureStatus\" data-target=\"#myModal-yes-In-Foreclosure\" data-toggle=\"modal\" (click)=\"ModalAddValidation('Foreclosure',['dateOpened']);changeDRadioValue('foreclosureStatus','Foreclosure')\"\n                                                            [checked]=\"Foreclosure\" type=\"radio\" value=\"true\"> Yes <input formControlName=\"foreclosureStatus\" (click)=\"resetRadio('Foreclosure',['dateOpened']);\" type=\"radio\" value=\"false\" [checked]=\"!Foreclosure\">                                                        No\n\n                                                    </div>\n                                                    <div class=\"col-lg-6 checkbox float-left\">\n                                                        <label>In Bankruptcy <em>*</em> </label><input formControlName=\"bankruptcyStatus\" data-target=\"#myModal-yes-In-Bankruptcy\" data-toggle=\"modal\" (click)=\"ModalAddValidation('Bankruptcy',['bkFillingDate','bkChapterID']);changeDRadioValue('bankruptcyStatus','Bankruptcy')\"\n                                                            [checked]=\"Bankruptcy\" id=\"gridRadios1\" type=\"radio\" value=\"true\"> Yes <input formControlName=\"bankruptcyStatus\" (click)=\"resetRadio('Bankruptcy',['bkFillingDate','bkChapterID']);\" type=\"radio\" value=\"false\"\n                                                            [checked]=\"!Bankruptcy\"> No\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!-- ********************************************************************Modal-3******************************************************************************* -->\n                                            <section>\n                                                <div formGroupName=\"foreclosure\" class=\"modal fade\" data-backdrop=\"static\" id=\"myModal-yes-In-Foreclosure\" role=\"dialog\">\n                                                    <div class=\"modal-dialog modal-lg\">\n                                                        <div class=\"modal-content\">\n                                                            <div class=\"modal-header\">\n                                                                <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"ModalAddValidation('Foreclosure',['dateOpened'],'foreclosure');changeDRadioValue('foreclosureStatus','Foreclosure')\">&times;</button>\n                                                                <h4 class=\"modal-title\">FORECLOSURE INFORMATION</h4>\n                                                            </div>\n                                                            <div class=\"modal-body\">\n                                                                <div class=\"container-fluid\">\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Foreclosure & Attorney Fees Due: $</label> <input formControlName=\"attorneyFeesDue\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Name:</label> <input formControlName=\"name\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Address:</label> <input formControlName=\"address\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label for=\"inputPassword6\">State:</label>\n                                                                            <div class=\"dropdown\">\n                                                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"stateID\">\n                                                <option value=null disabled selected>--Select--</option>\n                                                <option  *ngFor=\" let type of _States\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                              </select>\n                                                                            </div>\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">City:</label> <input formControlName=\"city\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">Scheduled Sale Date $</label> <input formControlName=\"scheduledSaleDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\"\n                                                                                value=\"2011-08-19\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Zip:</label> <input formControlName=\"zip\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Phone:</label> <input formControlName=\"phone\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Email:</label> <input formControlName=\"email\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"\">\n                                                                            <div class=\"form-control-feedback\" *ngIf=\"email.errors?.email && (email.dirty || email.touched)\">\n                                                                                <p class=\"text-danger\">Email not valid</p>\n                                                                            </div>\n\n\n                                                                        </div>\n                                                                        <div class=\"col-lg-3\">\n                                                                            <label [ngClass]=\"{'has-danger': dateOpened.invalid && (dateOpened.dirty || dateOpened.touched),'has-success': dateOpened.valid && (dateOpened.dirty || dateOpened.touched)}\">Opened Date: <em>*</em><input formControlName=\"dateOpened\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"2011-08-19\"></label>\n                                                                        </div>\n                                                                    </div>\n\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"modal-footer\">\n                                                                <button class=\"btn btn-success\" data-dismiss=\"modal\" type=\"button\" style=\"display:none\" #closeforcMoule>Save</button> <button class=\"btn btn-success\" (click)=\"validateFormGroup('foreclosure',['email','dateOpened'],'closeforcMoule','Foreclosure')\"\n                                                                    type=\"button\">Save</button>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!-- ********************************************************************fin-Modal-3******************************************************************************* -->\n                                            <!-- ********************************************************************Modal-4******************************************************************************* -->\n                                            <section>\n                                                <div formGroupName=\"bankruptcy\" class=\"modal fade\" data-backdrop=\"static\" id=\"myModal-yes-In-Bankruptcy\" role=\"dialog\">\n                                                    <div class=\"modal-dialog modal-lg\">\n                                                        <div class=\"modal-content\">\n                                                            <div class=\"modal-header\">\n                                                                <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"ModalAddValidation('Bankruptcy',['bkFillingDate','bkChapterID'],'bankruptcy');changeDRadioValue('bankruptcyStatus','Bankruptcy')\">&times;</button>\n                                                                <h4 class=\"modal-title\">BANKRUPTCY</h4>\n                                                            </div>\n                                                            <div class=\"modal-body\">\n                                                                <div class=\"container-fluid\">\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\" [ngClass]=\"{'has-danger': bkFillingDate.invalid && (bkFillingDate.dirty || bkFillingDate.touched),'has-success': bkFillingDate.valid && (bkFillingDate.dirty || bkFillingDate.touched)}\">BK Filing Date: <em>*</em></label>                                                                            <input formControlName=\"bkFillingDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"2011-08-19\">\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">BK Discharge Date: </label> <input formControlName=\"bkDisChargeDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\"\n                                                                                value=\"2011-08-19\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': bkChapterID.invalid && (bkChapterID.dirty || bkChapterID.touched),'has-success': bkChapterID.valid && (bkChapterID.dirty || bkChapterID.touched)}\">BK Chapter: <em>*</em></label>\n                                                                            <div class=\"dropdown\">\n                                                                                <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"bkChapterID\">\n                                                <option value=null disabled selected>--Select--</option>\n                                                <option  *ngFor=\" let type of _BkChapter\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                              </select>\n                                                                            </div>\n                                                                        </div>\n                                                                        <div class=\"col-lg-6\">\n                                                                            <label class=\"col-form-label\" for=\"example-date-input\">BK Dismissal Date: </label> <input formControlName=\"bkDismissalDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\"\n                                                                                value=\"2011-08-19\">\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"modal-footer\">\n                                                                <button type=\"button\" data-dismiss=\"modal\" style=\"display:none\" #bkclosebtn></button> <button class=\"btn btn-success\" type=\"button\" (click)=\"validateFormGroup('bankruptcy',['bkFillingDate','bkChapterID'],'bkclosebtn','Bankruptcy')\">Save</button>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!-- ********************************************************************fin-Modal-4******************************************************************************* -->\n                                            <div class=\"col-lg-3\">\n                                                <label [ngClass]=\"{'has-danger': originationDate.invalid && (originationDate.dirty || originationDate.touched),'has-success': originationDate.valid && (originationDate.dirty || originationDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\"></label>Origination Date <em>*</em> <input formControlName=\"originationDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" >\n                                                <div class=\"form-control-feedback\" *ngIf=\"originationDate.errors && (originationDate.dirty || originationDate.touched)\">\n                                                    <p class=\"text-danger\" *ngIf=\"originationDate.errors.required\">Origination Date is required</p>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-3\">\n                                                <label [ngClass]=\"{'has-danger': paidToDate.invalid && (paidToDate.dirty || paidToDate.touched),'has-success': paidToDate.valid && (paidToDate.dirty || paidToDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\"></label>Paid to Date <em>*</em> <input formControlName=\"paidToDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" >\n                                            </div>\n                                            <div class=\"col-lg-3 \">\n                                                <label [ngClass]=\"{'has-danger': nextPayementDate.invalid && (nextPayementDate.dirty || nextPayementDate.touched),'has-success': nextPayementDate.valid && (nextPayementDate.dirty || nextPayementDate.touched)}\"class=\"col-form-label\" for=\"example-date-input\" ></label>Next Payment Date <em>*</em> <input formControlName=\"nextPayementDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" >\n                                            </div>\n                                            <div class=\"col-lg-3 \">\n                                                <label [ngClass]=\"{'has-danger': payersLastPayementMadeDate.invalid && (payersLastPayementMadeDate.dirty || payersLastPayementMadeDate.touched),'has-success': payersLastPayementMadeDate.valid && (payersLastPayementMadeDate.dirty || payersLastPayementMadeDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\"></label>Payer's Last Payment Made Date <em>*</em> <input formControlName=\"payersLastPayementMadeDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" >\n                                            </div>\n                                            \n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': originalLoanAmount.invalid && (originalLoanAmount.dirty || originalLoanAmount.touched),'has-success': originalLoanAmount.valid && (originalLoanAmount.dirty || originalLoanAmount.touched)}\">Original Loan Amount: $ <em>*</em></label>                                                    <input formControlName=\"originalLoanAmount\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                </div>\n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': unPaidPrincipalBalance.invalid && (unPaidPrincipalBalance.dirty || unPaidPrincipalBalance.touched),'has-success': unPaidPrincipalBalance.valid && (unPaidPrincipalBalance.dirty || unPaidPrincipalBalance.touched)}\">Unpaid Principal Balance: $ <em>*</em></label>                                                    <input formControlName=\"unPaidPrincipalBalance\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                </div>\n                                                <div class=\"col-lg-3\">\n                                                    <label [ngClass]=\"{'has-danger': noteMaturityDate.invalid && (noteMaturityDate.dirty || noteMaturityDate.touched),'has-success': noteMaturityDate.valid && (noteMaturityDate.dirty || noteMaturityDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\">Note Maturity Date <em>*</em> </label><input formControlName=\"noteMaturityDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"\" >\n                                                </div>\n\n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': accruedLateCharges.invalid && (accruedLateCharges.dirty || accruedLateCharges.touched),'has-success': accruedLateCharges.valid && (accruedLateCharges.dirty || accruedLateCharges.touched)}\">Accrued Late Charges: $: <em>*</em></label>                                                    <input formControlName=\"accruedLateCharges\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                </div>\n                                            \n                                            \n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"inputPassword6\">Loan Charges: $</label> <input formControlName=\"loanCharges\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                </div>\n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"inputPassword6\"># of Payments Last 12:</label> <input formControlName=\"ofPayementsLast12\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                </div>\n                                                <div class=\"col-lg-3\">\n                                                    <label [ngClass]=\"{'has-danger': firstPayementDate.invalid && (firstPayementDate.dirty || firstPayementDate.touched),'has-success': firstPayementDate.valid && (firstPayementDate.dirty || firstPayementDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\">First Payment Date<em>*</em></label> <input formControlName=\"firstPayementDate\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"\">\n                                                </div>\n                                                <div class=\"col-lg-3\">\n                                                    <label for=\"exampleFormControlSelect1\" [ngClass]=\"{'has-danger': closingType.invalid && (closingType.dirty || closingType.touched),'has-success': closingType.valid && (closingType.dirty || closingType.touched)}\">Closing type: <em>*</em></label>\n\n                                                    <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"closingType\" id=\"selectbox3\">\n                                    <option value=null disabled selected>--Select--</option>              \n                                  <option [ngClass]=\"{'#consumergoods3': type.typeId==2 || type.typeId==3}\" *ngFor=\" let type of _ClosingType\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                               </select>\n                                                </div>\n                                                <!-- ********************************************************************Modal-5******************************************************************************* -->\n                                                <section>\n                                                    <div formGroupName=\"escrowImpounds\" class=\"modal fade\" data-backdrop=\"static\" data-target=\"#consumergoods3\" id=\"consumergoods3\">\n                                                        <div class=\"modal-dialog modal-lg\">\n                                                            <div class=\"modal-content\">\n                                                                <div class=\"modal-header\">\n                                                                    <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"resetSelect('closingType')\">&times;</button>\n                                                                    <h4 class=\"modal-title\"></h4>\n                                                                </div>\n                                                                <div class=\"modal-body\">\n                                                                    <div class=\"container-fluid\">\n                                                                        <div class=\"row\">\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">Note Payment Amount: $</label> <input formControlName=\"notePaymentAmount\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">TAX Portion: $</label> <input formControlName=\"taxPortion\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                        </div>\n                                                                        <div class=\"row\">\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">Insurance Portion: $</label> <input formControlName=\"insurancePortion\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">Annual Insurance Premium: $</label> <input formControlName=\"annualInsurancePremium\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                        </div>\n                                                                        <div class=\"row\">\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">Annual Taxes: $</label> <input formControlName=\"annualTaxes\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                            <div class=\"col-md-6\">\n                                                                                <label for=\"inputPassword6\">Trust Balance: $</label> <input formControlName=\"trustBalance\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                            </div>\n                                                                        </div>\n                                                                    </div>\n                                                                </div>\n                                                                <div class=\"modal-footer\">\n                                                                    <button class=\"btn btn-success\" data-dismiss=\"modal\" type=\"button\">Save</button>\n                                                                </div>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </section>\n                                                <!-- ********************************************************************fin-Modal-5******************************************************************************* -->\n                                                \n                                                    <div class=\"col-lg-3\">\n                                                        <label [ngClass]=\"{'has-danger': noteInterestRate.invalid && (noteInterestRate.dirty || noteInterestRate.touched),'has-success': noteInterestRate.valid && (noteInterestRate.dirty || noteInterestRate.touched)}\" for=\"inputPassword6\">Note Interest Rate: <em>*</em></label>                                                        <span>%</span> <input formControlName=\"noteInterestRate\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"0.000\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Sold Interest Rate:</label> <span>%</span> <input formControlName=\"soldInterestRate\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label [ngClass]=\"{'has-danger': principaleBalance.invalid && (principaleBalance.dirty || principaleBalance.touched),'has-success': principaleBalance.valid && (principaleBalance.dirty || principaleBalance.touched)}\" for=\"inputPassword6\">Principale Blance:</label>                                                        <span>$</span> <em>*</em><input formControlName=\"principaleBalance\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label [ngClass]=\"{'has-danger': paymentFrequency.invalid && (paymentFrequency.dirty || paymentFrequency.touched),'has-success': paymentFrequency.valid && (paymentFrequency.dirty || paymentFrequency.touched)}\" for=\"inputPassword6\">Payments Frequency: <em>*</em></label>\n                                                        <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"paymentFrequency\">\n                                                        <option  *ngFor=\" let type of _paymentFrequency\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                                        </select>\n                                                    </div>\n                                                    <div class=\"col-lg-12\">\n                                                        <p class=\"info-sell\">Seller must be a licensed Broker/Company to have a reduced Sold Rate that results in a servicing spread to the seller.</p>\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lateCharge.invalid && (lateCharge.dirty || lateCharge.touched),'has-success': lateCharge.valid && (lateCharge.dirty || lateCharge.touched)}\">Late charge is: <em>*</em></label>                                                        <input formControlName=\"lateCharge\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label [ngClass]=\"{'has-danger': loanServicer.invalid && (loanServicer.dirty || loanServicer.touched),'has-success': loanServicer.valid && (loanServicer.dirty || loanServicer.touched)}\" for=\"inputPassword6\">loan Servicer: <em>*</em></label>\n                                                        <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"loanServicer\">\n                                                 <option  *ngFor=\" let type of _loanServicer\"  [selected]=\"type.typeId == '1'\"  [value]=\"type.typeId\">{{type.typeName}}</option>\n                                               </select>\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': afterDays.invalid && (afterDays.dirty || afterDays.touched),'has-success': afterDays.valid && (afterDays.dirty || afterDays.touched)}\">After Days <em>*</em></label>                                                        <input formControlName=\"afterDays\" class=\"form-control\" placeholder=\"\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Unpaid Interest: $</label> <input formControlName=\"UnpaidInterest\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Property Taxes Due: $</label> <input formControlName=\"propertyTaxesDue\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Payment Trust: $</label> <input formControlName=\"payementTrust\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">P&I: $</label> <input class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': TotalMonthlyLoanPayement.invalid && (TotalMonthlyLoanPayement.dirty || TotalMonthlyLoanPayement.touched),'has-success': TotalMonthlyLoanPayement.valid && (TotalMonthlyLoanPayement.dirty || TotalMonthlyLoanPayement.touched)}\">Total Monthly Loan Payment: $ <em>*</em></label>                                                        <input formControlName=\"TotalMonthlyLoanPayement\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                    </div>\n                                                \n                                            \n                                            <section>\n                                                <div class=\"form-row align-items-center\">\n                                                    <div class=\"col-lg-12\">\n                                                        <h6 class=\"title-step\">Check all that apply:</h6>\n                                                    </div>\n                                                    <div class=\"col-lg-12\">\n                                                        <div class=\"col-lg-6 checkbox float-left\">\n                                                            <label><input formControlName=\"deedMortgageStatus\"   type=\"radio\" value=\"false\"  [checked]=\"!DeedMortgage\"> 1st Trust Deed/Mortgage</label>\n                                                        </div>\n                                                        <div class=\"col-lg-6 checkbox float-left\">\n                                                            <label><input formControlName=\"deedMortgageStatus\"  data-target=\"#myModal-select-Junior\" (click)=\"ModalAddValidation('DeedMortgage',['lienPosition1','prnicipalBalance1','current1','informationAsOf1']);changeDRadioValue('deedMortgageStatus','DeedMortgage')\" data-toggle=\"modal\"  type=\"radio\" value=\"true\" [checked]=\"DeedMortgage\" > Junior Trust Deed/Mortgage</label>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!-- ********************************************************************Modal-tow******************************************************************************* -->\n                                            <section>\n                                                <div formGroupName=\"deedMortgage\" class=\"modal fade\" data-backdrop=\"static\" id=\"myModal-select-Junior\" role=\"dialog\">\n                                                    <div class=\"modal-dialog modal-lg\">\n                                                        <div class=\"modal-content\">\n                                                            <div class=\"modal-header\">\n                                                                <button class=\"close\" data-dismiss=\"modal\" type=\"button\" (click)=\"ModalAddValidation('DeedMortgage',['lienPosition1','prnicipalBalance1','current1','informationAsOf1'],'deedMortgage');changeDRadioValue('deedMortgageStatus','DeedMortgage')\">&times;</button>\n                                                                <h4 class=\"modal-title\"></h4>\n                                                            </div>\n                                                            <div class=\"modal-body\">\n                                                                <div class=\"container-fluid\">\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-12\">\n                                                                            <label for=\"inputPassword6\">If Junior Trust Deed/Mortgage, lien position is:</label>\n                                                                            <select class=\"form-control\" (change)=\"SelectOptionAddValidation(['lienPosition1','prnicipalBalance1','current1','informationAsOf1'],$event.target.value)\" id=\"selectedjuniorTrust\">\n                                          <option value=\"0\" selected>2nd Trust Deed/Mortgage</option>\n                                          <option value=\"1\">3rd Trust Deed/Mortgage</option>\n                                          <option value=\"2\">Other</option>\n                                       </select>\n                                                                        </div>\n                                                                        <div class=\"col-lg-12\">\n                                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lienPosition1.invalid && (lienPosition1.dirty || lienPosition1.touched),'has-success': lienPosition1.valid && (lienPosition1.dirty || lienPosition1.touched)}\">Lien Position <em>*</em></label>\n                                                                            <select class=\"form-control\" id=\"\" formControlName=\"lienPosition1\">\n                                          <option value=\"0\" selected>1st Trust Deed/Mortgage</option>\n                                          <option value=\"1\">2nd Trust Deed/Mortgage</option>\n                                          <option value=\"2\">Other</option>\n                                       </select>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': prnicipalBalance1.invalid && (prnicipalBalance1.dirty || prnicipalBalance1.touched),'has-success': prnicipalBalance1.valid && (prnicipalBalance1.dirty || prnicipalBalance1.touched)}\">Principal Balance: $ <em>*</em></label>\n                                                                            <input formControlName=\"prnicipalBalance1\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Monthly Payment: $</label>\n                                                                            <input formControlName=\"monthlyPayment1\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-12\">\n                                                                            <h6 class=\"title-step-section\" [ngClass]=\"{'has-danger': current1.invalid && (current1.dirty || current1.touched),'has-success': current1.valid && (current1.dirty || current1.touched)}\">Current <em>*</em></h6>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-12\">\n                                                                        <div class=\"form-group row\">\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Yes\" formControlName=\"current1\"> Yes</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"No\" formControlName=\"current1\"> No</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-3 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Unknown\" formControlName=\"current1\" [checked]=\"true\"> Unknown</label>\n                                                                            </div>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-12\">\n                                                                            <h6 class=\"title-step-section\">Modified</h6>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-12\">\n                                                                        <div class=\"form-group row\">\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Yes\" formControlName=\"modified1\"> Yes</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"No\" formControlName=\"modified1\"> No</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-3 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Unknown\" formControlName=\"modified1\" [checked]=\"true\"> Unknown</label>\n                                                                            </div>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-6\">\n                                                                        <label class=\"col-form-label\" for=\"example-date-input\" [ngClass]=\"{'has-danger': informationAsOf1.invalid && (informationAsOf1.dirty || informationAsOf1.touched),'has-success': informationAsOf1.valid && (informationAsOf1.dirty || informationAsOf1.touched)}\">Information as of<em>*</em></label>\n                                                                        <input formControlName=\"informationAsOf1\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"2011-08-19\">\n                                                                    </div>\n                                                                </div><br><br>\n                                                                <div class=\"container-fluid\" id=\"formlienPosition2\" style=\"display:none;\">\n                                                                    <div class=\"row\">\n\n                                                                        <div class=\"col-lg-12\">\n                                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lienPosition2.invalid && (lienPosition2.dirty || lienPosition2.touched),'has-success': lienPosition2.valid && (lienPosition2.dirty || lienPosition2.touched)}\">Lien Position <em>*</em></label>\n                                                                            <select class=\"form-control\" id=\"\" formControlName=\"lienPosition2\">\n                                          <option value=\"1\" selected>2nd Trust Deed/Mortgage</option>\n                                          <option value=\"2\">Other</option>\n                                       </select>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': prnicipalBalance2.invalid && (prnicipalBalance2.dirty || prnicipalBalance2.touched),'has-success': prnicipalBalance2.valid && (prnicipalBalance2.dirty || prnicipalBalance2.touched)}\">Principal Balance: $ <em>*</em></label>\n                                                                            <input formControlName=\"prnicipalBalance2\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                        <div class=\"col-md-6\">\n                                                                            <label for=\"inputPassword6\">Monthly Payment: $</label>\n                                                                            <input formControlName=\"monthlyPayment2\" class=\"form-control\" placeholder=\"0.00\" type=\"text\" value=\"0.00\">\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-12\">\n                                                                            <h6 class=\"title-step-section\" [ngClass]=\"{'has-danger': current2.invalid && (current2.dirty || current2.touched),'has-success': current2.valid && (current2.dirty || current2.touched)}\">Current <em>*</em></h6>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-12\">\n                                                                        <div class=\"form-group row\">\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Yes\" formControlName=\"current2\"> Yes</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"No\" formControlName=\"current2\"> No</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-3 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Unknown\"  formControlName=\"current2\" [checked]=\"true\"> Unknown</label>\n                                                                            </div>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"row\">\n                                                                        <div class=\"col-lg-12\">\n                                                                            <h6 class=\"title-step-section\">Modified</h6>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-12\">\n                                                                        <div class=\"form-group row\">\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Yes\"  formControlName=\"modified2\"> Yes</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-2 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"No\"  formControlName=\"modified2\"> No</label>\n                                                                            </div>\n                                                                            <div class=\"col-lg-3 checkbox float-left\">\n                                                                                <label><input type=\"radio\" value=\"Unknown\"  formControlName=\"modified2\"  [checked]=\"true\"> Unknown</label>\n                                                                            </div>\n                                                                        </div>\n                                                                    </div>\n                                                                    <div class=\"col-lg-6\">\n                                                                        <label class=\"col-form-label\" for=\"example-date-input\" [ngClass]=\"{'has-danger': informationAsOf2.invalid && (informationAsOf2.dirty || informationAsOf2.touched),'has-success': informationAsOf2.valid && (informationAsOf2.dirty || informationAsOf2.touched)}\">Information as of<em>*</em></label>\n                                                                        <input formControlName=\"informationAsOf2\" class=\"form-control\" id=\"example-date-input\" type=\"date\" value=\"2011-08-19\">\n                                                                    </div>\n                                                                </div>\n                                                            </div>\n                                                            <div class=\"modal-footer\">\n                                                                <button class=\"btn btn-success\" data-dismiss=\"modal\" type=\"button\" #closeBtn style=\"display:none\"></button> <button type=\"button\" (click)=\"validateFormGroup('deedMortgage',['lienPosition1','prnicipalBalance1','current1','informationAsOf1'],'closeBtn','DeedMortgage')\">Save</button>\n                                                            </div>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </section>\n                                            <!-- ********************************************************************fin-Modal-tow******************************************************************************* -->\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tabcontent\" id=\"Paris\">\n                                <div class=\"row\">\n                                    <div class=\"col-lg-12 bg-light\">\n                                        <h4 class=\"infoclient\">PROPERTY INFORMATION</h4>\n                                    </div><br>\n                                    <div class=\"col-lg-12\" style=\"margin-top: 20px;\">\n                                        <div formGroupName=\"property\" class=\"form-row\">\n                                            <div class=\"col-lg-4\">\n                                                <label [ngClass]=\"{'has-danger': typePropertyID.invalid && (typePropertyID.dirty || typePropertyID.touched),'has-success': typePropertyID.valid && (typePropertyID.dirty || typePropertyID.touched)}\" for=\"inputPassword6\">Property Type: <em>*</em></label>\n                                                <div class=\"dropdown\">\n                                                    <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"typePropertyID\">\n                                             <option value=null disabled selected>--Select--</option>\n                                             <option  *ngFor=\" let type of _PropertyTypes\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                           </select>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-4\">\n                                                <label [ngClass]=\"{'has-danger': address.invalid && (address.dirty || address.touched),'has-success': address.valid && (address.dirty || address.touched)}\" for=\"inputPassword6\">Property Address: <em>*</em></label>                                                <input formControlName=\"address\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                            </div>\n                                            <div class=\"col-lg-4\">\n                                                <label [ngClass]=\"{'has-danger': city.invalid && (city.dirty || city.touched),'has-success': city.valid && (city.dirty || city.touched)}\" for=\"inputPassword6\">City: <em>*</em></label> <input formControlName=\"city\"\n                                                    class=\"form-control\" placeholder=\"\" type=\"text\">\n                                            </div>\n                                            <div class=\"col-lg-4\">\n                                                <label for=\"inputPassword6\">County:</label> <input formControlName=\"country\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                            </div>\n                                            <div class=\"col-lg-4\">\n                                                <label for=\"inputPassword6\">Zip:</label> <input formControlName=\"zip\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                            </div>\n                                            <div class=\"col-lg-4\">\n                                                <label [ngClass]=\"{'has-danger': occupancyStatusID.invalid && (occupancyStatusID.dirty || occupancyStatusID.touched),'has-success': occupancyStatusID.valid && (occupancyStatusID.dirty || occupancyStatusID.touched)}\" for=\"inputPassword6\">Occupancy Status: <em>*</em></label>\n                                                <div class=\"dropdown\">\n                                                    <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"occupancyStatusID\">\n                                             <option value=null disabled selected>--Select--</option>\n                                             <option  *ngFor=\" let type of _OccupancyStatus\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                           </select>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"inputPassword6\">State:</label>\n                                                <div class=\"dropdown\">\n                                                    <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"stateID\">\n                                             <option value=null disabled selected>--Select--</option>\n                                             <option  *ngFor=\" let type of _States\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                           </select>\n                                                </div>\n                                            </div>\n                                            <div class=\"col-lg-3\">\n                                                <label [ngClass]=\"{'has-danger': propertyMarketValue.invalid && (propertyMarketValue.dirty || propertyMarketValue.touched),'has-success': propertyMarketValue.valid && (propertyMarketValue.dirty || propertyMarketValue.touched)}\" for=\"inputPassword6\">Property Market Value: <em>*</em></label>                                                <input formControlName=\"propertyMarketValue\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                            </div>\n                                            <div class=\"col-lg-3 float-left\">\n                                                <label [ngClass]=\"{'has-danger': propertyValuationDate.invalid && (propertyValuationDate.dirty || propertyValuationDate.touched),'has-success': propertyValuationDate.valid && (propertyValuationDate.dirty || propertyValuationDate.touched)}\" class=\"col-form-label\" for=\"example-date-input\"></label>Property Valuation Date <em>*</em> <input formControlName=\"propertyValuationDate\"  class=\"form-control\" id=\"example-date-input\" type=\"date\" >\n                                            </div>\n                                            <div class=\"col-lg-3\">\n                                                <label for=\"inputPassword6\">Valuation Type:</label>\n                                                <div class=\"dropdown\">\n                                                    <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"valuationTypeID\">\n                                             <option value=null disabled selected>--Select--</option>\n                                             <option  *ngFor=\" let type of _ValuationTypes\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                           </select>\n                                                </div>\n                                            </div>\n                                        </div>\n                                        <section formGroupName=\"borrower\" style=\"margin-top: 30px;\">\n                                            <div class=\"row\">\n                                                <div class=\"col-lg-12 bg-light\">\n                                                    <h4 class=\"infoclient\">BORROWER INFORMATION</h4>\n                                                </div><br><br>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': firstName.invalid && (firstName.dirty || firstName.touched),'has-success': firstName.valid && (firstName.dirty || firstName.touched)}\">Primary Borrower First Name: <em>*</em></label>                                                        <input formControlName=\"firstName\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': lastName.invalid && (lastName.dirty || lastName.touched),'has-success': lastName.valid && (lastName.dirty || lastName.touched)}\">Last Name: <em>*</em></label>                                                        <input formControlName=\"lastName\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': maillingAddress.invalid && (maillingAddress.dirty || maillingAddress.touched),'has-success': maillingAddress.valid && (maillingAddress.dirty || maillingAddress.touched)}\">Mailing Address: <em>*</em></label>                                                        <input formControlName=\"maillingAddress\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': city.invalid && (city.dirty || city.touched),'has-success': city.valid && (city.dirty || city.touched)}\">City: <em>*</em></label> <input formControlName=\"city\"\n                                                            class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">State:</label>\n                                                        <div class=\"dropdown\">\n                                                            <select class=\"form-control c-select mb-2 mr-sm-2 mb-sm-0\" formControlName=\"stateID\">\n                                                                <option value=null disabled selected>--Select--</option>\n                                                                <option  *ngFor=\" let type of _States\" [value]=\"type.typeId\">{{type.typeName}}</option>\n                                                            </select>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Home Ph:</label> <input formControlName=\"homePh\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Work Ph:</label> <input formControlName=\"workPh\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Mobile Phone:</label> <input formControlName=\"mobilePhone\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': email.invalid && (email.dirty || email.touched),'has-success': email.valid && (email.dirty || email.touched)}\">Email: <em>*</em></label>\n                                                        <input formControlName=\"email\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">Fax:</label> <input formControlName=\"email\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3\">\n                                                        <label for=\"inputPassword6\">SS/Tax ID #:</label> <input formControlName=\"fssTaxID\" class=\"form-control\" placeholder=\"\" type=\"text\">\n                                                    </div>\n                                                    <div class=\"col-lg-3 text-center for-primary\">\n                                                        <p>for primary borrower.</p>\n                                                    </div>\n                                                </div>\n                                        </section>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tabcontent\" id=\"Tokyo\">\n                                <div class=\"row\">\n                                    <div class=\"col-lg-12 bg-light\">\n                                        <h4 class=\"infoclient\">ASKING PRICE</h4>\n                                    </div><br>\n                                    <div class=\"col-lg-12\">\n                                        <section style=\"margin-top: 20px;\">\n                                            <div class=\"form-row\">\n                                                <div class=\"col-lg-6\">\n                                                    <label [ngClass]=\"{'has-danger': askingPrice.invalid && (askingPrice.dirty || askingPrice.touched),'has-success': askingPrice.valid && (askingPrice.dirty || askingPrice.touched)}\" for=\"inputPassword6\">ASKING PRICE: <em>*</em></label>                                                    <input formControlName=\"askingPrice\" class=\"form-control\" placeholder=\"0.00\" type=\"text\">\n                                                </div>\n                                                <div class=\"col-lg-6\">\n                                                    <label for=\"inputPassword6\">Min ASKING PRICE:</label> <input formControlName=\"minAskingPrice\" class=\"form-control\" type=\"text\">\n                                                </div>\n                                            </div>\n                                            <section style=\"margin-top: 20px;\">\n                                                <div class=\"col-lg-12 bg-light\">\n                                                    <h4 class=\"infoclient\">COMMENTS</h4>\n                                                </div><br>\n                                                <div class=\"form-row\">\n                                                    <div class=\"col-lg-12\">\n                                                        <p class=\"text-comments\">Please place comments on all Important Information on the Note.</p>\n                                                        <p class=\"text-comments-bleu\">Helpful Hint: The more information the better. Any additional information on the Borrower, Property, or \"Story of the Loan\" can increase your likelihood of sale.</p>\n                                                    </div>\n                                                    <div class=\"col-sm-12\">\n                                                        <textarea formControlName=\"comments\" class=\"form-control\" id=\"comment\" placeholder=\"Comment\" rows=\"2\"></textarea>\n                                                    </div>\n                                                </div>\n                                                \n                                            </section>\n                                        </section>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"tabcontent\" id=\"Canada\">\n                                <div class=\"row\">\n                                  <div class=\"col-lg-12 bg-light\">\n                                    <h4 class=\"infoclient\">DOCUMENTS NEEDED</h4>\n                                  </div><br><br>\n                                  <div class=\"col-lg-12 alert alert-warning\" role=\"alert\">\n                                    This section only allows word, excel and pdf file formats, also images formats(png, jpg, jpeg, gif, tiff), otherwise documents won't be uploaded. 50 MB of documents is the maximum attachments allowed per note. All documents that are available will be forwarded to Buyer when an Indicative Offer is accepted\n                                  </div><br>\n                                  <section class=\"page-register\" style=\"margin-top: 20px;\">\n\n                                        <div class=\"row\">\n                                                <div class=\"col-lg-8\">\n                                                  <p class=\"pull-left text-file\" >Payment History <em>*</em></p>\n                                                </div>\n                                                <div class=\"col-lg-4 text-right\">\n                                                  <input accept=\".pdf, .doc\" (change)=\"fileChangeEvent($event,'PaymentHistory')\" class=\"btn btn-outline-success btn-lg\" type=\"file\"> \n                                                  \n                                                </div>\n                                                 </div>\n\n                                    <div class=\"row\" id=\"row-append\">\n                                      <div class=\"col-lg-8\">\n                                        <p class=\"pull-left text-file\">Other</p>\n                                      </div>\n                                      <div class=\"col-lg-4 text-right\">\n                                        <input multiple=\"multiple\"  accept=\".pdf, .doc\" (change)=\"fileChangeEvent($event,'other')\" class=\"btn btn-outline-success btn-lg\" type=\"file\"> \n                                      </div>\n                                      <p class=\"pull-left note col-lg-6\">Note: If the notes are in FCI loan servicing then the Payment History will automatically update. If notes are not serviced by FCI then monthly updates of Payment History need to be submitted.</p>\n                                             \n                                    </div>\n        \n                                    <br>\n                                    <div class=\"row\">\n                                      <div class=\"col-lg-12 bg-light\">\n                                        <h4 class=\"infoclient\">PHOTOS</h4>\n                                      </div><br>\n                                        <div class=\"col-lg-12\">\n                                          <p class=\"text-comments text-left\">Please add photo to increase buyer interest</p>\n                                        </div>\n                                        <div class=\"col-lg-8\">\n                                          <p class=\"pull-left text-file\">Primary Photo</p>\n                                        </div>\n                                        <div class=\"col-lg-4 text-right\">\n                                          <input accept=\".jpg , .png\" multiple=\"true\" (change)=\"fileChangeEvent($event,'photo')\" class=\"btn btn-outline-success btn-lg\" type=\"file\"> \n                                        \n                                        </div>\n                                        <div class=\"col-lg-12 text-right\">\n                                          <a href=\"\" id=\"btn3\"><small class=\"text-right\" style=\"margin-left: 60px;\">Add Other Document</small></a>\n                                        </div>\n                                        <button class=\"btn btn-success\" type=\"submit\">Save</button>\n                                    </div>\n                                  </section>\n                                </div>\n                              </div>\n                            <!-- <pre>{{ sellNoteForm.value | json}} </pre> -->\n                        </form>\n                    </div>\n                </section>\n            </div>\n        </div>\n    </div>\n</section>\n<br><br>"

/***/ }),

/***/ "../../../../../src/app/components/sellnote/sellnote.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SellnoteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_sellnote_service__ = __webpack_require__("../../../../../src/app/services/sellnote.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { loanNote } from '../../models/loanNotes';




var SellnoteComponent = (function () {
    function SellnoteComponent(_httpService, _sellNoteSer, dashboardServ, nav, loading, pagerServ, alertService, router) {
        var _this = this;
        this._httpService = _httpService;
        this._sellNoteSer = _sellNoteSer;
        this.dashboardServ = dashboardServ;
        this.nav = nav;
        this.loading = loading;
        this.pagerServ = pagerServ;
        this.alertService = alertService;
        this.router = router;
        this.Foreclosure = false;
        this.Bankruptcy = false;
        this.DeedMortgage = false;
        this.errorMessage = " ";
        this.errorTypeMessage = " ";
        this.filesToUpload = [];
        this.selectedFileNames = [];
        this.isLoadingData = false;
        //public _loannote:loanNote;
        this._rootUrl = __WEBPACK_IMPORTED_MODULE_6__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + 'api';
        this._urlPostLoanNote = this._rootUrl + "/loanNotes";
        this._urlClosingTypes = this._rootUrl + "/types/closingtypes";
        this._urlLoanTypes = this._rootUrl + "/types/loantypes";
        this._urlamortizationType = this._rootUrl + "/types/amortizationtypes";
        this._urlloanStatus = this._rootUrl + "/types/loanstatus";
        this._urlrateType = this._rootUrl + "/types/ratetypes";
        this._urlpaymentFrequency = this._rootUrl + "/types/paymentfrequencies";
        this._urlStates = this._rootUrl + "/types/states";
        this._urlValuationTypes = this._rootUrl + "/types/valuationtypes";
        this._urlOccupancyStatus = this._rootUrl + "/types/occupancystatus";
        this._urlPropertyTypes = this._rootUrl + "/types/propertytypes";
        this._urlBkChapter = this._rootUrl + "/types/bkchaptertypes";
        this._urlloanServicer = this._rootUrl + "/types/loanServicer";
        this._isValidDeed = false;
        this.nav.show();
        this._sellNoteSer.getDropDownData(this._urlClosingTypes).subscribe(function (data) { _this._ClosingType = data; });
        this._sellNoteSer.getDropDownData(this._urlLoanTypes).subscribe(function (data) { _this._LoanTypes = data; });
        this._sellNoteSer.getDropDownData(this._urlamortizationType).subscribe(function (data) { _this._amortizationType = data; });
        this._sellNoteSer.getDropDownData(this._urlloanStatus).subscribe(function (data) { _this._loanStatus = data; });
        this._sellNoteSer.getDropDownData(this._urlrateType).subscribe(function (data) { _this._rateType = data; });
        this._sellNoteSer.getDropDownData(this._urlpaymentFrequency).subscribe(function (data) { _this._paymentFrequency = data; });
        this._sellNoteSer.getDropDownData(this._urlStates).subscribe(function (data) { _this._States = data; });
        this._sellNoteSer.getDropDownData(this._urlPropertyTypes).subscribe(function (data) { _this._PropertyTypes = data; });
        this._sellNoteSer.getDropDownData(this._urlOccupancyStatus).subscribe(function (data) { _this._OccupancyStatus = data; });
        this._sellNoteSer.getDropDownData(this._urlValuationTypes).subscribe(function (data) { _this._ValuationTypes = data; });
        this._sellNoteSer.getDropDownData(this._urlBkChapter).subscribe(function (data) { _this._BkChapter = data; });
        this._sellNoteSer.getDropDownData(this._urlloanServicer).subscribe(function (data) { _this._loanServicer = data; });
        setTimeout(function () {
            /** popup **/
            $('#selectbox').change(function () {
                if ($(this).find(':selected').hasClass("#consumergoods")) {
                    $('#consumergoods').modal('show');
                }
            });
            $('#selectbox2').change(function () {
                if ($(this).find(':selected').hasClass("#consumergoods2")) {
                    $('#consumergoods2').modal('show');
                }
            });
            $('#selectbox3').change(function () {
                if ($(this).find(':selected').hasClass("#consumergoods3")) {
                    $('#consumergoods3').modal('show');
                }
            });
        }, 500);
        // this._loannote = new loanNote();
        //this._loannote.PI = 1232;
        // this.sellNoteForm = fb.group({
        //   'closingType': [null, Validators.required],
        //   'loanType': [null, Validators.required],
        //   'validate' : ''
        // });
        // this.sellNoteForm.controls['closingType'].valueChanges.subscribe(c => 
        //   console.log(c)
        // );
    }
    SellnoteComponent.prototype.ngOnInit = function () {
        this.onCreateFormControls();
        this.onCreateForm();
    };
    SellnoteComponent.prototype.onCreateFormControls = function () {
        this.minAskingPrice = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number 
        this.principaleBalance = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number 
        this.accruedLateCharges = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number 
        this.adjPayementChangeDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //Date
        this.adjustementFrequency = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.afterDays = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.askingPrice = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.ballonPayement = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false); //Boolean
        this.bankruptcyStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false); //Boolean
        this.ceiling = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.comments = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](""); //String
        this.deedMortgageStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false); //Boolean
        this.drawPeriodStartDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //Date
        //this.escrowImpoundStatus = new FormControl(false, Validators.required); //Boolean
        this.firstPayementDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.floor = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.foreclosureStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false); //Boolean
        this.indexName = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](''); //String
        this.lateCharge = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.loanCharges = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.loanTermsModified = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Boolean
        this.margin = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        //this.name = new FormControl("", Validators.required); //String
        this.nextAdjustement = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //Date
        this.nextPayementDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.noteInterestRate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.noteMaturityDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.ofPayementsLast12 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //String
        this.onForbearancePlan = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Boolean
        this.originalLoanAmount = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.originationDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.paidToDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.payementTrust = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number 
        this.payersLastPayementMadeDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.percentagePrice = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](0, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.PI = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](0, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.prePayPenalty = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Boolean
        this.propertyTaxesDue = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.registerswMers = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Boolean
        this.repaymentPeriodStartDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //Date
        this.soldInterestRate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.TotalMonthlyLoanPayement = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.UnpaidInterest = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")); //number
        this.unPaidPrincipalBalance = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.closingType = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        this.loanServicer = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select 
        this.amortizationType = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        // this.loanNote = new FormControl(null, Validators.required); //select
        this.loanStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        this.loanType = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        this.rateType = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        this.paymentFrequency = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //select
        /***** BORROWER INFORMATION *****/
        this.propertyMarketValue = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number
        this.propertyValuationDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //Date
        this.address = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //string
        this.city = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required); //string
        this.country = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](""); //string
        this.zip = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](""); //string
        this.valuationTypeID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //number
        this.typePropertyID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number     
        this.occupancyStatusID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null, [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]); //number    
        this.loanNoteId = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //number
        this.stateID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null); //number
        /***** Force closure *****/
        this.attorneyFeesDue = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](0);
        this.name = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.Fcaddress = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.Fccity = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.scheduledSaleDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](new Date());
        this.Fczip = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.phone = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.email = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.dateOpened = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.FcstateID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.loanNoteID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        /******* bankruptcy *******/
        this.bkFillingDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.bkDisChargeDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](new Date());
        this.bkDismissalDate = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](new Date());
        this.bkbankruptcyStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false);
        this.bkloanNoteID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.bkChapterID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        /************* escrowImpounds ************/
        this.escrowImpoundsStatus = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false);
        this.notePaymentAmount = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.annualInsurancePremium = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.taxPortion = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.annualTaxes = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.insurancePortion = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.trustBalance = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]('');
        this.esloanNoteID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        /********** BORROWER INFORMATION *********/
        this.isCompany = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false);
        this.firstName = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required);
        this.lastName = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required);
        this.maillingAddress = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required);
        this.homePh = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.workPh = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.fssTaxID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.companyName = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.companyContactName = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.braddress = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.brcity = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required);
        this.brcountry = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.brzip = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.brphone = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.brmobilePhone = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.bremail = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("", [__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required]);
        this.brstateID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.brloanNoteId = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        /****************** deedMortgage ****************/
        this.firstTrust = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false);
        this.juniorTrust = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](false);
        this.lienPosition = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]();
        this.lienPosition1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](null);
        this.monthlyPayment1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.current1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("Unknown");
        this.prnicipalBalance1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.modified1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.informationAsOf1 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]();
        this.lienPosition2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.monthlyPayment2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.current2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.prnicipalBalance2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.modified2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.informationAsOf2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
        this.deedloanNoteID = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]("");
    };
    SellnoteComponent.prototype.onCreateForm = function () {
        this.sellNoteForm = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
            minAskingPrice: this.minAskingPrice,
            principaleBalance: this.principaleBalance,
            accruedLateCharges: this.accruedLateCharges,
            adjPayementChangeDate: this.adjPayementChangeDate,
            adjustementFrequency: this.adjustementFrequency,
            afterDays: this.afterDays,
            askingPrice: this.askingPrice,
            ballonPayement: this.ballonPayement,
            bankruptcyStatus: this.bankruptcyStatus,
            ceiling: this.ceiling,
            comments: this.comments,
            deedMortgageStatus: this.deedMortgageStatus,
            drawPeriodStartDate: this.drawPeriodStartDate,
            //escrowImpoundStatus: this.escrowImpoundStatus, //Boolean
            firstPayementDate: this.firstPayementDate,
            floor: this.floor,
            foreclosureStatus: this.foreclosureStatus,
            indexName: this.indexName,
            lateCharge: this.lateCharge,
            loanCharges: this.loanCharges,
            loanTermsModified: this.loanTermsModified,
            margin: this.margin,
            //name: this.name, //String
            nextAdjustement: this.nextAdjustement,
            nextPayementDate: this.nextPayementDate,
            noteInterestRate: this.noteInterestRate,
            noteMaturityDate: this.noteMaturityDate,
            ofPayementsLast12: this.ofPayementsLast12,
            onForbearancePlan: this.onForbearancePlan,
            originalLoanAmount: this.originalLoanAmount,
            originationDate: this.originationDate,
            paidToDate: this.paidToDate,
            payementTrust: this.payementTrust,
            payersLastPayementMadeDate: this.payersLastPayementMadeDate,
            percentagePrice: this.percentagePrice,
            PI: this.PI,
            prePayPenalty: this.prePayPenalty,
            propertyTaxesDue: this.propertyTaxesDue,
            registerswMers: this.registerswMers,
            repaymentPeriodStartDate: this.repaymentPeriodStartDate,
            soldInterestRate: this.soldInterestRate,
            TotalMonthlyLoanPayement: this.TotalMonthlyLoanPayement,
            UnpaidInterest: this.UnpaidInterest,
            unPaidPrincipalBalance: this.unPaidPrincipalBalance,
            closingType: this.closingType,
            loanServicer: this.loanServicer,
            amortizationType: this.amortizationType,
            //loanNote:this.loanNote, //select
            loanStatus: this.loanStatus,
            loanType: this.loanType,
            rateType: this.rateType,
            paymentFrequency: this.paymentFrequency,
            /***** BORROWER INFORMATION *****/
            property: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                propertyMarketValue: this.propertyMarketValue,
                propertyValuationDate: this.propertyValuationDate,
                address: this.address,
                city: this.city,
                country: this.country,
                zip: this.zip,
                valuationTypeID: this.valuationTypeID,
                typePropertyID: this.typePropertyID,
                occupancyStatusID: this.occupancyStatusID,
                loanNoteId: this.loanNoteId,
                stateID: this.stateID //number
            }),
            /***** Force closure *****/
            foreclosure: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                attorneyFeesDue: this.attorneyFeesDue,
                name: this.name,
                address: this.Fcaddress,
                city: this.Fccity,
                scheduledSaleDate: this.scheduledSaleDate,
                zip: this.Fczip,
                phone: this.phone,
                email: this.email,
                dateOpened: this.dateOpened,
                stateID: this.FcstateID,
                loanNoteID: this.loanNoteID
            }),
            /******* bankruptcy *******/
            bankruptcy: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                bkFillingDate: this.bkFillingDate,
                bkDisChargeDate: this.bkDisChargeDate,
                bkDismissalDate: this.bkDismissalDate,
                bankruptcyStatus: this.bkbankruptcyStatus,
                loanNoteID: this.bkloanNoteID,
                bkChapterID: this.bkChapterID
            }),
            /************* escrowImpounds ************/
            escrowImpounds: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                escrowImpoundsStatus: this.escrowImpoundsStatus,
                notePaymentAmount: this.notePaymentAmount,
                annualInsurancePremium: this.annualInsurancePremium,
                taxPortion: this.taxPortion,
                annualTaxes: this.annualTaxes,
                insurancePortion: this.insurancePortion,
                trustBalance: this.trustBalance,
                loanNoteID: this.esloanNoteID
            }),
            /********** BORROWER INFORMATION *********/
            borrower: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                isCompany: this.isCompany,
                firstName: this.firstName,
                lastName: this.lastName,
                maillingAddress: this.maillingAddress,
                homePh: this.homePh,
                workPh: this.workPh,
                fssTaxID: this.fssTaxID,
                companyName: this.companyName,
                companyContactName: this.companyContactName,
                address: this.braddress,
                city: this.brcity,
                country: this.brcountry,
                zip: this.brzip,
                phone: this.brphone,
                mobilePhone: this.brmobilePhone,
                email: this.bremail,
                stateID: this.brstateID,
                loanNoteId: this.brloanNoteId,
            }),
            /****************** deedMortgage ****************/
            deedMortgage: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
                firstTrust: this.firstTrust,
                juniorTrust: this.juniorTrust,
                lienPosition: this.lienPosition,
                lienPosition1: this.lienPosition1,
                monthlyPayment1: this.monthlyPayment1,
                current1: this.current1,
                prnicipalBalance1: this.prnicipalBalance1,
                modified1: this.modified1,
                informationAsOf1: this.informationAsOf1,
                lienPosition2: this.lienPosition2,
                monthlyPayment2: this.monthlyPayment2,
                current2: this.current2,
                prnicipalBalance2: this.prnicipalBalance2,
                modified2: this.modified2,
                informationAsOf2: this.informationAsOf2,
                loanNoteID: this.deedloanNoteID,
            })
        });
    };
    // submit(){
    // }
    SellnoteComponent.prototype.onSubmitSellNote = function () {
        var _this = this;
        //  this.loading.show();
        console.log(this.sellNoteForm.controls);
        var i = 0;
        Object.keys(this.sellNoteForm.controls).forEach(function (key) {
            if (_this.sellNoteForm.get(key).valid == false) {
                console.log("controle :" + key + " :" + _this.sellNoteForm.get(key).valid);
                i++;
            }
        });
        console.log(this.upload());
        //this.uploadFiles(9);
        // console.log(i);
        // if(this.upload()){
        //   this.uploadFiles(1);
        // }else{
        //      this.alertService.clear();
        //      this.alertService.error(this.errorMessage +" "+this.errorTypeMessage);
        // }
        if (this.sellNoteForm.valid) {
            console.log("upload error ===>" + this.upload());
            if (this.upload()) {
                var formDatax = new FormData();
                formDatax.append("loannote", JSON.stringify(this.sellNoteForm.value));
                for (var k = 0; k < this.filesToUpload.length; k++) {
                    formDatax.append(this.filesToUpload[k].uploadedType, this.filesToUpload[k].file, this.filesToUpload[k].file.name);
                }
                this._httpService
                    .post(this._urlPostLoanNote, formDatax)
                    .subscribe(function (result) {
                    // this.uploadFiles(result.json());
                    setTimeout(function () {
                        _this.loading.hide();
                        _this.alertService.clear();
                        window.scrollTo(0, 0);
                        _this.alertService.success('Your Loan has been successfully listed', true);
                        //this.router.navigate(['about-us']);
                    }, 500);
                });
            }
            else {
                //this.alertService.clear();
                this.alertService.error(this.errorTypeMessage + " " + this.errorMessage);
            }
        }
        else {
            this.validateAllFormFields(this.sellNoteForm);
            setTimeout(function () {
                _this.loading.hide();
                window.scrollTo(0, 0);
                //this.alertService.clear();
                _this.alertService.error(_this.errorTypeMessage + " " + _this.errorMessage);
            }, 300);
        }
    };
    SellnoteComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    /*************************************Start Upload Files(Doc & Img)************************************/
    SellnoteComponent.prototype.fileChangeEvent = function (fileInput, uploadedType) {
        //Clear Uploaded Files result message
        this.uploadResult = "";
        this.filesToUploadR = fileInput.target.files;
        for (var i = 0; i < this.filesToUploadR.length; i++) {
            this.filesToUpload.push({ "uploadedType": uploadedType, "file": this.filesToUploadR[i] });
        }
        //this.filesToUpload.push(this.filesToUploadR[0]);
        console.log(this.filesToUpload);
    };
    SellnoteComponent.prototype.validateFileNumber = function () {
        var validationResult = false;
        var photo = 0;
        var PaymentHistory = 0;
        var other = 0;
        for (var j = 0; j < this.filesToUpload.length; j++) {
            if (this.filesToUpload[j].uploadedType == "photo") {
                photo++;
                if (photo > 3) {
                    validationResult = false;
                }
                else {
                    validationResult = true;
                }
            }
            if (this.filesToUpload[j].uploadedType == "other") {
                other++;
                if (other > 3) {
                    validationResult = false;
                }
                else {
                    validationResult = true;
                }
            }
            if (this.filesToUpload[j].uploadedType == "PaymentHistory") {
                PaymentHistory++;
                if (PaymentHistory > 1) {
                    validationResult = false;
                }
                else {
                    validationResult = true;
                }
            }
        }
        return validationResult;
    };
    SellnoteComponent.prototype.cancelUpload = function () {
        this.filesToUpload = [];
        // this.fileUploadVar.nativeElement.value = "";    
        this.selectedFileNames = [];
        this.uploadResult = "";
        this.errorMessage = "";
    };
    SellnoteComponent.prototype.upload = function () {
        if (this.filesToUpload != undefined) {
            for (var i = 0; i < this.filesToUpload.length; i++) {
                this.selectedFileNames.push(this.filesToUpload[i].file.name);
            }
            if (this.filesToUpload.length == 0) {
                this.errorMessage = " select 1 or than";
                return false;
            }
            else if (!this.validateFileNumber()) {
                this.errorMessage = " The max file selected is 3 for";
                return false;
            }
            else if (this.validatePDFSelectedOnly(this.selectedFileNames) && this.validateSize()) {
                return true;
            }
            else {
                this.errorTypeMessage = " and just Doc or pdf allowed to upload and file size is limited to 1Mb";
                return false;
            }
        }
        else {
            this.errorMessage = "not file selected";
            return false;
        }
    };
    SellnoteComponent.prototype.validatePDFSelectedOnly = function (filesSelected) {
        console.log("selected name -->" + filesSelected);
        for (var i = 0; i < filesSelected.length; i++) {
            if (filesSelected[i].slice(-3).toUpperCase() != "PDF" && filesSelected[i].slice(-3).toUpperCase() != "DOC") {
                return false;
            }
            else {
                return true;
            }
        }
    };
    SellnoteComponent.prototype.validateSize = function () {
        var returnValidationStatus = false;
        for (var i = 0; i < this.filesToUpload.length; i++) {
            if (this.filesToUpload[i].file.size > 100000) {
                return false;
            }
            else {
                returnValidationStatus = true;
            }
        }
        return returnValidationStatus;
    };
    SellnoteComponent.prototype.uploadFiles = function (id) {
        var _this = this;
        console.log('--->length' + this.filesToUpload.length);
        this.uploadResult = "";
        if (this.filesToUpload.length > 0) {
            this.isLoadingData = true;
            var formData = new FormData();
            formData.append("id", id);
            for (var i = 0; i < this.filesToUpload.length; i++) {
                formData.append(this.filesToUpload[i].uploadedType, this.filesToUpload[i].file, this.filesToUpload[i].file.name);
            }
            // console.log();  return false;           
            var apiUrl = "https://localhost:44305/api/loanNotes/UploadFiles";
            this._httpService.post(apiUrl, formData).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log("<==resumt==>  " + data);
                _this.uploadResult = data;
                _this.errorMessage = "";
            }, function (err) {
                console.error(err);
                _this.errorMessage = err;
                _this.isLoadingData = false;
            }, function () {
                _this.isLoadingData = false;
                //this.fileUploadVar.nativeElement.value = "";
                //  this.selectedFileNames = [];
                // this.filesToUpload = [];
            });
        }
    };
    /*************************************End Upload Files(Doc & Img)************************************/
    SellnoteComponent.prototype.clearArrayOfValidators = function (attrs) {
        console.log("Start Clearing Validator");
        for (var _i = 0; _i < attrs.length; _i++) {
            var item = attrs[_i];
            this["" + item].clearValidators();
            console.log(this["" + item].validator);
        }
    };
    SellnoteComponent.prototype.addArrayOfValidators = function (attrs) {
        console.log("Start Adding Validator");
        for (var _i = 0; _i < attrs.length; _i++) {
            var item = attrs[_i];
            this["" + item].setValidators([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].required]);
            this["" + item].updateValueAndValidity();
            console.log(this["" + item].validator);
        }
    };
    SellnoteComponent.prototype.addEmailValidation = function (attrs) {
        for (var _i = 0; _i < attrs.length; _i++) {
            var item = attrs[_i];
            this["" + item].setValidators(__WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* Validators */].email);
            this["" + item].updateValueAndValidity();
            console.log(this["" + item].validator);
        }
    };
    SellnoteComponent.prototype.changeDRadioValue = function (formControl, boolvar) {
        this.sellNoteForm.controls[formControl].setValue(false);
        this["" + boolvar] = !this["" + boolvar];
    };
    SellnoteComponent.prototype.ModalAddValidation = function (checkedvalue, attrs, resetGroup) {
        if (!this["" + checkedvalue]) {
            this.addArrayOfValidators(attrs);
        }
        if (this["" + checkedvalue]) {
            if (this._isValidDeed == false) {
                this.clearArrayOfValidators(attrs);
                this.sellNoteForm.controls[resetGroup].reset();
            }
        }
    };
    SellnoteComponent.prototype.resetRadio = function (checkedvalue, attrs) {
        this["" + checkedvalue] = false;
        this.clearArrayOfValidators(attrs);
    };
    /** Add required validator to array of form controls in select dropdown option to show form modal, close modal, change dropdown option **/
    SellnoteComponent.prototype.modalSelectAddValidation = function (attrs, SelectedModal, status) {
        if (SelectedModal == "Line Of Credit") {
            if (status == "selected") {
                this.addArrayOfValidators(attrs);
            }
            if (status == "closed") {
                this.clearArrayOfValidators(attrs);
            }
        }
        else {
            this.clearArrayOfValidators(attrs);
        }
    };
    SellnoteComponent.prototype.SelectOptionAddValidation = function (attrs, selectedValue) {
        if (selectedValue == 1 || selectedValue == 2) {
            this.addArrayOfValidators(attrs);
        }
        else {
            this.clearArrayOfValidators(attrs);
        }
    };
    //
    SellnoteComponent.prototype.resetSelect = function (select) {
        this["" + select].reset();
    };
    SellnoteComponent.prototype.validateFormGroup = function (formGroup, attrs, closebtn, checkedvalue) {
        //  console.log(this['lienPosition1'].validator);
        //console.log(this.sellNoteForm.get(formGroup).get('email').valid);
        //console.log(this.sellNoteForm.controls['deedMortgage'].valid);
        //console.log(this.sellNoteForm.controls[formGroup].valid); 
        if (this.sellNoteForm.controls[formGroup].valid == true) {
            this._isValidDeed = true;
            this["" + closebtn].nativeElement.click();
        }
        else {
            for (var _i = 0; _i < attrs.length; _i++) {
                var item = attrs[_i];
                this.sellNoteForm.get(formGroup).get(item).markAsDirty();
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeBtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], SellnoteComponent.prototype, "closeBtn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeforcMoule'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], SellnoteComponent.prototype, "closeforcMoule", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('bkclosebtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], SellnoteComponent.prototype, "bkclosebtn", void 0);
    SellnoteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sellnote',
            template: __webpack_require__("../../../../../src/app/components/sellnote/sellnote.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sellnote/sellnote.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__["a" /* httpServiceToken */], __WEBPACK_IMPORTED_MODULE_1__services_sellnote_service__["a" /* SellnoteService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["f" /* buyNotesService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["b" /* NavbarService */],
            __WEBPACK_IMPORTED_MODULE_4__structExports_services__["i" /* loadingService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["l" /* pagerService */], __WEBPACK_IMPORTED_MODULE_4__structExports_services__["d" /* alertService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]])
    ], SellnoteComponent);
    return SellnoteComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/services/services.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/services/services.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n        <section>\n          <div class=\"col-lg-12 bg-light\">\n            <h4 class=\"infoclient\">Pub</h4>\n          </div>\n        </section>\n        <section style=\"margin-top: 20px;\">\n          <div class=\"col-lg-12 doc-space\"></div>\n        </section>\n      </div>\n      <div class=\"col-lg-10 TRANSACTION-step\">\n        <section style=\"margin-top: 20px;\">\n          <div class=\"container-fluid\">\n            <div class=\"row page-due-diligence\">\n              <div class=\"col-md-6\">\n                <div class=\"card text-center\">\n                  <div class=\"card-header\">\n                    <h3 class=\"title-due-diligence-page\">BPO</h3>\n                    <h5 class=\"su-title-due-diligence-page\">(Broker Price Opinion)</h5>\n                  </div>\n                  <div class=\"card-body\">\n                    <div class=\"col-md-6 offset-md-3\">\n                      <a class=\"btn btn-outline-success btn-lg\" type=\"button\" (click) =\"toPayement('BPO')\">Purchase BPO</a>\n                    </div>\n                    <h6 class=\"prix-title-due-diligence-page\">$115.00</h6>\n                  </div>\n                  <div class=\"card-footer text-muted\">\n                    <p class=\"card-text\">A Broker Price Opinion is a report on a specific property by a local real estate agent. It typically shows Comparable Sales, Comparable Listings, neighborhood data, estimated repairs, an opinion of value \"As Is\" and \"Repaired\", and photos of the subject property and area. It will be a \"drive by inspection\" without access to the interior.</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-6\">\n                <div class=\"card text-center\">\n                  <div class=\"card-header\">\n                    <h3 class=\"title-due-diligence-page\">O&E</h3>\n                    <h5 class=\"su-title-due-diligence-page\">(Ownership and Encumbrance Report Price Opinion)</h5>\n                  </div>\n                  <div class=\"card-body\">\n                    <div class=\"col-md-6 offset-md-3\">\n                      <a class=\"btn btn-outline-danger btn-lg\" type=\"button\" (click) =\"toPayement('Purchase O&E')\">Purchase O&E</a>\n                    </div>\n                    <h6 class=\"prix-title-due-diligence-page\">$115.00</h6>\n                  </div>\n                  <div class=\"card-footer text-muted\">\n                    <p class=\"card-text\">Also known as a Property Report. This is an information only title search (not insured) for the recorded owner and liens on the subject property. It typically shows the last recorded owner, asset valuations, taxes and if current, recorded liens, and recorded judgments..</p>\n                  </div>\n                </div>\n              </div>\n            </div><br>\n            <div class=\"row page-due-diligence\">\n              <div class=\"col-md-6\">\n                <div class=\"card text-center\">\n                  <div class=\"card-header\">\n                    <h3 class=\"title-due-diligence-page\">Due Diligence Report Package</h3><!-- <h5 class=\"su-title-due-diligence-page\">(Broker Price Opinion)</h5> -->\n                  </div>\n                  <div class=\"card-body\">\n                    <div class=\"col-md-6 offset-md-3\">\n                      <a class=\"btn btn-outline-warning btn-lg\" type=\"button\" (click) =\"toPayement('Due Diligence Report Package')\">Purchase Report</a>\n                    </div>\n                    <h6 class=\"prix-title-due-diligence-page\">Starting at $115.00</h6>\n                  </div>\n                  <div class=\"card-footer text-muted\">\n                    <p class=\"card-text\">Due Diligence Report Package include, Review of the Loan life to ensure that the note, assignments, Allonges, title insurance and current property insurance are present. As well as delivery of MERS report, PACER search results and state and Federal court record search results.</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-6\">\n                <div class=\"card text-center\">\n                  <div class=\"card-header\">\n                    <h3 class=\"title-due-diligence-page\">Assignment</h3>\n                    <h5 class=\"su-title-due-diligence-page\">Preparation and Recording</h5>\n                  </div>\n                  <div class=\"card-body\">\n                    <div class=\"col-md-6 offset-md-3\">\n                      <a class=\"btn btn-outline-primary btn-lg\" type=\"button\" (click) =\"toPayement('Assignment')\">Purchase AOM</a>\n                    </div>\n                    <h6 class=\"prix-title-due-diligence-page\">$200.00</h6>\n                  </div>\n                  <div class=\"card-footer text-muted\">\n                    <p class=\"card-text\">Due Diligence Report Package include, Review of the Loan life to ensure that the note, assignments, Allonges, title insurance and current property insurance are present. As well as delivery of MERS report, PACER search results and state and Federal court record search results.</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </section>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/services/services.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServicesComponent = (function () {
    function ServicesComponent(nav, router) {
        this.nav = nav;
        this.router = router;
    }
    ServicesComponent.prototype.ngOnInit = function () {
        this.nav.show();
    };
    ServicesComponent.prototype.toPayement = function (typePayment) {
        this.router.navigate(['payment'], { queryParams: { TypePayement: typePayment } });
    };
    ServicesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-services',
            template: __webpack_require__("../../../../../src/app/components/services/services.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/services/services.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]])
    ], ServicesComponent);
    return ServicesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/submitoffer/submitoffer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/submitoffer/submitoffer.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 50px;\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n                <div class=\"col-sm-3 float-left\">\r\n                    <p class=\"text-center page-detail\">{{loanNote?.property?.address}},{{loanNote?.property?.city}}, {{loanNote?.property?.zip}}</p>\r\n                </div>\r\n                <div class=\"col-sm-1 float-left text-center\"><img src=\"assets/img/border.png\"></div>\r\n                <div class=\"col-sm-4 float-left\">\r\n                    <p class=\"text-center page-detail-espace-tow\">\r\n                        Seller asking: <span>${{loanNote?.askingPrice}} ({{loanNote?.askingPrice / loanNote?.principaleBalance | number : '1.2-2'}}% UPB)</span><br>\r\n                        <small class=\"small-text\">Pricing may vary over time and is subject to Seller Approval</small>\r\n                    </p>\r\n                </div>\r\n                <div class=\"col-sm-1 float-left text-center\"><img src=\"assets/img/border.png\"></div>\r\n\r\n            </div>\r\n        </div>\r\n        <hr class=\"sort-by\">\r\n    </div>\r\n</section><br>\r\n<section>\r\n    <div class=\"container\">\r\n        <form class=\"formulaire\">\r\n            <div class=\"form-group row\">\r\n                <label for=\"inputEmail3\" class=\"col-sm-2 col-form-label\">Offer Amount :</label>\r\n                <div class=\"col-sm-10\">\r\n                    <input name=\"OfferAmount\" type=\"number\" class=\"form-control\" id=\"offerAmount\" placeholder=\"Offer Amount\" [(ngModel)]=\"_viewModel.OfferAmount\">\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group row\">\r\n                <label for=\"inputPassword3\" class=\"col-sm-2 col-form-label\">Comment :</label>\r\n                <div class=\"col-sm-10\">\r\n                    <textarea name=\"Comment\" class=\"form-control\" rows=\"2\" id=\"comment\" placeholder=\"Comment\" [(ngModel)]=\"_viewModel.Comment\"></textarea>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group row\">\r\n                <label for=\"inputPassword3\" class=\"col-sm-12 col-form-label\"><h5>Closing Service :</h5></label>\r\n                <div class=\"col-sm-10 offset-sm-2\">\r\n                    <div class=\"col-sm-4 checkbox float-left\" *ngFor=\"let closingType of _closingTypes\">\r\n                        <label><input name=\"ClosingTypes-{{closingType.ClosingTypeId}}\" type=\"radio\" [checked]=\"closingType.ClosingTypeId == _selectedClosingType\" (change)=\"ClosingTypeChanged(closingType.ClosingTypeId)\">{{closingType.Name}}</label>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group row\">\r\n                <label for=\"inputPassword3\" class=\"col-sm-12 col-form-label\"><h5>Due Diligence Options :</h5></label>\r\n                <div class=\"col-sm-10 offset-sm-2\">\r\n                    <div class=\"col-sm-4 checkbox float-left\" *ngFor=\"let dueDiligenceOption of _dueDiligenceOptions\">\r\n                        <label><input name=\"DueDiligenceOptions\" type=\"radio\" [checked]=\"dueDiligenceOption.DueDiligenceOptionsId == _selectedDueDiligenceOption\" (change)=\"DueDilugenceOptionChanged(dueDiligenceOption.DueDiligenceOptionsId)\">{{dueDiligenceOption.TypeDueDiligence}}</label>\r\n                    </div>\r\n                    <div class=\"col-lg-2 float-left\">\r\n                        <label>Days :</label>\r\n                    </div>\r\n                    <div class=\"col-lg-2 float-left days-submit\">\r\n                        <input type=\"number\" name=\"DaysCount\" class=\"form-control\" id=\"inputEmail3\" placeholder=\"3\" [(ngModel)]=\"_viewModel.DaysCount\" [disabled]=\"_viewModel.DueDiligenceOption != 2\" />\r\n                    </div>\r\n                </div>\r\n            </div><br><br>\r\n            <div class=\"col-sm-4 offset-sm-4\">\r\n                <button type=\"submit\" (click)=\"submit()\" class=\"btn btn-success btn-lg\">Submit</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/submitoffer/submitoffer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubmitOfferComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_submitoffer_viewmodel__ = __webpack_require__("../../../../../src/app/models/submitoffer.viewmodel.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SubmitOfferComponent = (function () {
    function SubmitOfferComponent(nav, _httpService, route, toastr, vcref, alertService) {
        this.nav = nav;
        this._httpService = _httpService;
        this.route = route;
        this.toastr = toastr;
        this.alertService = alertService;
        this.id = 0;
        this._closingTypes = [];
        this._dueDiligenceOptions = [];
        this._rootUrl = __WEBPACK_IMPORTED_MODULE_8__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + 'api';
        this._urlLoanNote = this._rootUrl + "/loannotes/";
        this._urlClosingtypes = this._rootUrl + "/closingtypes/";
        this._urlDuedilligenceOptions = this._rootUrl + "/duedilligenceoptions/";
        this._urlOffer = this._rootUrl + "/offers/";
        this.toastr.setRootViewContainerRef(vcref);
        this._viewModel = new __WEBPACK_IMPORTED_MODULE_5__models_submitoffer_viewmodel__["a" /* SubmitOfferViewModel */]({
            loanNoteId: this._loanId,
            offerAmount: 0,
            comment: "",
            closingType: 0,
            dueDiligenceOption: 0
        });
        //this._viewModel.RegisterObserver(this);
    }
    SubmitOfferComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nav.show();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var sub = this.route.params.subscribe(function (params) {
            _this._loanId = +params['id'];
            _this._viewModel.LoanNoteId = +params['id'];
            _this._httpService.get("" + _this._urlLoanNote + _this._loanId, options).subscribe(function (res) {
                console.log(res);
                _this._loan = res.json();
            });
            _this._httpService.get("" + _this._urlLoanNote + _this._loanId + "/closingtypes", options).subscribe(function (res) {
                _this._closingTypes = res.json();
            });
            _this._httpService.get("" + _this._urlDuedilligenceOptions, options).subscribe(function (res) {
                _this._dueDiligenceOptions = res.json();
            });
            console.log(_this._dueDiligenceOptions);
        });
        this.id = this.route.snapshot.params['id'];
        this.getLoanNoteAllDetail(3).subscribe(function (data) {
            console.log("==========>" + data);
            _this.loanNote = data[0];
            _this.bankruptcyChapter = data[1];
            _this.PropertyType = data[2];
        });
    };
    SubmitOfferComponent.prototype.DueDilugenceOptionChanged = function (id) {
        this._viewModel.DueDiligenceOption = id;
    };
    SubmitOfferComponent.prototype.ClosingTypeChanged = function (id) {
        this._viewModel.ClosingType = id;
    };
    SubmitOfferComponent.prototype.ReceiveNotification = function (message) {
        console.log(message);
    };
    SubmitOfferComponent.prototype.ClosingTypesSelectionChanged = function () {
        console.log(event.target);
    };
    SubmitOfferComponent.prototype.getLoanNoteAllDetail = function (id) {
        return __WEBPACK_IMPORTED_MODULE_7_rxjs_Rx__["a" /* Observable */].forkJoin(this._httpService.get(__WEBPACK_IMPORTED_MODULE_8__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "loans/" + id + "?includes=loanStatus;loanType;amortizationType;rateType;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;deedMortgage;escrowImpounds;").map(function (res) { return res.json(); }), this._httpService.get(__WEBPACK_IMPORTED_MODULE_8__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "loans/" + id + "/bankruptcy").map(function (res) { return res.json(); }), this._httpService.get(__WEBPACK_IMPORTED_MODULE_8__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "loans/" + id + "/Property").map(function (res) { return res.json(); }));
    };
    SubmitOfferComponent.prototype.submit = function () {
        var _this = this;
        console.log();
        var submitoffer = this._viewModel.getOfferPOCO();
        if (submitoffer.OfferAmount >= this._loan.minAskingPrice) {
            console.log("dkhl");
            this._httpService
                .post(this._urlOffer, this._viewModel.getOfferPOCO())
                .subscribe(function (result) {
                // if(result.status) ....; 
                // ajout d'alerte pour success ou failure
                // hadi nkhlik tkelef lya biha ila moumkin 
                // mazal matelit 3la alert component akhay j'aime beaucoupe ta fa
                //console.log(result)
                setTimeout(function () {
                    _this.alertService.clear();
                    _this.alertService.success('Offer submited successfuly');
                }, 300);
            }, function (error) {
                _this.alertService.clear();
                _this.alertService.error("you have already submitted an offer on this Note");
            });
        }
        else {
            this.alertService.clear();
            this.alertService.error('your Offer Amount is rejected because its less than min asking price');
        }
    };
    SubmitOfferComponent.prototype.showSuccess = function (message, title) {
        this.toastr.success(message, title);
    };
    SubmitOfferComponent.prototype.showError = function (message, title) {
        this.toastr.error(message, title);
    };
    SubmitOfferComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            moduleId: module.i,
            selector: 'app-submitoffer',
            template: __webpack_require__("../../../../../src/app/components/submitoffer/submitoffer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/submitoffer/submitoffer.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_1__services_httpServiceToken_service__["a" /* httpServiceToken */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastsManager"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"], __WEBPACK_IMPORTED_MODULE_3__structExports_services__["d" /* alertService */]])
    ], SubmitOfferComponent);
    return SubmitOfferComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n        <section>\n          <div class=\"col-lg-12 bg-light\">\n            <h4 class=\"infoclient\">Pub</h4>\n          </div>\n        </section>\n        <section style=\"margin-top: 20px;\">\n          <div class=\"col-lg-12 doc-space\"></div>\n        </section>\n      </div>\n      <div class=\"col-lg-10 bg-light\">\n        <h3 class=\"text-center title-condition-page\">WEBSITE USAGE TERMS FOR THIS WEBSITE</h3>\n        <p class=\"text-one\">Please read the following. By using this website, you agree to and accept the terms and conditions described below. We reserve the right to modify or make changes to the terms of usage, pricing, programs and the contents of the website without notice, so you are advised and cautioned to review the terms and conditions regularly. Each time you use this website you agree to and accept the current terms and conditions.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">AUTHORIZED USE ONLY</span> The entire content of this website is and shall remain the intellectual property of FCI EXCHANGE, LLC. (FCI) or any affiliated or related entity. As such, it is protected by State, Federal and International copyright and other applicable laws. The unauthorized use of this website or its contents is strictly prohibited without prior written permission, and such violation may result in both civil and criminal liability. Such intellectual property rights include any proprietary software used on this website.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">CONFIDENTIAL INFORMATION</span> By using this website, you may be given authorization to view certain public and nonpublic information regarding promissory notes, real property, pictures, locations, documents, and other information that is contained herein. You agree that any and all information obtained from FCI and/or contained herein constitutes confidential information and that you will not use, disclose, or divulge any information contained herein except to your financial advisors, attorneys, accountants and other professionals advising you whether to enter into a transaction.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">HOW WE MAY USE YOUR INFORMATION</span> We also may use the information you provide us through electronic mail, including your email address, to respond to you. We also may use the information we collect from your visit to our Web website to identify levels and areas of interest in us which enables us to improve and refine the content of the Web site.</p><br>\n        <p class=\"text-one\">We do not sell any of this information to third parties. We do, however, reserve the right to disclose such information to our employees, contractors, agents, and designees to the extent necessary to enable them to perform certain website-related services (e.g., web hosting, improve website-related services and features, or maintenance services) on our behalf. We also reserve the right to disclose such information to any third party if we believe that we are required to do so for any or all of the following reasons: (i) by law; (ii) to comply with legal processes or governmental requests; (iii) to prevent, investigate, detect, or prosecute criminal offenses or attacks on the technical integrity of the website or our network; and/or (iv) to protect the rights, property, or safety of us and our affiliates, their partners, and employees, the users of this Web website, or the public.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">DISCLAIMERS</span> The contents and access to this website are provided for convenience only and in so providing such material, access and information, FCI makes no representations or warranties of any kind, express or implied, with respect to the website or any material or information provided. By using this website, in addition to any other waivers or releases made by a user of this website, you expressly promise, acknowledge and agree that FCI, its agents, employees, representatives, members of its board of directors, attorneys, shareholders or any other related or affiliated person or entity (the Related Parties) shall not be liable for any damages of any kind, including but not limited to consequential, compensatory, special and punitive damages. This express promise extends to any equitable claims of any nature, including, but not limited to comparative liability and indemnification, and if any third party sues or makes any claim against any of the parties listed in this paragraph as a direct or indirect result of information provided to you, the user agrees to indemnify, defend and hold FCI and the Related Parties harmless. Any attorney chosen by a user to defend FCI and any of the Related Parties, shall be competent and experienced in the appropriate area of law involved, and shall be subject to the subjective approval of FCI and the Related Parties involved. If FCI does not approve of the attorney chosen by the user, it has the right to retain the attorney of its choice and the user shall be liable for all attorney fees and costs incurred immediately as they accrue and become due.</p><br>\n        <p class=\"text-one\">Our website contains links to other websites. Please be aware that we are not and cannot be responsible for the privacy practices of such other websites and that this Privacy Policy applies solely to the information which we collect through this Web website. We encourage you to ensure that you read the privacy statements of all the destination websites which you visit. Furthermore, we are not responsible for any damages, including but not limited to emotional or physical, that you receive from the other website as a result of its content or transmission of viruses from that site, or other damaging transmissions from that website to your computer.</p>\n        <p></p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">JURISDICTION AND VENUE MATTERS</span> By using this website, a user expressly agrees that jurisdiction and venue for any action against FCI or any of the Related Parties shall be in Orange County, California, in the appropriate state or federal court. There shall be no other place or court which shall have concurrent jurisdiction and any user of this website waives any rights concerning jurisdiction and venue and acknowledges and agrees to these provisions.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">PRIVACY CONSIDERATIONS</span> All personal information obtained by FCI is treated as private and confidential. The information available to users of this website is intended only for the legitimate business use of FCI's customers and clients. FCI will not intentionally violate any right of privacy nor make such information available to third parties for solicitation by third parties. FCI comes into possession and knowledge of a limited amount of information necessary for it to provide its services to its customers and clients, but treats all information received as private and confidential.</p><br>\n        <p class=\"text-one\"><span class=\"text-bold\">SECURITY</span> We attempt to adopt commercially reasonable security measures consistent with legal industry practice to assist in protecting against the loss, misuse, and alteration of personally identifiable information which is under our control. However, no system of transmitting data over the Internet can be guaranteed to be 100% secure. As a result, while we undertake commercially reasonable efforts to protect your personally identifiable information, we cannot guarantee the security of our servers, the means by which information is transmitted between your computer and our servers, or any information provided to us or to any third party through or in connection with the website. You provide all such information entirely at your own risk.</p><br>\n      </div>\n    </div>\n  </div>\n</section>\n"

/***/ }),

/***/ "../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsAndConditionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TermsAndConditionsComponent = (function () {
    function TermsAndConditionsComponent() {
    }
    TermsAndConditionsComponent.prototype.ngOnInit = function () {
    };
    TermsAndConditionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-terms-and-conditions',
            template: __webpack_require__("../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TermsAndConditionsComponent);
    return TermsAndConditionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/trading-terms/trading-terms.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/trading-terms/trading-terms.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-lg-2\">\n        <section>\n          <div class=\"col-lg-12 bg-light\">\n            <h4 class=\"infoclient\">Pub</h4>\n          </div>\n        </section>\n        <section style=\"margin-top: 20px;\">\n          <div class=\"col-lg-12 doc-space\"></div>\n        </section>\n      </div>\n      <div class=\"col-lg-10 bg-light\">\n        <h3 class=\"text-center title-condition-page\">Terms of Use for Trading on this Site</h3>\n        <p class=\"su title-condition\">ANY MISUSE OF THE INFORMATION CONTAINED HEREIN, INCLUDING UNAUTHORIZED CONTACT OR HARASSING THE UNDERLYING BORROWER OR SELLER OF THE NOTE OR OTHERWISE MAY SUBJECT YOU TO PERSONAL LIABILITY BY THE UNDERLYING BORROWER, THE SELLER OF THE NOTE, AND FCI. DATA IS BEING MONITORED AND WILL BE PROVIDED TO LAW ENFORCEMENT UPON ANY INDICATION OF MISUSE.</p><br>\n        <p class=\"title-condition\">The following terms constitute a binding agreement (\"Agreement\") between you and FCI Exchange, Inc., (\"FCI Exchange\", \"FCI Note Exchange\", \"we\", or \"us\"). This Agreement will govern all purchases of notes or properties (each and collectively, \"Note(s)\" or \"Loan(s)\") or Pools (\"Pool or \"Pools\") that include notes or properties, that You may, from time to time, purchase from sellers (\"Sellers\") that utilize FCI Exchange and/or You were introduced to by FCI Exchange. Please read this Agreement, the terms of use (\"Terms of Use\") on FCI Exchange’s web site at www.fciexchange.com and any subdomain thereof (the \"Site\") carefully and print and retain a copy of these documents for your records. By registering electronically and Logging into The Site, you agree to the following terms together with the Terms of Use, consent to our privacy policy, agree to transact business with us and receive communications relating to the Notes electronically, and agree to have any dispute with us resolved by binding arbitration.</p><br>\n\n        <p class=\"title-condition\">This Agreement is effective on September 13, 2010, for current users, and upon acceptance for new users.</p><br>\n\n\n        <p class=\"text-one before-text\">By using FCI Exchange, you are agreeing that you are a prospective Note purchaser or seller or are an agent for either a purchaser or seller who wishes to enter into secondary market transactions through the purchase or sale of Notes.</p><br>\n\n        <p class=\"text-one before-text\">\n          <span class=\"text-bold\">1. Purpose and Confidentiality.</span>Offer amounts, even at the Full Asking Price, are nonbinding until a Purchase and Sale Agreement has been signed.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          <span class=\"text-bold\">2. Confidentiality.</span>You agree that any information provided by FCI Exchange, through fciexchange.com, its officers, employees, affiliates, agents or other representatives of the Notes and all information and documentation in connection with the Notes, including personal information and the Note itself (\"Non-Public Information\") is for Your confidential, personal use to evaluate the Notes. At no time may the Non-Public Information be disclosed, used, or otherwise except for the purpose of evaluating any potential Offer or Active Transaction and Non-Public Information can only be disclosed for the sole purpose of evaluating Notes for potential Offers or Active Transactions to those who have knowledge of this Agreement and are strictly limited to Your officers, agents, employees, or representatives including attorney, accountant, and investment advisor (\"Approved Associates\"), or otherwise required by law. You shall not, and shall also direct all Approved Associates who may be shown Non-Public Information not to disclose that information to anyone. You agree that you will not contact borrowers, property owners, or tenants/residents of the Notes listed on the Site directly, or indirectly, by yourself or through an affiliate or agent of yours. You agree that all contact will be facilitated through FCI Exchange's website or directly with the Seller and/or Buyer as permitted and directed by FCI Exchange.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          Not Withstanding the above paragraph or any other part to this Agreement You shall (a) comply with all federal and state laws and regulations governing or relating to privacy rights in relation to such information, including the Gramm-Leach-Bliley Act, (b) not disclose any of such information to any person other than those of its personnel, agents, or to meet regulatory requirements, who need to know the information for the sole purpose of evaluating the potential purchase of the Note and (c) implement and observe procedures and measures as necessary to ensure the security and confidentiality of such information.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          You shall indemnify, defend and hold harmless FCI Exchange and officers, agents, employees, or representatives including attorney, accountant, and investment advisor (\"Indemnified Associates\") from and against all suits, liabilities, losses, damages or expenses of any kind (including attorney's fees and expenses) incurred or suffered by any Indemnified Associate arising out of or in connection with this Agreement, including without limitation the unauthorized use or disclosure of the Non-Public Information in violation of this Agreement, and any negligent or intentional acts or omissions in relation to this Agreement by You or Approved Associate. Your obligations shall survive the expiration or termination of this Agreement.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          <span class=\"text-bold\">3. Purchase and Sale of Notes.</span>Subject to the terms and conditions of this Agreement, we will provide you the opportunity through the Site:\n        </p><br>\n        <small>To review Note packages that Sellers have posted on its Site, including all Note packages and all documents pertaining thereto; and</small>\n        <small>To advertise your Notes for sale to prospective Buyers and create a medium through which you can communicate with the prospective Buyer.</small>\n\n        <p class=\"text-one before-text\">\n          FCI Exchange will provide the medium through which each Note can be posted on its Site. While FCI Exchange will attempt to verify the accuracy of the information provided by Sellers, FCI Exchange cannot guarantee that any of the information provided by Sellers will be true, accurate or correct. By using this site, Buyer or Seller acknowledge that it is the Seller’s sole responsibility for the accuracy and validity of the Note and Buyers should due a complete due diligence on all information provided by the Seller prior to purchasing the Note.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          <span class=\"text-bold\">4. Limitation of Liability</span>You will not hold FCI Exchange responsible for other users' content, actions, or inactions, or Notes they list. You acknowledge that we are not a traditional auctioneer. Instead, our Sites are venues to allow anyone to offer, sell, and buy Notes at anytime from anywhere. We are not involved in the actual transaction between buyers and sellers. While we may help facilitate the resolution of disputes through various programs, we have no control over and do not guarantee the quality, safety, or legality of items advertised, the truth or accuracy of users’ content or listings, including any statements included in the documents Sellers post on or through the Site, the ability of sellers to sell items, the ability of buyers to pay for items, or that a buyer or seller will actually complete a transaction or return an item.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          We do not transfer legal ownership of items from the Seller to the Buyer, and nothing in this agreement shall modify the governing provisions of California Commercial Code § 2401(2) and Uniform Commercial Code § 2-401(2), unless the buyer and the seller agree otherwise. Further, we cannot guarantee continuous or secure access to our sites, services, or tools, and operation of our sites, services, or tools may be interfered with by numerous factors outside of our control. Accordingly, to the extent legally permitted, we exclude all implied warranties, terms and conditions. We are not liable for any loss of money, potential finance opportunity, financial gain or loss, goodwill or reputation, or any special, indirect or consequential damages arising, directly or indirectly, out of your use of or your inability to use our sites, services, and tools.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          Some jurisdictions do not allow the disclaimer of warranties or exclusion of damages, so such disclaimers and exclusions may not apply to you.\n        </p><br>\n\n        <p class=\"text-one before-text\">\n          Regardless of the previous paragraphs, if we are found to be liable, our liability to you or to any third party is limited to the greater of (a) the total fees you paid to us in the 12 months prior to the action giving rise to the liability, and (b) $100.\n        </p><br>\n        \n        <p class=\"text-one before-text\">\n          <span class=\"text-bold\">4. Limitation of Liability</span>You will not hold FCI Exchange responsible for other users' content, actions, or inactions, or Notes they list. You acknowledge that we are not a traditional auctioneer. Instead, our Sites are venues to allow anyone to offer, sell, and buy Notes at anytime from anywhere. We are not involved in the actual transaction between buyers and sellers. While we may help facilitate the resolution of disputes through various programs, we have no control over and do not guarantee the quality, safety, or legality of items advertised, the truth or accuracy of users’ content or listings, including any statements included in the documents Sellers post on or through the Site, the ability of sellers to sell items, the ability of buyers to pay for items, or that a buyer or seller will actually complete a transaction or return an item.\n        </p><br>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/trading-terms/trading-terms.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TradingTermsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TradingTermsComponent = (function () {
    function TradingTermsComponent() {
    }
    TradingTermsComponent.prototype.ngOnInit = function () {
    };
    TradingTermsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-trading-terms',
            template: __webpack_require__("../../../../../src/app/components/trading-terms/trading-terms.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/trading-terms/trading-terms.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TradingTermsComponent);
    return TradingTermsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/transaction/transaction.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".btn-status {\r\n    background: transparent;\r\n    /*border:1px solid black;*/\r\n    border-radius: 0px;\r\n    color: black;\r\n    border-top: 1px solid black;\r\n    border-bottom: 1px solid black;\r\n    border-left: 1px solid transparent;\r\n    border-right: 1px solid transparent;\r\n}\r\n\r\n.btn-status:hover {\r\n    /*border:1px solid black;*/\r\n    border-radius: 0px;\r\n    color: black;\r\n    border-top: 1px solid black;\r\n    border-bottom: 1px solid black;\r\n    border-left: 1px solid transparent;\r\n    border-right: 1px solid transparent;\r\n    background: transparent;\r\n}\r\n\r\n.toast-top-right {\r\n    bottom: 12px !important;\r\n}\r\n\r\n.has-danger {\r\n    color: red;\r\n    font-style: italic;\r\n}\r\n\r\n.has-success {\r\n    color: green;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/transaction/transaction.component.html":
/***/ (function(module, exports) {

module.exports = "<section style=\"margin-top: 20px;\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"col-lg-2\">\n\n                <section>\n                    <div class=\"col-lg-12 bg-light\">\n                        <h4 class=\"infoclient\">DOCUMENTS NEEDED</h4>\n                    </div>\n                </section>\n                <section style=\"margin-top: 20px;\">\n                    <div class=\"col-lg-12 doc-space\">\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                            <label class=\"form-check-label\" for=\"exampleCheck1\">Document 1...</label>\n                        </div>\n                        <div class=\"form-check\">\n                            <button type=\"button\" class=\"btn btn-outline-primary\"><i class=\"fa fa-download\" aria-hidden=\"true\"></i> Download</button>\n                            <button type=\"button\" class=\"btn btn-outline-success\"><i class=\"fa fa-upload\" aria-hidden=\"true\"></i> Upload</button>\n                        </div>\n                    </div>\n                </section>\n            </div>\n\n            <!--**************************Trasanction**********************************-->\n\n            <div class=\"col-lg-10 TRANSACTION-step\">\n                <section>\n                    <div class=\"col-lg-12 bg-light\">\n                        <h4 class=\"infoclient\">TRANSACTION</h4>\n                    </div>\n                </section>\n                <section style=\"margin-top: 20px;\">\n                    <div class=\"col-lg-12\">\n                        <div id=\"accordion\">\n\n\n                            <!--*********************************************************       begin step1    *********************************************************       -->\n\n                            <div class=\"card\">\n                                <form novalidate [formGroup]=\"formStepOne\">\n                                    <div class=\"card-header\" id=\"headingOne\">\n                                        <h5 class=\"mb-0 float-left\">\n                                            <button class=\"btn btn-link\" data-toggle=\"collapse\" data-target=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">\n                                                  Step 1\n                                                </button>\n                                        </h5>\n                                        <h5 class=\"mb-0 text-right\">\n                                            <span class=\"btn-status\">\n                                                         <span [ngClass]=\"{REJECT:'btn-danger text-white',Pending:'btn-info text-white','He is waiting for your confirmation':'btn-success text-white'}[statusStepOne]\">{{statusStepOne}}</span>\n                                            </span>\n                                        </h5>\n                                    </div>\n                                    <div id=\"collapseOne\" class=\"collapse show\" aria-labelledby=\"headingOne\" data-parent=\"#accordion\">\n                                        <div class=\"card-body\">\n                                            <div class=\"form-row align-items-center\">\n                                                <div class=\"col-lg-12\">\n                                                    <h6 class=\"title-step\">Indicative Offer :</h6>\n                                                </div>\n                                            </div>\n                                            <div class=\"row\">\n                                                <div class=\"col-md-12 padding-left\">\n                                                    <div class=\"form-row\" style=\"margin-top: 30px;\">\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': offerAmountStepOne.invalid && (offerAmountStepOne.dirty || offerAmountStepOne.touched),'has-sucess':offerAmountStepOne.valid && (offerAmountStepOne.dirty || offerAmountStepOne.touched) }\">Offer Amount : $ <em>*</em></label>\n                                                            <input class=\"form-control\" type=\"text\" formControlName=\"offerAmountStepOne\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"offerAmountStepOne.errors && (offerAmountStepOne.dirty || offerAmountStepOne.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"offerAmountStepOne.errors.required\">Amount is required</p>\n                                                                <p class=\"text-danger\" *ngIf=\"offerAmountStepOne.errors.pattern\">Amount is not valid</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-4\">\n                                                            <label for=\"inputPassword6\">Closing Service :</label>\n                                                            <select class=\"form-control\" id=\"selected\" formControlName=\"closingServiceStepOne\">\n                                                                       <option value=\"3\">Direct</option>\n                                                                       <option value=\"2\">Escrow/Closing Service</option>\n                                                                       <option value=\"1\">Direct or Escrow/Closing Service </option>\n                                                                    </select>\n                                                        </div>\n                                                        <div class=\"col-lg-4\">\n                                                            <label for=\"inputPassword6\">Due Diligence Option</label>\n                                                            <select class=\"form-control\" formControlName=\"dueDiligenceOptionStepOne\" (change)=\"onChangedDueDiligenceStepOne($event)\" id=\"selected\">\n                                                                        <option value=\"2\"> Non Exclusive</option>  \n                                                                        <option value=\"1\">Exclusive</option>\n                                                           \n                                                                    </select>\n                                                        </div>\n                                                        <div class=\"col-lg-1\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': daysStepOne.invalid && (daysStepOne.dirty || daysStepOne.touched),'has-sucess':daysStepOne.valid && (daysStepOne.dirty || daysStepOne.touched) }\">Days :</label>\n                                                            <input class=\"form-control\" formControlName=\"daysStepOne\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"daysStepOne.errors && (daysStepOne.dirty || daysStepOne.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"daysStepOne.errors.pattern\">Days is not valid</p>\n                                                            </div>\n                                                        </div>\n                                                        <br><br>\n                                                        <div class=\"col-sm-4 text-center\"><button type=\"button\" [disabled]=\"!checkStepOne || isChangeValuesStepOne\" (click)=\"onSubmitStepOne('ACCEPT')\" class=\"btn btn-outline-success btn-lg\">Accept</button></div>\n                                                        <div class=\"col-sm-4 text-center\"><button type=\"button\" [disabled]=\"!checkStepOne\" (click)=\"onSubmitStepOne('REJECT')\" class=\"btn btn-outline-danger btn-lg\">Reject</button></div>\n                                                        <div class=\"col-sm-4 text-center\"><button type=\"button\" [disabled]=\"!checkStepOne || !isChangeValuesStepOne\" (click)=\"onSubmitStepOne('COUNTER')\" class=\"btn btn-outline-info btn-lg\">Counter</button></div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <!-- <pre>{{formStepOne.value | json}}</pre> -->\n                                </form>\n                            </div>\n                            <!--*********************************************************       end step1    *********************************************************       -->\n                            <!--*********************************************************       debut step2    *********************************************************       -->\n\n                            <div class=\"card\">\n                                <div class=\"card-header\" id=\"headingTwo\">\n                                    <h5 class=\"mb-0\">\n                                        <button class=\"btn btn-link collapsed\" data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">\n                                                  Step 2\n                                                </button>\n                                    </h5>\n                                    <h5 class=\"mb-0 text-right\">\n                                        <span class=\"btn-status\">\n                                                         <span [ngClass]=\"{REJECT:'btn-danger text-white',Pending:'btn-info text-white','He is waiting for your confirmation':'btn-success text-white'}[statusStepTwo]\">{{statusStepTwo}}</span>\n                                        </span>\n                                    </h5>\n                                </div>\n                                <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordion\">\n                                    <div class=\"card-body\">\n                                        <div class=\"form-row align-items-center\">\n                                            <div class=\"col-lg-12\">\n                                                <h6 class=\"title-step\">Due Diligence :</h6>\n                                            </div>\n                                        </div>\n                                        <form novalidate [formGroup]=\"formStepTwo\">\n                                            <div class=\"row\">\n                                                <div class=\"col-lg-12\">\n                                                    <div class=\"col-lg-8 float-left step2-btn\">\n                                                        <label for=\"inputPassword6\">due diligence:</label>\n                                                        <select class=\"form-control\" id=\"selected\" formControlName=\"DueDilidenceStepTwo\" (change)=\"onchangeDueDiligenceStepTwo($event)\">\n                                                                       <option value=\"false\">End due diligence</option>\n                                                                       <option value=\"true\">Extend</option>\n                                                                    </select>\n                                                    </div>\n                                                    <div class=\"col-lg-4 float-left input-days\"><label for=\"inputPassword6\">Days :</label>\n                                                        <input class=\"form-control\" type=\"text\" formControlName=\"daysStepTwo\" id=\"days-input\">\n                                                    </div>\n                                                    <div *ngIf=\"isInitStepTwo\">\n                                                        <div class=\"col-sm-12 text-center float-left\">\n                                                            <button type=\"button\" [disabled]=\"!checkStepTwo\" (click)=\"onSubmitStepTwo('INIT')\" class=\"btn btn-outline-success btn-lg\">Envoyer</button>\n                                                        </div>\n                                                    </div>\n                                                    <div class=\"col-lg-12 float-left\" *ngIf=\"!isInitStepTwo\">\n                                                        <div class=\"col-sm-3 text-center float-left\">\n                                                            <button type=\"button\" [disabled]=\"!checkStepTwo || isChangeValuesStepTow\" (click)=\"onSubmitStepTwo('ACCEPT')\" class=\"btn btn-outline-success btn-lg\">Accept</button>\n                                                        </div>\n                                                        <div class=\"col-sm-3 text-center float-left\">\n                                                            <button type=\"button\" [disabled]=\"!checkStepTwo\" (click)=\"onSubmitStepTwo('REJECT')\" class=\"btn btn-outline-danger btn-lg\">Reject</button>\n                                                        </div>\n                                                        <div class=\"col-sm-3 text-center float-left\">\n                                                            <button type=\"button\" [disabled]=\"!checkStepTwo || !isChangeValuesStepTow\" (click)=\"onSubmitStepTwo('COUNTER')\" class=\"btn btn-outline-info btn-lg\">Counter</button>\n                                                        </div>\n                                                        <div class=\"col-sm-3 text-center float-left\">\n                                                            <button type=\"button\" class=\"btn btn-outline-info btn-lg\">Order Due Diligence Report</button>\n                                                        </div>\n                                                    </div>\n                                                </div>\n                                            </div>\n                                            <!-- <pre>{{formStepTwo.value | json}}</pre> -->\n                                        </form>\n                                    </div>\n                                </div>\n                            </div>\n\n\n                            <!--*********************************************************       end step2    *********************************************************       -->\n                            <!--*********************************************************       begin step3    *********************************************************       -->\n                            <div class=\"card\">\n                                <div class=\"card-header\" id=\"headingThree\">\n                                    <h5 class=\"mb-0\">\n                                        <button class=\"btn btn-link collapsed\" data-toggle=\"collapse\" data-target=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">\n                                          Step 3\n                                        </button>\n                                    </h5>\n                                    <h5 class=\"mb-0 text-right\">\n                                        <span class=\"btn-status\">\n                                                         <span [ngClass]=\"{REJECT:'btn-danger text-white',Pending:'btn-info text-white','He is waiting for your confirmation':'btn-success text-white'}[statusStepTheree]\">{{statusStepTheree}}</span>\n                                        </span>\n                                    </h5>\n\n                                </div>\n                                <div id=\"collapseThree\" class=\"collapse\" aria-labelledby=\"headingThree\" data-parent=\"#accordion\">\n                                    <!-- begin info step 3 for seller and buyer -->\n                                    <div class=\"card-footer text-muted\" style=\"background-color: #fff\">\n                                        <div class=\"form-row align-items-center\">\n                                            <div class=\"col-lg-12\">\n                                                <h6 class=\"title-step\">{{detectedUser | uppercase}} INFORMATION :</h6>\n                                            </div>\n                                        </div>\n                                        <div class=\"row\">\n                                          \n                                            <div class=\"col-md-12 padding-left\">\n                                                <form novalidate [formGroup]=\"formStepThereeInformation\">\n                                                    <div class=\"form-row\" style=\"margin-top: 30px;\">\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': company.invalid && (company.dirty || company.touched),'has-sucess':company.valid && (company.dirty || company.touched) }\"> Company <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"company\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"company.errors && (company.dirty || company.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"company.errors.required\">Company is required</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': contactPerson.invalid && (contactPerson.dirty || contactPerson.touched),'has-sucess':contactPerson.valid && (contactPerson.dirty || contactPerson.touched) }\"> Contact Person <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"contactPerson\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"contactPerson.errors && (contactPerson.dirty || contactPerson.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"contactPerson.errors.required\">Contact Person is required</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': address.invalid && (address.dirty || address.touched),'has-sucess':address.valid && (address.dirty || address.touched) }\"> Address <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"address\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"address.errors && (address.dirty || address.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"address.errors.required\">Address is required</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': city.invalid && (city.dirty || city.touched),'has-sucess':city.valid && (city.dirty || city.touched) }\"> City <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"city\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"city.errors && (city.dirty || city.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"city.errors.required\">City Person is required</p>\n                                                            </div>\n                                                        </div>\n\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': state.invalid && (state.dirty || state.touched),'has-sucess':state.valid && (state.dirty || state.touched) }\"> State <em>*</em></label>\n                                                            <select class=\"form-control\" id=\"selected\" formControlName=\"state\">\n                                                                        <option *ngFor=\"let item of states\" value=\"{{item.id}}\">{{item.text}}</option> \n                                                            </select>\n                                                            <div class=\"form-control-feedback\" *ngIf=\"state.errors && (state.dirty || state.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"state.errors.required\">State Person is required</p>\n                                                            </div>\n                                                        </div>\n\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': zip.invalid && (zip.dirty || zip.touched),'has-sucess':zip.valid && (zip.dirty || zip.touched) }\"> Zip <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"zip\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"zip.errors && (zip.dirty || zip.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"zip.errors.required\">Zip Person is required</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': phone.invalid && (phone.dirty || phone.touched),'has-sucess':phone.valid && (phone.dirty || phone.touched) }\"> Phone <em>*</em></label>\n                                                            <input class=\"form-control\" type=\"text\" formControlName=\"phone\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"phone.errors && (phone.dirty || phone.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"phone.errors.required\">Phone Person is required</p>\n                                                                <p class=\"text-danger\" *ngIf=\"phone.errors.pattern\">Phone Person is valid</p>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': email.invalid && (email.dirty || email.touched),'has-sucess':email.valid && (email.dirty || email.touched) }\"> Email <em>*</em></label>\n                                                            <input class=\"form-control\" formControlName=\"email\" type=\"text\">\n                                                            <div class=\"form-control-feedback\" *ngIf=\"email.errors && (email.dirty || email.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"email.errors.required\">Email Person is required</p>\n                                                                <p class=\"text-danger\" *ngIf=\"email.errors.email\">Email is not valid</p>\n                                                            </div>\n                                                        </div>\n\n                                                        \n\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\" [ngClass]=\"{'has-danger': loanServicerId.invalid && (loanServicerId.dirty || loanServicerId.touched),'has-sucess':loanServicerId.valid && (loanServicerId.dirty || loanServicerId.touched) }\"> Loan Service <em>*</em></label>\n                                                            <select class=\"form-control\" id=\"selected\" formControlName=\"loanServicerId\">\n                                                                                    <option value=\"1\">Service 1</option>\n                                                                                    <option value=\"2\">Service 2</option>\n                                                                                    <option value=\"3\">Service 3</option>\n                                                            </select>\n                                                            <div class=\"form-control-feedback\" *ngIf=\"loanServicerId.errors && (loanServicerId.dirty || loanServicerId.touched)\">\n                                                                <p class=\"text-danger\" *ngIf=\"loanServicerId.errors.required\">Loan service is required</p>\n                                                            </div>\n                                                        </div>\n\n                                                        <div class=\"col-lg-3\">\n                                                            <label for=\"inputPassword6\"> Other </label>\n                                                            <input class=\"form-control\" formControlName=\"other\" type=\"text\">\n                                                        </div>\n                                                        <div class=\"col-lg-6\">\n                                                            <button type=\"button\" [disabled]=\"!checkInformationBuyerSeller\" class=\"btn btn-success btn-lg btn-form-transaction\" (click)=\"onSubmitStepThereeInformation()\">Submit</button>\n                                                        </div>\n                                                    </div>\n                                                    <!-- <pre>{{formStepThereeInformation.value | json}}</pre> -->\n                                                </form>\n\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <!-- end info step 3 for seller and buyer -->\n                                    <div class=\"card-body\">\n                                        <div class=\"form-row align-items-center\">\n                                            <div class=\"col-lg-12\">\n                                                <h6 class=\"title-step\">Indicative Offer :</h6>\n                                            </div>\n                                        </div>\n                                        <form novalidate [formGroup]=\"formStepTheree\">\n                                            <div class=\"row\">\n                                                <div class=\"col-md-12 padding-left\">\n                                                    <div class=\"form-row\" style=\"margin-top: 30px;\">\n\n                                                        <div class=\"col-lg-6\">\n                                                            <label for=\"inputPassword6\">Offer Amount : $</label>\n                                                            <input formControlName=\"offerAmountStepTheree\" class=\"form-control\" type=\"text\">\n                                                        </div>\n                                                        <div class=\"col-lg-6\">\n                                                            <label for=\"inputPassword6\">Closing Service :</label>\n                                                            <select class=\"form-control\" id=\"selected\" formControlName=\"closingServiceStepTheree\">\n                                                                <option value=\"3\">Direct</option>\n                                                            <option value=\"2\">Escrow/Closing Service</option>\n                                                            <option value=\"1\">Direct or Escrow/Closing Service </option>\n                                                            </select>\n                                                        </div><br><br>\n                                                        <div class=\"row\" *ngIf=\"isInitStepTheree\">\n                                                            <div class=\"col-sm-12 text-center float-left\">\n                                                                <button type=\"button\" [disabled]=\"!checkStepTheree\" (click)=\"onSubmitStepTheree('INIT')\" class=\"btn btn-outline-success btn-lg\">Send</button>\n                                                            </div>\n                                                        </div>\n                                                        <div class=\"col-lg-12\" *ngIf=\"!isInitStepTheree\">\n                                                                <div class=\"col-sm-4 text-center float-left\">\n                                                                    <button type=\"button\" [disabled]=\"!checkStepTheree || isChangeValuesStepTheree\" (click)=\"onSubmitStepTheree('ACCEPT')\" class=\"btn btn-outline-success btn-lg\">Accept</button>\n                                                                </div>\n                                                                <div class=\"col-sm-4 text-center float-left\">\n                                                                    <button type=\"button\" [disabled]=\"!checkStepTheree\" (click)=\"onSubmitStepTheree('REJECT')\" class=\"btn btn-outline-danger btn-lg\">Reject</button>\n                                                                </div>\n                                                                <div class=\"col-sm-4 text-center float-left\">\n                                                                    <button type=\"button\" [disabled]=\"!checkStepTheree || !isChangeValuesStepTheree\" (click)=\"onSubmitStepTheree('COUNTER')\" class=\"btn btn-outline-info btn-lg\">Counter</button>\n                                                                </div>\n                                                        </div>\n\n                                                    </div>\n                                                </div>\n                                            </div>\n                                        </form>\n                                    </div>\n                                    <!-- <pre>{{formStepTheree.value | json}}</pre> -->\n\n\n\n                                </div>\n                            </div>\n\n\n                            <!--*********************************************************       End step3    *********************************************************       -->\n\n\n                        </div>\n                    </div>\n                </section>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/components/transaction/transaction.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return transactionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_services__ = __webpack_require__("../../../../../src/app/structExports/services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__ = __webpack_require__("../../../../rxjs/_esm5/observable/IntervalObservable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__structExports_models__ = __webpack_require__("../../../../../src/app/structExports/models.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var transactionComponent = (function () {
    function transactionComponent(nav, loading, alertService, router, activatedRouter, TransactionService, toastr, vcref) {
        this.nav = nav;
        this.loading = loading;
        this.alertService = alertService;
        this.router = router;
        this.activatedRouter = activatedRouter;
        this.TransactionService = TransactionService;
        this.toastr = toastr;
        this.alive = true;
        this.checkStepOne = false;
        this.checkStepTwo = false;
        this.checkStepTheree = false;
        this.checkStepFour = false;
        this.checkStepFive = false;
        this.checkInformationBuyerSeller = true;
        this.isFinishStepOne = false;
        this.isChangeValuesStepOne = false;
        this.isFinishStepTow = false;
        this.isChangeValuesStepTow = false;
        this.isInitStepTwo = false;
        this.isActiveStepTwo = true;
        this.isFinishStepTheree = false;
        this.isChangeValuesStepTheree = false;
        this.isInitStepTheree = false;
        this.toastr.setRootViewContainerRef(vcref);
        this.states = __WEBPACK_IMPORTED_MODULE_7__structExports_models__["k" /* statesData */];
        this.checkUser = localStorage.getItem('OSB0303');
        this.OfferId = this.activatedRouter.snapshot.queryParamMap.get('offerId');
        if (this.checkUser == "OSB0313B") {
            this.detectedUser = "buyer";
        }
        else if (this.checkUser == "OSB0313S") {
            this.detectedUser = "seller";
        }
        else {
            this.router.navigate(['my-exchange']);
        }
    }
    //OSB0313B buyer
    //OSB0313S seller
    transactionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loading.show();
        this.nav.show();
        this.onCreateFormControlStepOne();
        this.onCreateFormStepOne();
        this.onCreateFormControlStepTwo();
        this.onCreateFormStepTwo();
        this.onCreateFormControlStepTheree();
        this.onCreateFormStepTheree();
        this.onCreateFormControlStepThereeInformation();
        this.onCreateFormStepThereeInformation();
        this.getTransaction();
        setTimeout(function () {
            _this.loading.hide();
        }, 1000);
    };
    /*create formcontrol and formGroup for step one */
    transactionComponent.prototype.onCreateFormControlStepOne = function () {
        this.offerAmountStepOne = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]);
        this.closingServiceStepOne = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.dueDiligenceOptionStepOne = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.daysStepOne = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].pattern("^[0-9]+$"));
        this.statusDetail = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.IdDetailStep = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.role = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.turnUser = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.idTransaction = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
    };
    transactionComponent.prototype.onCreateFormStepOne = function () {
        this.formStepOne = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            offerAmountStepOne: this.offerAmountStepOne,
            closingServiceStepOne: this.closingServiceStepOne,
            dueDiligenceOptionStepOne: this.dueDiligenceOptionStepOne,
            daysStepOne: this.daysStepOne,
            status: this.statusDetail,
            IdDetailStep: this.IdDetailStep,
            role: this.role,
            turnUser: this.turnUser,
            idTransaction: this.idTransaction
        });
    };
    /*create formcontrol and formGroup for step tow */
    transactionComponent.prototype.onCreateFormControlStepTwo = function () {
        this.daysStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.DueDilidenceStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.statusDetailStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.IdDetailStepStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.roleStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.turnUserStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.idTransaction = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
    };
    transactionComponent.prototype.onCreateFormStepTwo = function () {
        this.formStepTwo = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            daysStepTwo: this.daysStepTwo,
            DueDilidenceStepTwo: this.DueDilidenceStepTwo,
            statusDetailStepTwo: this.statusDetailStepTwo,
            IdDetailStepStepTwo: this.IdDetailStepStepTwo,
            roleStepTwo: this.roleStepTwo,
            turnUserStepTwo: this.turnUserStepTwo,
            idTransaction: this.idTransaction,
        });
    };
    /*create formcontrol and formGroup for step Theree */
    transactionComponent.prototype.onCreateFormControlStepTheree = function () {
        this.offerAmountStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]();
        this.closingServiceStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.statusDetailStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.turnUserStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.roleStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.idTransaction = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.IdDetailStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
    };
    transactionComponent.prototype.onCreateFormStepTheree = function () {
        this.formStepTheree = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            offerAmountStepTheree: this.offerAmountStepTheree,
            closingServiceStepTheree: this.closingServiceStepTheree,
            statusDetailStepTheree: this.statusDetailStepTheree,
            turnUserStepTheree: this.turnUserStepTheree,
            roleStepTheree: this.roleStepTheree,
            idTransaction: this.idTransaction,
            IdDetailStepTheree: this.IdDetailStepTheree
        });
    };
    /*setep theree informatyion seller , buyer */
    transactionComponent.prototype.onCreateFormControlStepThereeInformation = function () {
        this.company = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.contactPerson = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.address = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.city = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.state = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.zip = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.phone = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].pattern("^[0-9]+$")]);
        this.email = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].email]);
        this.other = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.loanServicerId = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* Validators */].required);
        this.roleInformation = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
        this.transactionID = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('');
    };
    transactionComponent.prototype.onCreateFormStepThereeInformation = function () {
        this.formStepThereeInformation = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            company: this.company,
            contactPerson: this.contactPerson,
            address: this.address,
            city: this.city,
            state: this.state,
            zip: this.zip,
            phone: this.phone,
            email: this.email,
            other: this.other,
            loanServicerId: this.loanServicerId,
            roleInformation: this.roleInformation,
            transactionID: this.transactionID,
        });
    };
    transactionComponent.prototype.getTransaction = function () {
        var _this = this;
        this.TransactionService.getTransaction(this.OfferId).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
            _this.transaction = res;
            console.log(_this.transaction);
            _this.detectedStepByCurrentUserAndStep();
        });
    };
    transactionComponent.prototype.detectedStepByCurrentUserAndStep = function () {
        if (this.transaction && this.detectedUser) {
            //***************************************** begin setp 1 ***************************************
            this.stepOneTrait();
            //***************************************** end setp 1 ***************************************
            //***************************************** begin setp 2 ***************************************
            this.stepTwoTrait();
            //***************************************** end setp 2 ***************************************
            //***************************************** begin setp 3 ***************************************
            this.stepThereeTrait();
            //***************************************** end setp 2 ***************************************
        }
    };
    /*All treatemnts the first step for buyer and seller*/
    transactionComponent.prototype.stepOneTrait = function () {
        var _this = this;
        /* setp is pending */
        if (this.transaction.DetTrans1[0].stepStatus == "PENDING") {
            /*turn seller */
            if (this.transaction.DetTrans1[0].turnUser == "seller") {
                if (this.detectedUser == "buyer") {
                    var aliveRequestNews = true;
                    this.statusStepOne = "Pending";
                    this.checkStepOne = false;
                    this.formStepOne.controls['role'].setValue("buyer");
                    this.formStepOne.controls['offerAmountStepOne'].disable();
                    this.formStepOne.controls['closingServiceStepOne'].disable();
                    this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
                    this.formStepOne.controls['daysStepOne'].disable();
                    /* get stream data */
                    // this.subscription= this.getDataWithInterval(aliveRequestNews);
                }
                else {
                    // this.subscription.unsubscribe();
                    this.statusStepOne = "He is waiting for your confirmation";
                    this.checkStepOne = true;
                    this.formStepOne.controls['role'].setValue("seller");
                    this.turnUser.setValue("buyer");
                    this.formStepOne.controls['offerAmountStepOne'].enable();
                    this.formStepOne.controls['closingServiceStepOne'].enable();
                    this.formStepOne.controls['dueDiligenceOptionStepOne'].enable();
                    this.formStepOne.controls['daysStepOne'].enable();
                }
            }
            /*turn buyer*/
            if (this.transaction.DetTrans1[0].turnUser == "buyer") {
                if (this.detectedUser == "seller") {
                    var aliveRequestNews = true;
                    this.statusStepOne = "Pending";
                    this.checkStepOne = false;
                    this.formStepOne.controls['role'].setValue("seller");
                    this.formStepOne.controls['offerAmountStepOne'].disable();
                    this.formStepOne.controls['closingServiceStepOne'].disable();
                    this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
                    this.formStepOne.controls['daysStepOne'].disable();
                    /* get stream data */
                    //  this.subscription = this.getDataWithInterval(aliveRequestNews);
                }
                else {
                    //this.subscription.unsubscribe();
                    this.statusStepOne = "He is waiting for your confirmation";
                    this.checkStepOne = true;
                    this.formStepOne.controls['role'].setValue("buyer");
                    this.turnUser.setValue("seller");
                    this.formStepOne.controls['offerAmountStepOne'].enable();
                    this.formStepOne.controls['closingServiceStepOne'].enable();
                    this.formStepOne.controls['dueDiligenceOptionStepOne'].enable();
                    this.formStepOne.controls['daysStepOne'].enable();
                }
            }
            /*transaction is accept for buyer and seller*/
        }
        else if (this.transaction.DetTrans1[0].stepStatus == "ACCEPT") {
            this.isFinishStepOne = true;
            this.statusStepOne = "ACCEPT";
            this.checkStepOne = false;
            this.formStepOne.controls['offerAmountStepOne'].disable();
            this.formStepOne.controls['closingServiceStepOne'].disable();
            this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
            this.formStepOne.controls['daysStepOne'].disable();
            /*transaction is reject for buyer or seller*/
        }
        else {
            this.isFinishStepOne = true;
            this.statusStepOne = "REJECT";
            this.checkStepOne = false;
            this.formStepOne.controls['offerAmountStepOne'].disable();
            this.formStepOne.controls['closingServiceStepOne'].disable();
            this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
            this.formStepOne.controls['daysStepOne'].disable();
            this.alertService.error('This is transaction is Reject in step one !');
        }
        /* the last negociation for this transaction on setp one avalaible for buyer and seller */
        this.formStepOne.controls['daysStepOne'].setValue(this.transaction.negotiationStep1[0].days);
        this.formStepOne.controls['offerAmountStepOne'].setValue(this.transaction.negotiationStep1[0].offerAmount);
        this.formStepOne.controls['closingServiceStepOne'].setValue(this.transaction.negotiationStep1[0].closingServiceId, {
            onlySelf: true
        });
        this.formStepOne.controls['dueDiligenceOptionStepOne'].setValue(this.transaction.negotiationStep1[0].dueDiligenceOptionId, {
            onlySelf: true
        });
        this.closingServiceStepOne.setValue(this.transaction.negotiationStep1[0].closingServiceId);
        this.dueDiligenceOptionStepOne.setValue(this.transaction.negotiationStep1[0].dueDiligenceOptionId);
        this.IdDetailStep.setValue(this.transaction.DetTrans1[0].ID);
        this.formStepOne.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
        /* we subscribe the form so that we can see the modfication */
        this.formStepOne.valueChanges.takeWhile(function () { return _this.alive; }).subscribe(function (form) {
            if (_this.formStepOne.controls['daysStepOne'].value != _this.transaction.negotiationStep1[0].days ||
                _this.formStepOne.controls['offerAmountStepOne'].value != _this.transaction.negotiationStep1[0].offerAmount ||
                _this.formStepOne.controls['closingServiceStepOne'].value != _this.transaction.negotiationStep1[0].closingServiceId ||
                _this.formStepOne.controls['dueDiligenceOptionStepOne'].value != _this.transaction.negotiationStep1[0].dueDiligenceOptionId) {
                _this.isChangeValuesStepOne = true;
            }
            else {
                _this.isChangeValuesStepOne = false;
            }
        });
    };
    /*all treatments and checks to submit the first step*/
    transactionComponent.prototype.onSubmitStepOne = function (typeConfirmation) {
        var _this = this;
        this.loading.show();
        if (!this.isFinishStepOne) {
            if (this.checkStepOne) {
                if (this.formStepOne.valid) {
                    if (typeConfirmation == "ACCEPT") {
                        this.alertService.clear();
                        if (this.isChangeValuesStepOne == false) {
                            this.statusDetail.setValue("ACCEPT");
                            this.toastr.success("Your operation is done", "ACCEPT", {
                                showCloseButton: true
                            });
                            this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                                res;
                            });
                            setTimeout(function () {
                                _this.getTransaction();
                                _this.loading.hide();
                            }, 1500);
                        }
                        else {
                            this.alertService.info('This operation not available because you are making the change in step!');
                            this.loading.hide();
                            window.scrollTo(0, 0);
                        }
                    }
                    if (typeConfirmation == "COUNTER") {
                        this.alertService.clear();
                        if (this.isChangeValuesStepOne == true) {
                            this.statusDetail.setValue("COUNTER");
                            this.toastr.info("Your operation is done", "COUNTER", {
                                showCloseButton: true
                            });
                            this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                                res;
                            });
                            setTimeout(function () {
                                _this.getTransaction();
                                _this.loading.hide();
                            }, 1500);
                        }
                        else {
                            this.alertService.info('This operation is not available because you do not make any changes !');
                            this.loading.hide();
                            window.scrollTo(0, 0);
                        }
                    }
                }
                else {
                    this.loading.hide();
                    this.validateAllFormFields(this.formStepOne);
                    this.toastr.error("fill in all mandatory fields", "ERROR", {
                        showCloseButton: true
                    });
                }
                if (typeConfirmation == "REJECT") {
                    this.alertService.clear();
                    this.statusDetail.setValue("REJECT");
                    this.toastr.error("Your operation is done", "REJECT", {
                        showCloseButton: true
                    });
                    this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                        res;
                    });
                    setTimeout(function () {
                        _this.getTransaction();
                        _this.loading.hide();
                    }, 1500);
                }
            }
            else {
                this.alertService.clear();
                this.alertService.info('wait for the answer of the other');
                this.loading.hide();
                window.scrollTo(0, 0);
            }
        }
        else {
            this.alertService.clear();
            this.alertService.info('This step is finished');
            this.loading.hide();
            window.scrollTo(0, 0);
        }
    };
    /*All treatemnts the scond step for buyer and seller*/
    transactionComponent.prototype.stepTwoTrait = function () {
        var _this = this;
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.transaction.DetTrans2)) {
            /* setp is init */
            if (this.transaction.DetTrans2[0].stepStatus == "INIT") {
                if (this.detectedUser == "seller") {
                    this.statusStepTwo = "Pending";
                    this.checkStepTwo = false;
                    this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                    this.formStepTwo.controls['daysStepTwo'].disable();
                    this.formStepTwo.controls['DueDilidenceStepTwo'].disable();
                }
                else {
                    this.isInitStepTwo = true;
                    this.statusStepTwo = "He is waiting for your confirmation";
                    this.checkStepTwo = true;
                    this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                    this.turnUserStepTwo.setValue("seller");
                }
                this.IdDetailStepStepTwo.setValue(this.transaction.DetTrans2[0].ID);
                this.formStepTwo.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
            }
            else {
                this.isInitStepTwo = false;
                /*PENDING*/
                if (this.transaction.DetTrans2[0].stepStatus == "PENDING") {
                    /*turn seller */
                    if (this.transaction.DetTrans2[0].turnUser == "seller") {
                        if (this.detectedUser == "buyer") {
                            this.statusStepTwo = "Pending";
                            this.checkStepTwo = false;
                            this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                            this.formStepTwo.controls['daysStepTwo'].disable();
                            this.formStepTwo.controls['DueDilidenceStepTwo'].disable();
                        }
                        else {
                            this.statusStepTwo = "He is waiting for your confirmation";
                            this.checkStepTwo = true;
                            this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                            this.turnUserStepTwo.setValue("buyer");
                        }
                    }
                    /*turn buyer*/
                    if (this.transaction.DetTrans2[0].turnUser == "buyer") {
                        if (this.detectedUser == "seller") {
                            this.statusStepTwo = "Pending";
                            this.checkStepTwo = false;
                            this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                            this.formStepTwo.controls['daysStepTwo'].disable();
                            this.formStepTwo.controls['DueDilidenceStepTwo'].disable();
                        }
                        else {
                            this.statusStepTwo = "He is waiting for your confirmation";
                            this.checkStepTwo = true;
                            this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                            this.turnUserStepTwo.setValue("seller");
                        }
                    }
                }
                else if (this.transaction.DetTrans2[0].stepStatus == "ACCEPT") {
                    this.isFinishStepTow = true;
                    this.statusStepTwo = "ACCEPT";
                    this.checkStepOne = false;
                    this.formStepTwo.controls['daysStepTwo'].disable();
                    this.formStepTwo.controls['DueDilidenceStepTwo'].disable();
                    /*transaction is reject for buyer or seller*/
                }
                else {
                    this.isFinishStepTow = true;
                    this.statusStepTwo = "REJECT";
                    this.checkStepOne = false;
                    this.alertService.error('This is transaction is Reject in step tow !');
                    this.formStepTwo.controls['daysStepTwo'].disable();
                    this.formStepTwo.controls['DueDilidenceStepTwo'].disable();
                }
                this.formStepTwo.controls['daysStepTwo'].setValue(this.transaction.negotiationStep2[0].days);
                this.formStepTwo.controls['DueDilidenceStepTwo'].setValue(this.transaction.negotiationStep2[0].endDueDiligence, {
                    onlySelf: true
                });
                this.DueDilidenceStepTwo.setValue(this.transaction.negotiationStep2[0].endDueDiligence);
                this.IdDetailStepStepTwo.setValue(this.transaction.DetTrans2[0].ID);
                this.formStepTwo.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
                this.formStepTwo.valueChanges.takeWhile(function () { return _this.alive; }).subscribe(function (form) {
                    if (_this.formStepTwo.controls['daysStepTwo'].value != _this.transaction.negotiationStep2[0].days ||
                        _this.formStepTwo.controls['DueDilidenceStepTwo'].value != _this.transaction.negotiationStep2[0].endDueDiligence) {
                        _this.isChangeValuesStepTow = true;
                    }
                    else {
                        _this.isChangeValuesStepTow = false;
                    }
                });
            } /*end else INIT*/
        }
    };
    /*all treatments and checks to submit the first step*/
    transactionComponent.prototype.onSubmitStepTwo = function (typeConfirmation) {
        var _this = this;
        this.loading.show();
        if (!this.isFinishStepTow) {
            if (this.checkStepTwo) {
                if (typeConfirmation == "INIT") {
                    this.statusDetailStepTwo.setValue("COUNTER");
                    this.toastr.info("Your operation is done", "Init", {
                        showCloseButton: true
                    });
                    this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                        res;
                    });
                    setTimeout(function () {
                        _this.getTransaction();
                        _this.loading.hide();
                    }, 1500);
                }
                if (typeConfirmation == "ACCEPT") {
                    this.alertService.clear();
                    if (this.isChangeValuesStepTow == false) {
                        this.statusDetailStepTwo.setValue("ACCEPT");
                        this.toastr.success("Your operation is done", "ACCEPT", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                            res;
                        });
                        setTimeout(function () {
                            _this.getTransaction();
                            _this.loading.hide();
                        }, 1500);
                    }
                    else {
                        this.alertService.info('This operation not available because you are making the change in step!');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }
                }
                if (typeConfirmation == "REJECT") {
                    this.statusDetailStepTwo.setValue("REJECT");
                    this.toastr.error("Your operation is done", "REJECT", {
                        showCloseButton: true
                    });
                }
                if (typeConfirmation == "COUNTER") {
                    this.alertService.clear();
                    if (this.isChangeValuesStepTow == true) {
                        this.statusDetailStepTwo.setValue("COUNTER");
                        this.toastr.info("Your operation is done", "COUNTER", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                            res;
                        });
                        setTimeout(function () {
                            _this.getTransaction();
                            _this.loading.hide();
                        }, 1500);
                    }
                    else {
                        this.alertService.info('This operation is not available because you do not make any changes !');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }
                }
            }
            else {
                this.alertService.clear();
                this.alertService.info('Wait for the answer of the other/This step is not available currently');
                window.scrollTo(0, 0);
                this.loading.hide();
            }
        }
        else {
            this.alertService.clear();
            this.alertService.info('This step is finished');
            window.scrollTo(0, 0);
            this.loading.hide();
        }
    };
    /*All treatemnts the third step for buyer and seller*/
    transactionComponent.prototype.stepThereeTrait = function () {
        var _this = this;
        if (!__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.transaction.DetTrans3)) {
            if (__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.transaction.vestingInctructionBuyer) && this.detectedUser == "buyer") {
                this.statusStepTheree = "Before going back to the negotiation, fill in the information ";
                this.checkStepTheree = false;
                this.formStepTheree.controls['offerAmountStepTheree'].disable();
                this.formStepTheree.controls['closingServiceStepTheree'].disable();
                this.formStepThereeInformation.controls['roleInformation'].setValue("buyer");
                this.formStepThereeInformation.controls['transactionID'].setValue(this.transaction.Transaction[0].offerID);
            }
            else if (__WEBPACK_IMPORTED_MODULE_6_lodash__["isEmpty"](this.transaction.vestingInctructionSeller) && this.detectedUser == "seller") {
                this.statusStepTheree = "Before going back to the negotiation, fill in the information";
                this.checkStepTheree = false;
                this.formStepTheree.controls['offerAmountStepTheree'].disable();
                this.formStepTheree.controls['closingServiceStepTheree'].disable();
                this.formStepThereeInformation.controls['roleInformation'].setValue("seller");
                this.formStepThereeInformation.controls['transactionID'].setValue(this.transaction.Transaction[0].offerID);
            }
            else {
                if (this.detectedUser == "seller") {
                    this.getInformationBuyerSeller(this.transaction.vestingInctructionSeller);
                }
                else {
                    this.getInformationBuyerSeller(this.transaction.vestingInctructionBuyer);
                }
                /* setp is init */
                if (this.transaction.DetTrans3[0].stepStatus == "INIT") {
                    if (this.detectedUser == "seller") {
                        this.statusStepTheree = "Pending";
                        this.checkStepTheree = false;
                        this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                        this.formStepTheree.controls['offerAmountStepTheree'].disable();
                        this.formStepTheree.controls['closingServiceStepTheree'].disable();
                    }
                    else {
                        this.isInitStepTheree = true;
                        this.statusStepTheree = "He is waiting for your confirmation";
                        this.checkStepTheree = true;
                        this.formStepTheree.controls['roleStepTheree'].setValue("buyer");
                        this.turnUserStepTheree.setValue("seller");
                        this.formStepTheree.controls['offerAmountStepTheree'].enable();
                        this.formStepTheree.controls['closingServiceStepTheree'].enable();
                    }
                    this.IdDetailStepTheree.setValue(this.transaction.DetTrans3[0].ID);
                    this.formStepTheree.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
                    this.formStepTheree.controls['offerAmountStepTheree'].setValue(this.transaction.negotiationStep1[0].offerAmount);
                    this.formStepTheree.controls['closingServiceStepTheree'].setValue(this.transaction.negotiationStep1[0].closingServiceId, {
                        onlySelf: true
                    });
                    this.closingServiceStepTheree.setValue(this.transaction.negotiationStep1[0].closingServiceId);
                }
                else {
                    this.isInitStepTheree = false;
                    /*PENDING*/
                    if (this.transaction.DetTrans3[0].stepStatus == "PENDING") {
                        /*turn seller */
                        if (this.transaction.DetTrans3[0].turnUser == "seller") {
                            if (this.detectedUser == "buyer") {
                                this.statusStepTheree = "Pending";
                                this.checkStepTheree = false;
                                this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                                this.formStepTheree.controls['offerAmountStepTheree'].disable();
                                this.formStepTheree.controls['closingServiceStepTheree'].disable();
                            }
                            else {
                                this.statusStepTheree = "He is waiting for your confirmation";
                                this.checkStepTheree = true;
                                this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                                this.turnUserStepTheree.setValue("buyer");
                            }
                        }
                        /*turn buyer*/
                        if (this.transaction.DetTrans3[0].turnUser == "buyer") {
                            if (this.detectedUser == "seller") {
                                this.statusStepTheree = "Pending";
                                this.checkStepTheree = false;
                                this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                                this.formStepTheree.controls['offerAmountStepTheree'].disable();
                                this.formStepTheree.controls['closingServiceStepTheree'].disable();
                            }
                            else {
                                this.statusStepTheree = "He is waiting for your confirmation";
                                this.checkStepTheree = true;
                                this.formStepTheree.controls['roleStepTheree'].setValue("buyer");
                                this.turnUserStepTheree.setValue("seller");
                            }
                        }
                        /*transaction is ACCEPT for buyer and seller*/
                    }
                    else if (this.transaction.DetTrans3[0].stepStatus == "ACCEPT") {
                        this.isFinishStepTheree = true;
                        this.statusStepTheree = "ACCEPT";
                        this.checkStepTheree = false;
                        this.formStepTheree.controls['offerAmountStepTheree'].disable();
                        this.formStepTheree.controls['closingServiceStepTheree'].disable();
                        /*transaction is reject for buyer or seller*/
                    }
                    else {
                        this.isFinishStepTheree = true;
                        this.statusStepTheree = "REJECT";
                        this.checkStepTheree = false;
                        this.alertService.error('This is transaction is Reject in step Theree !');
                        this.formStepTheree.controls['offerAmountStepTheree'].disable();
                        this.formStepTheree.controls['closingServiceStepTheree'].disable();
                    }
                    this.formStepTheree.controls['offerAmountStepTheree'].setValue(this.transaction.negotiationStep3[0].offerAmount);
                    this.formStepTheree.controls['closingServiceStepTheree'].setValue(this.transaction.negotiationStep3[0].closingServiceId, {
                        onlySelf: true
                    });
                    this.closingServiceStepTheree.setValue(this.transaction.negotiationStep3[0].closingServiceId);
                    this.IdDetailStepTheree.setValue(this.transaction.DetTrans3[0].ID);
                    this.formStepTheree.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
                    this.formStepTheree.valueChanges.takeWhile(function () { return _this.alive; }).subscribe(function (form) {
                        if (_this.formStepTheree.controls['offerAmountStepTheree'].value != _this.transaction.negotiationStep3[0].offerAmount ||
                            _this.formStepTheree.controls['closingServiceStepTheree'].value != _this.transaction.negotiationStep3[0].closingServiceId) {
                            _this.isChangeValuesStepTheree = true;
                        }
                        else {
                            _this.isChangeValuesStepTheree = false;
                        }
                    });
                }
                /*end else INIT*/
            }
        }
    };
    /*all treatments and checks to submit the third step*/
    transactionComponent.prototype.onSubmitStepTheree = function (typeConfirmation) {
        var _this = this;
        this.loading.show();
        if (!this.isFinishStepTheree) {
            if (this.checkStepTheree) {
                if (typeConfirmation == "INIT") {
                    this.statusDetailStepTheree.setValue("COUNTER");
                    this.toastr.info("Your operation is done", "Init", {
                        showCloseButton: true
                    });
                    this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                        res;
                    });
                    setTimeout(function () {
                        _this.getTransaction();
                        _this.loading.hide();
                    }, 1500);
                }
                if (typeConfirmation == "ACCEPT") {
                    if (this.isChangeValuesStepTheree == false) {
                        this.statusDetailStepTheree.setValue("ACCEPT");
                        this.toastr.success("Your operation is done", "ACCEPT", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                            res;
                        });
                        setTimeout(function () {
                            _this.getTransaction();
                            _this.loading.hide();
                        }, 1500);
                    }
                    else {
                        this.alertService.info('This operation not available because you are making the change in step!');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }
                }
                if (typeConfirmation == "REJECT") {
                    this.statusDetailStepTheree.setValue("REJECT");
                    this.toastr.error("Your operation is done", "REJECT", {
                        showCloseButton: true
                    });
                }
                if (typeConfirmation == "COUNTER") {
                    if (this.isChangeValuesStepTheree == true) {
                        this.statusDetailStepTheree.setValue("COUNTER");
                        this.toastr.info("Your operation is done", "COUNTER", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(function () { return _this.alive; }).subscribe(function (res) {
                            res;
                        });
                        setTimeout(function () {
                            _this.getTransaction();
                            _this.loading.hide();
                        }, 1500);
                    }
                    else {
                        this.alertService.info('This operation is not available because you do not make any changes !');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }
                }
            }
            else {
                this.alertService.clear();
                this.alertService.info('Wait for the answer of the other/This step is not available currently');
                window.scrollTo(0, 0);
                this.loading.hide();
            }
        }
        else {
            this.alertService.clear();
            this.alertService.info('This step is finished');
            window.scrollTo(0, 0);
            this.loading.hide();
        }
    };
    /*get stream data with interval the time  if not the current user turn */
    transactionComponent.prototype.getDataWithInterval = function (aliveRequestNews) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_IntervalObservable__["a" /* IntervalObservable */].create(20000).takeWhile(function () { return _this.alive && aliveRequestNews; }).subscribe(function () {
            _this.getTransaction();
            console.log("buyer");
        });
    };
    transactionComponent.prototype.onSubmitStepThereeInformation = function () {
        var _this = this;
        this.loading.show();
        if (this.checkInformationBuyerSeller) {
            if (this.formStepThereeInformation.valid) {
                this.TransactionService.postStepThereeInformation(this.formStepThereeInformation.value).subscribe(function (res) {
                    res;
                });
                setTimeout(function () {
                    _this.loading.hide();
                    _this.getTransaction();
                }, 1500);
                this.toastr.clearAllToasts();
                this.toastr.success("Your operation is done", "Envoyer", {
                    showCloseButton: true
                });
            }
            else {
                this.validateAllFormFields(this.formStepThereeInformation);
                this.loading.hide();
                this.toastr.error("fill in all mandatory fields", "ERROR", {
                    showCloseButton: true
                });
            }
        }
        else {
            this.alertService.clear();
            this.alertService.info("Please,stopped doing anything we controlled all!");
            setTimeout(function () {
                _this.loading.hide();
            }, 300);
            window.scrollTo(0, 0);
        }
    };
    /* this for get information ... seller or buyer about step theree, if exist and stoped sumbit*/
    transactionComponent.prototype.getInformationBuyerSeller = function (vestingInctructionBuyer) {
        this.checkInformationBuyerSeller = false;
        this.formStepThereeInformation.controls['company'].setValue(vestingInctructionBuyer.company);
        this.formStepThereeInformation.controls['contactPerson'].setValue(vestingInctructionBuyer.contactPerson);
        this.formStepThereeInformation.controls['address'].setValue(vestingInctructionBuyer.address);
        this.formStepThereeInformation.controls['city'].setValue(vestingInctructionBuyer.city);
        this.formStepThereeInformation.controls['state'].setValue(vestingInctructionBuyer.state);
        this.formStepThereeInformation.controls['phone'].setValue(vestingInctructionBuyer.phone);
        this.formStepThereeInformation.controls['zip'].setValue(vestingInctructionBuyer.zip);
        this.formStepThereeInformation.controls['email'].setValue(vestingInctructionBuyer.email);
        this.formStepThereeInformation.controls['other'].setValue(vestingInctructionBuyer.other);
        this.formStepThereeInformation.controls['loanServicerId'].setValue(vestingInctructionBuyer.loanServicerId);
        this.formStepThereeInformation.controls['company'].disable();
        this.formStepThereeInformation.controls['contactPerson'].disable();
        this.formStepThereeInformation.controls['address'].disable();
        this.formStepThereeInformation.controls['city'].disable();
        this.formStepThereeInformation.controls['state'].disable();
        this.formStepThereeInformation.controls['phone'].disable();
        this.formStepThereeInformation.controls['zip'].disable();
        this.formStepThereeInformation.controls['email'].disable();
        this.formStepThereeInformation.controls['other'].disable();
        this.formStepThereeInformation.controls['loanServicerId'].disable();
    };
    /* this is methode declanche when forms is not valid for marke all fields as touched ...*/
    transactionComponent.prototype.validateAllFormFields = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]) {
                control.markAsTouched({
                    onlySelf: true
                });
            }
            else if (control instanceof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]) {
                _this.validateAllFormFields(control);
            }
        });
    };
    transactionComponent.prototype.onChangedDueDiligenceStepOne = function (event) {
        if (event.target.value == 2) {
            this.formStepOne.controls['daysStepOne'].disable();
            //this.daysStepOne.disabled;
        }
        else {
            this.formStepOne.controls['daysStepOne'].enable();
        }
    };
    transactionComponent.prototype.onchangeDueDiligenceStepTwo = function (event) {
        if (event.target.value == "false") {
            this.formStepTwo.controls['daysStepTwo'].disable();
        }
        else {
            this.formStepTwo.controls['daysStepTwo'].enable();
        }
    };
    transactionComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
        localStorage.removeItem('OSB0303');
    };
    transactionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-transaction',
            template: __webpack_require__("../../../../../src/app/components/transaction/transaction.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/transaction/transaction.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__structExports_services__["b" /* NavbarService */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["i" /* loadingService */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["d" /* alertService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__structExports_services__["o" /* transactionService */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]])
    ], transactionComponent);
    return transactionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/config/AppSettings.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppSettings; });
var AppSettings = (function () {
    function AppSettings() {
    }
    AppSettings.API_ENDPOINT = 'https://exchangeloans.azurewebsites.net/exapibeta/';
    return AppSettings;
}());



/***/ }),

/***/ "../../../../../src/app/guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return authGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var authGuard = (function () {
    function authGuard(_router) {
        this._router = _router;
    }
    authGuard.prototype.canActivate = function () {
        if (localStorage.getItem('currentuser')) {
            return true;
        }
        else {
            this._router.navigate(['login']);
            return false;
        }
    };
    authGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], authGuard);
    return authGuard;
}());



/***/ }),

/***/ "../../../../../src/app/models/Offers.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Offers */
var Offers = (function () {
    function Offers() {
    }
    return Offers;
}());



/***/ }),

/***/ "../../../../../src/app/models/alerts.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Alert */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertType; });
var Alert = (function () {
    function Alert() {
    }
    return Alert;
}());

var AlertType;
(function (AlertType) {
    AlertType[AlertType["Success"] = 0] = "Success";
    AlertType[AlertType["Error"] = 1] = "Error";
    AlertType[AlertType["Info"] = 2] = "Info";
    AlertType[AlertType["Warning"] = 3] = "Warning";
})(AlertType || (AlertType = {}));


/***/ }),

/***/ "../../../../../src/app/models/allSelect2Static.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return aboutUsData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return millingData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return propertyTypesData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CurrentCapitalAvailableData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return idealPropertyValueData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LoanTypesData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return purchaseYourNoteData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LienPositionData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return statesData; });
var aboutUsData = [
    {
        id: 'internetSearch',
        text: 'InternetSearch'
    },
    {
        id: 'industryTradeGroup',
        text: 'Industry Trade Group'
    },
    {
        id: 'refferalRecomendation',
        text: 'Referral/ Recommendation'
    },
    {
        id: 'fciLenderServices',
        text: 'FCI Lender Services'
    },
    {
        id: 'tvOrRadio',
        text: 'TV or Radio'
    },
    {
        id: 'newsArticle',
        text: 'News or Article'
    },
    {
        id: 'email',
        text: 'Email'
    }
];
var millingData = [
    {
        id: 'featureListing',
        text: 'Feature Listings'
    },
    {
        id: 'offersAndOther',
        text: 'Offers and other changes to Notes on My WATCH LIST'
    },
    {
        id: 'newListing',
        text: 'New Listings matching My Product Alerts'
    }
];
var propertyTypesData = [
    {
        id: 'residential',
        text: 'Residential'
    },
    {
        id: 'construction',
        text: 'Construction'
    },
    {
        id: 'land',
        text: 'Land'
    },
    {
        id: 'commercialRealEstate',
        text: 'Commercial Real Estate'
    },
    {
        id: 'manufacturedHousing',
        text: 'Manufactured Housing'
    },
    {
        id: 'multiUnitApartment',
        text: 'Multi-Unit Apartments'
    },
    {
        id: 'other',
        text: 'Other'
    }
];
var CurrentCapitalAvailableData = [
    {
        id: 'under250K',
        text: 'Under $ 250K'
    },
    {
        id: 'between_250K500K',
        text: '$250K - $ 500K'
    },
    {
        id: 'between_500K750K',
        text: '$ 500K – $ 750K'
    },
    {
        id: '750K-1MM',
        text: '$ 750K - $1MM'
    },
    {
        id: 'moreThan1MM',
        text: '$ 1MM +'
    },
];
var idealPropertyValueData = [
    {
        id: 'under100K',
        text: 'Under 100K'
    },
    {
        id: 'between_100K300K',
        text: '100K – 300K'
    },
    {
        id: 'between_301K750K',
        text: '301K – 750K'
    },
    {
        id: 'moreThan750K',
        text: '750kplus',
    },
];
var LoanTypesData = [
    {
        id: 'performing',
        text: 'Performing'
    },
    {
        id: 'nonPerforming',
        text: 'Non Performing'
    },
    {
        id: 'newlyOriginated',
        text: 'Newly Originated'
    }
];
var purchaseYourNoteData = [
    {
        id: 'InBulk',
        text: 'In Bulk'
    },
    {
        id: 'Single',
        text: 'Single'
    }
];
var LienPositionData = [
    {
        id: 'firstLien',
        text: '1st Lien'
    },
    {
        id: 'secondLien',
        text: '2nd Lien'
    },
    {
        id: 'thirdLien',
        text: '3rd Lien'
    },
    {
        id: 'Other',
        text: 'Other'
    },
];
var statesData = [
    {
        id: "DefaultItem",
        text: "Choose your state"
    },
    {
        id: ' 1',
        text: 'ALABAMA'
    },
    {
        id: '2',
        text: 'ALASKA'
    },
    {
        id: '3',
        text: 'ARIZONA'
    },
    {
        id: '4',
        text: 'CAROLINA'
    },
    {
        id: ' 5',
        text: 'COLORADO'
    },
    {
        id: '6',
        text: 'FLORIDA'
    },
];


/***/ }),

/***/ "../../../../../src/app/models/deedMortgage.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export deedMortgage */
var deedMortgage = (function () {
    function deedMortgage() {
    }
    return deedMortgage;
}());



/***/ }),

/***/ "../../../../../src/app/models/exelModels.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return headerLaons; });
var headerLaons = [
    {
        "name": "",
        "prePayPenalty": "",
        "registerswMers": "",
        "ballonPayement": "",
        "onForbearancePlan": "",
        "loanTermsModified": "",
        "originationDate": "",
        "paidToDate": "",
        "nextPayementDate": "",
        "payersLastPayementMadeDate": "",
        "originalLoanAmount": "",
        "unPaidPrincipalBalance": "",
        "noteMaturityDate": "",
        "accruedLateCharges": "",
        "loanCharges": "",
        "ofPayementsLast12": "",
        "firstPayementDate": "",
        "noteInterestRate": "",
        "soldInterestRate": "",
        "lateCharge": "",
        "afterDays": "",
        "UnpaidInterest": "",
        "propertyTaxesDue": "",
        "payementTrust": "",
        "PI": "",
        "TotalMonthlyLoanPayement": "",
        "comments": "",
        "askingPrice": "",
        "percentagePrice": "",
        "nextAdjustement": "",
        "adjPayementChangeDate": "",
        "adjustementFrequency": "",
        "floor": "",
        "indexName": "",
        "margin": "",
        "ceiling": "",
        "drawPeriodStartDate": "",
        "repaymentPeriodStartDate": ""
    }
];


/***/ }),

/***/ "../../../../../src/app/models/loans.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export loans */
//import {statut,Users,deedMortgage} from '../structExports/models';
var loans = (function () {
    function loans() {
    }
    return loans;
}());



/***/ }),

/***/ "../../../../../src/app/models/statut.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export statut */
var statut = (function () {
    function statut() {
    }
    return statut;
}());



/***/ }),

/***/ "../../../../../src/app/models/submitoffer.viewmodel.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubmitOfferViewModel; });
var SubmitOfferViewModel = (function () {
    function SubmitOfferViewModel(model) {
        this._observers = [];
        this._closingType = 0;
        this._dueDiligenceOption = 0;
        this._offerAmount = 0;
        this._daysCount = 3;
        this._loanNoteId = 0;
        this._loanNoteId = model.loanNoteId || 0;
        this.Comment = model.comment || "";
        this.OfferAmount = model.offerAmount || 0;
        this.ClosingType = model.closingType || [];
        this.DueDiligenceOption = model.dueDiligenceOption || 0;
    }
    Object.defineProperty(SubmitOfferViewModel.prototype, "LoanNoteId", {
        get: function () {
            return this._loanNoteId;
        },
        set: function (value) {
            this._loanNoteId = value;
            this.NotifyObservers({ property: 'LoanNoteId', value: value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SubmitOfferViewModel.prototype, "ClosingType", {
        get: function () {
            return this._closingType;
        },
        set: function (value) {
            this._closingType = value;
            this.NotifyObservers({ property: 'ClosingType', value: value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SubmitOfferViewModel.prototype, "DueDiligenceOption", {
        get: function () {
            return this._dueDiligenceOption;
        },
        set: function (value) {
            this._dueDiligenceOption = value;
            this.NotifyObservers({ property: 'DueDilligenceOption', value: value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SubmitOfferViewModel.prototype, "DaysCount", {
        get: function () {
            return this._daysCount;
        },
        set: function (value) {
            this._daysCount = value;
            this.NotifyObservers({ property: 'DaysCount', value: value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SubmitOfferViewModel.prototype, "OfferAmount", {
        get: function () {
            return this._offerAmount;
        },
        set: function (value) {
            this._offerAmount = value;
            this.NotifyObservers({ property: 'OfferAmount', value: value });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SubmitOfferViewModel.prototype, "Comment", {
        get: function () {
            return this._comment;
        },
        set: function (value) {
            this.NotifyObservers({ property: 'Comment', value: value });
            this._comment = value;
        },
        enumerable: true,
        configurable: true
    });
    SubmitOfferViewModel.prototype.getOfferPOCO = function () {
        return { ClosingTypeId: this.ClosingType, OfferTypeId: 1, OfferAmount: this.OfferAmount, Comment: this.Comment, loanNoteId: this._loanNoteId, DueDiligenceOptionId: this.DueDiligenceOption, DaysCount: this.DaysCount };
    };
    /* IObservable iplementation members*/
    SubmitOfferViewModel.prototype.RegisterObserver = function (observer) {
        this._observers.push(observer);
    };
    SubmitOfferViewModel.prototype.RemoveObserver = function (observer) {
        for (var i = 0; i < this._observers.length; i++)
            if (this._observers[i] === observer)
                this._observers.slice(i, 1);
    };
    SubmitOfferViewModel.prototype.NotifyObservers = function (message) {
        for (var i = 0; i < this._observers.length; i++)
            this._observers[i].ReceiveNotification(message);
    };
    return SubmitOfferViewModel;
}());



/***/ }),

/***/ "../../../../../src/app/models/users.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Users */
var Users = (function () {
    function Users() {
    }
    return Users;
}());



/***/ }),

/***/ "../../../../../src/app/routings/routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRouting; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__structExports_components__ = __webpack_require__("../../../../../src/app/structExports/components.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__ = __webpack_require__("../../../../../src/app/guards/auth.guard.ts");



var routes = [
    {
        path: '',
        redirectTo: 'about-us',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["t" /* loginComponent */]
    },
    {
        path: 'buy-notes',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["q" /* buyNotesComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* authGuard */]]
    },
    {
        path: 'my-exchange',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["u" /* myExchangeComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* authGuard */]]
    },
    {
        path: 'buy-note/:id',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["s" /* loanNoteDetailComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* authGuard */]]
    },
    {
        path: 'SubmitOffer/:id',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["m" /* SubmitOfferComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* authGuard */]]
    },
    {
        path: 'register',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["i" /* RegisterComponent */],
    },
    {
        path: 'transaction',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["v" /* transactionComponent */]
    },
    {
        path: 'about-us',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["a" /* AboutUsComponent */],
    },
    {
        path: 'contact-us',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["b" /* ContactUsComponent */],
    },
    {
        path: 'faq',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["c" /* FaqComponent */],
    },
    {
        path: 'services',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["l" /* ServicesComponent */],
    },
    {
        path: 'sellnote',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["k" /* SellnoteComponent */],
    },
    {
        path: 'terms-and-conditions',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["n" /* TermsAndConditionsComponent */],
    },
    {
        path: 'trading-terms',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["o" /* TradingTermsComponent */],
    },
    {
        path: 'faq/how-to-buy-notes',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["e" /* HowToBuyNotesComponent */],
    },
    {
        path: 'faq/how-to-sell-notes',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["f" /* HowToSellNotesComponent */],
    },
    {
        path: 'payment',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["h" /* PaymentComponent */],
    },
    {
        path: 'forgotpassword',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["d" /* ForgotPasswordComponent */],
    },
    {
        path: 'resetpassword/:code',
        component: __WEBPACK_IMPORTED_MODULE_1__structExports_components__["j" /* ResetPasswordComponent */],
    },
];
var AppRouting = __WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forRoot(routes);


/***/ }),

/***/ "../../../../../src/app/services/alerts.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return alertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__structExports_models__ = __webpack_require__("../../../../../src/app/structExports/models.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var alertService = (function () {
    function alertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"]();
        this.keepAfterRouteChange = false;
        router.events.subscribe(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationStart */]) {
                if (_this.keepAfterRouteChange) {
                    _this.keepAfterRouteChange = false;
                }
                else {
                    _this.clear();
                }
            }
        });
    }
    alertService.prototype.getAlert = function () {
        return this.subject.asObservable();
    };
    alertService.prototype.alert = function (type, message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.keepAfterRouteChange = keepAfterRouteChange;
        this.subject.next({ type: type, message: message });
    };
    alertService.prototype.success = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__structExports_models__["a" /* AlertType */].Success, message, keepAfterRouteChange);
    };
    alertService.prototype.error = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__structExports_models__["a" /* AlertType */].Error, message, keepAfterRouteChange);
    };
    alertService.prototype.info = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__structExports_models__["a" /* AlertType */].Info, message, keepAfterRouteChange);
    };
    alertService.prototype.warn = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__structExports_models__["a" /* AlertType */].Warning, message, keepAfterRouteChange);
    };
    alertService.prototype.clear = function () {
        this.subject.next();
    };
    alertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], alertService);
    return alertService;
}());



/***/ }),

/***/ "../../../../../src/app/services/authentication.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return authentificationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var authentificationService = (function () {
    //private _currentUser: BehaviorSubject<string>;
    function authentificationService(_http, httptoken) {
        this._http = _http;
        this.httptoken = httptoken;
        this._http = _http;
        this.urlApi = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Token";
        this.urlInfoUser = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/account/userinfo";
    }
    authentificationService.prototype.login = function (username, password, rememberme) {
        var body = "username=" + username + "&password=" + password + "&grant_type=password" + "&rm=" + rememberme;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this._http.post(this.urlApi, body, options).map(function (res) {
            var token = res.json().access_token;
            if (token) {
                // console.log(token);
                localStorage.setItem('currentuser', JSON.stringify({
                    username: res.json().userName, access_token: res.json().access_token,
                    expires_in: res.json().expires_in, token_type: res.json().token_type,
                    userId: res.json().userId, role: res.json().role
                }));
                localStorage.setItem('currentUserInfo', JSON.stringify({ userId: res.json().userId, role: res.json().role }));
                return true;
            }
            else {
                return false;
            }
        });
    };
    authentificationService.prototype.getCurrentUser = function () {
        var info = JSON.parse(localStorage.getItem('currentuser'));
        //console.log(info);
        if (localStorage.getItem('currentuser')) {
            var info_1 = JSON.parse(localStorage.getItem('currentuser'));
            this.currentUser = info_1.username;
        }
        return this.currentUser;
    };
    authentificationService.prototype.logout = function () {
        localStorage.clear();
    };
    authentificationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], authentificationService);
    return authentificationService;
}());



/***/ }),

/***/ "../../../../../src/app/services/buyNotes.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return buyNotesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var buyNotesService = (function () {
    function buyNotesService(_httpServ, http) {
        this._httpServ = _httpServ;
        this.http = http;
        this.url = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loanNotes";
        if (localStorage.currentuser) {
            var infoCurrentUser = JSON.parse(localStorage.currentuser);
            var token = infoCurrentUser.access_token;
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', "Bearer " + token);
            this.optionsReq = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
            this.optionsReq.headers = headers;
        }
    }
    buyNotesService.prototype.getAllLoanNotes = function () {
        return this.http.get(this.url, this.optionsReq).map(function (res) { return res.json(); });
    };
    buyNotesService.prototype.getNonPublishedLoanNotes = function () {
        return this._httpServ.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/NonPublishedloanNotes").map(function (res) { return res.json(); });
    };
    buyNotesService.prototype.changePulishedLoanNotes = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this._httpServ.put(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/ChangePublishedStatus/" + id, "", options).map(function (res) { return res; });
    };
    buyNotesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__["a" /* httpServiceToken */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], buyNotesService);
    return buyNotesService;
}());



/***/ }),

/***/ "../../../../../src/app/services/contactUs.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return contactUsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var contactUsService = (function () {
    // ,private _httpToken:httpServiceToken
    function contactUsService(http, _http) {
        this.http = http;
        this._http = _http;
        this.urlApi = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/ContactUs";
    }
    //  getDropDownData(apiUrl:string){
    //         return this._httpToken.get(apiUrl).map((res :Response) => res.json());
    //   }
    contactUsService.prototype.contactUs = function (ObjectContactUs) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var opts = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        opts.headers = headers;
        return this.http.post(this.urlApi, ObjectContactUs, opts).map(function (res) { return res.json(); });
    };
    contactUsService.prototype.getUserInfo = function () {
        return this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/info").map(function (res) { return res.json(); });
    };
    contactUsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], contactUsService);
    return contactUsService;
}());



/***/ }),

/***/ "../../../../../src/app/services/exel.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcelService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver__ = __webpack_require__("../../../../file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_file_saver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_file_saver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx__ = __webpack_require__("../../../../xlsx/xlsx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_xlsx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_xlsx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ExcelService = (function () {
    function ExcelService() {
        this.wopts = { bookType: 'xlsx', type: 'array' };
    }
    ExcelService.prototype.exportAsExcelFile = function (json, excelFileName) {
        var worksheet = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].json_to_sheet(json);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = __WEBPACK_IMPORTED_MODULE_2_xlsx__["write"](workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.exportLiveTape = function (json, excelFileName) {
        var worksheet = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].json_to_sheet(json);
        var loannoteID;
        var range = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].decode_range(worksheet['!ref']);
        for (var R = range.s.r + 1; R <= range.e.r; ++R) {
            for (var C = range.s.c; C <= range.e.c; ++C) {
                var cell_address = { c: C, r: R };
                var cell_ref = __WEBPACK_IMPORTED_MODULE_2_xlsx__["utils"].encode_cell(cell_address);
                if (cell_ref.indexOf("A") >= 0) {
                    loannoteID = worksheet[cell_ref].v;
                }
                if (cell_ref.indexOf("Q") >= 0) {
                    worksheet[cell_ref].s = {
                        fgColor: { theme: 8, tint: 0.3999755851924192, rgb: '9ED2E0' }
                    };
                    //console.log("||||==>"+loannoteID);
                    worksheet[cell_ref].l = { Target: "https://exchangeloans.azurewebsites.net/SubmitOffer/" + loannoteID, Tooltip: "SubmitOffer" };
                    //  console.log("****==>"+JSON.stringify(worksheet[cell_ref].l));
                }
            }
        }
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = __WEBPACK_IMPORTED_MODULE_2_xlsx__["write"](workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ExcelService.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        __WEBPACK_IMPORTED_MODULE_1_file_saver__["saveAs"](data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    };
    ExcelService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], ExcelService);
    return ExcelService;
}());



/***/ }),

/***/ "../../../../../src/app/services/httpServiceToken.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return httpServiceToken; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpServiceToken = (function (_super) {
    __extends(httpServiceToken, _super);
    function httpServiceToken(backend, options) {
        var _this = this;
        if (localStorage.currentuser) {
            var infoCurrentUser = JSON.parse(localStorage.currentuser);
            var token = infoCurrentUser.access_token; // your custom token getter function here
            options.headers.set('Authorization', "Bearer " + token);
            _this = _super.call(this, backend, options) || this;
        }
        return _this;
    }
    httpServiceToken.prototype.request = function (url, options) {
        var infoCurrentUser = JSON.parse(localStorage.currentuser);
        var token = infoCurrentUser.access_token;
        ;
        if (typeof url === 'string') {
            if (!options) {
                // let's make option object
                options = { headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]() };
            }
            options.headers.set('Authorization', "Bearer " + token);
        }
        else {
            // we have to add the token to the url object
            url.headers.set('Authorization', "Bearer " + token);
        }
        return _super.prototype.request.call(this, url, options).catch(this.catchAuthError(this));
    };
    httpServiceToken.prototype.catchAuthError = function (self) {
        // we have to pass httpServiceToken's own instance here as `self`
        return function (res) {
            console.log(res);
            if (res.status === 401 || res.status === 403) {
                // if not authenticated
                console.log(res);
            }
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(res);
        };
    };
    httpServiceToken = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* XHRBackend */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]])
    ], httpServiceToken);
    return httpServiceToken;
}(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]));



/***/ }),

/***/ "../../../../../src/app/services/loading.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var loadingService = (function () {
    function loadingService() {
        this.visible = false;
    }
    loadingService.prototype.hide = function () { this.visible = false; };
    loadingService.prototype.show = function () { this.visible = true; };
    loadingService.prototype.toggle = function () { this.visible = !this.visible; };
    loadingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], loadingService);
    return loadingService;
}());



/***/ }),

/***/ "../../../../../src/app/services/loanNoteDetail.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loanNoteDetailService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var loanNoteDetailService = (function () {
    function loanNoteDetailService(_http) {
        this._http = _http;
    }
    loanNoteDetailService.prototype.getLoanNoteAllDetail = function (id) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].forkJoin(this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loans/" + id + "?includes=loanStatus;loanType;amortizationType;rateType;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;deedMortgage;escrowImpounds;").map(function (res) { return res.json(); }), this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loans/" + id + "/bankruptcy").map(function (res) { return res.json(); }), this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loans/" + id + "/Property").map(function (res) { return res.json(); }));
    };
    loanNoteDetailService.prototype.getAllNotes = function (id) {
        return this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loans?includes=loanStatus;").map(function (res) { return res.json(); });
    };
    loanNoteDetailService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], loanNoteDetailService);
    return loanNoteDetailService;
}());

//loanStatus;loanType;amortizationType;rateType;bankruptcy;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;


/***/ }),

/***/ "../../../../../src/app/services/myExchange.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return myExchangeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var myExchangeService = (function () {
    function myExchangeService(_httpServ) {
        this._httpServ = _httpServ;
        this.url = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/loanNotes";
        var currentUserInfo = JSON.parse(localStorage.currentUserInfo);
        this.userId = currentUserInfo.userId;
        this.urlOffers = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/buyer/";
        this.urlOffersByLoan = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/offersloan/";
    }
    myExchangeService.prototype.getMyLoans = function () {
        return this._httpServ.get(this.url + ("?UserId=" + this.userId)).map(function (res) { return res.json(); });
    };
    myExchangeService.prototype.getMyOffers = function () {
        return this._httpServ.get(this.urlOffers + (this.userId + "/offers")).map(function (res) { return res.json(); });
    };
    myExchangeService.prototype.getOffersByLoan = function (id) {
        return this._httpServ.get(this.urlOffersByLoan + ("" + id)).map(function (res) { return res.json(); });
    };
    myExchangeService.prototype.setLoansExel = function (data) {
        return this._httpServ.post(this.url, data).map(function (res) { return res.json(); });
    };
    myExchangeService.prototype.changePassword = function (oldpassword, newpassword, confirmpassword) {
        var infoCurrentUser = JSON.parse(localStorage.currentuser);
        var userId = infoCurrentUser.userId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "OldPassword=" + oldpassword + "&NewPassword=" + newpassword + "&ConfirmPassword=" + confirmpassword + "&UserId=" + userId;
        return this._httpServ.post(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Account/ChangePassword", body, options).map(function (res) {
            return res;
        });
    };
    myExchangeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], myExchangeService);
    return myExchangeService;
}());



/***/ }),

/***/ "../../../../../src/app/services/navbar.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarService = (function () {
    function NavbarService() {
        this.visible = false;
    }
    NavbarService.prototype.hide = function () { this.visible = false; };
    NavbarService.prototype.show = function () { this.visible = true; };
    NavbarService.prototype.toggle = function () { this.visible = !this.visible; };
    NavbarService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], NavbarService);
    return NavbarService;
}());



/***/ }),

/***/ "../../../../../src/app/services/pager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return pagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);

var pagerService = (function () {
    function pagerService() {
    }
    pagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
        if (currentPage === void 0) { currentPage = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        var totalPages = Math.ceil(totalItems / pageSize);
        var startPage, endPage;
        if (totalPages <= 10) {
            startPage = 1;
            endPage = totalPages;
        }
        else {
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            }
            else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            }
            else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        var pages = __WEBPACK_IMPORTED_MODULE_0_lodash__["range"](startPage, endPage + 1);
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    };
    return pagerService;
}());



/***/ }),

/***/ "../../../../../src/app/services/payment.sevice.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return paymentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var paymentService = (function () {
    // ,private _httpToken:httpServiceToken
    function paymentService(http, _http) {
        this.http = http;
        this._http = _http;
        this.urlApi = __WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "/api/Result";
    }
    //  getDropDownData(apiUrl:string){
    //         return this._httpToken.get(apiUrl).map((res :Response) => res.json());
    //   }
    paymentService.prototype.Payment = function (ObjectPayment) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var opts = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        opts.headers = headers;
        return this.http.post(this.urlApi, ObjectPayment, opts).map(function (res) { return res.json(); });
    };
    paymentService.prototype.getUserInfo = function () {
        return this._http.get(__WEBPACK_IMPORTED_MODULE_4__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "/api/info").map(function (res) { return res.json(); });
    };
    paymentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_2__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], paymentService);
    return paymentService;
}());



/***/ }),

/***/ "../../../../../src/app/services/register.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return registerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var registerService = (function () {
    function registerService(http) {
        this.http = http;
        this.urlApi = __WEBPACK_IMPORTED_MODULE_3__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Account/Register";
    }
    registerService.prototype.register = function (ObjectRegister) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        var opts = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]();
        opts.headers = headers;
        return this.http.post(this.urlApi, ObjectRegister, opts).map(function (res) { return res.json(); });
    };
    registerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], registerService);
    return registerService;
}());



/***/ }),

/***/ "../../../../../src/app/services/sellnote.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SellnoteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SellnoteService = (function () {
    function SellnoteService(_httpToken) {
        this._httpToken = _httpToken;
    }
    SellnoteService.prototype.getDropDownData = function (apiUrl) {
        return this._httpToken.get(apiUrl).map(function (res) { return res.json(); });
    };
    SellnoteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_httpServiceToken_service__["a" /* httpServiceToken */]])
    ], SellnoteService);
    return SellnoteService;
}());



/***/ }),

/***/ "../../../../../src/app/services/transaction.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return transactionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_AppSettings__ = __webpack_require__("../../../../../src/app/config/AppSettings.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var transactionService = (function () {
    function transactionService(http) {
        this.http = http;
        this.optionsReq = this.addJwtToHtpp();
    }
    transactionService.prototype.getTransaction = function (OfferId) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + ("api/Transaction/" + OfferId), this.optionsReq).map(function (res) { return res.json(); });
    };
    transactionService.prototype.postStepOne = function (ObjectStep) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Transaction/StepOne", ObjectStep, this.optionsReq).map(function (res) { return res.json(); });
    };
    transactionService.prototype.postStepTow = function (ObjectStep) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Transaction/StepTwo", ObjectStep, this.optionsReq).map(function (res) { return res.json(); });
    };
    transactionService.prototype.postStepTheree = function (ObjectStep) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Transaction/StepTheree", ObjectStep, this.optionsReq).map(function (res) { return res.json(); });
    };
    transactionService.prototype.postStepThereeInformation = function (objectInfo) {
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__config_AppSettings__["a" /* AppSettings */].API_ENDPOINT + "api/Transaction/StepThereeInformation", objectInfo, this.optionsReq).map(function (res) { return res.json(); });
    };
    transactionService.prototype.addJwtToHtpp = function () {
        if (localStorage.currentuser) {
            var infoCurrentUser = JSON.parse(localStorage.currentuser);
            var token = infoCurrentUser.access_token;
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', "Bearer " + token);
            return new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        }
    };
    transactionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], transactionService);
    return transactionService;
}());



/***/ }),

/***/ "../../../../../src/app/structExports/components.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "t", function() { return __WEBPACK_IMPORTED_MODULE_0__components_login_login_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_loading_loading_component__ = __webpack_require__("../../../../../src/app/components/loading/loading.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "r", function() { return __WEBPACK_IMPORTED_MODULE_1__components_loading_loading_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_2__components_navbar_navbar_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_buyNotes_buyNotes_component__ = __webpack_require__("../../../../../src/app/components/buyNotes/buyNotes.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "q", function() { return __WEBPACK_IMPORTED_MODULE_3__components_buyNotes_buyNotes_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_alerts_alerts_component__ = __webpack_require__("../../../../../src/app/components/alerts/alerts.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_4__components_alerts_alerts_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_myExchange_myExchange_component__ = __webpack_require__("../../../../../src/app/components/myExchange/myExchange.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "u", function() { return __WEBPACK_IMPORTED_MODULE_5__components_myExchange_myExchange_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_loanNoteDetail_loanNoteDetail_component__ = __webpack_require__("../../../../../src/app/components/loanNoteDetail/loanNoteDetail.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "s", function() { return __WEBPACK_IMPORTED_MODULE_6__components_loanNoteDetail_loanNoteDetail_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_submitoffer_submitoffer_component__ = __webpack_require__("../../../../../src/app/components/submitoffer/submitoffer.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_7__components_submitoffer_submitoffer_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_8__components_register_register_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_about_us_about_us_component__ = __webpack_require__("../../../../../src/app/components/about-us/about-us.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_9__components_about_us_about_us_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_contact_us_contact_us_component__ = __webpack_require__("../../../../../src/app/components/contact-us/contact-us.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_10__components_contact_us_contact_us_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_services_services_component__ = __webpack_require__("../../../../../src/app/components/services/services.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_11__components_services_services_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_transaction_transaction_component__ = __webpack_require__("../../../../../src/app/components/transaction/transaction.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "v", function() { return __WEBPACK_IMPORTED_MODULE_12__components_transaction_transaction_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_sellnote_sellnote_component__ = __webpack_require__("../../../../../src/app/components/sellnote/sellnote.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_13__components_sellnote_sellnote_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_faq_faq_component__ = __webpack_require__("../../../../../src/app/components/faq/faq.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_14__components_faq_faq_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_terms_and_conditions_terms_and_conditions_component__ = __webpack_require__("../../../../../src/app/components/terms-and-conditions/terms-and-conditions.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_15__components_terms_and_conditions_terms_and_conditions_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_trading_terms_trading_terms_component__ = __webpack_require__("../../../../../src/app/components/trading-terms/trading-terms.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_16__components_trading_terms_trading_terms_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_how_to_buy_notes_how_to_buy_notes_component__ = __webpack_require__("../../../../../src/app/components/how-to-buy-notes/how-to-buy-notes.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_17__components_how_to_buy_notes_how_to_buy_notes_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_how_to_sell_notes_how_to_sell_notes_component__ = __webpack_require__("../../../../../src/app/components/how-to-sell-notes/how-to-sell-notes.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_18__components_how_to_sell_notes_how_to_sell_notes_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_payment_payment_component__ = __webpack_require__("../../../../../src/app/components/payment/payment.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_19__components_payment_payment_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_forgotPassword_forgotPassword_component__ = __webpack_require__("../../../../../src/app/components/forgotPassword/forgotPassword.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_20__components_forgotPassword_forgotPassword_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_resetPassword_resetPassword_component__ = __webpack_require__("../../../../../src/app/components/resetPassword/resetPassword.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_21__components_resetPassword_resetPassword_component__["a"]; });
// this file to structure the exports of ours components
























/***/ }),

/***/ "../../../../../src/app/structExports/models.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_alerts__ = __webpack_require__("../../../../../src/app/models/alerts.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__models_alerts__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_loans__ = __webpack_require__("../../../../../src/app/models/loans.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_users__ = __webpack_require__("../../../../../src/app/models/users.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_statut__ = __webpack_require__("../../../../../src/app/models/statut.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_deedMortgage__ = __webpack_require__("../../../../../src/app/models/deedMortgage.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_Offers__ = __webpack_require__("../../../../../src/app/models/Offers.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_exelModels__ = __webpack_require__("../../../../../src/app/models/exelModels.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_6__models_exelModels__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__ = __webpack_require__("../../../../../src/app/models/allSelect2Static.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["h"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_7__models_allSelect2Static__["i"]; });
// this file to structure the exports of ours models










/***/ }),

/***/ "../../../../../src/app/structExports/services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_navbar_service__ = __webpack_require__("../../../../../src/app/services/navbar.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__services_navbar_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_loading_service__ = __webpack_require__("../../../../../src/app/services/loading.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_1__services_loading_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_pager_service__ = __webpack_require__("../../../../../src/app/services/pager.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_2__services_pager_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_authentication_service__ = __webpack_require__("../../../../../src/app/services/authentication.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_3__services_authentication_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_httpServiceToken_service__ = __webpack_require__("../../../../../src/app/services/httpServiceToken.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_4__services_httpServiceToken_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_buyNotes_service__ = __webpack_require__("../../../../../src/app/services/buyNotes.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_5__services_buyNotes_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alerts_service__ = __webpack_require__("../../../../../src/app/services/alerts.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_6__services_alerts_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_exel_service__ = __webpack_require__("../../../../../src/app/services/exel.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_7__services_exel_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_myExchange_service__ = __webpack_require__("../../../../../src/app/services/myExchange.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_8__services_myExchange_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_loanNoteDetail_service__ = __webpack_require__("../../../../../src/app/services/loanNoteDetail.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_9__services_loanNoteDetail_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_register_service__ = __webpack_require__("../../../../../src/app/services/register.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_10__services_register_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_transaction_service__ = __webpack_require__("../../../../../src/app/services/transaction.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_11__services_transaction_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_sellnote_service__ = __webpack_require__("../../../../../src/app/services/sellnote.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_12__services_sellnote_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_payment_sevice__ = __webpack_require__("../../../../../src/app/services/payment.sevice.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_13__services_payment_sevice__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_contactUs_service__ = __webpack_require__("../../../../../src/app/services/contactUs.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_14__services_contactUs_service__["a"]; });
// this file to structure the exports of ours Services

















/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map