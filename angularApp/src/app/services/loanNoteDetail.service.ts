import { Injectable } from '@angular/core';
import { Http,Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import { httpServiceToken } from '../services/httpServiceToken.service';
import {AppSettings} from '../config/AppSettings';
@Injectable()
export class loanNoteDetailService {

  constructor(private _http : httpServiceToken) {  }

  getLoanNoteAllDetail(id: number) {
    return Observable.forkJoin(
      this._http.get(AppSettings.API_ENDPOINT+"api/loans/"+id+"?includes=loanStatus;loanType;amortizationType;rateType;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;deedMortgage;escrowImpounds;").map(res => res.json()),
      this._http.get(AppSettings.API_ENDPOINT+"api/loans/"+id+"/bankruptcy").map(res => res.json()),
      this._http.get(AppSettings.API_ENDPOINT+"api/loans/"+id+"/Property").map((res :Response) => res.json())
    );
  }  

    getAllNotes(id : number){
    
        return this._http.get(AppSettings.API_ENDPOINT+"api/loans?includes=loanStatus;").map((res :Response) => res.json());
    }

}



 
//loanStatus;loanType;amortizationType;rateType;bankruptcy;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;
