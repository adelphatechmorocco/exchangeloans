import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { httpServiceToken } from '../services/httpServiceToken.service';
import 'rxjs/add/operator/map';
import { from } from 'rxjs/observable/from';
import {AppSettings} from '../config/AppSettings';

@Injectable()

export class authentificationService {
    private  urlApi: string;
    private  urlInfoUser:string;
    private currentUser:string;
    //private _currentUser: BehaviorSubject<string>;
   
    constructor(private _http: Http,private httptoken:httpServiceToken) {
         this._http = _http;
         this.urlApi = AppSettings.API_ENDPOINT+"api/Token";
         this.urlInfoUser =  AppSettings.API_ENDPOINT+"api/account/userinfo";

    }

    login(username: string, password: string, rememberme:boolean) :Observable<boolean> {
        let body = "username=" + username + "&password=" + password + "&grant_type=password" + "&rm=" + rememberme;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post(this.urlApi, body, options).map(
            (res: Response) => {
                let token = res.json().access_token;
                           
                if (token) {
                   // console.log(token);
                    localStorage.setItem('currentuser', JSON.stringify({
                        username: res.json().userName, access_token: res.json().access_token,
                        expires_in: res.json().expires_in, token_type: res.json().token_type,
                        userId: res.json().userId, role: res.json().role
                    }));
                    localStorage.setItem('currentUserInfo',JSON.stringify( {userId: res.json().userId, role: res.json().role}));
                    return true;
                  
                } else {
                    return false;
                }

            },

        );
    }
    getCurrentUser(): string{
            
        let info = JSON.parse(localStorage.getItem('currentuser'));
        //console.log(info);
        if(localStorage.getItem('currentuser'))
        {
          let info = JSON.parse(localStorage.getItem('currentuser'));
         this.currentUser = info.username;
        }    
        return this.currentUser; 
      }
    

    logout(): void {
        localStorage.clear();
    }
  


}
