import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';


@Injectable()
export class ExcelService {

    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
 

  constructor() { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }


  public exportLiveTape(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
       
  
   
    var loannoteID;
    var range = XLSX.utils.decode_range(worksheet['!ref']);
    for(var R = range.s.r+1; R <= range.e.r; ++R) {
      
          for(var C = range.s.c; C <= range.e.c; ++C) {
                var cell_address = {c:C, r:R};
                var cell_ref = XLSX.utils.encode_cell(cell_address);
                if(cell_ref.indexOf("A")>=0){
                     loannoteID = worksheet[cell_ref].v;
                }
                if(cell_ref.indexOf("Q")>=0){
                      worksheet[cell_ref].s = {
                        fgColor: { theme: 8, tint: 0.3999755851924192, rgb: '9ED2E0' }
                      }
                      //console.log("||||==>"+loannoteID);
                      
                      worksheet[cell_ref].l = { Target:"https://exchangeloans.azurewebsites.net/SubmitOffer/"+loannoteID, Tooltip:"SubmitOffer" };
                    //  console.log("****==>"+JSON.stringify(worksheet[cell_ref].l));
                }
        }
    }

    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });

    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}