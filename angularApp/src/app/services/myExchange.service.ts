import { Injectable } from '@angular/core';
import { Http,Response,RequestOptions,Headers } from '@angular/http';
import  'rxjs/add/operator/map';
import { httpServiceToken } from '../services/httpServiceToken.service';
import {AppSettings} from '../config/AppSettings';

@Injectable()
export class myExchangeService {

 private url:string;
 private urlOffers:string;
 private userId:string;
 private urlOffersByLoan:string;
 
 constructor(private _httpServ: httpServiceToken) {
    this.url            = AppSettings.API_ENDPOINT+"api/loanNotes";
    let currentUserInfo = JSON.parse(localStorage.currentUserInfo);
    this.userId         = currentUserInfo.userId;
    this.urlOffers      = AppSettings.API_ENDPOINT+"api/buyer/";
   this.urlOffersByLoan = AppSettings.API_ENDPOINT+"api/offersloan/";
   }
 

 getMyLoans(){
   
     return this._httpServ.get(this.url+`?UserId=${this.userId}`).map((res :Response) => res.json());
 }
 
 getMyOffers(){
   return this._httpServ.get(this.urlOffers+`${this.userId}/offers`).map((res :Response) => res.json());
  }
  getOffersByLoan(id:number){

   return this._httpServ.get(this.urlOffersByLoan+`${id}`).map((res:Response) => res.json());
  }
 setLoansExel(data:any){
     return this._httpServ.post(this.url,data).map((res:Response) => res.json());
  }

  changePassword(oldpassword: string, newpassword: string, confirmpassword: string){
   let infoCurrentUser =JSON.parse(localStorage.currentuser);
   let userId = infoCurrentUser.userId;
   let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
   let options = new RequestOptions({ headers: headers });
   let body = "OldPassword=" + oldpassword + "&NewPassword=" + newpassword + "&ConfirmPassword="+confirmpassword +"&UserId="+userId;
   return this._httpServ.post(AppSettings.API_ENDPOINT+"api/Account/ChangePassword",body,options).map((res : Response) => {
       
       return res;

   });
}

}