import {Injectable} from '@angular/core';
import {Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {AppSettings} from '../config/AppSettings';

@Injectable()
export class httpServiceToken extends Http {

  constructor (backend: XHRBackend, options: RequestOptions) {
    if(localStorage.currentuser){
      let infoCurrentUser =JSON.parse(localStorage.currentuser);
      let token = infoCurrentUser.access_token; // your custom token getter function here
      options.headers.set('Authorization', `Bearer ${token}`);
      super(backend, options);
    }
  
  }

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response>{
    let infoCurrentUser =JSON.parse(localStorage.currentuser);
    let token = infoCurrentUser.access_token;;
    if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
      if (!options) {
        // let's make option object
        options = {headers: new Headers()};
      }
      options.headers.set('Authorization', `Bearer ${token}`);
    } else {
    // we have to add the token to the url object
      url.headers.set('Authorization', `Bearer ${token}`);
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }

  private catchAuthError (self: httpServiceToken) {
    // we have to pass httpServiceToken's own instance here as `self`
    return (res: Response) => {
      console.log(res);
      if (res.status === 401 || res.status === 403) {
        // if not authenticated
        console.log(res);
      }
      return Observable.throw(res);
    };
  }
}