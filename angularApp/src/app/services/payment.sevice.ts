import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';
import { httpServiceToken } from '../services/httpServiceToken.service';
import 'rxjs/add/operator/map';
import { AppSettings} from '../config/AppSettings'



@Injectable()

export class paymentService {
    private urlApi:string;
    // ,private _httpToken:httpServiceToken
	constructor(private http:Http,private _http : httpServiceToken){
		this.urlApi = AppSettings.API_ENDPOINT+"/api/Result";
	}
//  getDropDownData(apiUrl:string){

//         return this._httpToken.get(apiUrl).map((res :Response) => res.json());

//   }
	Payment(ObjectPayment:any){
		let headers : Headers = new Headers();
		headers.append('Content-Type','application/json');
		let opts : RequestOptions= new RequestOptions();
        opts.headers = headers; 
        
		return this.http.post(this.urlApi,ObjectPayment,opts).map(res => res.json());
	}
	getUserInfo(){
    
		return this._http.get(AppSettings.API_ENDPOINT+"/api/info").map((res :Response) => res.json());
	}

}