import { Injectable } from '@angular/core';
import { httpServiceToken } from '../services/httpServiceToken.service';
import { Http ,Response} from '@angular/http';
import 'rxjs/add/operator/map';



@Injectable()
export class SellnoteService {


  constructor(private _httpToken:httpServiceToken) { }

  getDropDownData(apiUrl:string){

        return this._httpToken.get(apiUrl).map((res :Response) => res.json());

  }

}
