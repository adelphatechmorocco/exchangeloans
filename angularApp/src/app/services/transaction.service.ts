import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';
import { httpServiceToken } from '../services/httpServiceToken.service';
import { AppSettings } from '../config/AppSettings';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()

export class transactionService {
	private optionsReq: RequestOptions;
	constructor(private http: Http) {
		this.optionsReq = this.addJwtToHtpp();
	}

	getTransaction(OfferId: number) {
		return this.http.get(AppSettings.API_ENDPOINT + `api/Transaction/${OfferId}`,this.optionsReq).map((res :Response) => res.json()); 
	}


	postStepOne(ObjectStep:any){
		return this.http.post(AppSettings.API_ENDPOINT +"api/Transaction/StepOne",ObjectStep,this.optionsReq).map((res:Response) => res.json() );
	}

	postStepTow(ObjectStep:any){
        return this.http.post(AppSettings.API_ENDPOINT +"api/Transaction/StepTwo",ObjectStep,this.optionsReq).map((res:Response) => res.json() );
    }
    postStepTheree(ObjectStep:any){
        return this.http.post(AppSettings.API_ENDPOINT +"api/Transaction/StepTheree",ObjectStep,this.optionsReq).map((res:Response) => res.json() );
	}
	postStepThereeInformation(objectInfo:any){
		return this.http.post(AppSettings.API_ENDPOINT+"api/Transaction/StepThereeInformation",objectInfo,this.optionsReq).map((res: Response) => res.json());
	}

	private addJwtToHtpp() {
		if (localStorage.currentuser) {
			let infoCurrentUser = JSON.parse(localStorage.currentuser);
			let token = infoCurrentUser.access_token;
			let headers: Headers = new Headers();
			headers.append('Authorization', `Bearer ${token}`);
			return new RequestOptions({ headers: headers });
		}
	}


}


