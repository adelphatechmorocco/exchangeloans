import { Injectable } from '@angular/core';
import { Http ,Response,RequestOptions ,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { httpServiceToken } from '../services/httpServiceToken.service';
import {AppSettings} from '../config/AppSettings';
@Injectable()
export class buyNotesService {

  private url: string ;
  private optionsReq : RequestOptions;

  constructor(private _httpServ: httpServiceToken,private http : Http) {
    this.url = AppSettings.API_ENDPOINT+"api/loanNotes";
  
    if(localStorage.currentuser){
      let infoCurrentUser =JSON.parse(localStorage.currentuser);
      let token = infoCurrentUser.access_token; 
      let headers : Headers = new Headers();
	  	headers.append('Authorization', `Bearer ${token}`);
		  this.optionsReq = new RequestOptions();
	    this.optionsReq.headers = headers;     
    }

   }
  getAllLoanNotes(){
   return this.http.get(this.url,this.optionsReq).map((res :Response) => res.json()); 
  }

 
  getNonPublishedLoanNotes(){
    return this._httpServ.get(AppSettings.API_ENDPOINT+"api/NonPublishedloanNotes").map((res :Response) => res.json()); 
   }

   changePulishedLoanNotes(id:number){
     
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    return this._httpServ.put(AppSettings.API_ENDPOINT+"api/ChangePublishedStatus/"+id,"",options).map((res :Response) => res); 
   } 
 

  }


