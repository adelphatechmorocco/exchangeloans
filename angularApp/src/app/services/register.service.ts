import { Injectable } from '@angular/core';
import { Headers, Response, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {AppSettings} from '../config/AppSettings';


@Injectable()

export class registerService {
    private urlApi:string;
	constructor(private http:Http){
		this.urlApi = AppSettings.API_ENDPOINT+"api/Account/Register";
	}

	register(ObjectRegister:any){
		let headers : Headers = new Headers();
		headers.append('Content-Type','application/json');
		let opts : RequestOptions= new RequestOptions();
		opts.headers = headers; 
		return this.http.post(this.urlApi,ObjectRegister,opts).map(res => res.json());
	}
  
}


