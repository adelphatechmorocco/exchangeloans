 
  import { Injectable } from '@angular/core';
  import { Headers, Response, Http, RequestOptions } from '@angular/http';
  import { httpServiceToken } from '../services/httpServiceToken.service';
  import 'rxjs/add/operator/map';
  import { AppSettings} from '../config/AppSettings'
  @Injectable()
  export class contactUsService {
      private urlApi:string;
      // ,private _httpToken:httpServiceToken
      constructor(private http:Http,private _http : httpServiceToken){
          this.urlApi = AppSettings.API_ENDPOINT+"api/ContactUs";
      }
  //  getDropDownData(apiUrl:string){
  //         return this._httpToken.get(apiUrl).map((res :Response) => res.json());
  //   }
      contactUs(ObjectContactUs:any){
          let headers : Headers = new Headers();
          headers.append('Content-Type','application/json');
          let opts : RequestOptions= new RequestOptions();
          opts.headers = headers; 
          
          return this.http.post(this.urlApi,ObjectContactUs,opts).map(res => res.json());
      }
      getUserInfo(){
      
          return this._http.get(AppSettings.API_ENDPOINT+"api/info").map((res :Response) => res.json());
      }
     
  }