import {Injectable} from '@angular/core';
import {Router,CanActivate} from '@angular/router';

@Injectable()
export class authGuard implements CanActivate{
    constructor (private _router : Router){

    }
    canActivate(){
        if(localStorage.getItem('currentuser'))
        {
            return true;
        }else{
            this._router.navigate(['login']);
            return false;
        }
      
    }

}                                                                                                            