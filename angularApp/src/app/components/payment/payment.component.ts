import { Component, OnInit } from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { type } from '../../models/type';
import { NavbarService, loadingService, alertService,paymentService } from '../../structExports/services';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/Forms';
import { Route } from '@angular/compiler/src/core';
import { Form } from '@angular/Forms/src/directives/form_interface';
import { propertyTypesData } from '../../structExports/models';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
formPayment:FormGroup;
card:FormControl;
creditType:FormControl;
expiration:FormControl;
Name:FormControl;
lastName:FormControl;
address:FormControl;
city:FormControl;
state:FormControl;
zip:FormControl;
userEmail:FormControl;
FCI:FormControl;
borrowerName:FormControl;
propertyCity:FormControl;
propertyAddress:FormControl;
propertyState:FormControl;
propertyZip:FormControl;
propertyType:FormControl;
mode:FormControl;
tell:FormControl;
modePayment :string;
amount:string;
userInfo: any;


  constructor(private nav: NavbarService, private loading: loadingService,
    private alertService: alertService, private paymentServ: paymentService,private activatedRoute:ActivatedRoute) { 
        this.modePayment = this.activatedRoute.snapshot.queryParamMap.get('TypePayement');
        // alert(this.modePayment);
        if (this.modePayment == "BPO"){
          this.amount = "115.00"
        } 
        else if(this.modePayment == "Purchase O&E"){
          this.amount = "115.00"
        }
        else if(this.modePayment=="Due Diligence Report Package"){
          this.amount = "115.00"
        }
        else if(this.modePayment == "Assignment"){
        this.amount="200.00"
        }
        
        
      }

  ngOnInit() {
    this.getUserInfo();
    this.onCreateFormControls();
    this.onCreateForm();
    this.nav.show();
    
  }
  getUserInfo(){
    this.paymentServ.getUserInfo().subscribe(cdata => {
      this.userInfo = cdata;
      this.formPayment.controls['userEmail'].setValue(this.userInfo.UserName);
     this.formPayment.controls['tell'].setValue(this.userInfo.PhoneNumber);
    });
  }
  onCreateFormControls() {
    this.creditType = new FormControl('',Validators.required);
    this.card= new FormControl('',[Validators.required, Validators.pattern("^[0-9]+$")]);
    this.zip=new FormControl('',Validators.required);
    this.Name=new FormControl('',Validators.required);
    this.lastName=new FormControl('',Validators.required);
    this.address=new FormControl('',Validators.required);
    this.city=new FormControl('',Validators.required);
    this.state=new FormControl('',Validators.required);
    this.expiration=new FormControl('',Validators.required);
    this.userEmail=new FormControl('',[Validators.pattern("[^ @]*@[^ @]*")]);
    this.FCI=new FormControl('');
    this.borrowerName=new FormControl('');
    this.propertyCity=new FormControl('');
    this.propertyAddress=new FormControl('');
    this.propertyState=new FormControl('');
    this.propertyZip=new FormControl('');
    this.propertyType=new FormControl('');
    this.mode=new FormControl(this.modePayment);
    this.tell= new FormControl('');
    
    
      }
      onCreateForm() {
        this.formPayment= new FormGroup({
          creditType:this.creditType,
          card:this.card,
          zip:this.zip,
          Name:this.Name,
          lastName:this.lastName,
          address:this.address,
          city:this.city,
          state:this.state, 
          expiration:this.expiration,
          userEmail:this.userEmail,
          FCI:this.FCI,
          borrowerName:this.borrowerName,
          propertyCity:this.propertyCity,
          propertyAddress:this.propertyAddress,
          propertyState:this.propertyState,
          propertyZip:this.propertyZip,
          propertyType:this.propertyType,
          mode:this.mode,
          tell:this.tell,
        });
}
onSubmitPayment() {
       
  this.loading.show();
 
  if (this.formPayment.valid) {
    console.log(this.formPayment.value);
    this.paymentServ.Payment(this.formPayment.value).subscribe(res => {
      this.alertService.clear();
      this.loading.hide();
       this.formPayment.reset(); 
      this.alertService.success(res);   

      
    });
  } else {
    this.validateAllFormFields(this.formPayment);
    setTimeout(() => {
      this.loading.hide();
      window.scrollTo(0, 0);
      this.alertService.clear();
      this.alertService.error('You must complete all required fields');
    }, 300);

  }
 }
 validateAllFormFields(formGroup: FormGroup) {

  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });}



}
