import { Component, OnInit ,OnDestroy,ElementRef,ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';
import 'rxjs/add/operator/takeWhile';
import { loans, Offers, headerLaons } from '../../structExports/models';
import { NavbarService, loadingService, pagerService, myExchangeService, ExcelService, alertService } from '../../structExports/services';


@Component({
  selector: 'app-myExchange',
  templateUrl: './myExchange.component.html',
  styleUrls: ['./myExchange.component.css']
})
export class myExchangeComponent implements OnInit {
 
  loansUser: loans[];
  OffersUser: Offers[];
  pagerLoans: any = {};
  pagedItemsLoans: any[];
  pagerOffers: any = {};
  pagedItemsOffers: any[];
  templateLoans: any;
  arrayOfJsonResult: loans[];
  isExistTemplate: boolean;
  alive:boolean = true; 
  user: any = {};
  cuser:any;
  role:any;

  isActivtedLoanOffers:boolean = false;
  listOfOffersLoan:Offers[];

  constructor(private nav: NavbarService, private loading: loadingService,
    private pagerService: pagerService, private myExchangeSer: myExchangeService, private ExcelService: ExcelService, private alertService: alertService,private router:Router) {
    this.templateLoans = headerLaons;
    this.isExistTemplate = false;
    this.cuser = JSON.parse(localStorage.currentuser); 
    this.cuser = this.cuser.username;
    this.role = JSON.parse(localStorage.currentuser).role; 
  }

  ngOnInit() {
    this.nav.show();
    this.loading.show();
    this.getLoansUser();
    this.getOffersUser();

    setTimeout(() => {
      this.loading.hide();
    }, 400);
  }
  onViewOffersLoan(idLoan:number){
    this.loading.show();
    this.myExchangeSer.getOffersByLoan(idLoan).subscribe((res) => {
        this.isActivtedLoanOffers = true;
        this.listOfOffersLoan = res;
       
        //console.log(this.listOfOffersLoan);
    });
    setTimeout(() => {
      this.loading.hide();
    }, 300); 
     
  }
  navigateToTransactionSeller(OffId:number){
    this.router.navigate(['transaction'],{queryParams : { offerId: OffId}});
    localStorage.setItem('OSB0303',"OSB0313S");

  }
  navigateToTransaction(OffId:number){
      this.router.navigate(['transaction'],{queryParams : { offerId: OffId}});
      localStorage.setItem('OSB0303',"OSB0313B");
  }
  exportToExcel(event: any) {
    this.ExcelService.exportAsExcelFile(this.templateLoans, 'Loans');
  }


  onFileChange(evt: any) {

    const target: DataTransfer = <DataTransfer>(evt.target);
    const reader: FileReader = new FileReader();

    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      this.arrayOfJsonResult = (XLSX.utils.sheet_to_json(ws, { raw: true }));
      this.isExistTemplate = true;

    };

    reader.readAsBinaryString(target.files[0]);

  }

  SaveJsonAsResultLoans() {
    if (_.isEmpty(this.arrayOfJsonResult)) {
      this.alertService.clear();
      this.alertService.error('You have to download the template and fill in and upload !');

    } else {
      this.loading.show();
      this.alertService.clear();
      this.myExchangeSer.setLoansExel(this.arrayOfJsonResult).takeWhile(() => this.alive).subscribe(res => {
        this.arrayOfJsonResult = [];
        this.isExistTemplate = false;
        this.getLoansUser();
        setTimeout(() => {
          this.loading.hide();
          this.alertService.success('Your operation has been successfully completed');
        }, 400);

      });

    }

  }  
  setPageLoans(page: number) {
    this.loading.show();
    if (page < 1 || page > this.pagerLoans.totalPages) {
      return;
    }
    this.pagerLoans = this.pagerService.getPager(this.loansUser.length, page);
    this.pagedItemsLoans = this.loansUser.slice(this.pagerLoans.startIndex, this.pagerLoans.endIndex + 1);
    setTimeout(() => {
      this.loading.hide();
    }, 400);
  }
  setPageOffers(page: number) {
    this.loading.show();
    if (page < 1 || page > this.pagerOffers.totalPages) {
      return;
    }
    this.pagerOffers = this.pagerService.getPager(this.OffersUser.length, page);
    this.pagedItemsOffers = this.OffersUser.slice(this.pagerOffers.startIndex, this.pagerOffers.endIndex + 1);
    setTimeout(() => {
      this.loading.hide();
    }, 400);
  }

  getLoansUser() {
    this.myExchangeSer.getMyLoans().takeWhile(() => this.alive).subscribe(res => {
      this.loansUser = res;
      this.setPageLoans(1);
    });
  }
  getOffersUser() {
    this.myExchangeSer.getMyOffers().takeWhile(() => this.alive).subscribe(res => {
      this.OffersUser = res;
      this.setPageOffers(1);
      console.log(this.OffersUser);

    });
  }
  ChangePassword(){
    this.myExchangeSer.changePassword(this.user.oldpassword,this.user.newpassword,this.user.confirmpassword).takeWhile(() => this.alive).subscribe(

      res => {

        setTimeout(() => {
          this.alertService.clear();
          this.alertService.success("Password Changed successfully");
        }, 300);
      },
      err => {
        this.alertService.clear();
        this.alertService.error("Error detected when password Changed");
  
      }

    );
  }
  ngOnDestroy(){
    this.alive = false;
  }



}
