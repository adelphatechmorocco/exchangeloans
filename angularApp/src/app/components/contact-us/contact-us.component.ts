import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { type } from '../../models/type';
import { NavbarService, loadingService, alertService, registerService,contactUsService } from '../../structExports/services';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/Forms';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
formcontactus:FormGroup;
name:FormControl;
company:FormControl;
address:FormControl;
city:FormControl;
state:FormControl;
zip:FormControl;
telephone:FormControl; 
extension:FormControl;
fax:FormControl;
email:FormControl;
message:FormControl;
userInfo:any;
  constructor(private nav: NavbarService, private loading: loadingService,
    private alertService: alertService, private contactUsServ: contactUsService) {
      // this.contactUsServ.getDropDownData(this._urlStates).subscribe(data => {this._States = data});
  }
  
  ngOnInit() {
    this.getUserInfo();
    this.onCreateFormControls();
    this.onCreateForm();
    this.nav.show();
  }
  getUserInfo(){
    this.contactUsServ.getUserInfo().subscribe(cdata => {
      console.log(cdata);
      this.userInfo = cdata;
      this.formcontactus.controls['name'].setValue(this.userInfo.firstName+" "+this.userInfo.lastName);
     this.formcontactus.controls['telephone'].setValue(this.userInfo.PhoneNumber);
     this.formcontactus.controls['email'].setValue(this.userInfo.UserName);
     this.formcontactus.controls['address'].setValue(this.userInfo.address);
     this.formcontactus.controls['company'].setValue(this.userInfo.company);
     this.formcontactus.controls['city'].setValue(this.userInfo.city);
     this.formcontactus.controls['state'].setValue(this.userInfo.state);
     this.formcontactus.controls['zip'].setValue(this.userInfo.zip);
     this.formcontactus.controls['fax'].setValue(this.userInfo.fax);
    //  address   city
    
    });
  }
  // private _rootUrl:string = 'https://localhost:44305/api';
  // private _urlStates:string = `${this._rootUrl}/types/states`;
  // private _States:type[];
  onCreateFormControls() {
    this.name = new FormControl('',Validators.required);
    this.state= new FormControl('',Validators.required);
    this.telephone=new FormControl('',[Validators.required, Validators.pattern("^[0-9]+$")]);
    this.email=new FormControl('',[Validators.required, Validators.pattern("[^ @]*@[^ @]*")]);
    this.address=new FormControl('');
    this.city=new FormControl('');
    this.company=new FormControl('');
    this.extension=new FormControl('');
    this.message=new FormControl('');
    this.zip=new FormControl('');
    this.fax=new FormControl('');
      }
      onCreateForm() {
        this.formcontactus= new FormGroup({
          name:this.name,
          company:this.company,
          address:this.address,
          city:this.city,
          state:this.state,
          zip:this.zip,
          telephone:this.telephone,
          extension:this.extension, 
          fax:this.fax,
          email:this.email,
          message:this.message
        });
      }
      onresetcontactus(){
        this.formcontactus.reset(); 
      }
      onSubmitContactus() {
       
        this.loading.show();
       
        if (this.formcontactus.valid) {
          this.contactUsServ.contactUs(this.formcontactus.value).subscribe(res => {
            this.alertService.clear();
            this.loading.hide();
             let mail = this.email.value;
             this.formcontactus.reset(); 
            this.alertService.success('Your Request has been successfully Send ,thank you', true);   
    
            
          });
        } else {
          this.validateAllFormFields(this.formcontactus);
          setTimeout(() => {
            this.loading.hide();
            window.scrollTo(0, 0);
            this.alertService.clear();
            this.alertService.error('You must complete all required fields');
          }, 300);
    
        }
       }
       validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
          }
        });}
  }