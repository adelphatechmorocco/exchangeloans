import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../structExports/services';
import {Router} from '@angular/router';
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  constructor( private nav:NavbarService,private router:Router) { }

  ngOnInit() {
    this.nav.show();
  }
  toPayement(typePayment:string){
      
      this.router.navigate(['payment'],{queryParams :{TypePayement : typePayment}} );
  }



}
