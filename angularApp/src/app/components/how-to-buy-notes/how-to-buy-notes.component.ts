import { Component, OnInit } from '@angular/core';
import { NavbarService} from '../../structExports/services';

@Component({
  selector: 'app-how-to-buy-notes',
  templateUrl: './how-to-buy-notes.component.html',
  styleUrls: ['./how-to-buy-notes.component.css']
})
export class HowToBuyNotesComponent implements OnInit {

  constructor(private nav: NavbarService) {

  }

  ngOnInit() {
    this.nav.show();
  }

}
