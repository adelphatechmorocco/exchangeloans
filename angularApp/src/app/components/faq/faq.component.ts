import { Component, OnInit } from '@angular/core';
import { NavbarService} from '../../structExports/services';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(private nav: NavbarService) {

   }

  ngOnInit() {
    this.nav.show();
  }

}
