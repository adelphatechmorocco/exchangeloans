import { Component, OnInit ,OnDestroy} from '@angular/core';
import { Router } from '@angular/router'
import * as _ from 'lodash';
import 'rxjs/add/operator/takeWhile';
import { loans } from '../../structExports/models';
import { buyNotesService, NavbarService, loadingService, pagerService, alertService,ExcelService } from '../../structExports/services';


@Component({
  selector: 'app-buyNotes',
  templateUrl: './buyNotes.component.html',
  styleUrls: ['./buyNotes.component.css']
})
export class buyNotesComponent implements OnInit,OnDestroy{

  allLoansList: loans[];
  pager: any = {};
  pagedItems: any[];
  sortFiledUpb: boolean = false;
  sortFiledState: boolean = false;
  filterFiledUpb: string;
  filterFiledState: string;
  resetFilter: boolean = false;
  private alive: boolean = true;
  publishedtext:string = "Publish";
  unPublishedtext:string = "UnPublish";
  role:any;
  checkP: boolean = true;
  checkUnP: boolean = true;
  IsViewSwitch:boolean = true;
  array: Array<any> = [];


  constructor(private dashboardServ: buyNotesService, private nav: NavbarService,
    private loading: loadingService, private pagerServ: pagerService, private alertService: alertService, private _router:Router,private ExcelService: ExcelService) {
    this.alertService.clear();
    this.filterFiledUpb = "";
    this.filterFiledState = "";
    this.role = JSON.parse(localStorage.currentuser).role;
  }

  ngOnInit() {
    this.nav.show();
    this.getAllLoanNotes();  
  }
  getAllLoanNotes() {
    this.loading.show();
    this.dashboardServ.getAllLoanNotes().takeWhile(() => this.alive)
                          .subscribe(res => {
                              this.allLoansList = res;
                              this.setPage(1);
                              console.log(this.allLoansList);
                              setTimeout(() => {
                                this.loading.hide();
                              }, 500);
                        });



                        
  }

  setPage(page: number) {
    this.loading.show();
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerServ.getPager(this.allLoansList.length, page);
    this.pagedItems = this.allLoansList.slice(this.pager.startIndex, this.pager.endIndex + 1);
    setTimeout(() => {
      this.loading.hide();
    }, 500);
  }

  onFilter() {
    if (!this.isEmpty(this.filterFiledUpb) || !this.isEmpty(this.filterFiledState)) {

      this.loading.show();
      let loansFilteredList = [];
      loansFilteredList = this.allLoansList.filter((loans: loans) => (!this.isEmpty(this.filterFiledUpb)) ?  loans.unPaidPrincipalBalance == this.filterFiledUpb : ' ');
      loansFilteredList = loansFilteredList.filter((loans: loans) => (!this.isEmpty(this.filterFiledState)) ? loans.property.city == this.filterFiledState : ' ');
      if (!_.isEmpty(loansFilteredList)) {
        this.allLoansList = loansFilteredList;
        this.setPage(1);
        setTimeout(() => {
          this.loading.hide();
        }, 500);
        this.resetFilter = true;
        this.alertService.clear();

      } else {
        setTimeout(() => {
          this.loading.hide();
        }, 200);
        setTimeout(() => {
          this.alertService.clear();
          this.alertService.info('No loans with this filter you can try with other.');
        }, 300);


      }
    } else {
      this.alertService.clear();
      this.alertService.info('Fill in the fields so that you can filter.');
      return;
    }
  }


  onResetFilter() {
    this.resetFilter = false;
    this.filterFiledState = "";
    this.filterFiledUpb = "";
    this.getAllLoanNotes();

  }
  // get diagnostic() { return JSON.stringify(this.allLoansList); }

  onSortFiledUpb(sortFiledUpb: boolean) {
    this.sortFiledUpb = !sortFiledUpb;
    this.sortFiledState =false;
    if (this.sortFiledUpb == true) {
      this.allLoansList = _.sortBy(this.allLoansList,(loans:loans) => loans.unPaidPrincipalBalance);
        this.allLoansList = this.allLoansList.reverse();
     
      this.setPage(1);
    } else {
      this.allLoansList = _.sortBy(this.allLoansList, (loans: loans) => loans.unPaidPrincipalBalance);
      this.setPage(1);
     
  }
}
onSortFiledCity(sortFiledState: boolean) {
    this.sortFiledState = !sortFiledState;
    this.sortFiledUpb =false;
    if (this.sortFiledState == true) {
      this.allLoansList = _.sortBy(this.allLoansList, (loans: loans) => loans.property.city);
      this.allLoansList = this.allLoansList.reverse();
      this.setPage(1);
     
    } else {
      this.allLoansList = _.sortBy(this.allLoansList, (loans: loans) => loans.property.city);
      this.setPage(1);

    }
  }
  isEmpty(value) {
    return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
  }
  ngOnDestroy(){
    this.alive = false;
   
  }

  changePulishedStatus(id:number){
    this.dashboardServ.changePulishedLoanNotes(id).subscribe(res =>{
    
      console.log(res);
      this.checkP = !this.checkP;
      this.checkUnP = !this.checkUnP; 
       
    });
  }

  getNonPublished(){

    this.loading.show();
    this.dashboardServ.getNonPublishedLoanNotes().subscribe(res => {
      this.allLoansList = res;
      this.setPage(1);
      console.log(this.allLoansList);
      setTimeout(() => {
        this.loading.hide();
      }, 500);
    });
  }
  onViewOne(){
    this.IsViewSwitch = true;
  }

  onViewTow(){
  this.IsViewSwitch = false;
  }

  
getStyle(status:boolean) {
   if(status) {
     return "#868e96";
   } else {
     return "#007bff";
     
   }
  }

  SubmitOffer(item){
    this._router.navigate(["SubmitOffer", item.loanNoteID]);
  }
  
  
  getSafe(arr,propt,subpropt) {
  try {
      return arr[propt][subpropt];
  } catch (e) {
      return undefined;
  }
}

  exportLiveTape() {
   
    console.log( this.allLoansList);
     
      for(let j =0 ; j<this.allLoansList.length;j++){
         
        this.array.push( 
         
         {
           "Product ID":this.allLoansList[j].loanNoteID,
           "Seller ID":this.allLoansList[j].userID,
           "Serviced By":"None",
           "Payment Status":"None",
           "Status":"",
           "Lien Position":"",
           "Address": this.getSafe(this.allLoansList[j],"property","address"),
           "City": this.getSafe(this.allLoansList[j],"property","city"),
           "State": this.getSafe(this.allLoansList[j],"property","stateID"),
           "Zip": this.getSafe(this.allLoansList[j],"property","zip"),
           "Paid to Date":this.allLoansList[j].paidToDate,
           "Loan Maturity":this.allLoansList[j].noteMaturityDate,
           "Original Balance":this.allLoansList[j].originalLoanAmount,  
           "Principle Balance":this.allLoansList[j].principaleBalance,
           "Note Rate":this.allLoansList[j].noteInterestRate,   
           "Estimated Market Value":this.getSafe(this.allLoansList[j],"property","propertyMarketValue"), 
           "Submit Offer":"Submit Offer",
           "Asking Price":this.allLoansList[j].askingPrice,
           "% of UPB": this.allLoansList[j].principaleBalance !=null && this.allLoansList[j].principaleBalance !=0 ? this.allLoansList[j].askingPrice / this.allLoansList[j].principaleBalance : "None",
           "% of Value":"None",
           "View Payment History":"None",
           "Payment Amount":this.allLoansList[j].notePaymentAmount,  
           "Sold Rate":this.allLoansList[j].soldInterestRate,
           "Modified?":"None",
           "First Payment Date":this.allLoansList[j].firstPayementDate
           }
   
        );
     }
      
      this.ExcelService.exportLiveTape(this.array, 'Live Tape');
}


   

}



