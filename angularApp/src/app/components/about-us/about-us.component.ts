import { Component, OnInit } from '@angular/core';
import {  NavbarService } from '../../structExports/services';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(private nav: NavbarService) { 
this.nav.show();

  }

  ngOnInit() {
  }

}
