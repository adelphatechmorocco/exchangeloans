import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { AbstractControl, FormGroup, FormControl, Validators, } from '@angular/Forms';
import { NavbarService, loadingService, alertService, registerService } from '../../structExports/services';
import { aboutUsData, millingData, propertyTypesData, CurrentCapitalAvailableData, idealPropertyValueData, LoanTypesData, purchaseYourNoteData, LienPositionData, statesData } from '../../structExports/models';
import { Select2OptionData } from 'ng2-select2';
import * as _ from 'lodash';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [registerService]
})
export class RegisterComponent implements OnInit {

  public hearAboutUsData: Array<Select2OptionData> = aboutUsData;
  public millingData: Array<Select2OptionData> = millingData;
  public propertyTypesData: Array<Select2OptionData> = propertyTypesData;
  public CurrentCapitalAvailableData: Array<Select2OptionData> = CurrentCapitalAvailableData;
  public idealPropertyValueData: Array<Select2OptionData> = idealPropertyValueData;
  public LoanTypesData: Array<Select2OptionData> = LoanTypesData;
  public purchaseYourNoteData: Array<Select2OptionData> = purchaseYourNoteData;
  public LienPositionData: Array<Select2OptionData> = LienPositionData;
  public statesData: Array<Select2OptionData> = statesData;
  public stateData: Array<Select2OptionData> = statesData;
  public options: Select2Options;




  formRegister: FormGroup;

  FirstName: FormControl;
  lastName: FormControl;
  address: FormControl;
  city: FormControl;
  zip: FormControl;
  state: FormControl;
  Email: FormControl;
  confirmEmail: FormControl;
  PhoneNumber: FormControl;
  confirmPhoneNumber: FormControl;
  fax: FormControl;
  company: FormControl;
  Password: FormControl;
  confirmPassword: FormControl;
  broker: FormControl;
  licence: FormControl;

  otherhearAboutUs: FormControl;
  milling: FormControl;
  hearAboutUs: FormControl;
  propertyTypes: FormControl;
  capitalAvailable: FormControl;
  idealPropertyValue: FormControl;
  LoanTypes: FormControl;
  purchaseYourNote: FormControl;
  LienPosition: FormControl;
  states: FormControl;

  stateCheck: boolean;
  hearAboutUsCheck: boolean;
  propertyTypesCheck: boolean;
  CurrentCapitalAvailableCheck: boolean;
  idealPropertyValueCheck: boolean;
  LoanTypesCheck: boolean;

  isBroker: boolean = false;

  constructor(private nav: NavbarService, private loading: loadingService,
    private alertService: alertService, private registerServ: registerService, private router: Router) {
    this.options = {
      multiple: true,
    }
    this.hearAboutUsCheck = false;
    this.propertyTypesCheck = false;
    this.CurrentCapitalAvailableCheck = false;
    this.idealPropertyValueCheck = false;
    this.LoanTypesCheck = false;
    this.stateCheck = false;


  }
 
  ngOnInit() {
    this.onCreateFormControls();
    this.onCreateForm();
    this.formRegister.controls['purchaseYourNote'].setValue({
      "InBulk": "false",
      "Single": "false"
    });
    this.formRegister.controls['states'].setValue([]);

  }


  onCreateFormControls() {
    this.FirstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.address = new FormControl('', Validators.required);
    this.city = new FormControl('  ', Validators.required);
    this.zip = new FormControl('');
    this.state = new FormControl('DefaultItem', Validators.required);
    this.Email = new FormControl('', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]);
    this.confirmEmail = new FormControl('', [Validators.required, this.emailConfirming]);
    this.PhoneNumber = new FormControl('', [Validators.required, Validators.pattern("^[0-9]+$")]);
    this.confirmPhoneNumber = new FormControl('', [Validators.required, this.PhoneNumberConfirming]);
    this.fax = new FormControl('');
    this.company = new FormControl('');
    this.Password = new FormControl('', [Validators.maxLength(50), Validators.minLength(8), Validators.required]);
    this.confirmPassword = new FormControl('', [Validators.required, this.PasswordConfirming]);
    this.broker = new FormControl('', Validators.required);
    this.licence = new FormControl({ value: "", disabled: true }, Validators.required);
    this.hearAboutUs = new FormControl('', Validators.required);
    this.otherhearAboutUs = new FormControl('');
    this.milling = new FormControl('');
    this.propertyTypes = new FormControl('', Validators.required);
    this.capitalAvailable = new FormControl('', Validators.required);
    this.idealPropertyValue = new FormControl('', Validators.required);
    this.LoanTypes = new FormControl('', Validators.required);
    this.purchaseYourNote = new FormControl('');
    this.LienPosition = new FormControl('');
    this.states = new FormControl('');
    this.state = new FormControl('');
  }

  onCreateForm() {
    this.formRegister = new FormGroup({
      FirstName: this.FirstName,
      lastName: this.lastName,
      address: this.address,
      city: this.city,
      zip: this.zip,
      state: this.state,
      Email: this.Email,
      confirmEmail: this.confirmEmail,
      PhoneNumber: this.PhoneNumber,
      confirmPhoneNumber: this.confirmPhoneNumber,
      fax: this.fax,
      company: this.company,
      Password: this.Password,
      confirmPassword: this.confirmPassword,
      broker: this.broker,
      licence: this.licence,
      hearAboutUs: this.hearAboutUs,
      otherhearAboutUs: this.otherhearAboutUs,
      milling: this.milling,
      propertyTypes: this.propertyTypes,
      capitalAvailable: this.capitalAvailable,
      idealPropertyValue: this.idealPropertyValue,
      LoanTypes: this.LoanTypes,
      purchaseYourNote: this.purchaseYourNote,
      LienPosition: this.LienPosition,
      states: this.states
    });
  }
  onSubmitRegister() {
    this.loading.show();
   
    if (this.formRegister.valid && this.formRegister.controls['state'].value !="") {
      this.registerServ.register(this.formRegister.value).subscribe(
        res => {
                  this.alertService.clear();
                  this.loading.hide();
                  let mail = this.Email.value;
                  this.formRegister.reset(); 
                  this.alertService.success('Your registration has been successfully completed ,Enter your password and enjoy', true);
                  this.router.navigate(['login'],{queryParams : { email: mail}});
              },
       error => {
                this.loading.hide();
                this.alertService.clear();
                this.alertService.error('Email : '+ this.formRegister.controls['Email'].value +' is already taken ');
                window.scrollTo(0, 0);
              }
    );

    } else {
      this.validateAllFormFields(this.formRegister);
      setTimeout(() => {
        this.loading.hide();
        window.scrollTo(0, 0);
        this.alertService.clear();
        this.alertService.error('You must complete all required fields');
      }, 300);

    }
  }

  changedMilling(event) {
    let objectMilling = this.onChangeResult(event);
    this.formRegister.controls['milling'].setValue(objectMilling);
  }
  changedState(event) {
    var objectstate = event.data[0].id;
    if (_.isEmpty(objectstate)) {
      this.stateCheck = true;
    }
    else {
      //  alert(objectstate);
      this.stateCheck = false;
      this.formRegister.controls['state'].setValue(objectstate);
    }

  }

  changedhearAboutUs(event) {
    let objecthearAboutUs = this.onChangeResult(event);
    if (_.isEmpty(objecthearAboutUs)) {
      this.hearAboutUsCheck = true;
    } else {
      this.hearAboutUsCheck = false;
      this.formRegister.controls['hearAboutUs'].setValue(objecthearAboutUs);
    }

  }
  changedPropertyTypes(event) {
    let objectpropertyTypes = this.onChangeResult(event);
    if (_.isEmpty(objectpropertyTypes)) {
      this.propertyTypesCheck = true;
    } else {
      this.propertyTypesCheck = false;
      this.formRegister.controls['propertyTypes'].setValue(objectpropertyTypes);
    }

  }
  changedCurrentCapitalAvailable(event) {
    let objectCurrentCapitalAvailable = this.onChangeResult(event);
    if (_.isEmpty(objectCurrentCapitalAvailable)) {
      this.CurrentCapitalAvailableCheck = true;
    } else {
      this.CurrentCapitalAvailableCheck = false;
      this.formRegister.controls['capitalAvailable'].setValue(objectCurrentCapitalAvailable);
    }

  }
  changedIdealPropertyValue(event) {
    let objectidealPropertyValue = this.onChangeResult(event);

    if (_.isEmpty(objectidealPropertyValue)) {
      this.idealPropertyValueCheck = true;
    } else {
      this.idealPropertyValueCheck = false;
      this.formRegister.controls['idealPropertyValue'].setValue(objectidealPropertyValue);
    }

  }
  changedLoanTypes(event) {
    let objectLoanTypes = this.onChangeResult(event);
    if (_.isEmpty(objectLoanTypes)) {
      this.LoanTypesCheck = true;
    } else {
      this.LoanTypesCheck = false;
      this.formRegister.controls['LoanTypes'].setValue(objectLoanTypes);
    }

  }
  changedpurchaseYourNote(event) {
    let objectpurchaseYourNote = this.onChangeResult(event);
    this.formRegister.controls['purchaseYourNote'].setValue(objectpurchaseYourNote);
  }
  changedLienPosition(event) {
    let objectLienPosition = this.onChangeResult(event);
    this.formRegister.controls['LienPosition'].setValue(objectLienPosition);
  }
  changedStates(event) {
    if (event.data) {
      var itemObj = [];
      for (let item of event.data) {
        itemObj.push(item.id);
      }
    }

    let objectstates = itemObj;
    this.formRegister.controls['states'].setValue(objectstates);
  }
  IsBroker() {
    this.isBroker = true;
    this.licence.enable();
    this.licence.setValidators(Validators.required);
  }
  IsPrincipal() {
    this.isBroker = false;
    this.licence.clearValidators();
    this.licence.reset();
    this.licence.disable();
  }
  onChangeResult(event) {
    if (event.data) {
      var itemObj = new Object();
      for (let item of event.data) {
        let valueName = item.id;
        itemObj[valueName] = "true";

      }
    }
    return itemObj;
  }
  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });

    this.hearAboutUsCheck = this.checkValdateSelect2("hearAboutUs");
    this.LoanTypesCheck = this.checkValdateSelect2("propertyTypes");
    this.CurrentCapitalAvailableCheck = this.checkValdateSelect2("capitalAvailable");
    this.idealPropertyValueCheck = this.checkValdateSelect2("idealPropertyValue");
    this.LoanTypesCheck = this.checkValdateSelect2("LoanTypes");
    this.stateCheck = this.checkValdateSelect2("state");
    this.propertyTypesCheck = this.checkValdateSelect2("propertyTypes");

  }
  checkValdateSelect2(nameOfcontrol: string) {
    if (_.isEmpty(this.formRegister.controls[nameOfcontrol].value) || this.formRegister.controls[nameOfcontrol].value =="DefaultItem" ) {
      return true;
    }
  }
  emailConfirming(c: AbstractControl): any {
    if (!c.parent || !c) return;
    const pwd = c.parent.get('Email');
    const cpwd = c.parent.get('confirmEmail');

    if (!pwd || !cpwd) return;
    if (pwd.value !== cpwd.value) {
      return { invalid: true };

    }
  }
  PhoneNumberConfirming(c: AbstractControl): any {
    if (!c.parent || !c) return;
    const pwd = c.parent.get('PhoneNumber');
    const cpwd = c.parent.get('confirmPhoneNumber')

    if (!pwd || !cpwd) return;
    if (pwd.value !== cpwd.value) {
      return { invalid: true };

    }
  }
  PasswordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) return;
    const pwd = c.parent.get('Password');
    const cpwd = c.parent.get('confirmPassword')

    if (!pwd || !cpwd) return;
    if (pwd.value !== cpwd.value) {
      return { invalid: true };

    }
  }


}