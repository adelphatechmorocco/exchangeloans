import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Http, RequestOptions, Request, Response, Headers } from '@angular/http';
import { buyNotesService, NavbarService, loadingService, pagerService, alertService } from '../../structExports/services';
 
import { FormGroup , Validators,FormControl } from '@angular/forms';
import { AppSettings } from '../../config/AppSettings' ;
@Component({
  selector: 'app-reset-password',  
  templateUrl: './resetPassword.component.html',
  styleUrls: ['./resetPassword.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetpassword:FormGroup;
  emailforgot:FormControl;
  password:FormControl;
  confirmpassword:FormControl;
  replacertoken:string;
  re:string = "+exch+";
  yyyy:string;
  constructor(private _http:Http,private route: ActivatedRoute,private _alert:alertService, private router: Router) { }

  ngOnInit() {
    let re = "+exch+";
    console.log(this.route.snapshot.params['code']);
    this.replacertoken = this.route.snapshot.params['code']
    this.replacertoken = this.replacertoken.replace(re,"/")
    console.log( this.replacertoken.replace(re,"/"));
    this.yyyy =  this.replacertoken.replace(re,"/");
    this.emailforgot = new FormControl(null,Validators.required);
    this.password = new FormControl(null,Validators.required);
    this.confirmpassword = new FormControl(null,Validators.required);
    this.resetpassword = new FormGroup({ 
      
      Email : this.emailforgot,
      password:this.password, 
      confirmpassword:this.confirmpassword
    
    });  
  }



  onResetPassword(){
   // console.log(this.emailforgot.value);
      
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    let body = "Email="+this.emailforgot.value + "&Password=" + this.password.value + "&ConfirmPassword="+ this.confirmpassword.value + "&Code="+  this.replacertoken.replace("+exch+","/");  
      this._http.post(AppSettings.API_ENDPOINT+"api/Account/ResetPassword",body,options).subscribe(res => {
       this._alert.success("Password Changed Successfully",true);
       this.router.navigate(['login']);
        console.log(res);
      });
      
      
  }

}
