import { Component, OnInit } from '@angular/core';
import { Alert, AlertType } from '../../structExports/models';
import { alertService } from '../../structExports/services';
import {fadeInAnimation} from '../../animations/animation';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css'],
  animations : [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class alertsComponent implements OnInit {
	 alerts: Alert[] = [];

     constructor(private alertService: alertService) { }


     ngOnInit() {
        this.alertService.getAlert().subscribe((alert: Alert) => {
            if (!alert) {
                this.alerts = [];
                return;
            }
            this.alerts.push(alert);
        });
    }
     removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }
       cssClass(alert: Alert) {
        if (!alert) {
            return;
        }
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
     fontClass(alert: Alert) {
        if (!alert) {
            return;
        }
        switch (alert.type) {
            case AlertType.Success:
                return 'fa-check-circle';
            case AlertType.Error:
                return 'fa-exclamation-circle';
            case AlertType.Info:
                return 'fa-info-circle';
            case AlertType.Warning:
                return 'fa-exclamation';
        }
    }

}

