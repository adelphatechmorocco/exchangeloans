import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { SellnoteService } from '../../services/sellnote.service';
import { type } from '../../models/type';
import {Router} from '@angular/router';
//import { loanNote } from '../../models/loanNotes';
import { httpServiceToken } from '../../services/httpServiceToken.service';
import { buyNotesService, NavbarService, loadingService, pagerService, alertService } from '../../structExports/services';
 
import { FormGroup , Validators,FormControl } from '@angular/forms';
import { AppSettings } from '../../config/AppSettings';

@Component({
  selector: 'app-sellnote',
  templateUrl: './sellnote.component.html',
  styleUrls: ['./sellnote.component.css']
})
export class SellnoteComponent implements OnInit {
  
  private Foreclosure:boolean = false;
  private Bankruptcy:boolean = false;
  private DeedMortgage:boolean = false;

  errorMessage: string = " ";
  errorTypeMessage: string = " ";
  filesToUpload = [];
  filesToUploadR: Array<File>;
  selectedFileNames: string[] = [];
  public isLoadingData: Boolean = false;
  uploadResult: any;
   
  
  ngOnInit() {
    this.onCreateFormControls();
    this.onCreateForm();
  }

 
 
   
  //public _loannote:loanNote;
  private _rootUrl:string = AppSettings.API_ENDPOINT+'api';
  private _urlPostLoanNote:string = `${this._rootUrl}/loanNotes`;

  private _urlClosingTypes:string = `${this._rootUrl}/types/closingtypes`;  
  public _ClosingType:type[];
  
  private _urlLoanTypes:string = `${this._rootUrl}/types/loantypes`;
  private _LoanTypes:type[];

  private _urlamortizationType:string = `${this._rootUrl}/types/amortizationtypes`;
  private _amortizationType:type[];

  private _urlloanStatus:string = `${this._rootUrl}/types/loanstatus`;
  private _loanStatus:type[];

  private _urlrateType:string = `${this._rootUrl}/types/ratetypes`;
  private _rateType:type[];

  private _urlpaymentFrequency:string = `${this._rootUrl}/types/paymentfrequencies`;
  private _paymentFrequency:type[];

  private _urlStates:string = `${this._rootUrl}/types/states`;
  private _States:type[];
  
  private _urlValuationTypes:string = `${this._rootUrl}/types/valuationtypes`;
  private _ValuationTypes:type[];

  private _urlOccupancyStatus:string = `${this._rootUrl}/types/occupancystatus`;
  private _OccupancyStatus:type[];

  private _urlPropertyTypes:string = `${this._rootUrl}/types/propertytypes`;
  private _PropertyTypes:type[];

  private _urlBkChapter:string = `${this._rootUrl}/types/bkchaptertypes`;
  private _BkChapter:type[];

  private _urlloanServicer:string = `${this._rootUrl}/types/loanServicer`;
  private _loanServicer:type[];
  
  private _isValidDeed:boolean = false;
  

  sellNoteForm: FormGroup;
  minAskingPrice:FormControl; //number 
  principaleBalance:FormControl; //number * 
  accruedLateCharges:FormControl; //number *
  adjPayementChangeDate:FormControl; //Date
  adjustementFrequency:FormControl; //number
  afterDays:FormControl; //number
  askingPrice:FormControl; //number
  ballonPayement:FormControl; //Boolean
  bankruptcyStatus:FormControl; //Boolean
  ceiling:FormControl; //number
  comments:FormControl; //String
  deedMortgageStatus:FormControl; //Boolean
  drawPeriodStartDate:FormControl; //Date
  //escrowImpoundStatus:FormControl; //Boolean
  firstPayementDate:FormControl; //Date
  floor:FormControl; //number
  foreclosureStatus:FormControl; //Boolean
  indexName:FormControl; //String
  lateCharge:FormControl; //number
  loanCharges:FormControl; //number
  loanTermsModified:FormControl; //Boolean
  margin:FormControl; //number
  //name:FormControl; //String
  nextAdjustement:FormControl; //Date
  nextPayementDate:FormControl; //Date
  noteInterestRate:FormControl; //number
  noteMaturityDate:FormControl; //Date
  ofPayementsLast12:FormControl; //String
  onForbearancePlan:FormControl; //Boolean
  originalLoanAmount:FormControl; //number
  originationDate:FormControl; //Date
  paidToDate:FormControl; //Date
  payementTrust:FormControl; //number
  payersLastPayementMadeDate:FormControl; //Date
  percentagePrice:FormControl; //number
  PI:FormControl; //number
  prePayPenalty:FormControl; //Boolean
  propertyTaxesDue:FormControl; //number
  registerswMers:FormControl; //Boolean
  repaymentPeriodStartDate:FormControl; //Date
  soldInterestRate:FormControl; //number
  TotalMonthlyLoanPayement:FormControl; //number
  UnpaidInterest:FormControl; //number
  unPaidPrincipalBalance:FormControl; //number
  loanServicer:FormControl //number 
  amortizationType:FormControl; //number
  //loanNote:FormControl; //number
  loanStatus:FormControl; //number
  closingType:FormControl; //number
  loanType:FormControl; //number
  rateType:FormControl; //number
  paymentFrequency:FormControl; //number
  //user:string = ""; //String

  /***** PROPERTY INFORMATION *****/

  propertyMarketValue:FormControl; //number
  propertyValuationDate:FormControl; //Date
  address:FormControl; //string
  city:FormControl; //string
  country:FormControl; //string
  zip:FormControl; //string
  valuationTypeID:FormControl; //number
  typePropertyID:FormControl;  //number     
  occupancyStatusID:FormControl;  //number    
  loanNoteId:FormControl;  //number
  stateID:FormControl;  //number



  /***** Force closure *****/

  attorneyFeesDue:FormControl;
  name:FormControl;
  Fcaddress:FormControl;
  Fccity:FormControl;
  scheduledSaleDate:FormControl;
  Fczip:FormControl;
  phone:FormControl;
  email:FormControl;
  dateOpened:FormControl;
  FcstateID:FormControl;
  loanNoteID:FormControl;


  /******* bankruptcy *******/


  bkFillingDate:FormControl;
  bkDisChargeDate:FormControl;
  bkDismissalDate:FormControl;
  bkbankruptcyStatus:FormControl;
  bkloanNoteID:FormControl;
  bkChapterID:FormControl;

/************* escrowImpounds ************/

  escrowImpoundsStatus:FormControl;
  notePaymentAmount:FormControl;
  annualInsurancePremium:FormControl;
  taxPortion:FormControl;
  annualTaxes:FormControl;
  insurancePortion:FormControl;
  trustBalance:FormControl;
  esloanNoteID:FormControl;


  /********** BORROWER INFORMATION *********/

  isCompany:FormControl;
  firstName:FormControl;
  lastName:FormControl;
  maillingAddress:FormControl;
  homePh:FormControl;
  workPh:FormControl;
  fssTaxID:FormControl;
  companyName:FormControl;
  companyContactName:FormControl;
  braddress:FormControl;
  brcity:FormControl;
  brcountry:FormControl;
  brzip:FormControl;
  brphone:FormControl;
  brmobilePhone:FormControl;
  bremail:FormControl;
  brstateID:FormControl;
  brloanNoteId:FormControl;


/****************** deedMortgage ****************/

  firstTrust:FormControl;
  juniorTrust:FormControl;
  lienPosition:FormControl;
  lienPosition1:FormControl;
  monthlyPayment1:FormControl;
  current1:FormControl;
  prnicipalBalance1:FormControl;
  modified1:FormControl;
  informationAsOf1:FormControl;
  lienPosition2:FormControl;
  monthlyPayment2:FormControl;
  current2:FormControl;
  prnicipalBalance2:FormControl;
  modified2:FormControl;
  informationAsOf2:FormControl;
  deedloanNoteID:FormControl;


  onCreateFormControls(){
    
    this.minAskingPrice = new FormControl(0,Validators.pattern("^[0-9]+$")); //number 
    this.principaleBalance = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number 
    this.accruedLateCharges = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number 
    this.adjPayementChangeDate = new FormControl(null); //Date
    this.adjustementFrequency = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.afterDays = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.askingPrice = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.ballonPayement = new FormControl(false); //Boolean
    this.bankruptcyStatus = new FormControl(false); //Boolean
    this.ceiling = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.comments = new FormControl(""); //String
    this.deedMortgageStatus = new FormControl(false); //Boolean
    this.drawPeriodStartDate = new FormControl(null); //Date
    //this.escrowImpoundStatus = new FormControl(false, Validators.required); //Boolean
    this.firstPayementDate = new FormControl(null, Validators.required); //Date
    this.floor = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.foreclosureStatus = new FormControl(false); //Boolean
    this.indexName = new FormControl(''); //String
    this.lateCharge = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.loanCharges = new FormControl(null,Validators.pattern("^[0-9]+$")); //number
    this.loanTermsModified = new FormControl(false, Validators.required); //Boolean
    this.margin = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    //this.name = new FormControl("", Validators.required); //String
    this.nextAdjustement = new FormControl(null); //Date
    this.nextPayementDate = new FormControl(null,Validators.required); //Date
    this.noteInterestRate = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.noteMaturityDate = new FormControl('', Validators.required); //Date
    this.ofPayementsLast12 = new FormControl(null); //String
    this.onForbearancePlan = new FormControl(false, Validators.required); //Boolean
    this.originalLoanAmount = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.originationDate = new FormControl(null, Validators.required); //Date
    this.paidToDate = new FormControl(null,Validators.required); //Date
    this.payementTrust = new FormControl('',Validators.pattern("^[0-9]+$")); //number 
    this.payersLastPayementMadeDate = new FormControl(null, Validators.required); //Date
    this.percentagePrice = new FormControl(0, [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.PI = new FormControl(0,[Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.prePayPenalty = new FormControl(false, Validators.required); //Boolean
    this.propertyTaxesDue = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.registerswMers = new FormControl(false, Validators.required); //Boolean
    this.repaymentPeriodStartDate = new FormControl(null); //Date
    this.soldInterestRate = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.TotalMonthlyLoanPayement = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.UnpaidInterest = new FormControl('',Validators.pattern("^[0-9]+$")); //number
    this.unPaidPrincipalBalance = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]); //number

  this.closingType = new FormControl(null, Validators.required); //select
  this.loanServicer = new FormControl(null, Validators.required); //select 
  this.amortizationType = new FormControl(null, Validators.required); //select
 // this.loanNote = new FormControl(null, Validators.required); //select
  this.loanStatus = new FormControl(null, Validators.required); //select
  this.loanType = new FormControl(null, Validators.required); //select
  this.rateType = new FormControl(null, Validators.required); //select
  this.paymentFrequency = new FormControl(null, Validators.required); //select

    /***** BORROWER INFORMATION *****/

    this.propertyMarketValue = new FormControl(null, [Validators.required,Validators.pattern("^[0-9]+$")]); //number
    this.propertyValuationDate = new FormControl("", Validators.required); //Date
    this.address = new FormControl("", Validators.required); //string
    this.city = new FormControl("", Validators.required); //string
    this.country = new FormControl(""); //string
    this.zip = new FormControl(""); //string
    this.valuationTypeID = new FormControl(null); //number
    this.typePropertyID = new FormControl(null, [Validators.required,Validators.pattern("^[0-9]+$")]);  //number     
    this.occupancyStatusID = new FormControl(null, [Validators.required,Validators.pattern("^[0-9]+$")]);  //number    
    this.loanNoteId = new FormControl(null);  //number
    this.stateID = new FormControl(null);  //number


      /***** Force closure *****/

    this.attorneyFeesDue = new FormControl(0);
    this.name = new FormControl("");
    this.Fcaddress = new FormControl("");
    this.Fccity = new FormControl("");
    this.scheduledSaleDate = new FormControl(new Date());
    this.Fczip = new FormControl("");
    this.phone = new FormControl("");
    this.email = new FormControl("");
    this.dateOpened = new FormControl(null);
    this.FcstateID = new FormControl(null);
    this.loanNoteID = new FormControl(null);


     /******* bankruptcy *******/

     this.bkFillingDate = new FormControl(null);
     this.bkDisChargeDate = new FormControl(new Date());
     this.bkDismissalDate = new FormControl(new Date());
     this.bkbankruptcyStatus = new FormControl(false);
     this.bkloanNoteID = new FormControl(null);
     this.bkChapterID = new FormControl(null);


     /************* escrowImpounds ************/

     this.escrowImpoundsStatus = new FormControl(false);
     this.notePaymentAmount = new FormControl('');
     this.annualInsurancePremium = new FormControl('');
     this.taxPortion = new FormControl('');
     this.annualTaxes = new FormControl('');
     this.insurancePortion = new FormControl('');
     this.trustBalance = new FormControl('');
     this.esloanNoteID = new FormControl(null);

    /********** BORROWER INFORMATION *********/

    this.isCompany = new FormControl(false);
    this.firstName = new FormControl("",Validators.required);
    this.lastName = new FormControl("",Validators.required);
    this.maillingAddress = new FormControl("",Validators.required);
    this.homePh = new FormControl("");
    this.workPh = new FormControl("");
    this.fssTaxID = new FormControl("");
    this.companyName = new FormControl("");
    this.companyContactName = new FormControl("");
    this.braddress = new FormControl("");
    this.brcity = new FormControl("",Validators.required);
    this.brcountry = new FormControl("");
    this.brzip = new FormControl("");
    this.brphone = new FormControl("");
    this.brmobilePhone = new FormControl("");
    
    this.bremail = new FormControl("",[Validators.email,Validators.required] );
    this.brstateID = new FormControl(null);
    this.brloanNoteId = new FormControl(null);

    /****************** deedMortgage ****************/

    this.firstTrust = new FormControl(false);
    this.juniorTrust = new FormControl(false);
    this.lienPosition = new FormControl();
    this.lienPosition1 = new FormControl(null);
    this.monthlyPayment1 = new FormControl("");
    this.current1 = new FormControl("Unknown");
    this.prnicipalBalance1 = new FormControl("");
    this.modified1 = new FormControl("");
    this.informationAsOf1 = new FormControl();
    this.lienPosition2 = new FormControl("");
    this.monthlyPayment2 = new FormControl("");
    this.current2 = new FormControl("");
    this.prnicipalBalance2 = new FormControl("");
    this.modified2 = new FormControl("");
    this.informationAsOf2 = new FormControl("");
    this.deedloanNoteID = new FormControl("");



    
    
  }


  onCreateForm() {
   
    this.sellNoteForm = new FormGroup({
      minAskingPrice: this.minAskingPrice, //number * 
      principaleBalance: this.principaleBalance, //number * 
      accruedLateCharges: this.accruedLateCharges, //number *
      adjPayementChangeDate: this.adjPayementChangeDate, //Date
      adjustementFrequency: this.adjustementFrequency, //number
      afterDays: this.afterDays, //number
      askingPrice: this.askingPrice, //number
      ballonPayement: this.ballonPayement, //Boolean
      bankruptcyStatus: this.bankruptcyStatus, //Boolean
      ceiling: this.ceiling, //number
      comments: this.comments, //String
      deedMortgageStatus: this.deedMortgageStatus, //Boolean
      drawPeriodStartDate: this.drawPeriodStartDate, //Date
      //escrowImpoundStatus: this.escrowImpoundStatus, //Boolean
      firstPayementDate: this.firstPayementDate, //Date
      floor: this.floor, //number
      foreclosureStatus: this.foreclosureStatus, //Boolean
      indexName: this.indexName, //String
      lateCharge: this.lateCharge, //number
      loanCharges: this.loanCharges, //number
      loanTermsModified: this.loanTermsModified, //Boolean
      margin: this.margin, //number
      //name: this.name, //String
      nextAdjustement: this.nextAdjustement, //Date
      nextPayementDate: this.nextPayementDate, //Date
      noteInterestRate: this.noteInterestRate, //number
      noteMaturityDate: this.noteMaturityDate, //Date
      ofPayementsLast12: this.ofPayementsLast12, //String
      onForbearancePlan: this.onForbearancePlan, //Boolean
      originalLoanAmount: this.originalLoanAmount, //number
      originationDate: this.originationDate, //Date
      paidToDate: this.paidToDate, //Date
      payementTrust: this.payementTrust, //number
      payersLastPayementMadeDate: this.payersLastPayementMadeDate, //Date
      percentagePrice: this.percentagePrice, //number
      PI: this.PI, //number
      prePayPenalty: this.prePayPenalty, //Boolean
      propertyTaxesDue: this.propertyTaxesDue, //number
      registerswMers: this.registerswMers, //Boolean
      repaymentPeriodStartDate: this.repaymentPeriodStartDate, //Date
      soldInterestRate: this.soldInterestRate, //number
      TotalMonthlyLoanPayement: this.TotalMonthlyLoanPayement, //number
      UnpaidInterest: this.UnpaidInterest, //number
      unPaidPrincipalBalance: this.unPaidPrincipalBalance, //number
      
      closingType: this.closingType, //select
      loanServicer:this.loanServicer, //select 
      amortizationType:this.amortizationType, //select
      //loanNote:this.loanNote, //select
      loanStatus:this.loanStatus, //select
      loanType:this.loanType, //select
      rateType:this.rateType, //select
      paymentFrequency:this.paymentFrequency, //select
      
       /***** BORROWER INFORMATION *****/

       
      property: new FormGroup({
          propertyMarketValue: this.propertyMarketValue, //number
          propertyValuationDate: this.propertyValuationDate, //Date
          address: this.address, //string
          city: this.city, //string
          country: this.country, //string
          zip:this.zip, //string
          valuationTypeID: this.valuationTypeID, //number
          typePropertyID: this.typePropertyID,  //number     
          occupancyStatusID: this.occupancyStatusID,  //number    
          loanNoteId: this.loanNoteId,  //number
          stateID: this.stateID  //number
      }),  
      
      /***** Force closure *****/

      foreclosure: new FormGroup({
          
          attorneyFeesDue: this.attorneyFeesDue,
          name: this.name,
          address: this.Fcaddress,
          city: this.Fccity,
          scheduledSaleDate: this.scheduledSaleDate,
          zip: this.Fczip,
          phone: this.phone,
          email: this.email,
          dateOpened: this.dateOpened,
          stateID: this.FcstateID,
          loanNoteID: this.loanNoteID
      }),


      /******* bankruptcy *******/
      
        bankruptcy: new FormGroup({

        bkFillingDate:this.bkFillingDate,
        bkDisChargeDate:this.bkDisChargeDate,
        bkDismissalDate:this.bkDismissalDate,
        bankruptcyStatus:this.bkbankruptcyStatus,
        loanNoteID:this.bkloanNoteID,
        bkChapterID:this.bkChapterID

      }),


      /************* escrowImpounds ************/

      escrowImpounds: new FormGroup({
             
        escrowImpoundsStatus:this.escrowImpoundsStatus,
        notePaymentAmount:this.notePaymentAmount,
        annualInsurancePremium:this.annualInsurancePremium,
        taxPortion:this.taxPortion,
        annualTaxes:this.annualTaxes,
        insurancePortion:this.insurancePortion,
        trustBalance:this.trustBalance,
        loanNoteID:this.esloanNoteID

      }),

    /********** BORROWER INFORMATION *********/

      borrower: new FormGroup({

      isCompany:this.isCompany,
      firstName:this.firstName,
      lastName:this.lastName,
      maillingAddress:this.maillingAddress,
      homePh:this.homePh,
      workPh:this.workPh,
      fssTaxID:this.fssTaxID,
      companyName:this.companyName,
      companyContactName:this.companyContactName,
      address:this.braddress,
      city:this.brcity,
      country:this.brcountry,
      zip:this.brzip,
      phone:this.brphone,
      mobilePhone:this.brmobilePhone,
      email:this.bremail,
      stateID:this.brstateID,
      loanNoteId:this.brloanNoteId, 

      }),


      /****************** deedMortgage ****************/


      deedMortgage: new FormGroup({
           
        firstTrust:this.firstTrust,
        juniorTrust:this.juniorTrust,
        lienPosition:this.lienPosition,
        lienPosition1:this.lienPosition1,
        monthlyPayment1:this.monthlyPayment1,
        current1:this.current1,
        prnicipalBalance1:this.prnicipalBalance1,
        modified1:this.modified1,
        informationAsOf1:this.informationAsOf1,
        lienPosition2:this.lienPosition2,
        monthlyPayment2:this.monthlyPayment2,
        current2:this.current2,
        prnicipalBalance2:this.prnicipalBalance2,
        modified2:this.modified2,
        informationAsOf2:this.informationAsOf2,
        loanNoteID:this.deedloanNoteID,

      })

      

 

    });
     

  }



  constructor(private _httpService:httpServiceToken,private _sellNoteSer:SellnoteService,private dashboardServ: buyNotesService, private nav: NavbarService,
    private loading: loadingService, private pagerServ: pagerService, private alertService: alertService , private router : Router) { 
      this.nav.show();
    this._sellNoteSer.getDropDownData(this._urlClosingTypes).subscribe(data => {this._ClosingType = data});
    this._sellNoteSer.getDropDownData(this._urlLoanTypes).subscribe(data => {this._LoanTypes = data});
    this._sellNoteSer.getDropDownData(this._urlamortizationType).subscribe(data => {this._amortizationType = data});
    this._sellNoteSer.getDropDownData(this._urlloanStatus).subscribe(data => {this._loanStatus = data});
    this._sellNoteSer.getDropDownData(this._urlrateType).subscribe(data => {this._rateType = data});
    this._sellNoteSer.getDropDownData(this._urlpaymentFrequency).subscribe(data => {this._paymentFrequency = data});
    this._sellNoteSer.getDropDownData(this._urlStates).subscribe(data => {this._States = data});
    this._sellNoteSer.getDropDownData(this._urlPropertyTypes).subscribe(data => {this._PropertyTypes = data});
    this._sellNoteSer.getDropDownData(this._urlOccupancyStatus).subscribe(data => {this._OccupancyStatus = data});
    this._sellNoteSer.getDropDownData(this._urlValuationTypes).subscribe(data => {this._ValuationTypes = data});
    this._sellNoteSer.getDropDownData(this._urlBkChapter).subscribe(data => {this._BkChapter = data});
    this._sellNoteSer.getDropDownData(this._urlloanServicer).subscribe(data => {this._loanServicer = data});


    setTimeout(() => {
      /** popup **/
      $('#selectbox').change(function () {
        if ($(this).find(':selected').hasClass("#consumergoods")) {
            (<any>$('#consumergoods')).modal('show');
          }
      });

      $('#selectbox2').change(function () {
        if ($(this).find(':selected').hasClass("#consumergoods2")) {
            (<any>$('#consumergoods2')).modal('show');
          }
      });

      $('#selectbox3').change(function () {
        if ($(this).find(':selected').hasClass("#consumergoods3")) {
            (<any>$('#consumergoods3')).modal('show');
          }
      });
   
    

  }, 500);


   
       // this._loannote = new loanNote();
        //this._loannote.PI = 1232;

        // this.sellNoteForm = fb.group({
        //   'closingType': [null, Validators.required],
        //   'loanType': [null, Validators.required],
        //   'validate' : ''
        // });

        // this.sellNoteForm.controls['closingType'].valueChanges.subscribe(c => 
        //   console.log(c)
        // );
        
  }


 
  // submit(){
  
  // }

  onSubmitSellNote(){
  //  this.loading.show();
    console.log(this.sellNoteForm.controls);
   let i:number = 0;
    Object.keys(this.sellNoteForm.controls).forEach(key => {
      if(this.sellNoteForm.get(key).valid == false){
        console.log("controle :"+key+" :"+ this.sellNoteForm.get(key).valid);
        i++;
      } 
       
       
    });  
 
    console.log( this.upload());
    
    //this.uploadFiles(9);
   
       
  
  // console.log(i);
  // if(this.upload()){
  //   this.uploadFiles(1);
  // }else{
  //      this.alertService.clear();
  //      this.alertService.error(this.errorMessage +" "+this.errorTypeMessage);
  // }
 
  
  
   if (this.sellNoteForm.valid) {
  console.log("upload error ===>"+ this.upload());
      if(this.upload()){

        let formDatax: FormData = new FormData();
        formDatax.append("loannote",JSON.stringify(this.sellNoteForm.value));
          for (var k = 0; k < this.filesToUpload.length; k++)
          {                 
            formDatax.append(this.filesToUpload[k].uploadedType, this.filesToUpload[k].file, this.filesToUpload[k].file.name);
          } 
        this._httpService
        .post(this._urlPostLoanNote,formDatax)
          .subscribe(result => 
        { 
           
          // this.uploadFiles(result.json());
            setTimeout(() => {
                      this.loading.hide();
                       
                      this.alertService.clear();
                      window.scrollTo(0, 0);
                      this.alertService.success('Your Loan has been successfully listed',true);
                      //this.router.navigate(['about-us']);
                     
                  }, 500);
           
          });
          
      }else{
        //this.alertService.clear();
        this.alertService.error(this.errorTypeMessage + " " + this.errorMessage);
      }

  } else {
        
    this.validateAllFormFields(this.sellNoteForm);
    setTimeout(() => {
      this.loading.hide();
      window.scrollTo(0, 0);
      //this.alertService.clear();
      this.alertService.error(this.errorTypeMessage + " " + this.errorMessage);
    }, 300);
     
    }

  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

   

  /*************************************Start Upload Files(Doc & Img)************************************/

  fileChangeEvent(fileInput: any,uploadedType:string)
  {
      //Clear Uploaded Files result message
      this.uploadResult = "";
      this.filesToUploadR = <Array<File>>fileInput.target.files;
     for(var i = 0 ; i< this.filesToUploadR.length ;i++){
      
         this.filesToUpload.push({"uploadedType" : uploadedType,"file" : this.filesToUploadR[i]});
      }
      //this.filesToUpload.push(this.filesToUploadR[0]);
      console.log(this.filesToUpload);

     
  }  
 


  validateFileNumber(){
    var validationResult = false;
    var photo = 0;
    var PaymentHistory = 0;
    var other = 0;
     for(var j = 0 ;j<this.filesToUpload.length;j++){
       if(this.filesToUpload[j].uploadedType == "photo"){ 
         photo++;
         if(photo > 3){ validationResult = false }else{ validationResult = true }
       }
       if(this.filesToUpload[j].uploadedType == "other"){ other++; 
        if(other > 3){ validationResult = false }else{ validationResult = true }
      }
       if(this.filesToUpload[j].uploadedType == "PaymentHistory"){ PaymentHistory++; if(PaymentHistory > 1){ validationResult = false }else{ validationResult = true } }
     }
     return validationResult;
   }
                       
  cancelUpload()    
  {        
                           
       this.filesToUpload = [];      
      // this.fileUploadVar.nativeElement.value = "";    
       this.selectedFileNames = [];   
       this.uploadResult = "";
       this.errorMessage = ""; 
                                                    
      }    
upload() {
    if (this.filesToUpload != undefined) {
        for (let i = 0; i < this.filesToUpload.length; i++) {
            this.selectedFileNames.push(this.filesToUpload[i].file.name);
        }
        if (this.filesToUpload.length == 0) {
            this.errorMessage = " select 1 or than";
            return false;
        } else if (!this.validateFileNumber()) {
            this.errorMessage = " The max file selected is 3 for";
            return false;
        }
        else if(this.validatePDFSelectedOnly(this.selectedFileNames) && this.validateSize())
         {
             return true
      } else {
          this.errorTypeMessage = " and just Doc or pdf allowed to upload and file size is limited to 1Mb";
          return false;
      }
    }else{
      this.errorMessage = "not file selected"; return false;
    }

}

  validatePDFSelectedOnly(filesSelected: string[])
  {
    console.log("selected name -->"+filesSelected);
      for (var i = 0; i < filesSelected.length; i++)
      {
        if (filesSelected[i].slice(-3).toUpperCase() != "PDF" && filesSelected[i].slice(-3).toUpperCase() != "DOC")
        {                 
        return false;
        }
        else {
          return true;
        }
      }
  }     

  validateSize()
  {
      var returnValidationStatus = false;  
      for (var i = 0; i < this.filesToUpload.length; i++)
      {
        if (this.filesToUpload[i].file.size > 100000)
        {                 
           return false;
        }
        else {
          returnValidationStatus = true;
        }
      }
      return returnValidationStatus;
  } 


  uploadFiles(id)
  {  console.log('--->length'+this.filesToUpload.length);
    this.uploadResult = "";
    if (this.filesToUpload.length > 0)
      {
          this.isLoadingData = true;

          let formData: FormData = new FormData();
          
          formData.append("id", id);
         
          for (var i = 0; i < this.filesToUpload.length; i++)
          {                 
            formData.append(this.filesToUpload[i].uploadedType, this.filesToUpload[i].file, this.filesToUpload[i].file.name);
          } 
         // console.log();  return false;           
          let apiUrl = "https://localhost:44305/api/loanNotes/UploadFiles";
          this._httpService.post(apiUrl, formData).map(res => res.json())
              .subscribe
              (
                  data => {
                    console.log("<==resumt==>  "+data);
                      this.uploadResult = data;
                      this.errorMessage = "";
                      
                  },
                  err => {
                      console.error(err);
                      this.errorMessage = err;
                      this.isLoadingData = false;
                  },
                  () => {
                      this.isLoadingData = false
                          //this.fileUploadVar.nativeElement.value = "";
                    //  this.selectedFileNames = [];
                     // this.filesToUpload = [];
                  }
              );
      }
  }

  /*************************************End Upload Files(Doc & Img)************************************/
  clearArrayOfValidators(attrs){
    console.log("Start Clearing Validator");
    for (var _i = 0; _i < attrs.length; _i++) {
      var item = attrs[_i];
      this[`${item}`].clearValidators();
      console.log(this[`${item}`].validator);
    }
  }

  addArrayOfValidators(attrs){
    console.log("Start Adding Validator");
    for (var _i = 0; _i < attrs.length; _i++) {
      var item = attrs[_i];
      this[`${item}`].setValidators([Validators.required]);
      this[`${item}`].updateValueAndValidity();
      console.log(this[`${item}`].validator);
    }
  }

  addEmailValidation(attrs){
    for (var _i = 0; _i < attrs.length; _i++) {
      var item = attrs[_i];
      
      this[`${item}`].setValidators(Validators.email);
      this[`${item}`].updateValueAndValidity();
      console.log(this[`${item}`].validator);
    }
  }

  changeDRadioValue(formControl,boolvar){
 
    this.sellNoteForm.controls[formControl].setValue(false); 
    this[`${boolvar}`] = !this[`${boolvar}`];
  }
  
  ModalAddValidation(checkedvalue,attrs,resetGroup?:string){
 
    if(!this[`${checkedvalue}`]){
      this.addArrayOfValidators(attrs); 
    }if(this[`${checkedvalue}`] ){
      if(this._isValidDeed==false){
        this.clearArrayOfValidators(attrs);
        this.sellNoteForm.controls[resetGroup].reset();
      }
  
    }    
       
  }

  resetRadio(checkedvalue,attrs){
       
      this[`${checkedvalue}`] = false;
      this.clearArrayOfValidators(attrs); 
      
  }


/** Add required validator to array of form controls in select dropdown option to show form modal, close modal, change dropdown option **/
  modalSelectAddValidation(attrs,SelectedModal,status?:string){

    if(SelectedModal == "Line Of Credit"){
      if(status == "selected"){
        this.addArrayOfValidators(attrs);
      }
      if(status == "closed"){
        this.clearArrayOfValidators(attrs);
      }
    }else{
        this.clearArrayOfValidators(attrs);
    }

      
  }

  SelectOptionAddValidation(attrs,selectedValue){
    if(selectedValue == 1 || selectedValue == 2){
      this.addArrayOfValidators(attrs);
    }else{
      this.clearArrayOfValidators(attrs); 
    }
    
  }
//

  resetSelect(select){
    this[`${select}`].reset();
  }
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('closeforcMoule') closeforcMoule: ElementRef;
  @ViewChild('bkclosebtn') bkclosebtn: ElementRef;
  
  
  validateFormGroup(formGroup,attrs,closebtn,checkedvalue?:string){
  //  console.log(this['lienPosition1'].validator);
    
    //console.log(this.sellNoteForm.get(formGroup).get('email').valid);
    //console.log(this.sellNoteForm.controls['deedMortgage'].valid);
    //console.log(this.sellNoteForm.controls[formGroup].valid); 
     
     if(this.sellNoteForm.controls[formGroup].valid == true){
      
      this._isValidDeed = true;
      this[`${closebtn}`].nativeElement.click();
       
     }else{
      for (var _i = 0; _i < attrs.length; _i++) {
        var item = attrs[_i];
        this.sellNoteForm.get(formGroup).get(item).markAsDirty();
        
      }
     
      
     }
    
     
  }

  
}
