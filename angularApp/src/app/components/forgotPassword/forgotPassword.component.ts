import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
//import { loanNote } from '../../models/loanNotes';
import { Http, RequestOptions, Request, Response, Headers } from '@angular/http';
import { buyNotesService, NavbarService, loadingService, pagerService, alertService } from '../../structExports/services';
 
import { FormGroup , Validators,FormControl } from '@angular/forms';
import { AppSettings } from '../../config/AppSettings' ;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotPasswordComponent implements OnInit {

 
  constructor(private _http:Http,private _alert:alertService, private router: Router) { }

  forgotpassword:FormGroup;
  emailforgot:FormControl;
  ngOnInit() {
  
    this.emailforgot = new FormControl(null,Validators.required);
    this.forgotpassword = new FormGroup({ Email : this.emailforgot, }); 
  }
 

  onForgotPassword(){
      
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
      
      this._http.post(AppSettings.API_ENDPOINT+"api/Account/ForgotPassword","Email="+this.emailforgot.value,options).subscribe(res => {
        this._alert.success("Please Check your Email to reset your password",true);
        this.router.navigate(['login']);
        console.log(res);
      });
      
      console.log(this.emailforgot.value);
  }

}
