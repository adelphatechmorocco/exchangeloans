import { Component, OnInit } from '@angular/core';
import {loadingService} from '../../structExports/services';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class loadingComponent implements OnInit {

  constructor (private loading:loadingService)
  {
  
  }


  ngOnInit() {
  }

}
