import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {authentificationService,NavbarService} from '../../structExports/services';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    //username :string = "";
  constructor (private authServ:authentificationService,private router:Router,private nav:NavbarService)
  { 
      // let info = JSON.parse(localStorage.getItem('currentuser'));
      // //console.log(info);
      // if(localStorage.getItem('currentuser'))
      // {
      //   let info = JSON.parse(localStorage.getItem('currentuser'));
      //   this.username = info.username;
      // }  
  }

  onLogout(){
    this.authServ.logout();
    this.router.navigate(['login']);
  }

  ngOnInit() {
    
  }

}
