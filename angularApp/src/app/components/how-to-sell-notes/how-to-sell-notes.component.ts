import { Component, OnInit } from '@angular/core';
import { NavbarService} from '../../structExports/services';

@Component({
  selector: 'app-how-to-sell-notes',
  templateUrl: './how-to-sell-notes.component.html',
  styleUrls: ['./how-to-sell-notes.component.css']
})
export class HowToSellNotesComponent implements OnInit {


  constructor(private nav: NavbarService) {

  }

  ngOnInit() {
    this.nav.show();
  }

}
