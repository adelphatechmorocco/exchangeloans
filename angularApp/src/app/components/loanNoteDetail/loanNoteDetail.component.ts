import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { loanNoteDetailService, NavbarService ,loadingService,} from '../../structExports/services';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-loan-note-detail',
  templateUrl: './loanNoteDetail.component.html',
  styleUrls: ['./loanNoteDetail.component.css']
})
export class loanNoteDetailComponent implements OnInit {



  id: number = 0;
  loanNote: any;
  bankruptcyChapter: any;
  PropertyType: any;
  similarNotes: any[];
  @ViewChild('flexslider') el:ElementRef; 
  @ViewChild('owldemo') elowl:ElementRef;

  constructor(private _loanNoteDet: loanNoteDetailService, private nav: NavbarService, private route: ActivatedRoute,private loading :loadingService, private _router:Router) {

    setTimeout(() => {
      /** Flex Slider **/
      (<any>$(this.el.nativeElement)).flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        lightbox: true
    });
   
    /** owl Slider **/

    (<any>$(this.elowl.nativeElement)).owlCarousel({
     
      autoPlay: 4000, //Set AutoPlay to 3 seconds
 
      items : 3,
      itemsDesktop: [1200, 1], //5 items between 1000px and 901px
      itemsDesktoplgall: [900, 1], // betweem 900px and 601px
      itemsTablet: [650, 1], //2 items between 600 and 0;
          //Pagination
      pagination : false,
      paginationNumbers: false,
 
  });

  }, 1000);

   }

  ngOnInit() {
      this.nav.show();
      this.loading.show();
      this.id = this.route.snapshot.params['id'];
      
      this.getAllNotes(this.id);
      setTimeout(() => {
        this.loading.hide();
      }, 2000);

      this.getLoanNoteAllDetail(this.id);
  }
  

  
  
  getAllNotes(id:number){
    this._loanNoteDet.getAllNotes(id).subscribe(cdata => {

      this.similarNotes = cdata;
      this.similarNotes = this.similarNotes;
      console.log(this.similarNotes);
    });
  }



  getLoanNoteAllDetail(id:number) {
    this._loanNoteDet.getLoanNoteAllDetail(id).subscribe(
      data => {
        console.log(data);
        this.loanNote = data[0];
        this.bankruptcyChapter = data[1];
        this.PropertyType = data[2];
      }
    );
  }

  SubmitOffer(item){
    this._router.navigate(["SubmitOffer", item.loanNoteID]);
  }

}
