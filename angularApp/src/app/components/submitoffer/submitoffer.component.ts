﻿import { Component, ViewContainerRef , OnInit } from '@angular/core';
import { httpServiceToken } from '../../services/httpServiceToken.service';
import { Http, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { alertService,NavbarService } from '../../structExports/services';
import { ActivatedRoute } from '@angular/router';
import { SubmitOfferViewModel } from '../../models/submitoffer.viewmodel';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Observable } from "rxjs/Rx";
import {AppSettings} from '../../config/AppSettings';
@Component({
    moduleId: module.id,
    selector: 'app-submitoffer',
    templateUrl: './submitoffer.component.html',
    styleUrls: ['./submitoffer.component.css']
})
export class SubmitOfferComponent implements OnInit{
    private _viewModel:SubmitOfferViewModel;
    private _loan:any;
    private _loanId:number; 
    private _result:any;
 
    
    id: number = 0;
    loanNote: any;
    bankruptcyChapter: any;
    PropertyType: any;
    
    private _closingTypes:Array<any> = [];
    private _dueDiligenceOptions:Array<any> = [];
    
    private _selectedDueDiligenceOption:number;
    private _selectedClosingType:number;
    private _rootUrl:string = AppSettings.API_ENDPOINT+'api';
    private _urlLoanNote:string = `${this._rootUrl}/loannotes/`;
    private _urlClosingtypes:string = `${this._rootUrl}/closingtypes/`;
    private _urlDuedilligenceOptions:string = `${this._rootUrl}/duedilligenceoptions/`;
    private _urlOffer:string = `${this._rootUrl}/offers/`;
    constructor(private nav: NavbarService,private _httpService:httpServiceToken, private route:ActivatedRoute, public toastr:ToastsManager, vcref:ViewContainerRef,private alertService: alertService){
        this.toastr.setRootViewContainerRef(vcref);
        this._viewModel = new SubmitOfferViewModel({
            loanNoteId: this._loanId,
            offerAmount: 0,
            comment: "",
            closingType: 0,
            dueDiligenceOption: 0
        });
        //this._viewModel.RegisterObserver(this);
    }   
    ngOnInit(){
        this.nav.show();
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        
        let sub = this.route.params.subscribe(params => {
            this._loanId = +params['id'];
            this._viewModel.LoanNoteId = +params['id'];
            this._httpService.get(`${this._urlLoanNote}${this._loanId}` , options).subscribe(
                (res: Response) => {
                    console.log(res);
                    this._loan = res.json();
                });
            this._httpService.get(`${this._urlLoanNote}${this._loanId}/closingtypes`, options).subscribe(
                (res: Response) => {
                    
                    this._closingTypes = res.json();
                });
            this._httpService.get(`${this._urlDuedilligenceOptions}`, options).subscribe(
                (res: Response) => { 
                    this._dueDiligenceOptions = res.json();
                });
            console.log(this._dueDiligenceOptions);     
        }); 
        
        
        this.id = this.route.snapshot.params['id'];
        this.getLoanNoteAllDetail(3).subscribe(
            data => {
              console.log("==========>"+data);
              this.loanNote = data[0];
              this.bankruptcyChapter = data[1];
              this.PropertyType = data[2];
            }
          ); 
    }
    DueDilugenceOptionChanged(id){
        this._viewModel.DueDiligenceOption = id;
    }
    ClosingTypeChanged(id){
        this._viewModel.ClosingType = id;
    }
    ReceiveNotification(message){
        console.log(message);
    }
    ClosingTypesSelectionChanged(){
        console.log(event.target);
    }
    getLoanNoteAllDetail(id: number) {
        return Observable.forkJoin(
          this._httpService.get(AppSettings.API_ENDPOINT+"loans/"+id+"?includes=loanStatus;loanType;amortizationType;rateType;foreclosure;paymentFrequency;escrowImpounds;deedMortgage;borrower;property;closingType;deedMortgage;escrowImpounds;").map(res => res.json()),
          this._httpService.get(AppSettings.API_ENDPOINT+"loans/"+id+"/bankruptcy").map(res => res.json()),
          this._httpService.get(AppSettings.API_ENDPOINT+"loans/"+id+"/Property").map((res :Response) => res.json())
        );
      }  
     
 
    submit(){
        console.log();
        var submitoffer = this._viewModel.getOfferPOCO();
         
        if(submitoffer.OfferAmount >= this._loan.minAskingPrice){
            console.log("dkhl");
           this._httpService
            .post(this._urlOffer, this._viewModel.getOfferPOCO())
            .subscribe(
                result => 
                     {
                // if(result.status) ....; 
                // ajout d'alerte pour success ou failure
                // hadi nkhlik tkelef lya biha ila moumkin 
                // mazal matelit 3la alert component akhay j'aime beaucoupe ta fa
                //console.log(result)
                setTimeout(() => {
                    this.alertService.clear();
                    this.alertService.success('Offer submited successfuly');
                  }, 300);

            },
        
            error =>{
                this.alertService.clear();
                this.alertService.error("you have already submitted an offer on this Note");
            }
        ); 
        }else{

            this.alertService.clear();
            this.alertService.error('your Offer Amount is rejected because its less than min asking price');
        }
    }
    showSuccess(message, title) {
        this.toastr.success(message, title);
    }
    
    showError(message, title) {
        this.toastr.error(message, title);
    }   
}