import { Component, OnInit,OnDestroy } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Users } from '../../models/users';
import { authentificationService, NavbarService, httpServiceToken } from '../../structExports/services';
import 'rxjs/add/operator/takeWhile';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class loginComponent implements OnInit {
  user: any = {};
  loading: boolean = false;
  error: string = '';
  alive:boolean = true;
  rememberme: boolean = false;
  constructor(private _authenServ: authentificationService, private router: Router,private activeRoute:ActivatedRoute ,private nav: NavbarService) {
  }
  ngOnInit() {
    this._authenServ.logout();
    this.nav.hide();
    this.user.username = this.activeRoute.snapshot.queryParamMap.get('email');
  }
  onLogin() {
    this.loading = true;
    this._authenServ.login(this.user.username, this.user.password,this.rememberme).takeWhile(() => this.alive).subscribe(
      res => {
        if (res === true) {

          this.loading = false;
          this.router.navigate(['about-us']);
        }
      },
      err => {
        this.error = 'User name or password is incorrect';
        this.loading = false;
      },
      () => console.log('Task Complete')
    );


  }
  ngOnDestroy(){
    this.alive = false;
  }




}
