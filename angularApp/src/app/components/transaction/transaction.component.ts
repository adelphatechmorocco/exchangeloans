import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { buyNotesService, NavbarService, loadingService, pagerService, alertService, transactionService } from '../../structExports/services';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from "rxjs";
import { IntervalObservable } from "rxjs/observable/IntervalObservable";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import * as _ from 'lodash';
import { statesData } from '../../structExports/models';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class transactionComponent implements OnInit, OnDestroy {

  private alive: boolean = true;
  checkStepOne: boolean = false;
  checkStepTwo: boolean = false;
  checkStepTheree: boolean = false;
  checkStepFour: boolean = false;
  checkStepFive: boolean = false;

  //private subscription: Subscription;
  states: any;
  checkInformationBuyerSeller: boolean = true;

  
  statusStepOne: string;
  statusStepTwo: string;
  statusStepTheree: string;
  statusStepFour: string;
  statusStepFive: string;


  detectedUser: string;
  transaction: any;
  checkUser: any;
  OfferId: any;
  idTransaction: FormControl;

  /*setp one field*/
  formStepOne: FormGroup;
  offerAmountStepOne: FormControl;
  closingServiceStepOne: FormControl;
  dueDiligenceOptionStepOne: FormControl;
  daysStepOne: FormControl;
  statusDetail: FormControl;
  IdDetailStep: FormControl;
  role: FormControl;
  turnUser: FormControl;
  isFinishStepOne: boolean = false;
  isChangeValuesStepOne: boolean = false;


  /*setp two fields*/
  formStepTwo: FormGroup;
  daysStepTwo: FormControl;
  DueDilidenceStepTwo: FormControl;
  statusDetailStepTwo: FormControl;
  IdDetailStepStepTwo: FormControl;
  roleStepTwo: FormControl;
  turnUserStepTwo: FormControl;
  isFinishStepTow: boolean = false;
  isChangeValuesStepTow: boolean = false;
  isInitStepTwo: boolean = false;
  isActiveStepTwo: boolean = true;

  /*step theree fields */
  formStepTheree: FormGroup;
  offerAmountStepTheree: FormControl;
  closingServiceStepTheree: FormControl;
  roleStepTheree: FormControl;
  turnUserStepTheree: FormControl;
  statusDetailStepTheree: FormControl;
  IdDetailStepTheree: FormControl;
  isFinishStepTheree: boolean = false;
  isChangeValuesStepTheree: boolean = false;
  isInitStepTheree: boolean = false;

  /*step theree info seller and buyer */
  formStepThereeInformation: FormGroup;
  company: FormControl;
  contactPerson: FormControl;
  address: FormControl;
  city: FormControl;
  state: FormControl;
  zip: FormControl;
  phone: FormControl;
  email: FormControl;
  other: FormControl;
  loanServicerId: FormControl;
  roleInformation: FormControl;
  transactionID: FormControl;



  constructor(private nav: NavbarService, private loading: loadingService, private alertService: alertService,
      private router: Router, private activatedRouter: ActivatedRoute, private TransactionService: transactionService,
      public toastr: ToastsManager, vcref: ViewContainerRef) {

      this.toastr.setRootViewContainerRef(vcref);
      this.states = statesData;
      this.checkUser = localStorage.getItem('OSB0303');
      this.OfferId = this.activatedRouter.snapshot.queryParamMap.get('offerId');
      if (this.checkUser == "OSB0313B") {
          this.detectedUser = "buyer";
      } else if (this.checkUser == "OSB0313S") {
          this.detectedUser = "seller";
      } else {
          this.router.navigate(['my-exchange']);
      }
  }
  //OSB0313B buyer
  //OSB0313S seller
  ngOnInit() {
      this.loading.show();
      this.nav.show();

      this.onCreateFormControlStepOne();
      this.onCreateFormStepOne();

      this.onCreateFormControlStepTwo();
      this.onCreateFormStepTwo();

      this.onCreateFormControlStepTheree();
      this.onCreateFormStepTheree();

      this.onCreateFormControlStepThereeInformation();
      this.onCreateFormStepThereeInformation();

      this.getTransaction();

      setTimeout(() => {
          this.loading.hide();
      }, 1000);


  }


  /*create formcontrol and formGroup for step one */
  onCreateFormControlStepOne() {
      this.offerAmountStepOne = new FormControl('', [Validators.required, Validators.pattern("^[0-9]+$")]);
      this.closingServiceStepOne = new FormControl('', Validators.required);
      this.dueDiligenceOptionStepOne = new FormControl('', Validators.required);
      this.daysStepOne = new FormControl('', Validators.pattern("^[0-9]+$"));
      this.statusDetail = new FormControl('');
      this.IdDetailStep = new FormControl('');
      this.role = new FormControl('');
      this.turnUser = new FormControl('');
      this.idTransaction = new FormControl('');
  }

  onCreateFormStepOne() {
      this.formStepOne = new FormGroup({
          offerAmountStepOne: this.offerAmountStepOne,
          closingServiceStepOne: this.closingServiceStepOne,
          dueDiligenceOptionStepOne: this.dueDiligenceOptionStepOne,
          daysStepOne: this.daysStepOne,
          status: this.statusDetail,
          IdDetailStep: this.IdDetailStep,
          role: this.role,
          turnUser: this.turnUser,
          idTransaction: this.idTransaction
      });
  }
  /*create formcontrol and formGroup for step tow */
  onCreateFormControlStepTwo() {
      this.daysStepTwo = new FormControl('');
      this.DueDilidenceStepTwo = new FormControl('');
      this.statusDetailStepTwo = new FormControl('');
      this.IdDetailStepStepTwo = new FormControl('');
      this.roleStepTwo = new FormControl('');
      this.turnUserStepTwo = new FormControl('');
      this.idTransaction = new FormControl('');

  }
  onCreateFormStepTwo() {
      this.formStepTwo = new FormGroup({
          daysStepTwo: this.daysStepTwo,
          DueDilidenceStepTwo: this.DueDilidenceStepTwo,
          statusDetailStepTwo: this.statusDetailStepTwo,
          IdDetailStepStepTwo: this.IdDetailStepStepTwo,
          roleStepTwo: this.roleStepTwo,
          turnUserStepTwo: this.turnUserStepTwo,
          idTransaction: this.idTransaction,
      });
  }

  /*create formcontrol and formGroup for step Theree */
  onCreateFormControlStepTheree() {
      this.offerAmountStepTheree = new FormControl();
      this.closingServiceStepTheree = new FormControl('');
      this.statusDetailStepTheree = new FormControl('');
      this.turnUserStepTheree = new FormControl('');
      this.roleStepTheree = new FormControl('');
      this.idTransaction = new FormControl('');
      this.IdDetailStepTheree = new FormControl('');
  }
  onCreateFormStepTheree() {
      this.formStepTheree = new FormGroup({
          offerAmountStepTheree: this.offerAmountStepTheree,
          closingServiceStepTheree: this.closingServiceStepTheree,
          statusDetailStepTheree: this.statusDetailStepTheree,
          turnUserStepTheree: this.turnUserStepTheree,
          roleStepTheree: this.roleStepTheree,
          idTransaction: this.idTransaction,
          IdDetailStepTheree: this.IdDetailStepTheree
      });
  }
  /*setep theree informatyion seller , buyer */
  onCreateFormControlStepThereeInformation() {
      this.company = new FormControl('', Validators.required);
      this.contactPerson = new FormControl('', Validators.required);
      this.address = new FormControl('', Validators.required);
      this.city = new FormControl('', Validators.required);
      this.state = new FormControl('', Validators.required);
      this.zip = new FormControl('', Validators.required);
      this.phone = new FormControl('', [Validators.required,Validators.pattern("^[0-9]+$")]);
      this.email = new FormControl('',[Validators.required,Validators.email]);
      this.other = new FormControl('');
      this.loanServicerId = new FormControl('', Validators.required);
      this.roleInformation = new FormControl('');
      this.transactionID = new FormControl('');
  }


  onCreateFormStepThereeInformation() {
      this.formStepThereeInformation = new FormGroup({
          company: this.company,
          contactPerson: this.contactPerson,
          address: this.address,
          city: this.city,
          state: this.state,
          zip: this.zip,
          phone: this.phone,
          email: this.email,
          other: this.other,
          loanServicerId: this.loanServicerId,
          roleInformation: this.roleInformation,
          transactionID: this.transactionID,
      });
  }

  getTransaction() {
      this.TransactionService.getTransaction(this.OfferId).takeWhile(() => this.alive).subscribe(res => {
          this.transaction = res;
          console.log(this.transaction);
          this.detectedStepByCurrentUserAndStep();
      });
  }

  detectedStepByCurrentUserAndStep() {
      if (this.transaction && this.detectedUser) {
          //***************************************** begin setp 1 ***************************************
          this.stepOneTrait();
          //***************************************** end setp 1 ***************************************

          //***************************************** begin setp 2 ***************************************
          this.stepTwoTrait();
          //***************************************** end setp 2 ***************************************
          //***************************************** begin setp 3 ***************************************
          this.stepThereeTrait();
          //***************************************** end setp 2 ***************************************
      }

  }

  /*All treatemnts the first step for buyer and seller*/
  stepOneTrait() {
      /* setp is pending */
      if (this.transaction.DetTrans1[0].stepStatus == "PENDING") {

          /*turn seller */
          if (this.transaction.DetTrans1[0].turnUser == "seller") {

              if (this.detectedUser == "buyer") {
                  let aliveRequestNews = true;
                  this.statusStepOne = "Pending";
                  this.checkStepOne = false;
                  this.formStepOne.controls['role'].setValue("buyer");
                  this.formStepOne.controls['offerAmountStepOne'].disable();
                  this.formStepOne.controls['closingServiceStepOne'].disable();
                  this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
                  this.formStepOne.controls['daysStepOne'].disable();
                  /* get stream data */
                  // this.subscription= this.getDataWithInterval(aliveRequestNews);

              } else {
                  // this.subscription.unsubscribe();
                  this.statusStepOne = "He is waiting for your confirmation";
                  this.checkStepOne = true;
                  this.formStepOne.controls['role'].setValue("seller");
                  this.turnUser.setValue("buyer");
                  this.formStepOne.controls['offerAmountStepOne'].enable();
                  this.formStepOne.controls['closingServiceStepOne'].enable();
                  this.formStepOne.controls['dueDiligenceOptionStepOne'].enable();
                  this.formStepOne.controls['daysStepOne'].enable();
              }

          }


          /*turn buyer*/
          if (this.transaction.DetTrans1[0].turnUser == "buyer") {

              if (this.detectedUser == "seller") {
                  let aliveRequestNews = true;
                  this.statusStepOne = "Pending";
                  this.checkStepOne = false;
                  this.formStepOne.controls['role'].setValue("seller");
                  this.formStepOne.controls['offerAmountStepOne'].disable();
                  this.formStepOne.controls['closingServiceStepOne'].disable();
                  this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
                  this.formStepOne.controls['daysStepOne'].disable();
                  /* get stream data */
                  //  this.subscription = this.getDataWithInterval(aliveRequestNews);

              } else {

                  //this.subscription.unsubscribe();
                  this.statusStepOne = "He is waiting for your confirmation";
                  this.checkStepOne = true;
                  this.formStepOne.controls['role'].setValue("buyer");
                  this.turnUser.setValue("seller");
                  this.formStepOne.controls['offerAmountStepOne'].enable();
                  this.formStepOne.controls['closingServiceStepOne'].enable();
                  this.formStepOne.controls['dueDiligenceOptionStepOne'].enable();
                  this.formStepOne.controls['daysStepOne'].enable();
              }


          }

          /*transaction is accept for buyer and seller*/
      } else if (this.transaction.DetTrans1[0].stepStatus == "ACCEPT") {
          this.isFinishStepOne = true;
          this.statusStepOne = "ACCEPT";
          this.checkStepOne = false;
          this.formStepOne.controls['offerAmountStepOne'].disable();
          this.formStepOne.controls['closingServiceStepOne'].disable();
          this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
          this.formStepOne.controls['daysStepOne'].disable();
          /*transaction is reject for buyer or seller*/
      } else {
          this.isFinishStepOne = true;
          this.statusStepOne = "REJECT";
          this.checkStepOne = false;
          this.formStepOne.controls['offerAmountStepOne'].disable();
          this.formStepOne.controls['closingServiceStepOne'].disable();
          this.formStepOne.controls['dueDiligenceOptionStepOne'].disable();
          this.formStepOne.controls['daysStepOne'].disable();
          this.alertService.error('This is transaction is Reject in step one !');
      }


      /* the last negociation for this transaction on setp one avalaible for buyer and seller */
      this.formStepOne.controls['daysStepOne'].setValue(this.transaction.negotiationStep1[0].days);
      this.formStepOne.controls['offerAmountStepOne'].setValue(this.transaction.negotiationStep1[0].offerAmount);
      this.formStepOne.controls['closingServiceStepOne'].setValue(this.transaction.negotiationStep1[0].closingServiceId, {
          onlySelf: true
      });
      this.formStepOne.controls['dueDiligenceOptionStepOne'].setValue(this.transaction.negotiationStep1[0].dueDiligenceOptionId, {
          onlySelf: true
      });
      this.closingServiceStepOne.setValue(this.transaction.negotiationStep1[0].closingServiceId);
      this.dueDiligenceOptionStepOne.setValue(this.transaction.negotiationStep1[0].dueDiligenceOptionId);

      this.IdDetailStep.setValue(this.transaction.DetTrans1[0].ID);
      this.formStepOne.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);



      /* we subscribe the form so that we can see the modfication */
      this.formStepOne.valueChanges.takeWhile(() => this.alive).subscribe(form => {
          if (this.formStepOne.controls['daysStepOne'].value != this.transaction.negotiationStep1[0].days ||
              this.formStepOne.controls['offerAmountStepOne'].value != this.transaction.negotiationStep1[0].offerAmount ||
              this.formStepOne.controls['closingServiceStepOne'].value != this.transaction.negotiationStep1[0].closingServiceId ||
              this.formStepOne.controls['dueDiligenceOptionStepOne'].value != this.transaction.negotiationStep1[0].dueDiligenceOptionId) {
              this.isChangeValuesStepOne = true;

          } else {
              this.isChangeValuesStepOne = false;
          }
      });


  }

  /*all treatments and checks to submit the first step*/
  onSubmitStepOne(typeConfirmation: string) {
      this.loading.show();
      if (!this.isFinishStepOne) {
          if (this.checkStepOne) {
              if(this.formStepOne.valid)
              {
                  if (typeConfirmation == "ACCEPT") {
                    this.alertService.clear();
                    if (this.isChangeValuesStepOne == false) {

                        this.statusDetail.setValue("ACCEPT");
                        this.toastr.success("Your operation is done", "ACCEPT", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(() => this.alive).subscribe(res => {
                            res
                        });
                        setTimeout(() => {
                            this.getTransaction();
                            this.loading.hide();
                        }, 1500);

                    } else {
                        this.alertService.info('This operation not available because you are making the change in step!');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }

                }
              

                if (typeConfirmation == "COUNTER") {
                    this.alertService.clear();
                    if (this.isChangeValuesStepOne == true) {

                        this.statusDetail.setValue("COUNTER");
                        this.toastr.info("Your operation is done", "COUNTER", {
                            showCloseButton: true
                        });
                        this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(() => this.alive).subscribe(res => {
                            res
                        });
                        setTimeout(() => {
                            this.getTransaction();
                            this.loading.hide();
                        }, 1500);

                    } else {

                        this.alertService.info('This operation is not available because you do not make any changes !');
                        this.loading.hide();
                        window.scrollTo(0, 0);
                    }
                }

              }else{
                this.loading.hide();
                this.validateAllFormFields(this.formStepOne);
                this.toastr.error("fill in all mandatory fields", "ERROR", {
                  showCloseButton: true
                });
               
              }

              if (typeConfirmation == "REJECT") {
                this.alertService.clear();
                this.statusDetail.setValue("REJECT");
                this.toastr.error("Your operation is done", "REJECT", {
                    showCloseButton: true
                });
                this.TransactionService.postStepOne(this.formStepOne.value).takeWhile(() => this.alive).subscribe(res => {
                    res
                });
                setTimeout(() => {
                  this.getTransaction();
                  this.loading.hide();
              }, 1500);

              }



          } else {
              this.alertService.clear();
              this.alertService.info('wait for the answer of the other');
              this.loading.hide();
              window.scrollTo(0, 0);
          }

      } else {
          this.alertService.clear();
          this.alertService.info('This step is finished');
          this.loading.hide();
          window.scrollTo(0, 0);
      }

  }

  /*All treatemnts the scond step for buyer and seller*/
  stepTwoTrait() {
      if (!_.isEmpty(this.transaction.DetTrans2)) {
          /* setp is init */
          if (this.transaction.DetTrans2[0].stepStatus == "INIT") {

              if (this.detectedUser == "seller") {
                  this.statusStepTwo = "Pending";
                  this.checkStepTwo = false;
                  this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                  this.formStepTwo.controls['daysStepTwo'].disable();
                  this.formStepTwo.controls['DueDilidenceStepTwo'].disable();

              } else {
                  this.isInitStepTwo = true;
                  this.statusStepTwo = "He is waiting for your confirmation";
                  this.checkStepTwo = true;
                  this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                  this.turnUserStepTwo.setValue("seller");
              }
              this.IdDetailStepStepTwo.setValue(this.transaction.DetTrans2[0].ID);
              this.formStepTwo.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);


          } else {
              this.isInitStepTwo = false;
              /*PENDING*/
              if (this.transaction.DetTrans2[0].stepStatus == "PENDING") {

                  /*turn seller */
                  if (this.transaction.DetTrans2[0].turnUser == "seller") {

                      if (this.detectedUser == "buyer") {

                          this.statusStepTwo = "Pending";
                          this.checkStepTwo = false;
                          this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                          this.formStepTwo.controls['daysStepTwo'].disable();
                          this.formStepTwo.controls['DueDilidenceStepTwo'].disable();

                      } else {
                          this.statusStepTwo = "He is waiting for your confirmation";
                          this.checkStepTwo = true;
                          this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                          this.turnUserStepTwo.setValue("buyer");
                      }

                  }

                  /*turn buyer*/
                  if (this.transaction.DetTrans2[0].turnUser == "buyer") {

                      if (this.detectedUser == "seller") {
                          this.statusStepTwo = "Pending";
                          this.checkStepTwo = false;
                          this.formStepTwo.controls['roleStepTwo'].setValue("seller");
                          this.formStepTwo.controls['daysStepTwo'].disable();
                          this.formStepTwo.controls['DueDilidenceStepTwo'].disable();

                      } else {
                          this.statusStepTwo = "He is waiting for your confirmation";
                          this.checkStepTwo = true;
                          this.formStepTwo.controls['roleStepTwo'].setValue("buyer");
                          this.turnUserStepTwo.setValue("seller");
                      }


                  }


              } else if (this.transaction.DetTrans2[0].stepStatus == "ACCEPT") {
                  this.isFinishStepTow = true;
                  this.statusStepTwo = "ACCEPT";
                  this.checkStepOne = false;
                  this.formStepTwo.controls['daysStepTwo'].disable();
                  this.formStepTwo.controls['DueDilidenceStepTwo'].disable();


                  /*transaction is reject for buyer or seller*/
              } else {
                  this.isFinishStepTow = true;
                  this.statusStepTwo = "REJECT";
                  this.checkStepOne = false;
                  this.alertService.error('This is transaction is Reject in step tow !');
                  this.formStepTwo.controls['daysStepTwo'].disable();
                  this.formStepTwo.controls['DueDilidenceStepTwo'].disable();

              }
              this.formStepTwo.controls['daysStepTwo'].setValue(this.transaction.negotiationStep2[0].days);
              this.formStepTwo.controls['DueDilidenceStepTwo'].setValue(this.transaction.negotiationStep2[0].endDueDiligence, {
                  onlySelf: true
              });
              this.DueDilidenceStepTwo.setValue(this.transaction.negotiationStep2[0].endDueDiligence);

              this.IdDetailStepStepTwo.setValue(this.transaction.DetTrans2[0].ID);
              this.formStepTwo.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);

              this.formStepTwo.valueChanges.takeWhile(() => this.alive).subscribe(form => {
                  if (this.formStepTwo.controls['daysStepTwo'].value != this.transaction.negotiationStep2[0].days ||
                      this.formStepTwo.controls['DueDilidenceStepTwo'].value != this.transaction.negotiationStep2[0].endDueDiligence) {

                      this.isChangeValuesStepTow = true;
                  } else {
                      this.isChangeValuesStepTow = false;
                  }


              });
          } /*end else INIT*/
      }

  }
  /*all treatments and checks to submit the first step*/
  onSubmitStepTwo(typeConfirmation: string) {
      this.loading.show();

      if (!this.isFinishStepTow) {
          if (this.checkStepTwo) {

              if (typeConfirmation == "INIT") {
                  this.statusDetailStepTwo.setValue("COUNTER");
                  this.toastr.info("Your operation is done", "Init", {
                      showCloseButton: true
                  });
                  this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(() => this.alive).subscribe(res => {
                      res
                  });
                  setTimeout(() => {
                      this.getTransaction();
                      this.loading.hide();
                  }, 1500);
              }
              if (typeConfirmation == "ACCEPT") {
                  this.alertService.clear();
                  if (this.isChangeValuesStepTow == false) {
                      this.statusDetailStepTwo.setValue("ACCEPT");
                      this.toastr.success("Your operation is done", "ACCEPT", {
                          showCloseButton: true
                      });
                      this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(() => this.alive).subscribe(res => {
                          res
                      });
                      setTimeout(() => {
                          this.getTransaction();
                          this.loading.hide();
                      }, 1500);

                  } else {
                      this.alertService.info('This operation not available because you are making the change in step!');
                      this.loading.hide();
                      window.scrollTo(0, 0);
                  }
              }
              if (typeConfirmation == "REJECT") {
                  this.statusDetailStepTwo.setValue("REJECT");
                  this.toastr.error("Your operation is done", "REJECT", {
                      showCloseButton: true
                  });
              }
              if (typeConfirmation == "COUNTER") {
                  this.alertService.clear();
                  if (this.isChangeValuesStepTow == true) {
                      this.statusDetailStepTwo.setValue("COUNTER");
                      this.toastr.info("Your operation is done", "COUNTER", {
                          showCloseButton: true
                      });
                      this.TransactionService.postStepTow(this.formStepTwo.value).takeWhile(() => this.alive).subscribe(res => {
                          res
                      });
                      setTimeout(() => {
                          this.getTransaction();
                          this.loading.hide();
                      }, 1500);

                  } else {
                      this.alertService.info('This operation is not available because you do not make any changes !');
                      this.loading.hide();
                      window.scrollTo(0, 0);

                  }
              }


          } else {
              this.alertService.clear();
              this.alertService.info('Wait for the answer of the other/This step is not available currently');
              window.scrollTo(0, 0);
              this.loading.hide();
          }

      } else {
          this.alertService.clear();
          this.alertService.info('This step is finished');
          window.scrollTo(0, 0);
          this.loading.hide();
      }

  }
  /*All treatemnts the third step for buyer and seller*/
  stepThereeTrait() {
      if (!_.isEmpty(this.transaction.DetTrans3)) {

          if (_.isEmpty(this.transaction.vestingInctructionBuyer) && this.detectedUser == "buyer") {
              this.statusStepTheree = "Before going back to the negotiation, fill in the information ";
              this.checkStepTheree = false;
              this.formStepTheree.controls['offerAmountStepTheree'].disable();
              this.formStepTheree.controls['closingServiceStepTheree'].disable();
              this.formStepThereeInformation.controls['roleInformation'].setValue("buyer");
              this.formStepThereeInformation.controls['transactionID'].setValue(this.transaction.Transaction[0].offerID);
          } else if (_.isEmpty(this.transaction.vestingInctructionSeller) && this.detectedUser == "seller") {
              this.statusStepTheree = "Before going back to the negotiation, fill in the information";
              this.checkStepTheree = false;
              this.formStepTheree.controls['offerAmountStepTheree'].disable();
              this.formStepTheree.controls['closingServiceStepTheree'].disable();
              this.formStepThereeInformation.controls['roleInformation'].setValue("seller");
              this.formStepThereeInformation.controls['transactionID'].setValue(this.transaction.Transaction[0].offerID);

          } else {
              if (this.detectedUser == "seller") {
                  this.getInformationBuyerSeller(this.transaction.vestingInctructionSeller);
              } else {
                  this.getInformationBuyerSeller(this.transaction.vestingInctructionBuyer)
              }
              /* setp is init */
              if (this.transaction.DetTrans3[0].stepStatus == "INIT") {

                  if (this.detectedUser == "seller") {
                      this.statusStepTheree = "Pending";
                      this.checkStepTheree = false;
                      this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                      this.formStepTheree.controls['offerAmountStepTheree'].disable();
                      this.formStepTheree.controls['closingServiceStepTheree'].disable();
                  } else {
                      this.isInitStepTheree = true;
                      this.statusStepTheree = "He is waiting for your confirmation";
                      this.checkStepTheree = true;
                      this.formStepTheree.controls['roleStepTheree'].setValue("buyer");
                      this.turnUserStepTheree.setValue("seller");
                      this.formStepTheree.controls['offerAmountStepTheree'].enable();
                      this.formStepTheree.controls['closingServiceStepTheree'].enable();
                  }

                  this.IdDetailStepTheree.setValue(this.transaction.DetTrans3[0].ID);
                  this.formStepTheree.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);
                  this.formStepTheree.controls['offerAmountStepTheree'].setValue(this.transaction.negotiationStep1[0].offerAmount);
                  this.formStepTheree.controls['closingServiceStepTheree'].setValue(this.transaction.negotiationStep1[0].closingServiceId, {
                      onlySelf: true
                  });
                  this.closingServiceStepTheree.setValue(this.transaction.negotiationStep1[0].closingServiceId);
              } else {
                  this.isInitStepTheree = false;
                  /*PENDING*/
                  if (this.transaction.DetTrans3[0].stepStatus == "PENDING") {

                      /*turn seller */
                      if (this.transaction.DetTrans3[0].turnUser == "seller") {

                          if (this.detectedUser == "buyer") {

                              this.statusStepTheree = "Pending";
                              this.checkStepTheree = false;
                              this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                              this.formStepTheree.controls['offerAmountStepTheree'].disable();
                              this.formStepTheree.controls['closingServiceStepTheree'].disable();

                          } else {
                              this.statusStepTheree = "He is waiting for your confirmation";
                              this.checkStepTheree = true;
                              this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                              this.turnUserStepTheree.setValue("buyer");
                          }

                      }

                      /*turn buyer*/
                      if (this.transaction.DetTrans3[0].turnUser == "buyer") {

                          if (this.detectedUser == "seller") {
                              this.statusStepTheree = "Pending";
                              this.checkStepTheree = false;
                              this.formStepTheree.controls['roleStepTheree'].setValue("seller");
                              this.formStepTheree.controls['offerAmountStepTheree'].disable();
                              this.formStepTheree.controls['closingServiceStepTheree'].disable();
                          } else {
                              this.statusStepTheree = "He is waiting for your confirmation";
                              this.checkStepTheree = true;
                              this.formStepTheree.controls['roleStepTheree'].setValue("buyer");
                              this.turnUserStepTheree.setValue("seller");
                          }


                      }

                      /*transaction is ACCEPT for buyer and seller*/
                  } else if (this.transaction.DetTrans3[0].stepStatus == "ACCEPT") {
                      this.isFinishStepTheree = true;
                      this.statusStepTheree = "ACCEPT";
                      this.checkStepTheree = false;
                      this.formStepTheree.controls['offerAmountStepTheree'].disable();
                      this.formStepTheree.controls['closingServiceStepTheree'].disable();


                      /*transaction is reject for buyer or seller*/
                  } else {
                      this.isFinishStepTheree = true;
                      this.statusStepTheree = "REJECT";
                      this.checkStepTheree = false;
                      this.alertService.error('This is transaction is Reject in step Theree !');
                      this.formStepTheree.controls['offerAmountStepTheree'].disable();
                      this.formStepTheree.controls['closingServiceStepTheree'].disable();

                  }

                  this.formStepTheree.controls['offerAmountStepTheree'].setValue(this.transaction.negotiationStep3[0].offerAmount);
                  this.formStepTheree.controls['closingServiceStepTheree'].setValue(this.transaction.negotiationStep3[0].closingServiceId, {
                      onlySelf: true
                  });
                  this.closingServiceStepTheree.setValue(this.transaction.negotiationStep3[0].closingServiceId);

                  this.IdDetailStepTheree.setValue(this.transaction.DetTrans3[0].ID);
                  this.formStepTheree.controls['idTransaction'].setValue(this.transaction.Transaction[0].offerID);

                  this.formStepTheree.valueChanges.takeWhile(() => this.alive).subscribe(form => {
                      if (this.formStepTheree.controls['offerAmountStepTheree'].value != this.transaction.negotiationStep3[0].offerAmount ||
                          this.formStepTheree.controls['closingServiceStepTheree'].value != this.transaction.negotiationStep3[0].closingServiceId) {

                          this.isChangeValuesStepTheree = true;
                      } else {
                          this.isChangeValuesStepTheree = false;
                      }
                  });

              }
              /*end else INIT*/

          }

      }
  }

  /*all treatments and checks to submit the third step*/
  onSubmitStepTheree(typeConfirmation: string) {

      this.loading.show();

      if (!this.isFinishStepTheree) {
          if (this.checkStepTheree) {

              if (typeConfirmation == "INIT") {
                  this.statusDetailStepTheree.setValue("COUNTER");
                  this.toastr.info("Your operation is done", "Init", {
                      showCloseButton: true
                  });
                  this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(() => this.alive).subscribe(res => {
                    res
                });
                setTimeout(() => {
                    this.getTransaction();
                    this.loading.hide();
                }, 1500);
               }


              if (typeConfirmation == "ACCEPT") {
                  if (this.isChangeValuesStepTheree == false) {
                      this.statusDetailStepTheree.setValue("ACCEPT");
                      this.toastr.success("Your operation is done", "ACCEPT", {
                          showCloseButton: true
                      });
                      this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(() => this.alive).subscribe(res => {
                          res
                      });
                      setTimeout(() => {
                          this.getTransaction();
                          this.loading.hide();
                      }, 1500);
                  } else {
                      this.alertService.info('This operation not available because you are making the change in step!');
                      this.loading.hide();
                      window.scrollTo(0, 0);
                  }




              }
              if (typeConfirmation == "REJECT") {
                  this.statusDetailStepTheree.setValue("REJECT");
                  this.toastr.error("Your operation is done", "REJECT", {
                      showCloseButton: true
                  });
              }
              if (typeConfirmation == "COUNTER") {
                  if (this.isChangeValuesStepTheree == true) {
                      this.statusDetailStepTheree.setValue("COUNTER");
                      this.toastr.info("Your operation is done", "COUNTER", {
                          showCloseButton: true
                      });
                      this.TransactionService.postStepTheree(this.formStepTheree.value).takeWhile(() => this.alive).subscribe(res => {
                          res
                      });
                      setTimeout(() => {
                          this.getTransaction();
                          this.loading.hide();
                      }, 1500);
                  } else {
                      this.alertService.info('This operation is not available because you do not make any changes !');
                      this.loading.hide();
                      window.scrollTo(0, 0);
                  }

              }


          } else {
              this.alertService.clear();
              this.alertService.info('Wait for the answer of the other/This step is not available currently');
              window.scrollTo(0, 0);
              this.loading.hide();
          }

      } else {
          this.alertService.clear();
          this.alertService.info('This step is finished');
          window.scrollTo(0, 0);
          this.loading.hide();
      }

  }

  /*get stream data with interval the time  if not the current user turn */
  getDataWithInterval(aliveRequestNews) {
      return IntervalObservable.create(20000).takeWhile(() => this.alive && aliveRequestNews).subscribe(() => {
          this.getTransaction();
          console.log("buyer");
      });
  }


  onSubmitStepThereeInformation() {
      this.loading.show();
      if (this.checkInformationBuyerSeller) {
        if(this.formStepThereeInformation.valid)
        {
          this.TransactionService.postStepThereeInformation(this.formStepThereeInformation.value).subscribe(res => {
              res
          });
          setTimeout(() => {
              this.loading.hide();
              this.getTransaction();
          }, 1500);
          this.toastr.clearAllToasts();
          this.toastr.success("Your operation is done", "Envoyer", {
              showCloseButton: true
          });
        }else{
          this.validateAllFormFields(this.formStepThereeInformation);
          this.loading.hide();
          this.toastr.error("fill in all mandatory fields", "ERROR", {
            showCloseButton: true
          });
        }
        
      } else {
          this.alertService.clear();
          this.alertService.info("Please,stopped doing anything we controlled all!");
          setTimeout(() => {
              this.loading.hide();
          }, 300);
          window.scrollTo(0, 0);
      }
  }
  /* this for get information ... seller or buyer about step theree, if exist and stoped sumbit*/
  getInformationBuyerSeller(vestingInctructionBuyer: any) {
      this.checkInformationBuyerSeller = false;
      this.formStepThereeInformation.controls['company'].setValue(vestingInctructionBuyer.company);
      this.formStepThereeInformation.controls['contactPerson'].setValue(vestingInctructionBuyer.contactPerson);
      this.formStepThereeInformation.controls['address'].setValue(vestingInctructionBuyer.address);
      this.formStepThereeInformation.controls['city'].setValue(vestingInctructionBuyer.city);
      this.formStepThereeInformation.controls['state'].setValue(vestingInctructionBuyer.state);
      this.formStepThereeInformation.controls['phone'].setValue(vestingInctructionBuyer.phone);
      this.formStepThereeInformation.controls['zip'].setValue(vestingInctructionBuyer.zip);
      this.formStepThereeInformation.controls['email'].setValue(vestingInctructionBuyer.email);
      this.formStepThereeInformation.controls['other'].setValue(vestingInctructionBuyer.other);
      this.formStepThereeInformation.controls['loanServicerId'].setValue(vestingInctructionBuyer.loanServicerId);
      this.formStepThereeInformation.controls['company'].disable();
      this.formStepThereeInformation.controls['contactPerson'].disable();
      this.formStepThereeInformation.controls['address'].disable();
      this.formStepThereeInformation.controls['city'].disable();
      this.formStepThereeInformation.controls['state'].disable();
      this.formStepThereeInformation.controls['phone'].disable();
      this.formStepThereeInformation.controls['zip'].disable();
      this.formStepThereeInformation.controls['email'].disable();
      this.formStepThereeInformation.controls['other'].disable();
      this.formStepThereeInformation.controls['loanServicerId'].disable();
  }

  /* this is methode declanche when forms is not valid for marke all fields as touched ...*/
  validateAllFormFields(formGroup: FormGroup) {

      Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
              control.markAsTouched({
                  onlySelf: true
              });
          } else if (control instanceof FormGroup) {
              this.validateAllFormFields(control);
          }
      });

  }

onChangedDueDiligenceStepOne(event) {
      if (event.target.value == 2) {
          this.formStepOne.controls['daysStepOne'].disable();
          //this.daysStepOne.disabled;
      } else {
          this.formStepOne.controls['daysStepOne'].enable();
      }
  }
onchangeDueDiligenceStepTwo(event){
   if (event.target.value == "false") {
     
     this.formStepTwo.controls['daysStepTwo'].disable();
     } else {
        this.formStepTwo.controls['daysStepTwo'].enable();
     }
  }


  ngOnDestroy() {
      this.alive = false;
      localStorage.removeItem('OSB0303');
  }




}