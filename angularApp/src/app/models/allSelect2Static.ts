export const aboutUsData: any = [
    {
        id: 'internetSearch',
        text: 'InternetSearch'
    },
    {
        id: 'industryTradeGroup',
        text: 'Industry Trade Group'
    },
    {
        id: 'refferalRecomendation',
        text: 'Referral/ Recommendation'
    },
    {
        id: 'fciLenderServices',
        text: 'FCI Lender Services'
    },
    {
        id: 'tvOrRadio',
        text: 'TV or Radio'
    },
    {
        id: 'newsArticle',
        text: 'News or Article'
    },
    {
        id: 'email',
        text: 'Email'
    }

];

      
       
     
      
export const millingData: any = [
    {
        id: 'featureListing',
        text: 'Feature Listings'
    },
    {
        id: 'offersAndOther',
        text: 'Offers and other changes to Notes on My WATCH LIST'
    },
    {
        id: 'newListing',
        text: 'New Listings matching My Product Alerts'
    }

];


export const propertyTypesData: any = [
    {
        id: 'residential',
        text: 'Residential'
    },
    {
        id: 'construction',
        text: 'Construction'
    },
    {
        id: 'land',
        text: 'Land'
    },
    {
        id: 'commercialRealEstate',
        text: 'Commercial Real Estate'
    },
    {
        id: 'manufacturedHousing',
        text: 'Manufactured Housing'
    },
    {
        id: 'multiUnitApartment',
        text: 'Multi-Unit Apartments'
    },
    {
        id: 'other',
        text: 'Other'
    }

];


export const CurrentCapitalAvailableData: any = [
    {
        id: 'under250K',
        text:'Under $ 250K' 
    },
    {
        id: 'between_250K500K',
        text: '$250K - $ 500K'
    },
    {
        id:'between_500K750K' ,
        text: '$ 500K – $ 750K'
    },
    {
        id: '750K-1MM',
        text:'$ 750K - $1MM' 
    },
    {
        id:'moreThan1MM' ,
        text: '$ 1MM +'
    },

];

export const idealPropertyValueData: any = [
    {
        id:'under100K' ,
        text: 'Under 100K'
    },
    {
        id:	'between_100K300K' ,
        text:'100K – 300K' 
    },
    {
        id: 'between_301K750K' ,
        text: '301K – 750K'
    },
    {
        id:	'moreThan750K' ,
        text: '750kplus',
    },
  

];


export const LoanTypesData: any = [
    {
        id:'performing' ,
        text: 'Performing'
    },
    {
        id:	'nonPerforming' ,
        text:'Non Performing' 
    },
    {
        id: 'newlyOriginated' ,
        text: 'Newly Originated'
    }
  

];

export const  purchaseYourNoteData: any = [
    {
        id:'InBulk' ,
        text: 'In Bulk'
    },
    {
        id:	'Single' ,
        text:'Single' 
    }

];
export const LienPositionData : any = [

    {
        id:'firstLien' ,
        text: '1st Lien'
    },
    {
        id:	'secondLien' ,
        text:'2nd Lien' 
    },
    {
        id:'thirdLien' ,
        text: '3rd Lien'
    },
    {
        id:	'Other' ,
        text:'Other' 
    },

];	

export const statesData : any = [
    {
        id:"DefaultItem",
        text:"Choose your state"
    },
    {
        id:' 1' ,
        text: 'ALABAMA'
    },
    {
        id:	'2' ,
        text:'ALASKA' 
    },
       {
        id:'3' ,
        text: 'ARIZONA'
    },
    {
        id:    '4' ,
        text:'CAROLINA' 
    },
       {
        id:' 5' ,
        text: 'COLORADO'
    },
    {
        id:    '6' ,
        text:'FLORIDA' 
    },

];	


							
								 
								  




