//import {statut,Users,deedMortgage} from '../structExports/models';
export class loans {
    public loanNoteID;
    public name;
    public prePayPenalty;
    public registerswMers;
    public ballonPayement;
    public onForbearancePlan;
    
    public loanTermsModified;
    public originationDate;
    public paidToDate;
    public nextPayementDate;
    public payersLastPayementMadeDate;
    public originalLoanAmount;
    public unPaidPrincipalBalance;
    public noteMaturityDate;
    public accruedLateCharges;
    public loanCharges;
    public ofPayementsLast12;
    public firstPayementDate;
    public noteerestRate;
    public solderestRate;
    public lateCharge;
    public afterDays;
    public Unpaiderest;
    public propertyTaxesDue;
    public payementTrust;
    public property;
    public PI;
    public TotalMonthlyLoanPayement;
    public comments;
    public askingPrice;
    public percentagePrice;
    public loanStatus;
    public user;
    public deedMortgage;
    public userID;
    public principaleBalance;
    public soldInterestRate;
    public noteInterestRate;
    public notePaymentAmount;
    
}

