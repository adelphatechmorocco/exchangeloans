import { IObservable, IObserver } from '../interfaces/observable-observer.pattern';

export class SubmitOfferViewModel implements IObservable{
    
	private _observers:Array<IObserver> = [];
	private _closingType:number = 0;
	private _dueDiligenceOption:number = 0;
	private _offerAmount:number = 0;
	private _comment:string;
	private _daysCount:number = 3;
	private _loanNoteId:number = 0;

	constructor(model:any){		
		this._loanNoteId = model.loanNoteId || 0;
		this.Comment = model.comment || "";
		this.OfferAmount = model.offerAmount || 0;
		this.ClosingType = model.closingType || [];
		this.DueDiligenceOption = model.dueDiligenceOption || 0;
	}

	get LoanNoteId(){
		return this._loanNoteId;
	}
	set LoanNoteId(value){
		this._loanNoteId = value;
		this.NotifyObservers({ property: 'LoanNoteId', value: value});
	}

	get ClosingType(){
		return this._closingType;
	}

	set ClosingType(value){
		this._closingType = value;
		this.NotifyObservers({ property: 'ClosingType', value: value});
	}

	get DueDiligenceOption(){
		return this._dueDiligenceOption;
	}
	set DueDiligenceOption(value){
		this._dueDiligenceOption = value;
		this.NotifyObservers({ property: 'DueDilligenceOption', value: value});
	}

	get DaysCount(){
		return this._daysCount;
	}
	set DaysCount(value){
		this._daysCount = value;
		this.NotifyObservers({ property: 'DaysCount', value: value});
	}

	get OfferAmount(){
		return this._offerAmount;
	}
	set OfferAmount(value:number){
		this._offerAmount = value;
		this.NotifyObservers({ property: 'OfferAmount', value: value});
	}
	
	public get Comment(){
		return this._comment;
	}
	public set Comment(value:string){
		this.NotifyObservers({ property: 'Comment', value: value});
		this._comment = value;
	}

	getOfferPOCO(){
		return { ClosingTypeId: this.ClosingType, OfferTypeId: 1, OfferAmount: this.OfferAmount, Comment: this.Comment, loanNoteId: this._loanNoteId, DueDiligenceOptionId: this.DueDiligenceOption, DaysCount: this.DaysCount };
	}
	/* IObservable iplementation members*/
	RegisterObserver(observer: IObserver){
		this._observers.push(observer);
	}
	RemoveObserver(observer: IObserver){
		for(var i = 0; i < this._observers.length; i++)
			if(this._observers[i] === observer)
				this._observers.slice(i,1);
	}
	NotifyObservers(message){
		for(var i = 0; i < this._observers.length; i++)
			this._observers[i].ReceiveNotification(message)
	}
}