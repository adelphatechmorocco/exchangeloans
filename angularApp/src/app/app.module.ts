import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Http,HttpModule,RequestOptions, XHRBackend } from '@angular/http'; 
import { FormsModule,ReactiveFormsModule } from '@angular/Forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { Select2Module } from 'ng2-select2';

// this for all ours services 
import  {
          authentificationService,
          httpServiceToken,
          pagerService,
          NavbarService,
          loadingService,
          buyNotesService,
          alertService,
          ExcelService,
          myExchangeService,
          loanNoteDetailService,
          transactionService,
          SellnoteService,
          paymentService,
          contactUsService,
     

          
         } from './structExports/services';
// this for all ours components 
import {
        NavbarComponent,
        loadingComponent,
        alertsComponent,
        buyNotesComponent,
        loginComponent,
        myExchangeComponent,
        loanNoteDetailComponent,
        SubmitOfferComponent,
        RegisterComponent,
        AboutUsComponent,
        ContactUsComponent,
        ServicesComponent,
        FaqComponent,
        transactionComponent,
        SellnoteComponent,
        PaymentComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent

        } from './structExports/components';


import {AppRouting} from './routings/routing';
import {authGuard} from './guards/auth.guard';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { TradingTermsComponent } from './components/trading-terms/trading-terms.component';
import { HowToBuyNotesComponent } from './components/how-to-buy-notes/how-to-buy-notes.component';
import { HowToSellNotesComponent } from './components/how-to-sell-notes/how-to-sell-notes.component';



@NgModule({
  declarations: [
    AppComponent,
    buyNotesComponent,
    loginComponent,
    NavbarComponent,
    loadingComponent,
    alertsComponent,
    myExchangeComponent,
    loanNoteDetailComponent,
    SubmitOfferComponent,
    RegisterComponent,
    AboutUsComponent,
    ContactUsComponent,
    ServicesComponent,
    FaqComponent,
    transactionComponent,
    SellnoteComponent,
    TermsAndConditionsComponent,
    TradingTermsComponent,
    HowToBuyNotesComponent,
    HowToSellNotesComponent,
    PaymentComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AppRouting,
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    Select2Module
  ],
  providers: [
    buyNotesService,
    authGuard,
    authentificationService,
    NavbarService,
    loadingService,
    pagerService,
    alertService,
    ExcelService,
    myExchangeService,
    loanNoteDetailService,
    transactionService,
    SellnoteService,
    paymentService,
    contactUsService,


    {
      provide: httpServiceToken,
      useFactory: (backend: XHRBackend, options: RequestOptions) => {
        return new httpServiceToken(backend, options);
      },
      deps: [XHRBackend, RequestOptions]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
