import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { loginComponent,ForgotPasswordComponent,ResetPasswordComponent,SellnoteComponent,buyNotesComponent ,myExchangeComponent,loanNoteDetailComponent,SubmitOfferComponent,
    FaqComponent,RegisterComponent,AboutUsComponent,ContactUsComponent,ServicesComponent, HowToSellNotesComponent,PaymentComponent,HowToBuyNotesComponent,TradingTermsComponent,TermsAndConditionsComponent,transactionComponent} from '../structExports/components';

import { authGuard } from '../guards/auth.guard'

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'about-us',
        pathMatch: 'full'
    },
  
    {
        path: 'login',
        component: loginComponent
    },
    {
        path: 'buy-notes',
        component: buyNotesComponent,
        canActivate: [authGuard]
    },
    {
        path: 'my-exchange',
        component:myExchangeComponent,
        canActivate:[authGuard]
    },
    {
        path: 'buy-note/:id',
        component: loanNoteDetailComponent,
        canActivate: [authGuard]
    },
    {
        path: 'SubmitOffer/:id',
        component: SubmitOfferComponent,
        canActivate: [authGuard]
    },
    {
        path: 'register',
        component: RegisterComponent,
        
    },
    
    {
        path : 'transaction',
        component : transactionComponent

    },
    {
        path: 'about-us',
        component: AboutUsComponent,
        
    },
    {
        path: 'contact-us',
        component: ContactUsComponent,
        
    },
    {
        path: 'faq',
        component: FaqComponent,
        
    },

    {
        path: 'services',
        component: ServicesComponent,
        
    },
    {
        path: 'sellnote',
        component: SellnoteComponent,
        
    }, 
    {
        path: 'terms-and-conditions',
        component: TermsAndConditionsComponent,
        
    }, 
    {
        path: 'trading-terms',
        component: TradingTermsComponent,
        
    },
    {
        path: 'faq/how-to-buy-notes',
        component: HowToBuyNotesComponent,
        
    },
    {
        path: 'faq/how-to-sell-notes',
        component:  HowToSellNotesComponent,
        
    },
    {
        path: 'payment',
        component:  PaymentComponent,
        
    },
    {
        path: 'forgotpassword',
        component: ForgotPasswordComponent,
        
    },
    {
        path: 'resetpassword/:code',
        component: ResetPasswordComponent,
        
    },
    
    
   
   
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(routes);