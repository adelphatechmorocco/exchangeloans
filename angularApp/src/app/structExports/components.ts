// this file to structure the exports of ours components

export * from '../components/login/login.component';
export * from '../components/loading/loading.component';
export * from '../components/navbar/navbar.component';
export * from '../components/buyNotes/buyNotes.component';
export * from '../components/alerts/alerts.component';
export * from '../components/myExchange/myExchange.component';
export * from '../components/loanNoteDetail/loanNoteDetail.component';
export * from '../components/submitoffer/submitoffer.component';
export * from '../components/register/register.component';
export * from '../components/about-us/about-us.component';
export * from '../components/contact-us/contact-us.component';
export * from '../components/services/services.component';
export * from '../components/transaction/transaction.component';
export * from '../components/sellnote/sellnote.component'; 
export * from '../components/faq/faq.component';
export * from '../components/terms-and-conditions/terms-and-conditions.component';
export * from '../components/trading-terms/trading-terms.component';
export * from '../components/how-to-buy-notes/how-to-buy-notes.component';
export * from '../components/how-to-sell-notes/how-to-sell-notes.component';
export * from '../components/payment/payment.component';
export * from '../components/forgotPassword/forgotPassword.component';
export * from '../components/resetPassword/resetPassword.component';






