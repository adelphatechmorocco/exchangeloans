// this file to structure the exports of ours Services
export * from '../services/navbar.service';
export * from '../services/loading.service';
export * from '../services/pager.service';
export * from '../services/authentication.service';
export * from '../services/httpServiceToken.service'
export * from '../services/buyNotes.service';
export * from '../services/alerts.service';
export * from '../services/exel.service';
export * from '../services/myExchange.service';
export * from '../services/loanNoteDetail.service';
export * from '../services/register.service';
export * from '../services/transaction.service';
export * from '../services/sellnote.service';
export * from '../services/payment.sevice';
export * from '../services/contactUs.service';