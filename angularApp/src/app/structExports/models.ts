// this file to structure the exports of ours models

export * from '../models/alerts';
export * from '../models/loans';
export * from '../models/users';
export * from '../models/statut';
export * from '../models/deedMortgage';
export * from '../models/Offers';
export * from '../models/exelModels';
export * from '../models/allSelect2Static';