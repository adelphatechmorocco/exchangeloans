
export interface IObservable{
	RegisterObserver(Observer: IObserver);
	RemoveObserver(Observer: IObserver);
	NotifyObservers<T>(Message: T);
}
export interface IObserver{
	ReceiveNotification<T>(Message: T): void;
}

export interface INotifyPropertyChanged{
}

export class ObservableOption<TKey, TValue> implements IObservable{

	private _observers:Array<IObserver> = [];
	private _id:TKey;
	private _name:string;
	private _value:TValue;
	private _selected:boolean;

	constructor(id:TKey, name:string, value:TValue, selected:boolean){
		this._id = id;
		this._name = name;
		this._value = value;
		this._selected = selected;
	}

	public get id(){
		return this._id;
	}
	public get name(){
		return this._name;
	} 
	public get value(){
		return this._value;
	}
	public set value(value){
		this._value = value;
		this.NotifyObservers({id: this._id, name: this._name, value: this.value, selected: this._selected});
	}

	public get Selected(){
		return this._selected;
	}
	public set Selected(value){
		this._selected = value;
		this.NotifyObservers({id: this._id, name: this._name, value: this.value, selected: this._selected});
	}
	/* IObservable members */
	RegisterObserver(observer: IObserver){
		this._observers.push(observer);
	}
	RemoveObserver(observer: IObserver){
		for(var i = 0; i < this._observers.length; i++)
			if(this._observers[i] === observer)
				this._observers.slice(i,1);
	}
	NotifyObservers<T>(message: T){
		for(var i = 0; i < this._observers.length; i++){
			this._observers[i].ReceiveNotification<T>(message)
		}
	}
}