﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class transaction
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("transactionID")]
        public int ID { get; set; }

        public string name { get; set; }
        public DateTime? createAt { get; set; }
        public DateTime? updateAt { get; set; }
        public virtual ICollection<services> services { get; set; }

        [Key, ForeignKey("offer")]
        public int offerID { get; set; }
        public virtual Offer offer { get; set; }

        public virtual payment payment { get; set; }
        public virtual contract contract { get; set; }
    }
}