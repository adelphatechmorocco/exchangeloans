﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class OfferType
    {
        public int OfferTypeId { get; set; }
        public string OfferTypeName { get; set; }
    }
}