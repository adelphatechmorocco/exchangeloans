﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class detailTransaction
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("detailTransactionID")]
        public int ID { get; set; }
        public string step { get; set; }
        public String stepStatus { get; set; }
        public DateTime createAt { get; set; }
        public string turnUser { get; set; }
        public int transactionID { get; set; }
        [ForeignKey("transactionID")]
        public virtual transaction transaction { get; set; }
    }
}
