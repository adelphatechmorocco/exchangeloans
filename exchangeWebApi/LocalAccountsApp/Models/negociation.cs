﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class negotiation
    {

        [Column("negotiationID")]
        public int ID { get; set; }
        public double offerAmount { get; set; }
        public int closingServiceId { get; set; }
        public int dueDiligenceOptionId { get; set; }
        public int days { get; set; }
        public string role { get; set; }
        public string statusNegotiation { get; set; }
        public bool endDueDiligence { get; set; }
        public bool extendDueDiligence { get; set; }
        public DateTime? createAt { get; set; }
        public DateTime? updateAt { get; set; }

        public int detailTransactionID { get; set; }
        [ForeignKey("detailTransactionID")]
        public virtual detailTransaction detailTransaction { get; set; }
    }
}