﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class choicePropertyType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      
        public int choicePropertyId { get; set; }
        public bool? residential { get; set; }
        public bool? construction { get; set; }
        public bool? land { get; set; }
        public bool? commercialRealEstate { get; set; }
        public bool? manufacturedHousing { get; set; }
        public bool? multiUnitApartment { get; set; }
        public bool? other { get; set; }

        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }
    }
}