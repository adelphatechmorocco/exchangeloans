﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class OfferStatusByDate
    {
        [Key]
        [Column(Order = 0)]
        public int offerId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int offerStatusId { get; set; }
        [Key]
        [Column(Order = 2)]
        public DateTime modificationDate { get; set; }

        [ForeignKey("offerId")]
        public Offer Offer { get; set; }

        [ForeignKey("offerStatusId")]
        public OfferStatus OfferStatus { get; set; }
    }
}