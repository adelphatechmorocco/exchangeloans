﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class loanType
    {
        [Column("loanTypeID")]
        public int ID { get; set; }
        public string nameLoanType { get; set; }
        public virtual ICollection<loanNote> loanNote { get; set; }
    }
}