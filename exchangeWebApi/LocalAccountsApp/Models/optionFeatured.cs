﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class optionFeatured
    {
        [Column("optionFeaturedID")]
        public int ID { get; set; }
        public string name { get; set; }

        public float? price { get; set; }
        public ICollection<optionByDate> optionByDate { get; set; }
    }
}