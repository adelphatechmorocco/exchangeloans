﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class rateType
    {
        [Column("rateTypeID")]
        public int ID { get; set; }
        public string name { get; set; }
      

        //public int loanNoteId { get; set; }
        [JsonIgnore]
        public virtual ICollection<loanNote> loanNote { get; set; }

    }
}