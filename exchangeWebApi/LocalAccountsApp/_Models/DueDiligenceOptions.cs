﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class DueDiligenceOptions
    {
        public int DueDiligenceOptionsId { get; set; }
        public string TypeDueDiligence { get; set; }
    }
}