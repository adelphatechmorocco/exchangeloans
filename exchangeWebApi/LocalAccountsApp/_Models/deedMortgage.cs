﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class deedMortgage
    {
        [Column("deedMortgageID")]
        public int ID { get; set; }
        public bool firstTrust { get; set; }
        public bool juniorTrust { get; set; }
        public string lienPosition { get; set; }
        public string lienPosition1 { get; set; }
        public float monthlyPayment1 { get; set; }
        public string current1 { get; set; }
        public float prnicipalBalance1 { get; set; }
        public string modified1{ get; set; }
        public DateTime informationAsOf1 { get; set; }
        public string lienPosition2 { get; set; }
        public float monthlyPayment2 { get; set; }
        public string current2 { get; set; }
        public float prnicipalBalance2 { get; set; }
        public string modified2 { get; set; }
        public DateTime informationAsOf2 { get; set; }

        [Key, ForeignKey("loanNote")]
        public int loanNoteID { get; set; }
        [JsonIgnore]
        public virtual loanNote loanNote { get; set; }
    }
}