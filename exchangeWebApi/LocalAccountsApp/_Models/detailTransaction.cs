﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class detailTransaction
    {
        [Column("detailTransactionID")]
        public int ID { get; set; }
        public string step { get; set; }
        public bool stepStatus { get; set; }
        public DateTime createAt { get; set; }
        public DateTime updateAt { get; set; }
        public int transactionID { get; set; }
        [ForeignKey("transactionID")]
        public virtual transaction transaction { get; set; }
    }
}