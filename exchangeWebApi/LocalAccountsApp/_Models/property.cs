﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class property
    {
        [Column("propertyID")]
        public int ID { get; set; }
        public float propertyMarketValue { get; set; }
        public DateTime propertyValuationDate { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }

        public int valuationTypeID { get; set; }
        [ForeignKey("valuationTypeID")]
        public virtual valuationType valuationType { get; set; }

        public int typePropertyID { get; set; }
        [ForeignKey("typePropertyID")]
        public virtual typeProperty typeProperty { get; set; }

        public int occupancyStatusID { get; set; }
        [ForeignKey("occupancyStatusID")]
        public virtual occupancyStatus occupancyStatus { get; set; }

        [Key, ForeignKey("loanNote")]

        public int loanNoteId { get; set; }
        [JsonIgnore]
        public loanNote loanNote { get; set; }

        public int stateID { get; set; }

    }
}