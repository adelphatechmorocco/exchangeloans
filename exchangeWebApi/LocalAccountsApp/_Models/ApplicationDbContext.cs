﻿//using LocalAccountsApp.Migrations;
//using Microsoft.AspNet.Identity.EntityFramework;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;
//using System.Web;

//namespace LocalAccountsApp.Models
//{
//    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
//    {
//        public ApplicationDbContext()
//            : base("DefaultConnection", throwIfV1Schema: false)
//        {
//            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
//        }

//        public DbSet<loanNote> loanNotes { get; set; }
//        public DbSet<Offer> Offers { get; set; }
//        public DbSet<OfferStatus> OffreStatus { get; set; }
//        public DbSet<OfferType> OfferTypes { get; set; }
//        public DbSet<OfferHasDueDiligenceOptions> OfferHasDueDiligenceOptions { get; set; }
//        public DbSet<LoanOfferBuyer> LoanOfferBuyer { get; set; }

//        public static ApplicationDbContext Create()
//        {
//            return new ApplicationDbContext();
//        }
//    }
//}