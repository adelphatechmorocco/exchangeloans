﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class Offer
    {
        [Column("offerID")]
        public int offerID { get; set; }

        [Column("closingTypeId")]
        public int ClosingTypeId { get; set; }

        //[ForeignKey("ClosingTypeId")]
        //public closingType ClosingType { get; set; }

        [Column("offerTypeId")]
        public int OfferTypeId { get; set; }

        [ForeignKey("OfferTypeId")]
        public OfferType OfferType { get; set; }

        [Column("offerAmount")]
        public double OfferAmount { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        public ICollection<OfferStatusByDate> OfferStatusByDate { get; set; }

        //public int transactionId { get; set; }
        //[ForeignKey("transactionId")]
        public virtual transaction transaction { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}