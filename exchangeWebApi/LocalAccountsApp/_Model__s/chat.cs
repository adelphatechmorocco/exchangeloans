﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class chat
    {
        [Column("chatID")]
        public int ID { get; set; }
        public DateTime createAt { get; set; }
        public string chatSender { get; set; }
        public string chatMessage { get; set; }
        public int transactionID { get; set; }
        [ForeignKey("transactionID")]
        public virtual transaction transaction { get; set; }

    }
}