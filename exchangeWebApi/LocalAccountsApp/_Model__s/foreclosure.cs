﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class foreclosure
    {
        [Column("foreclosureID")]
        public int ID { get; set; }
        public bool forceclosureStatus { get; set; }
        public float attorneyFeesDue { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public DateTime scheduledSaleDate { get; set; }
        public string zip { get; set; }
        public string phone { get; set;}
        public string email { get; set; }
        public DateTime dateOpened { get; set; }
        public int stateID { get; set; }

        public int? loanNoteID { get; set; }
        [ForeignKey("loanNoteID")]
        public virtual loanNote loanNote { get; set; }
    }
}