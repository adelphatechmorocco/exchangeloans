﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class payment
    {
        [Column("paymentID")]
        public int ID { get; set; }
        public DateTime createAt { get; set; }
        public DateTime updateAt { get; set; }

        public int transactionID { get; set; }
        [ForeignKey("ID")]
        public virtual transaction transaction { get; set; }
    }
}