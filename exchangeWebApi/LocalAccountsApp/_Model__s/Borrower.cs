﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LocalAccountsApp.Models
{
	public class borrower
	{
        [Column("borrowerID")]
        public int ID { get; set; }
         public bool isCompany { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string maillingAddress { get; set; }
        public string homePh { get; set; }
        public string workPh { get; set; }
        public string fssTaxID { get; set; }
        public string companyName { get; set; }
        public string companyContactName { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string mobilePhone { get; set; }
        public string email { get; set; }
        public int stateID { get; set; }

        public int loanNoteId { get; set; }
        public loanNote loanNote { get; set; }

    }
}