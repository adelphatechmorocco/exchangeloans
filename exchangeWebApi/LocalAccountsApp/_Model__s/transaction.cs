﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class transaction
    {
        [Key]
        [Column("transactionID", Order = 0)]
        public int ID { get; set; }

        public string name { get; set; }
        public DateTime createAt { get; set; }
        public DateTime updateAt { get; set; }
        public virtual ICollection<services> services { get; set; }

        public int offerID { get; set; }
        [ForeignKey("offerID")]
        public virtual Offer offer { get; set; }

        public payment payment { get; set; }
        public contract contract { get; set; }
    }
}