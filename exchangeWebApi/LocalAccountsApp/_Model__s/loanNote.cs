﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class loanNote
    {
        [Column("loanNoteID")]
        public int    loanNoteID { get; set; }
        public string name { get; set; }
        public bool? prePayPenalty { get; set; }
        public bool? registerswMers { get; set; }
        public bool? ballonPayement { get; set; }
        public bool? onForbearancePlan { get; set; }
        public bool? loanTermsModified { get; set; }
        public DateTime? originationDate { get; set; }
        public DateTime? paidToDate { get; set; }
        public DateTime? nextPayementDate { get; set; }
        public DateTime? payersLastPayementMadeDate { get; set; }
        public float? originalLoanAmount { get; set; }
        public float? unPaidPrincipalBalance { get; set; }
        public DateTime? noteMaturityDate { get; set; }
        public float? accruedLateCharges { get; set; }
        public float? loanCharges { get; set; }
        public string ofPayementsLast12 { get; set; }
        public DateTime? firstPayementDate { get; set; }
        public float? noteInterestRate { get; set; }
        public float? soldInterestRate { get; set; }
        public float? lateCharge { get; set; }
        public int? afterDays { get; set; }
        public float? UnpaidInterest { get; set; }
        public float? propertyTaxesDue { get; set; }
        public float? payementTrust { get; set; }
        public float? PI { get; set; }
        public float? TotalMonthlyLoanPayement { get; set; }
        public string comments { get; set; }
        public float? askingPrice { get; set; }
        public float? percentagePrice { get; set; }
        public DateTime? nextAdjustement { get; set; }
        public DateTime? adjPayementChangeDate { get; set; }
        public float? adjustementFrequency { get; set; }
        public float? floor { get; set; }
        public string indexName { get; set; }
        public float? margin { get; set; }
        public float? ceiling { get; set; }

        public DateTime? drawPeriodStartDate { get; set; }
        public DateTime? repaymentPeriodStartDate { get; set; }

        public string userID { get; set; }
        [ForeignKey("userID")]
        public virtual ApplicationUser user { get; set;}

        public int? loanStatusID { get; set; }
        [ForeignKey("loanStatusID")]
        public virtual  loanStatus loanStatus { get; set; }

        public int? loanTypeID { get; set; }
        [ForeignKey("loanTypeID")]
        public virtual loanType loanType { get; set; }

        public int? amortizationTypeID { get; set; }
        [ForeignKey("amortizationTypeID")]
        public virtual amortizationType amortizationType { get; set; }

        public int? rateTypeId { get; set; }
        [ForeignKey("rateTypeId")]
        public virtual rateType rateType { get; set; }

        public bool? bankruptcyStatus { get; set; }
        public virtual bankruptcy bankruptcy { get; set; }

        public bool? foreclosureStatus { get; set; }
        public virtual foreclosure foreclosure { get; set; }

        public int? paymentFrequencyID { get; set; }
        [ForeignKey("paymentFrequencyID")]
        public virtual paymentFrequency paymentFrequency { get; set; }

        public virtual bool? escrowImpoundStatus { get; set; }
        public escrowImpounds escrowImpounds { get; set; }

        public virtual bool? deedMortgageStatus { get; set; }
        public virtual deedMortgage deedMortgage { get; set; }

        public virtual borrower borrower { get; set; }

        public virtual property property { get; set; }

        public int? closingTypeID { get; set; }
        [ForeignKey("closingTypeID")]
        public virtual closingType closingType { get; set; }

    }
}