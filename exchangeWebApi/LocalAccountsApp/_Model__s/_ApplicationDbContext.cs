﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace LocalAccountsApp.Models
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<transaction>()
                .HasRequired(t => t.offer).WithRequiredPrincipal(t => t.transaction);

            modelBuilder.Entity<contract>()
                .HasRequired(t => t.transaction).WithRequiredPrincipal(t => t.contract);

            modelBuilder.Entity<payment>()
                .HasRequired(t => t.transaction).WithRequiredPrincipal(t => t.payment);

            modelBuilder.Entity<bankruptcy>()
                .HasRequired(t => t.loanNote)
                .WithRequiredPrincipal(o => o.bankruptcy);

            modelBuilder.Entity<deedMortgage>()
                .HasRequired(t => t.loanNote)
                .WithRequiredPrincipal(o => o.deedMortgage);

            //modelBuilder.Entity<loanNote>()
            //    .HasOptional(t => t.bankruptcy)
            //    .WithMany(b => b.loanNote);

            //modelBuilder.Entity<rateType>()
            //    .HasRequired(t => t.loanNote)
            //    .WithRequiredPrincipal(o => o.rateType);

            modelBuilder.Entity<foreclosure>()
                .HasRequired(t => t.loanNote)
                .WithRequiredPrincipal(o => o.foreclosure);

            modelBuilder.Entity<borrower>()
                .HasRequired(t => t.loanNote)
                .WithRequiredPrincipal(o => o.borrower);

            //modelBuilder.Entity<escrowImpounds>()
            //    .HasRequired(t => t.loanNote)
            //    .WithRequiredPrincipal(o => o.escrowImpounds);

            modelBuilder.Entity<property>()
                .HasRequired(t => t.loanNote)
                .WithRequiredPrincipal(o => o.property);

            base.OnModelCreating(modelBuilder);
        }
        //public DbSet<loanNote> loanNotes { get; set; }
        //public DbSet<photo> photos { get; set; }
        //public DbSet<loanStatus> loanStatus { get; set; }
    }
}