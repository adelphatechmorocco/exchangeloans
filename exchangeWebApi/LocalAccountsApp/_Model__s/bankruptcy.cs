﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class bankruptcy
    {
        [Column("bankruptcyID")]
        public int ID { get; set; }
        public DateTime bkFillingDate { get; set; }
        public DateTime bkDisChargeDate { get; set; }
        public DateTime bkDismissalDate { get; set; }
        public bool bankruptcyStatus { get; set; }

        public int? loanNoteID { get; set; }
        [ForeignKey("loanNoteID")]
        public virtual loanNote loanNote { get; set; }

        public int bkChapterID { get; set; }
        [ForeignKey("bkChapterID")]
        public virtual bkChapter bkChapter { get; set; }
    }
}