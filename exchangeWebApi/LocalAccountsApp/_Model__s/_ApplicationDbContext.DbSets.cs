﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public partial class ApplicationDbContext 
    {
        internal IQueryable<loanNote> loanNotes;

        //public DbSet<loanNote> loanNotes { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferStatus> OffreStatus { get; set; }
        public DbSet<OfferType> OfferTypes { get; set; }
        public DbSet<OfferHasDueDiligenceOptions> OfferHasDueDiligenceOptions { get; set; }
        public DbSet<LoanOfferBuyer> LoanOfferBuyer { get; set; }
        public DbSet<DueDiligenceOptions> DueDilligenceOptions { get; set; }

        public DbSet<closingType> closingTypes { get; set; }
        public DbSet<amortizationType> amortizationType { get; set; }
        public DbSet<bankruptcy> bankruptcy { get; set; }
        public DbSet<bkChapter> bkChapter { get; set; }
        public DbSet<borrower> borrower { get; set; }
        public DbSet<capitalAvailable> capitalAvailable { get; set; }
        public DbSet<chat> chat { get; set; }
        public DbSet<contract> contract { get; set; }
        public DbSet<deedMortgage> deedMortgage { get; set; }
        public DbSet<detailTransaction> detailTransaction { get; set; }
        public DbSet<document> document { get; set; }
        public DbSet<escrowImpounds> escrowImpounds { get; set; }
        public DbSet<foreclosure> foreclosure { get; set; }
        public DbSet<loanNote> loanNote { get; set; }
        public DbSet<loanStatus> loanStatus { get; set; }
        public DbSet<loanType> loanType { get; set; }
        public DbSet<occupancyStatus> occupancyStatus { get; set; }
        public DbSet<payment> payment { get; set; }
        public DbSet<paymentFrequency> paymentFrequency { get; set; }
        public DbSet<photo> photo { get; set; }
        public DbSet<property> property { get; set; }
        public DbSet<propertyValue> propertyValue { get; set; }
        public DbSet<rateType> rateType { get; set; }
        public DbSet<state> state { get; set; }
        public DbSet<transaction> transaction { get; set; }
        public DbSet<typeProperty> typeProperty { get; set; }
        public DbSet<valuationType> valuationType { get; set; }
    }
}