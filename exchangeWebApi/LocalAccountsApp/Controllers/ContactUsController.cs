﻿using RestSharp;
using System.Web.Http;
using LocalAccountsApp.Models;


namespace LocalAccountsApp.Controllers
{

    public class ContactUsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public class ContactViewModel
        {
            public string name { get; set; }
            public string company { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string State { get; set; }
            public string zip { get; set; }
            public string telephone { get; set; }
            public string extension { get; set; }
            public string fax { get; set; }
            public string email { get; set; }
            public string message { get; set; }



        }
        // GET: api/loanNotes
        [Route("api/ContactUs")]
        public IHttpActionResult ContactUs(ContactViewModel contact)
        {

            var client = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/z81oqc/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", "{\"to\":\"support@adelphatech.com\",\n\t\"fromName\":\"" + contact.name + "\",\n\t\"subject\":\"New Request\",\n\t\"name\":\"" + contact.name + "\",\n\t\"company\":\"" + contact.company + "\",\n\t\"address\":\"" + contact.address + "\",\n\t\"city\":\"" + contact.city + "\",\n\t\"state\":\"" + contact.State + "\",\n\t\"zip\":\"" + contact.zip + "\",\n\t\"telephone\":\"" + contact.telephone + "\",\n\t\"extension\":\"" + contact.extension + "\",\n\t\"fax\":\"" + contact.fax + "\",\n\t\"email\":\"" + contact.email + "\",\n\t\"request\":\"" + contact.message + "\"\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Ok();
        }

    }
}
