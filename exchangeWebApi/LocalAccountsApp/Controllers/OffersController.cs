﻿using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using RestSharp;
using System.Data.Entity;

namespace LocalAccountsApp.Controllers
{
    public class OfferViewModel
    {
        public int LoanNoteId { get; set; }
        public int OfferAmount { get; set; }
        public string Comment { get; set; }
        public int ClosingTypeId { get; set; }
        public int DueDiligenceOptionId { get; set; }
        public int DaysCount { get; set; }
    }

    [RoutePrefix("api")]
    public class OffersController : ApiController
    {
        private ApplicationDbContext _dbContext;
        private string _userId;

        public OffersController()
        {
            _dbContext = new ApplicationDbContext();
            //_userId = "2aecf97c-dae0-443b-a9a4-127176bb28ff"; //
        }


        public IHttpActionResult Post(OfferViewModel offer)
        {
            var idloan = offer.LoanNoteId;
            var ifLoanExist = _dbContext.LoanOfferBuyer.FirstOrDefault(l => l.LoanNoteId == offer.LoanNoteId);
            _userId = User.Identity.GetUserId(); //_dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).Id; //"a4051966-e4e1-4239-beb0-71f7fdc30e3d";
            dynamic resultToReturn;
            try
            {
                
                var dbOffer = new Offer
                {
                    OfferAmount = offer.OfferAmount,
                    Comment = offer.Comment,
                    OfferTypeId = 1,
                    ClosingTypeId = offer.ClosingTypeId,
                    CreatedDate= DateTime.Now

                };
                //dqsfqsdhfjkqsdhfkqsd

                var loanOfferBuyer = new LoanOfferBuyer
                {
                    LoanNoteId = offer.LoanNoteId,
                    Offer = dbOffer,
                    UserId = _userId
                };

                resultToReturn = _dbContext.LoanOfferBuyer.Add(loanOfferBuyer);

                var dueDiligence = new OfferHasDueDiligenceOptions();

                dueDiligence.DueDiligenceOptionsId = offer.DueDiligenceOptionId;
                dueDiligence.DaysCount = offer.DaysCount;
                dueDiligence.Offer = dbOffer;

                _dbContext.OfferHasDueDiligenceOptions.Add(dueDiligence);

                var data = _dbContext.LoanOfferBuyer.FirstOrDefault(l => l.LoanNoteId == offer.LoanNoteId);
                var data2 = _dbContext.LoanOfferBuyer.FirstOrDefault(l => l.UserId == _userId);
                if (data == null || data2 == null)
                {
                    _dbContext.SaveChanges();
                }
                else
                {
                    return BadRequest();
                }


                int offerId = dbOffer.offerID;

                var dbOfferStatusByDate = new OfferStatusByDate
                {
                    offerId = offerId,
                    offerStatusId = 1,
                    modificationDate = DateTime.Now

                };
                _dbContext.OfferStatusByDate.Add(dbOfferStatusByDate);
                _dbContext.SaveChanges();


                if (ifLoanExist == null)
                {
                    var email = User.Identity.GetUserName();
                    var first = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).firstName;
                    var last = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).lastName;
                    var phone = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).PhoneNumber;
                    var address = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).address;
                    var zip = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).zip;
                    var stateid = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).stateID;
                    var city = _dbContext.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).city;
                    var state = _dbContext.state.FirstOrDefault(s => s.ID == stateid).name;
                    var client = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/zxktqu/");
                    var request = new RestRequest(Method.POST);
                    request.AddParameter("application/json", "{\t\"oppname\":\"Loan: " + offer.LoanNoteId + "\",\n\t\"email\":\"" + email + "\",\n\t\"value\":\"" + offer.OfferAmount + "\",\n\t\"personFullName\":\"" + first + " " + last + "\",\n\t\"phone\":\"" + phone + "\",\n\t\"street\":\"" + address + "\",\n\t\"city\":\"" + city + "\",\n\t\"state\":\"" + state + "\",\n\t\"zip\":\"" + zip + "\"\n}", ParameterType.RequestBody);

                    IRestResponse response = client.Execute(request);

                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.InnerException.StackTrace);
            }

            return Ok();
        }
        [Route("Buyer/{id}/Offers")]
        public IHttpActionResult GetBuyerOffers(string id)
        {
            //var result = _dbContext.LoanOfferBuyer.Include(l => l.Offer).Include( );
            var resultToReturn =
                    from o in _dbContext.LoanOfferBuyer
                    where o.UserId == id
                    select new
                    {
                        Offer = o.Offer,
                        OfferStatus = o.Offer.OfferStatusByDate.OrderByDescending(i => i.modificationDate).FirstOrDefault().OfferStatus
                    };

            return Ok(resultToReturn);
        }

        public class oppOffer
        {
            public string oppname { get; set; }
        }
        [HttpPut]
        [Route("createDDoffer")]
        public IHttpActionResult createDDoffer(oppOffer oppoffer)
        {
            int idoffer = 0;
            idoffer = Convert.ToInt32(string.Join(string.Empty, oppoffer.oppname.Skip(7)));


            var dbtransaction = new transaction
            {

                createAt = DateTime.UtcNow,
                offerID = idoffer

            };
            _dbContext.transaction.Add(dbtransaction);
            _dbContext.SaveChanges();
            var dbdetailtransaction = new detailTransaction
            {
                step = "1",
                stepStatus = "PENDING",
                createAt = DateTime.UtcNow,
                transactionID = idoffer,
                turnUser = "seller"

            };
            _dbContext.detailTransaction.Add(dbdetailtransaction);
            _dbContext.SaveChanges();

            int idDetailTransaction = dbdetailtransaction.ID;


            //int _dDOID = dueDiligenceOptionId.;
            var offerAmount = _dbContext.Offers.FirstOrDefault(o => o.offerID == idoffer).OfferAmount;
            int closingType = _dbContext.Offers.FirstOrDefault(o => o.offerID == idoffer).ClosingTypeId;

            var dueDiligOption = (from o in _dbContext.Offers
                                  join
                                  ohd in _dbContext.OfferHasDueDiligenceOptions
                                  on o.offerID equals ohd.OfferId
                                  join dd in _dbContext.DueDilligenceOptions
                                  on ohd.DueDiligenceOptionsId equals dd.DueDiligenceOptionsId
                                  where o.offerID == idoffer
                                  select new
                                  {
                                      ddd = ohd.DueDiligenceOptionsId,
                                      dc = ohd.DaysCount,
                                  }).ToList();

            int _dueDiligenceOptionId = Int32.Parse(dueDiligOption[0].ddd.ToString());
            int _daysCount = Int32.Parse(dueDiligOption[0].dc.ToString());
            var dbnegotiation = new negotiation
            {
                offerAmount = double.Parse(offerAmount.ToString()),
                closingServiceId = closingType,
                dueDiligenceOptionId = _dueDiligenceOptionId,
                days = _daysCount,
                role = "buyer",
                statusNegotiation = "PENDING",
                createAt = DateTime.Now,
                endDueDiligence = false,
                extendDueDiligence = false,
                detailTransactionID = idDetailTransaction

            };
            _dbContext.negotiation.Add(dbnegotiation);
            _dbContext.SaveChanges();



            var dbOfferStatusByDate = new OfferStatusByDate
            {
                offerId = idoffer,
                offerStatusId = 2,
                modificationDate = DateTime.Now

            };
            _dbContext.OfferStatusByDate.Add(dbOfferStatusByDate);
            _dbContext.SaveChanges();




            return Ok(_dueDiligenceOptionId);
        }


        [HttpPut]
        [Route("updateStatusOffer")]
        public IHttpActionResult updateStatusOffer(oppOffer oppoffer)
        {

            int idoffer = 0;
            idoffer = Convert.ToInt32(string.Join(string.Empty, oppoffer.oppname.Skip(7)));
            var dbOfferStatusByDate = new OfferStatusByDate
            {

                offerId = idoffer,
                offerStatusId = 3,
                modificationDate = DateTime.Now

            };
            _dbContext.OfferStatusByDate.Add(dbOfferStatusByDate);
            _dbContext.SaveChanges();
            var dd = _dbContext.Offers.FirstOrDefault(o => o.offerID == idoffer);
            dd.UpdatedDate = DateTime.Now;
            _dbContext.Entry(dd).State = EntityState.Modified;

            _dbContext.SaveChanges();

            return Ok();
        }
    }
}