﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Web.Http.Results;

namespace LocalAccountsApp.Controllers
{
    [RoutePrefix("api")]
    public class TransactionController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public class Result
        {
            public List<transaction> Transaction { get; set; }
            public int detectedStep { get; set; }

            public List<detailTransaction> DetTrans1 { get; set; }
            public List<detailTransaction> DetTrans2 { get; set; }
            public List<detailTransaction> DetTrans3 { get; set; }
            public List<negotiation> negotiationStep1 { get; set; }
            public List<negotiation> negotiationStep2 { get; set; }
            public List<negotiation> negotiationStep3 { get; set; }
            public vestingInstructions vestingInctructionSeller { get; set; }
            public vestingInstructions vestingInctructionBuyer { get; set; }

        }
        public class StepOneModel
        {
            public float offerAmountStepOne { get; set; }
            public int closingServiceStepOne { get; set; }
            public int idTransaction { get; set; }
            public int dueDiligenceOptionStepOne { get; set; }
            public int daysStepOne { get; set; }
            public string status { get; set; }
            public int IdDetailStep { get; set; }
            public string role { get; set; }
            public string turnUser { get; set; }

        }
        public class StepTowModel
        {
            public int daysStepTwo { get; set; }
            public bool DueDilidenceStepTwo { get; set; }
            public string statusDetailStepTwo { get; set; }
            public int IdDetailStepStepTwo { get; set; }
            public string roleStepTwo { get; set; }
            public string turnUserStepTwo { get; set; }
            public int idTransaction { get; set; }

        }

        public class StepThereeModel
        {
            public float offerAmountStepTheree { get; set; }
            public int closingServiceStepTheree { get; set; }
            public string statusDetailStepTheree { get; set; }
            public string turnUserStepTheree { get; set; }
            public string roleStepTheree { get; set; }
            public int idTransaction { get; set; }
            public int IdDetailStepTheree { get; set; }
        }
        public class InformationStepTheree
        {
            public string company { get; set; }
            public string contactPerson { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public int state { get; set; }
            public string zip { get; set; }
            public string phone { get; set; }
            public string email { get; set; }
            public string other { get; set; }
            public int loanServicerId { get; set; }
            public string roleInformation { get; set; }
            public int transactionID { get; set; }
        }


        [Route("Transaction/{offerId}")]
        public IHttpActionResult GetTransaction(int offerId)
        {
            var result = new Result();

            var Transaction = db.transaction.Where(t => t.offerID == offerId).ToList();

            var detailTransaction1 = db.detailTransaction
                                            .Where(t => t.transactionID == offerId)
                                            .Where(t => t.step == "1")
                                            .ToList();

            var NegociationStep1 = db.negotiation
                                          .Where(t => t.detailTransaction.transactionID == offerId)
                                          .Where(t => t.detailTransaction.step == "1")
                                          .OrderByDescending(t => t.ID)
                                          .Take(1)
                                          .ToList();



            var detailTransaction2 = db.detailTransaction
                                            .Where(t => t.transactionID == offerId)
                                            .Where(t => t.step == "2")
                                            .ToList();
            var NegociationStep2 = db.negotiation
                                          .Where(t => t.detailTransaction.transactionID == offerId)
                                          .Where(t => t.detailTransaction.step == "2")
                                          .OrderByDescending(t => t.ID)
                                          .Take(1)
                                          .ToList();

            var detailTransaction3 = db.detailTransaction
                                            .Where(t => t.transactionID == offerId)
                                            .Where(t => t.step == "3")
                                            .ToList();
            var NegociationStep3 = db.negotiation
                                          .Where(t => t.detailTransaction.transactionID == offerId)
                                          .Where(t => t.detailTransaction.step == "3")
                                          .OrderByDescending(t => t.ID)
                                          .Take(1)
                                          .ToList();
            var vestingInctructionBuyer = db.vestingInstructions.Where(l => l.role == "buyer").FirstOrDefault(l => l.transactionID == offerId);
            var vestingInctructionSeller = db.vestingInstructions.Where(l => l.role == "seller").FirstOrDefault(l => l.transactionID == offerId);



            result.Transaction = Transaction;

            result.DetTrans1 = detailTransaction1;
            result.negotiationStep1 = NegociationStep1;

            result.DetTrans2 = detailTransaction2;
            result.negotiationStep2 = NegociationStep2;

            result.DetTrans3 = detailTransaction3;
            result.negotiationStep3 = NegociationStep3;

            result.vestingInctructionBuyer = vestingInctructionBuyer;
            result.vestingInctructionSeller = vestingInctructionSeller;


            if (result.DetTrans1.Count() != 0)
            {
                result.detectedStep = 1;
            }

            if (result.DetTrans2.Count() != 0)
            {
                result.detectedStep = 2;
            }
            if (result.DetTrans3.Count() != 0)
            {
                result.detectedStep = 3;
            }




            return Ok(result);
        }
        [Route("Transaction/StepOne")]
        public IHttpActionResult PostStepOne(StepOneModel stepone)
        {
            var _negociation = new negotiation();

            _negociation.dueDiligenceOptionId = stepone.dueDiligenceOptionStepOne;
            _negociation.closingServiceId = stepone.closingServiceStepOne;
            if (stepone.daysStepOne == 0)
            {
                _negociation.days = 3;
            }
            else
            {
                _negociation.days = stepone.daysStepOne;
            }

            _negociation.offerAmount = stepone.offerAmountStepOne;
            _negociation.role = stepone.role;
            _negociation.detailTransactionID = stepone.IdDetailStep;
            _negociation.createAt = DateTime.UtcNow;
            _negociation.updateAt = DateTime.UtcNow;
            _negociation.statusNegotiation = stepone.status;

            db.negotiation.Add(_negociation);

            var negociationCheckAccept = db.negotiation.Where(t => t.detailTransactionID == stepone.IdDetailStep).Where(t => t.role == stepone.turnUser).OrderByDescending(t => t.ID).Take(1).ToList();
            var detail = db.detailTransaction.SingleOrDefault(t => t.ID == stepone.IdDetailStep);
            if (stepone.status == "COUNTER")
            {
                detail.stepStatus = "PENDING";
            }
            else if (stepone.status == "ACCEPT")
            {
                if (negociationCheckAccept.Count() != 0)
                {
                    if (stepone.status == "ACCEPT" && negociationCheckAccept[0].statusNegotiation == "ACCEPT")
                    {
                        detail.stepStatus = "ACCEPT";
                        var detaiTrans2 = new detailTransaction();
                        detaiTrans2.step = "2";
                        detaiTrans2.stepStatus = "INIT";
                        detaiTrans2.turnUser = "buyer";
                        detaiTrans2.transactionID = stepone.idTransaction;
                        detaiTrans2.createAt = DateTime.UtcNow;

                        db.detailTransaction.Add(detaiTrans2);
                    }
                }
                else
                {
                    detail.stepStatus = "PENDING";
                }

            }
            else
            {
                detail.stepStatus = "REJECT";
            }



            detail.turnUser = stepone.turnUser;

            db.SaveChanges();

            return Ok();
        }
        [Route("Transaction/StepTwo")]
        public IHttpActionResult PostStepTow(StepTowModel stepTow)
        {
            var _negociation = new negotiation();


            _negociation.endDueDiligence = stepTow.DueDilidenceStepTwo;
            _negociation.days = stepTow.daysStepTwo;
            _negociation.role = stepTow.roleStepTwo;
            _negociation.detailTransactionID = stepTow.IdDetailStepStepTwo;
            _negociation.createAt = DateTime.UtcNow;
            _negociation.updateAt = DateTime.UtcNow;
            _negociation.statusNegotiation = stepTow.statusDetailStepTwo;

            db.negotiation.Add(_negociation);

            var negociationCheckAccept = db.negotiation.Where(t => t.detailTransactionID == stepTow.IdDetailStepStepTwo).Where(t => t.role == stepTow.turnUserStepTwo).OrderByDescending(t => t.ID).Take(1).ToList();
            var detail = db.detailTransaction.SingleOrDefault(t => t.ID == stepTow.IdDetailStepStepTwo);
            if (stepTow.statusDetailStepTwo == "COUNTER")
            {
                detail.stepStatus = "PENDING";
            }
            else if (stepTow.statusDetailStepTwo == "ACCEPT")
            {
                if (negociationCheckAccept.Count() != 0)
                {
                    if (stepTow.statusDetailStepTwo == "ACCEPT" && negociationCheckAccept[0].statusNegotiation == "ACCEPT")
                    {
                        detail.stepStatus = "ACCEPT";
                        var detaiTrans3 = new detailTransaction();
                        detaiTrans3.step = "3";
                        detaiTrans3.stepStatus = "INIT";
                        detaiTrans3.turnUser = "buyer";
                        detaiTrans3.transactionID = stepTow.idTransaction;
                        detaiTrans3.createAt = DateTime.UtcNow;

                        db.detailTransaction.Add(detaiTrans3);
                    }
                }
                else
                {
                    detail.stepStatus = "PENDING";
                }

            }
            else
            {
                detail.stepStatus = "REJECT";
            }



            detail.turnUser = stepTow.turnUserStepTwo;


            db.SaveChanges();

            return Ok();


        }
        [Route("Transaction/StepTheree")]
        public IHttpActionResult PostStepTheree(StepThereeModel stepTheree)
        {
            var _negociation = new negotiation();
            _negociation.role = stepTheree.roleStepTheree;
            _negociation.offerAmount = stepTheree.offerAmountStepTheree;
            _negociation.closingServiceId = stepTheree.closingServiceStepTheree;
            _negociation.detailTransactionID = stepTheree.IdDetailStepTheree;
            _negociation.createAt = DateTime.UtcNow;
            _negociation.updateAt = DateTime.UtcNow;
            _negociation.statusNegotiation = stepTheree.statusDetailStepTheree;

            db.negotiation.Add(_negociation);

            var negociationCheckAccept = db.negotiation.Where(t => t.detailTransactionID == stepTheree.IdDetailStepTheree).Where(t => t.role == stepTheree.turnUserStepTheree).OrderByDescending(t => t.ID).Take(1).ToList();
            var detail = db.detailTransaction.SingleOrDefault(t => t.ID == stepTheree.IdDetailStepTheree);
            if (stepTheree.statusDetailStepTheree == "COUNTER")
            {
                detail.stepStatus = "PENDING";
            }
            else if (stepTheree.statusDetailStepTheree == "ACCEPT")
            {
                if (negociationCheckAccept.Count() != 0)
                {
                    if (stepTheree.statusDetailStepTheree == "ACCEPT" && negociationCheckAccept[0].statusNegotiation == "ACCEPT")
                    {
                        detail.stepStatus = "ACCEPT";
                        var detaiTrans4 = new detailTransaction();
                        detaiTrans4.step = "4";
                        detaiTrans4.stepStatus = "INIT";
                        detaiTrans4.turnUser = "buyer";
                        detaiTrans4.transactionID = stepTheree.idTransaction;
                        detaiTrans4.createAt = DateTime.UtcNow;

                        db.detailTransaction.Add(detaiTrans4);
                    }
                }
                else
                {
                    detail.stepStatus = "PENDING";
                }

            }
            else
            {
                detail.stepStatus = "REJECT";
            }



            detail.turnUser = stepTheree.turnUserStepTheree;


            db.SaveChanges();

            return Ok();


        }

        [Route("Transaction/StepThereeInformation")]
        public IHttpActionResult PostInformationStepTheree(InformationStepTheree information)
        {
            var _vestingInstructions = new vestingInstructions();
            _vestingInstructions.role = information.roleInformation;
            _vestingInstructions.loanServicerId = information.loanServicerId;
            _vestingInstructions.company = information.company;
            _vestingInstructions.zip = information.zip;
            _vestingInstructions.transactionID = information.transactionID;
            _vestingInstructions.phone = information.phone;
            _vestingInstructions.other = information.other;
            _vestingInstructions.state = information.state;
            _vestingInstructions.email = information.email;
            _vestingInstructions.contactPerson = information.contactPerson;
            _vestingInstructions.address = information.address;
            _vestingInstructions.city = information.city;

            db.vestingInstructions.Add(_vestingInstructions);
            db.SaveChanges();
            return Ok();
        }

    }
}
