﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;
using RestSharp;
using System.Net.Http;
using System.Collections.Generic;

namespace LocalAccountsApp.Controllers
{


    public class ValuesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET api/values
        public string Get()
        {
            var userName = this.RequestContext.Principal.Identity.Name;
            return String.Format("Hello, {0}.", userName);
        }
        public class UserInfomodel
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string UserName { get; set; }
            public string PhoneNumber { get; set; }
            public string id { get; set; }
            public string address { get; set; }
            public string zip { get; set; }
            public string fax { get; set; }
            public string company { get; set; }
            public string state { get; set; }
            public string city { get; set; }
        }
        [Route("api/info")]
        [HttpGet]
        public IHttpActionResult info()
        {
            UserInfomodel userCurrent = new UserInfomodel();
            var test = HttpContext.Current.User.Identity.Name;
            userCurrent.id = User.Identity.GetUserId();
            userCurrent.firstName = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).firstName;
            userCurrent.lastName = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).lastName;
            userCurrent.UserName = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).UserName;
            userCurrent.PhoneNumber = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).PhoneNumber;
            userCurrent.address = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).address;
            userCurrent.zip = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).zip;
            userCurrent.fax = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).fax;
            userCurrent.city = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).city;
            userCurrent.company = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).company;
            var stt = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).stateID;
            userCurrent.state = db.state.FirstOrDefault(u => u.ID == stt).name;

            return Json(userCurrent);
        }
    }
}