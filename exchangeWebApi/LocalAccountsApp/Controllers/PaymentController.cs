﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using System.IO;
using RestSharp;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using LocalAccountsApp.Models;
using System.Linq;
using Microsoft.AspNet.Identity;

namespace LocalAccountsApp.Controllers
{
    public class PaymentController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public class viewModelPayment
        {
            public string creditType { get; set; }

            public string card { get; set; }
            public string zip { get; set; }
            public string Name { get; set; }
            public string lastName { get; set; }
            public string address { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string expiration { get; set; }
            public string userEmail { get; set; }
            public string FCI { get; set; }
            public string borrowerName { get; set; }
            public string propertyCity { get; set; }
            public string propertyAddress { get; set; }
            public string propertyState { get; set; }
            public string propertyZip { get; set; }
            public string propertyType { get; set; }
            public string mode { get; set; }
            public string amount { get; set; }
            public string tell { get; set; }

        }

        /*
         
             Test Card Brand	Number
                
                American Express 	370000000000002

                Discover	        6011000000000012

                JCB	                3088000000000017

                Diners Club/ Carte Blanche	38000000000006

                Visa	    4007000000027
 	                        4012888818888
 	                        4111111111111111

                Mastercard	5424000000000015
 	                        2223000010309703
 	                        2223000010309711
             
             */

        //Route : api/AuthorizeNet/GetResult
        [Route("api/Result")]
        [HttpPost]
        public IHttpActionResult Result(viewModelPayment payment)
        {
            if (payment.mode == "BPO")
            {
                payment.amount = "150";
            }
            else if (payment.mode == "Purchase O&E")
            {
                payment.amount = "150";
            }
            else if (payment.mode == "Due Diligence Report Package")
            {
                payment.amount = "150";
            }
            else if (payment.mode == "Assignment")
            {
                payment.amount = "200";
            }
            else
            {
                payment.amount = "200";
            }
            // By default, this sample code is designed to post to our test server for
            // developer accounts: https://test.authorize.net/gateway/transact.dll
            // for real accounts (even in test mode), please make sure that you are
            // posting to: https://secure.authorize.net/gateway/transact.dll


            String post_url = "https://test.authorize.net/gateway/transact.dll";
            Dictionary<string, string> post_values = new Dictionary<string, string>();
            //the API Login ID and Transaction Key must be replaced with valid values

            post_values.Add("x_login", "2Zy8rAE87kD2");
            post_values.Add("x_tran_key", "6Scn6826WK47kxSM");
            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", "|");
            post_values.Add("x_relay_response", "FALSE");
            post_values.Add("x_type", "AUTH_CAPTURE");
            post_values.Add("x_method", "CC");

            //Credit Card Number
            post_values.Add("x_card_num", payment.card);
            //Expiration Date Card Number
            post_values.Add("x_exp_date", payment.expiration);
            //Order Amount
            post_values.Add("x_amount", payment.amount);
            post_values.Add("x_description", "Sample Transaction");
            post_values.Add("x_first_name", payment.Name);
            post_values.Add("x_last_name", payment.lastName);
            post_values.Add("x_address", payment.address);
            post_values.Add("x_state", payment.state);
            post_values.Add("x_zip", payment.zip);
            // Additional fields can be added here as outlined in the AIM integration
            // guide at: http://developer.authorize.net

            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
            String post_string = "";

            foreach (KeyValuePair<string, string> post_value in post_values)
            {
                post_string += post_value.Key + "=" +
                HttpUtility.UrlEncode(post_value.Value) + "&";
            }
            ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            post_string = post_string.TrimEnd('&');

            // The following section provides an example of how to add line item details to
            // the post string.  Because line items may consist of multiple values with the
            // same key/name, they cannot be simply added into the above array.
            //
            // This section is commented out by default.
            /*
            string[] line_items = {
                "item1<|>golf balls<|><|>2<|>18.95<|>Y",
                "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
                "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y"};

            foreach( string value in line_items )
            {
                post_string += "&x_line_item=" + HttpUtility.UrlEncode(value);
            }
            */

            // create an HttpWebRequest object to communicate with Authorize.net
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            String post_response;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                post_response = responseStream.ReadToEnd();
                responseStream.Close();
            }

            string[] res = post_response.Split('|');
            string result = res[3].ToString();
            // the response string is broken into an array
            // The split character specified here must match the delimiting character specified above
            //Array response_array = post_response.Split('|');

            // the results are output to the screen in the form of an html numbered list.
            // var result = "<OL> \n";
            //foreach (string value in response_array)
            //{
            //    result = result + "<LI>" + value + "&nbsp;</LI> \n";
            //}
            //result = result + "</OL> \n";
            // individual elements of the array could be accessed to read certain response
            // fields.  For example, response_array[0] would return the Response Code,
            // response_array[2] would return the Response Reason Code.
            // for a list of response fields, please review the AIM Implementation Guide
            //string[] response = post_response.Split('|');

            if (result.Contains("This transaction has been approved."))
            {

                var client = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/zfl7w8/");
                var request = new RestRequest(Method.POST);
                request.AddParameter("application/json", "{\n\t\"to\":\"support@adelphatech.com\",\n\t\"fromName\":\"" + payment.Name + " " + payment.lastName + "\",\n\t\"subject\":\"Order D-D :" + payment.mode + "\",\n\t\"email\":\"" + payment.userEmail + "\",\n\t\"fci\":\"" + payment.FCI + "\",\n\t\"borrozerName\":\"" + payment.borrowerName + "\",\n\t\"propertyCity\":\"" + payment.propertyCity + "\",\n\t\"propertyState\":\"" + payment.propertyState + "\",\n\t\"propertyZip\":\"" + payment.propertyZip + "\",\n\t\"propertyAdress\":\"" + payment.propertyAddress + "\",\n\t\"propertyType\":\"" + payment.propertyType + "\",\n\t\"primaryContact\":\"" + payment.userEmail + "\",\n\t\"nameOpp\":\"" + payment.mode + DateTime.UtcNow + "\",\n\t\"value\":\"" + payment.amount + "\",\n\t\"phone\":\"" + payment.tell + "\"\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
            }




            return Json(result);

        }
        private static bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }
    }

}
