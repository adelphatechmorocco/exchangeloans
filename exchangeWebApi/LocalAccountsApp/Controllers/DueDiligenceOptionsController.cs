﻿using LocalAccountsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LocalAccountsApp.Controllers
{
    [RoutePrefix("api")]
    public class DueDiligenceOptionsController : ApiController
    {
        private ApplicationDbContext _dbContext;

        public DueDiligenceOptionsController()
        {
            _dbContext = new ApplicationDbContext();
        }
        [Route("duedilligenceoptions")]
        public IHttpActionResult Get()
        {
            var resultToReturn = _dbContext.DueDilligenceOptions.ToList();
            return Ok(resultToReturn);
        }
    }
}