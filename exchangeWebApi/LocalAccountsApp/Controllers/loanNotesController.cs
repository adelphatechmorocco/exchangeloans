﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using LocalAccountsApp.Models;
using Microsoft.AspNet.Identity;
using RestSharp;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json.Linq;

namespace LocalAccountsApp.Controllers
{

    public class loanNoteBindingModel
    {

        public float? minAskingPrice { get; set; }
        public float? principaleBalance { get; set; }
        //public bool isPublished { get; set; }
        public float? accruedLateCharges { get; set; }
        public DateTime? adjPayementChangeDate { get; set; }
        public float? adjustementFrequency { get; set; }
        public int? afterDays { get; set; }
        public float? askingPrice { get; set; }
        public bool? ballonPayement { get; set; }
        public bool? bankruptcyStatus { get; set; }
        public float? ceiling { get; set; }
        public string comments { get; set; }
        public bool? deedMortgageStatus { get; set; }
        public DateTime? drawPeriodStartDate { get; set; }
        // public bool escrowImpoundStatus { get; set; }
        public DateTime? firstPayementDate { get; set; }
        public float? floor { get; set; }
        public bool? foreclosureStatus { get; set; }
        public string indexName { get; set; }
        public float? lateCharge { get; set; }
        public float? loanCharges { get; set; }
        public bool? loanTermsModified { get; set; }
        public float? margin { get; set; }
        //public string name { get; set; }
        public DateTime? nextAdjustement { get; set; }
        public DateTime? nextPayementDate { get; set; }
        public float? noteInterestRate { get; set; }
        public DateTime? noteMaturityDate { get; set; }
        public string ofPayementsLast12 { get; set; }
        public bool? onForbearancePlan { get; set; }
        public float? originalLoanAmount { get; set; }
        public DateTime? originationDate { get; set; }
        public DateTime? paidToDate { get; set; }
        public float? payementTrust { get; set; }
        public DateTime? payersLastPayementMadeDate { get; set; }
        public float? percentagePrice { get; set; }
        public float? PI { get; set; }
        public bool? prePayPenalty { get; set; }
        public float? propertyTaxesDue { get; set; }
        public bool? registerswMers { get; set; }
        public DateTime? repaymentPeriodStartDate { get; set; }
        public float? soldInterestRate { get; set; }
        public float? TotalMonthlyLoanPayement { get; set; }
        public float? UnpaidInterest { get; set; }
        public float? unPaidPrincipalBalance { get; set; }
        public int? loanServicer { get; set; }
        public int? amortizationType { get; set; }
        public int? loanNote { get; set; }
        public int? loanStatus { get; set; }
        public int? closingType { get; set; }
        public int? loanType { get; set; }
        public int? rateType { get; set; }
        public int? paymentFrequency { get; set; }
        //public string userID { get; set; }
        public property property { get; set; }
        public foreclosure foreclosure { get; set; }
        public bankruptcy bankruptcy { get; set; }
        public escrowImpounds escrowImpounds { get; set; }
        public borrower borrower { get; set; }
        public deedMortgage deedMortgage { get; set; }

    }


    public class loanNotesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        [Route("api/loanNotes/UploadFiles")]
        public IHttpActionResult UploadFiles()
        {

            int i = 0;
            int cntSuccess = 0;
            var uploadedFileNames = new List<string>();
            string result = string.Empty;
            string loanid = "xxc";
            string[] uploadedType = { "PaymentHistory", "other", "photo" };

            HttpResponseMessage response = new HttpResponseMessage();
            Regex pattern = new Regex("[;,\t\r ]|[\n]{2}");
            var httpRequest = HttpContext.Current.Request;
            loanid = httpRequest.Form[0];


            using (var db = new ApplicationDbContext())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {


                        if (httpRequest.Files.GetMultiple("other").Count() > 0)
                        {
                            foreach (HttpPostedFile file in httpRequest.Files.GetMultiple("other"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var doc = new document
                                    {
                                        name = generatedfilename,
                                        loanNoteID = Int32.Parse(loanid),
                                        createAt = DateTime.Today,
                                        documentPath = "other"

                                    };
                                    file.SaveAs(filePath);
                                    db.document.Add(doc);
                                    db.SaveChanges();
                                }
                                catch (Exception e)
                                {
                                    throw e;
                                }
                            }
                        }
                        if (httpRequest.Files.GetMultiple("photo").Count() > 0)
                        {
                            foreach (HttpPostedFile photo in httpRequest.Files.GetMultiple("photo"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + photo.FileName;

                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var dbphoto = new photo
                                    {
                                        title = generatedfilename,
                                        loanNoteID = Int32.Parse(loanid)
                                    };
                                    photo.SaveAs(filePath);
                                    db.photo.Add(dbphoto);
                                    db.SaveChanges();
                                }
                                catch (Exception e)
                                {

                                    throw e;
                                }
                            }
                        }
                        if (httpRequest.Files.GetMultiple("PaymentHistory").Count() > 0)
                        {
                            foreach (HttpPostedFile file in httpRequest.Files.GetMultiple("PaymentHistory"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var doc = new document
                                    {
                                        name = generatedfilename,
                                        loanNoteID = Int32.Parse(loanid),
                                        createAt = DateTime.Today,
                                        documentPath = "PaymentHistory"

                                    };
                                    file.SaveAs(filePath);
                                    db.document.Add(doc);
                                    db.SaveChanges();
                                }
                                catch (Exception e)
                                {

                                    throw e;
                                }

                            }
                        }



                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback(); //Required according to MSDN article 
                        throw; //Not in MSDN article, but recommended so the exception still bubbles up
                    }
                }
            }


            return Ok("dddddd");







            ////if (httpRequest.Files.Count > 0)
            ////{
            ////    loanid = httpRequest.Form["id"];

            ////    foreach (string file in httpRequest.Files)
            ////    {

            ////        var postedFile = httpRequest.Files[i];
            ////        var filePath = HttpContext.Current.Server.MapPath
            ////             ("~/UploadedFiles/" + postedFile.FileName);
            ////        try
            ////        {
            ////            postedFile.SaveAs(filePath);
            ////            uploadedFileNames.Add(httpRequest.Files[i].FileName);
            ////            cntSuccess++;

            ////            var doc = new document
            ////            {
            ////                name = User.Identity.GetUserId() + Path.GetRandomFileName() + DateTime.Now.ToString("yyyyMMddHHmmss") + pattern.Replace(postedFile.FileName, "_"),
            ////                loanNoteID = Int32.Parse(loanid),
            ////                createAt = DateTime.Today

            ////            };
            ////            db.document.Add(doc);
            ////            db.SaveChanges();
            ////        }
            ////        catch (Exception ex)
            ////        {
            ////            throw ex;
            ////        }
            ////        i++;


            ////    }
            ////}
            ////result = cntSuccess.ToString() + " files uploaded succesfully.";
            ////result += "<ul>";
            ////foreach (var f in uploadedFileNames)
            ////{
            ////    result += "<li>" + f + "</li>";
            ////}
            ////result += "</ul>";

            //// return Json(httpRequest.Files.GetMultiple("other").Count());
            //return Json(HttpContext.Current.Server.MapPath("~/App_Data/uploads/"));
        }

        // GET: api/loanNotes

        public IQueryable<loanNote> GetloanNotes(string userId = "")
        {
            var listLaonNote = db.loanNote.Include(loanNote => loanNote.loanStatus).Include(loanNote => loanNote.user).Include(loanNote => loanNote.property).Include(loanNote => loanNote.loanServicer).AsQueryable();
            if (String.IsNullOrEmpty(userId))
            {
                listLaonNote = listLaonNote.Where(l => l.isPublished == true);
            }
            if (!String.IsNullOrEmpty(userId))
            {
                listLaonNote = listLaonNote.Where(l => l.userID == userId);
            }
            return listLaonNote;
        }

        //update loan status a travers prosperworks
        public class oppVM
        {
            public string nameopp { get; set; }
            public string stage { get; set; }
        }

        [HttpPut]
        [Route("api/ChangePublishedStatusOpp/")]
        public IHttpActionResult PublishedloanNoteProsper(oppVM oppvm)
        {
            int id = 0;

            id = Convert.ToInt32(string.Join(string.Empty, oppvm.nameopp.Skip(6)));
            Boolean statusOpp = false;
            if (oppvm.stage == "1976521")
            {
                statusOpp = true;
            }
            if (oppvm.stage == "1976520")
            {
                statusOpp = false;

            }
          
            var loanNote = db.loanNote.FirstOrDefault(l => l.loanNoteID == id);
            if (loanNote == null)
            {
                return NotFound();
            }

            loanNote.isPublished = statusOpp;
            db.Entry(loanNote).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (loanNoteExists(loanNote.loanNoteID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            return Ok();
        }





        [Authorize(Roles = "admin")]
        [HttpPut]
        [Route("api/ChangePublishedStatus/{id}")]
        public IHttpActionResult PublishedloanNote(int id)
        {
            var loanNote = db.loanNote.FirstOrDefault(l => l.loanNoteID == id);
            if (loanNote == null)
            {
                return NotFound();
            }
            //update opportunity to published

            var email = User.Identity.GetUserName();
            var client = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/z0hoom/");
            var request = new RestRequest(Method.POST);
            request.AddParameter("application/json", "{\t\"isPublish\":\"" + loanNote.isPublished + "\",\n\t\"oppname\":\"Loan: " + loanNote.loanNoteID + "\",\n\t\"email\":\"" + email + "\"\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);


            //update opportunity to unpublished

            var client1 = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/z0h8qj/");
            var request1 = new RestRequest(Method.POST);
            request1.AddParameter("application/json", "{\t\"isPublish\":\"" + loanNote.isPublished + "\",\n\t\"oppname\":\"Loan: " + loanNote.loanNoteID + "\",\n\t\"email\":\"" + email + "\"\n}", ParameterType.RequestBody);
            IRestResponse response1 = client1.Execute(request1);


            loanNote.isPublished = !loanNote.isPublished;
            db.Entry(loanNote).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (loanNoteExists(loanNote.loanNoteID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        [Authorize(Roles = "admin")]
        [Route("api/NonPublishedloanNotes")]
        public IQueryable<loanNote> GetNonPublishedloanNotes()
        {

            var listLaonNote = db.loanNote.Include(loanNote => loanNote.loanStatus).Include(loanNote => loanNote.user).Include(loanNote => loanNote.property).AsQueryable(); ;

            listLaonNote = listLaonNote.Where(l => l.isPublished == false);

            return listLaonNote;

            //User.IsInRole("admin");
        }

        [Route("api/loans")]
        public IHttpActionResult Get(string includes = "")
        {
            var result = db.Set<loanNote>().AsQueryable();
            includes.Split(';')
                .ToList().ForEach(i =>
                {
                    if (!String.IsNullOrEmpty(i))
                        result = result.Include(i).Take(10);

                });
            var resultToReturn = result.ToList();
            return Ok(resultToReturn);
        }
        [Route("api/loans/{id}")]
        public IHttpActionResult Get(int id, string includes = "")
        {
            var result = db.Set<loanNote>().AsQueryable();
            includes.Split(';')
                .ToList().ForEach(i =>
                {
                    if (!String.IsNullOrEmpty(i))
                        result = result.Include(i);

                });
            var resultToReturn = result.FirstOrDefault(l => l.loanNoteID == id);
            return Ok(resultToReturn);
        }

        [Route("api/loannotes/{id}/closingtype")]
        public IHttpActionResult GetClosingTypes(int id)
        {

            var resultToReturn = db.loanNote.Include(l => l.closingType).Where(l => l.loanNoteID == id).Select(l => l.closingType);
            return Ok(resultToReturn);
        }

        [Route("api/loannotes/{id}/closingtypes")]
        public IHttpActionResult GetClosingTypesChoices(int id)
        {
            var result = db.loanNote.Include(l => l.closingType).Where(l => l.loanNoteID == id).Select(l => l.closingType).ToList();
            dynamic resultToReturn;
            switch (result.First()?.ClosingTypeId)
            {
                case 1:
                case 2:
                    return Ok(result);
                default:
                    resultToReturn = db.closingTypes.ToList();
                    return Ok(resultToReturn);
            }
        }

        [Route("api/loans/{id}/borrower")]
        public IHttpActionResult GetBorrower(int id)
        {
            var resultToReturn = db.borrower.Where(b => b.ID == id).FirstOrDefault();
            return Ok(resultToReturn);
        }

        [Route("api/loans/{id}/bankruptcy")]
        public IHttpActionResult GetBankruptcyWithChapter(int id)
        {
            var resultToReturn = db.bankruptcy.Where(b => b.loanNoteID == id).Include(b => b.bkChapter).FirstOrDefault();
            return Ok(resultToReturn);
        }

        [Route("api/loans/{id}/Property")]
        public IHttpActionResult GetPropertyWithProperty(int id)
        {
            var resultToReturn = db.property.Where(b => b.loanNoteId == id).Include(b => b.typeProperty).Include(b => b.valuationType).FirstOrDefault();
            return Ok(resultToReturn);
        }


        // GET: api/loanNotes/5
        [ResponseType(typeof(loanNote))]
        public IHttpActionResult GetloanNote(int id)
        {
            loanNote loanNote = db.Set<loanNote>().Where(l => l.loanNoteID == id).FirstOrDefault();
            if (loanNote == null)
            {
                return NotFound();
            }

            return Ok(loanNote);
        }

        // PUT: api/loanNotes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutloanNote(int id, loanNote loanNote)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != loanNote.loanNoteID)
            {
                return BadRequest();
            }

            db.Entry(loanNote).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!loanNoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        [Route("api/offersloan/{id}")]
        public IHttpActionResult getOffersLoan(int id)
        {
            var offersloan = (from
                              lob in db.LoanOfferBuyer
                              join
                              o in db.Offers
                              on lob.OfferId equals o.offerID
                              where lob.LoanNoteId == id
                              select new
                              {
                                  offer = o,
                                  offerBuyer = lob
                              }).ToList();

            return Ok(offersloan);
        }

        // POST: api/loanNotes
        //[ResponseType(typeof(loanNote))]
        //public IHttpActionResult PostloanNote(loanNote loanNote)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.loanNote.Add(loanNote);

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (loanNoteExists(loanNote.loanNoteID))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = loanNote.loanNoteID }, loanNote);
        //}
        // POST: api/loanNotes
        [ResponseType(typeof(loanNote))]
        public IHttpActionResult PostloanNote()
        {
            int lastloanID = 0;
            int i = 0;
            int cntSuccess = 0;
            var uploadedFileNames = new List<string>();
            string result = string.Empty;
            string loanid = "xxc";
            var httpRequest = HttpContext.Current.Request;
            var loan = httpRequest.Form["loannote"];
            //JObject jsonx = JObject.Parse(httpRequest.Form.Get("loannote"));

            loanNoteBindingModel ModelLoanNote = Newtonsoft.Json.JsonConvert.DeserializeObject<loanNoteBindingModel>(httpRequest.Form.Get("loannote"));


            using (var db = new ApplicationDbContext())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {



                        var dbLoanNote = new loanNote
                        {
                            minAskingPrice = ModelLoanNote.minAskingPrice,
                            principaleBalance = ModelLoanNote.principaleBalance,
                            accruedLateCharges = ModelLoanNote.accruedLateCharges,
                            adjPayementChangeDate = ModelLoanNote.adjPayementChangeDate,
                            adjustementFrequency = ModelLoanNote.adjustementFrequency,
                            afterDays = ModelLoanNote.afterDays,
                            askingPrice = ModelLoanNote.askingPrice,
                            ballonPayement = ModelLoanNote.ballonPayement,
                            bankruptcyStatus = ModelLoanNote.bankruptcyStatus,
                            ceiling = ModelLoanNote.ceiling,
                            comments = ModelLoanNote.comments,
                            deedMortgageStatus = ModelLoanNote.deedMortgageStatus,
                            drawPeriodStartDate = ModelLoanNote.drawPeriodStartDate,
                            repaymentPeriodStartDate = ModelLoanNote.repaymentPeriodStartDate,
                            //name = DateTime.Now.ToString("yyyyMMddHHmmss"),
                            firstPayementDate = ModelLoanNote.firstPayementDate,
                            floor = ModelLoanNote.floor,
                            foreclosureStatus = ModelLoanNote.foreclosureStatus,
                            indexName = ModelLoanNote.indexName,
                            lateCharge = ModelLoanNote.lateCharge,
                            loanCharges = ModelLoanNote.loanCharges,
                            loanTermsModified = ModelLoanNote.loanTermsModified,
                            margin = ModelLoanNote.margin,


                            nextAdjustement = ModelLoanNote.nextAdjustement,
                            nextPayementDate = ModelLoanNote.nextPayementDate,
                            noteInterestRate = ModelLoanNote.noteInterestRate,
                            noteMaturityDate = ModelLoanNote.noteMaturityDate,
                            ofPayementsLast12 = ModelLoanNote.ofPayementsLast12,
                            onForbearancePlan = ModelLoanNote.onForbearancePlan,
                            originalLoanAmount = ModelLoanNote.originalLoanAmount,
                            originationDate = ModelLoanNote.originationDate,
                            paidToDate = ModelLoanNote.paidToDate,
                            payementTrust = ModelLoanNote.payementTrust,
                            payersLastPayementMadeDate = ModelLoanNote.payersLastPayementMadeDate,
                            percentagePrice = ModelLoanNote.percentagePrice,
                            PI = ModelLoanNote.PI,
                            prePayPenalty = ModelLoanNote.prePayPenalty,
                            propertyTaxesDue = ModelLoanNote.propertyTaxesDue,
                            registerswMers = ModelLoanNote.registerswMers,
                            loanServicerID = ModelLoanNote.loanServicer,
                            soldInterestRate = ModelLoanNote.soldInterestRate,
                            TotalMonthlyLoanPayement = ModelLoanNote.TotalMonthlyLoanPayement,
                            UnpaidInterest = ModelLoanNote.UnpaidInterest,
                            unPaidPrincipalBalance = ModelLoanNote.unPaidPrincipalBalance,


                            amortizationTypeID = ModelLoanNote.amortizationType,
                            loanStatusID = ModelLoanNote.loanStatus,
                            closingTypeID = ModelLoanNote.closingType,
                            loanTypeID = ModelLoanNote.loanType,
                            rateTypeId = ModelLoanNote.rateType,
                            paymentFrequencyID = ModelLoanNote.paymentFrequency,
                            userID = User.Identity.GetUserId(),
                            isPublished = false



                        };

                        db.loanNote.Add(dbLoanNote);
                        db.SaveChanges();
                        lastloanID = dbLoanNote.loanNoteID;

                        if (ModelLoanNote.deedMortgageStatus == true)
                        {

                            var dbdeedMortgage = new deedMortgage
                            {
                                juniorTrust = ModelLoanNote.deedMortgage.juniorTrust,
                                lienPosition1 = ModelLoanNote.deedMortgage.lienPosition1,
                                lienPosition2 = ModelLoanNote.deedMortgage.lienPosition2,
                                monthlyPayment1 = ModelLoanNote.deedMortgage.monthlyPayment1,
                                monthlyPayment2 = ModelLoanNote.deedMortgage.monthlyPayment2,
                                current1 = ModelLoanNote.deedMortgage.current1,
                                current2 = ModelLoanNote.deedMortgage.current2,
                                prnicipalBalance1 = ModelLoanNote.deedMortgage.prnicipalBalance1,
                                prnicipalBalance2 = ModelLoanNote.deedMortgage.prnicipalBalance2,
                                modified1 = ModelLoanNote.deedMortgage.modified1,
                                modified2 = ModelLoanNote.deedMortgage.modified2,
                                informationAsOf1 = ModelLoanNote.deedMortgage.informationAsOf1,
                                informationAsOf2 = ModelLoanNote.deedMortgage.informationAsOf2,
                                loanNoteID = lastloanID

                            };
                            db.deedMortgage.Add(dbdeedMortgage);


                        }




                        var dbproperty = new property
                        {
                            typePropertyID = ModelLoanNote.property.typePropertyID,
                            address = ModelLoanNote.property.address,
                            city = ModelLoanNote.property.city,
                            country = ModelLoanNote.property.country,
                            zip = ModelLoanNote.property.zip,
                            occupancyStatusID = ModelLoanNote.property.occupancyStatusID,
                            stateID = ModelLoanNote.property.stateID,
                            propertyMarketValue = ModelLoanNote.property.propertyMarketValue,
                            propertyValuationDate = ModelLoanNote.property.propertyValuationDate,
                            valuationTypeID = ModelLoanNote.property.valuationTypeID,
                            loanNoteId = lastloanID
                        };

                        db.property.Add(dbproperty);

                        var dbborrower = new borrower
                        {
                            isCompany = ModelLoanNote.borrower.isCompany,
                            firstName = ModelLoanNote.borrower.firstName,
                            lastName = ModelLoanNote.borrower.lastName,
                            maillingAddress = ModelLoanNote.borrower.maillingAddress,
                            homePh = ModelLoanNote.borrower.homePh,
                            workPh = ModelLoanNote.borrower.workPh,
                            fssTaxID = ModelLoanNote.borrower.fssTaxID,
                            companyName = ModelLoanNote.borrower.companyName,
                            companyContactName = ModelLoanNote.borrower.companyContactName,
                            address = ModelLoanNote.borrower.address,
                            city = ModelLoanNote.borrower.city,
                            country = ModelLoanNote.borrower.country,
                            zip = ModelLoanNote.borrower.zip,
                            phone = ModelLoanNote.borrower.phone,
                            mobilePhone = ModelLoanNote.borrower.mobilePhone,
                            email = ModelLoanNote.borrower.email,
                            stateID = ModelLoanNote.borrower.stateID,
                            loanNoteId = lastloanID
                        };

                        db.borrower.Add(dbborrower);

                        if (ModelLoanNote.foreclosureStatus == true)
                        {
                            var dbforeclosure = new foreclosure
                            {
                                forceclosureStatus = true,
                                attorneyFeesDue = ModelLoanNote.foreclosure.attorneyFeesDue,
                                name = ModelLoanNote.foreclosure.name,
                                address = ModelLoanNote.foreclosure.address,
                                city = ModelLoanNote.foreclosure.city,
                                scheduledSaleDate = ModelLoanNote.foreclosure.scheduledSaleDate,
                                zip = ModelLoanNote.foreclosure.zip,
                                phone = ModelLoanNote.foreclosure.phone,
                                email = ModelLoanNote.foreclosure.email,
                                dateOpened = ModelLoanNote.foreclosure.dateOpened,
                                stateID = ModelLoanNote.foreclosure.stateID,
                                loanNoteID = lastloanID
                            };
                            db.foreclosure.Add(dbforeclosure);
                        }

                        if (ModelLoanNote.loanType == 3)
                        {

                        }

                        if (ModelLoanNote.bankruptcyStatus == true)
                        {
                            var dbbankruptcy = new bankruptcy
                            {
                                bkFillingDate = ModelLoanNote.bankruptcy.bkFillingDate,
                                bkDisChargeDate = ModelLoanNote.bankruptcy.bkDisChargeDate,
                                bkDismissalDate = ModelLoanNote.bankruptcy.bkDismissalDate,
                                bkChapterID = ModelLoanNote.bankruptcy.bkChapterID,
                                loanNoteID = lastloanID

                            };
                            db.bankruptcy.Add(dbbankruptcy);
                        }

                        if (ModelLoanNote.closingType == 3 || ModelLoanNote.closingType == 2)
                        {
                            var dbescrowImpounds = new escrowImpounds
                            {
                                notePaymentAmount = ModelLoanNote.escrowImpounds.notePaymentAmount,
                                annualInsurancePremium = ModelLoanNote.escrowImpounds.annualInsurancePremium,
                                taxPortion = ModelLoanNote.escrowImpounds.taxPortion,
                                annualTaxes = ModelLoanNote.escrowImpounds.annualTaxes,
                                insurancePortion = ModelLoanNote.escrowImpounds.insurancePortion,
                                trustBalance = ModelLoanNote.escrowImpounds.trustBalance,
                                loanNoteId = lastloanID

                            };
                            db.escrowImpounds.Add(dbescrowImpounds);
                        }



                        /** Save File **/

                        if (httpRequest.Files.GetMultiple("other").Count() > 0)
                        {
                            foreach (HttpPostedFile file in httpRequest.Files.GetMultiple("other"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_"+lastloanID + "_"+ DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var doc = new document
                                    {
                                        name = generatedfilename,
                                        loanNoteID = lastloanID,
                                        createAt = DateTime.Today,
                                        documentPath = "other"

                                    };
                                    file.SaveAs(filePath);
                                    db.document.Add(doc);
                                }
                                catch (Exception e)
                                {
                                    throw e;
                                }
                            }
                        }
                        if (httpRequest.Files.GetMultiple("photo").Count() > 0)
                        {
                            foreach (HttpPostedFile photo in httpRequest.Files.GetMultiple("photo"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_" + lastloanID + "_"+ DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + photo.FileName;

                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var dbphoto = new photo
                                    {
                                        title = generatedfilename,
                                        loanNoteID = lastloanID
                                    };
                                    photo.SaveAs(filePath);
                                    db.photo.Add(dbphoto);
                                }
                                catch (Exception e)
                                {

                                    throw e;
                                }
                            }
                        }
                        if (httpRequest.Files.GetMultiple("PaymentHistory").Count() > 0)
                        {
                            foreach (HttpPostedFile file in httpRequest.Files.GetMultiple("PaymentHistory"))
                            {
                                string generatedfilename = User.Identity.GetUserId() + "_" + lastloanID + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + file.FileName;
                                var filePath = HttpContext.Current.Server.MapPath("~/App_Data/uploads/" + generatedfilename);
                                try
                                {
                                    var doc = new document
                                    {
                                        name = generatedfilename,
                                        loanNoteID = lastloanID,
                                        createAt = DateTime.Today,
                                        documentPath = "PaymentHistory"

                                    };
                                    file.SaveAs(filePath);
                                    db.document.Add(doc);
                                }
                                catch (Exception e)
                                {

                                    throw e;
                                }

                            }
                        }

                        /* Save All temporary*/
                        db.SaveChanges();


                       


                        if (!ModelState.IsValid)
                        {
                            return BadRequest(ModelState);
                        }

                        /* Save in Datebase if all good */
                        dbContextTransaction.Commit();
                        //get user info
                        var email = User.Identity.GetUserName();
                        var first = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).firstName;
                        var last = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).lastName;
                        var phone = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).PhoneNumber;
                        var address = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).address;
                        var zip = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).zip;
                        var stateid = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).stateID;
                        var city = db.Users.FirstOrDefault(u => u.Email == HttpContext.Current.User.Identity.Name).city;
                        var state = db.state.FirstOrDefault(s => s.ID == stateid).name;


                        //create loan Opportunity and person in prosperworks
                        var client = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/zcvcax/");
                        var request = new RestRequest(Method.POST);
                        request.AddParameter("application/json", "{\t\"oppName\":\"Loan: " + lastloanID + "\",\n\t\"email\":\"" + email + "\",\n\t\"value\":\"" + ModelLoanNote.askingPrice + "\",\n\t\"personFullName\":\"" + first + " " + last + "\",\n\t\"phone\":\"" + phone + "\",\n\t\"street\":\"" + address + "\",\n\t\"city\":\"" + city + "\",\n\t\"state\":\"" + state + "\",\n\t\"zip\":\"" + zip + "\"\n}", ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);




                        //var client1 = new RestClient("https://hooks.zapier.com/hooks/catch/2766013/zzr94l/");
                        //var request1 = new RestRequest(Method.POST);
                        //request1.AddParameter("undefined", "{\n\t\"name\":\""+first+" "+last+"\",\n\t\"email\":\""+email+"\",\n\t\"oppname\":\"ooo loan zineb\"\n\t\n}", ParameterType.RequestBody);
                        //IRestResponse response = client1.Execute(request1);

                    }
                    catch (Exception)
                    {
                        /* Rollback cancel all saved recording */
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }








            return Json(httpRequest.Files.GetMultiple("PaymentHistory").Count());
        }

        // DELETE: api/loanNotes/5
        [ResponseType(typeof(loanNote))]
        public IHttpActionResult DeleteloanNote(int id)
        {
            loanNote loanNote = db.loanNote.Find(id);
            if (loanNote == null)
            {
                return NotFound();
            }

            db.loanNote.Remove(loanNote);
            db.SaveChanges();

            return Ok(loanNote);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool loanNoteExists(int id)
        {
            return db.loanNote.Count(e => e.loanNoteID == id) > 0;
        }
    }
}