﻿using LocalAccountsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LocalAccountsApp.Controllers
{
    [RoutePrefix("api/types")]
    public class TypesController : ApiController
    {
        private ApplicationDbContext _dbContext;
        public TypesController()
        {   
            _dbContext = ApplicationDbContext.Create();
        }

        [Route("closingTypes")]
        public IHttpActionResult GetClosingTypes()
        {
            return Ok(_dbContext.closingTypes.Select(c => new { typeId = c.ClosingTypeId, typeName = c.Name }).ToList());
        }

        [Route("amortizationtypes")]
        public IHttpActionResult GetAmortisationTypes()
        {
            //var isAdmin = IsAdmin();

            return Ok(_dbContext.amortizationType.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        private bool IsAdmin()
        {
            return User.IsInRole("Admin");
        }

        [Route("loantypes")]
        public IHttpActionResult GetLoanTypes()
        {
            return Ok(_dbContext.loanType.Select(c => new { typeId = c.ID, typeName = c.nameLoanType }).ToList());
        }

        [Route("paymentfrequencies")]
        public IHttpActionResult GetPaymentFrequenciesTypes()
        {
            return Ok(_dbContext.paymentFrequency.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("ratetypes")]
        public IHttpActionResult GetRateTypes()
        {
            return Ok(_dbContext.rateType.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("propertytypes")]
        public IHttpActionResult GetPropertyTypes()
        {
            return Ok(_dbContext.typeProperty.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("bkchaptertypes")]
        public IHttpActionResult GetBkChapterTypes()
        {
            return Ok(_dbContext.bkChapter.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("valuationtypes")]
        public IHttpActionResult GetValuationTypes()
        {
            return Ok(_dbContext.valuationType.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("occupancystatus")]
        public IHttpActionResult GetOccupancyStatus()
        {
            return Ok(_dbContext.occupancyStatus.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("loanstatus")]
        public IHttpActionResult GetLoanStatus()
        {
            return Ok(_dbContext.loanStatus.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("states")]
        public IHttpActionResult GetStates()
        {
            return Ok(_dbContext.state.Select(c => new { typeId = c.ID, typeName = c.name }).ToList());
        }

        [Route("loanServicer")]
        public IHttpActionResult GetloanServicer()
        {
            return Ok(_dbContext.loanServicer.Select(c => new { typeId = c.loanServicerID, typeName = c.name }).ToList());
        }
    }
}
