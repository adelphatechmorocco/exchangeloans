﻿using LocalAccountsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LocalAccountsApp.Controllers
{
    public class ClosingTypesController : ApiController
    {
        private ApplicationDbContext _dbContext;

        public ClosingTypesController()
        {
            _dbContext = new ApplicationDbContext();
        }

        public IHttpActionResult Get()
        {
            var result = _dbContext.closingTypes.ToList();
            return Ok(result);
        }
    }
}