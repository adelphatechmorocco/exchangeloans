﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class OfferStatus
    {
        [Column("offerStatusId")]
        public int OfferStatusId { get; set; }

        [Column("offerStatusName")]
        public string OfferStatusName { get; set; }

        [Column("description")]
        public string Description { get; set; }

        public ICollection<OfferStatusByDate> OfferStatusByDate { get; set; }
    }
}