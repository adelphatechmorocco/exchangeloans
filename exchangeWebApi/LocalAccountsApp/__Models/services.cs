﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class services
    {
        [Column("servicesID")]
        public int ID { get; set; }
        public string name { get; set; }
        public float price { get; set; }
        public string description { get; set; }
        public string abreviation { get; set; }
        public virtual ICollection<transaction> transactions { get; set; }
    }
}