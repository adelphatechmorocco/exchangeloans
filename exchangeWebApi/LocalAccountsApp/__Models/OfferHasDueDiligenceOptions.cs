﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class OfferHasDueDiligenceOptions
    {
        [Key]
        [Column("offerId", Order = 0)]
        public int OfferId { get; set; }

        [ForeignKey("OfferId")]
        public Offer Offer { get; set; }

        [Key]
        [Column( "dueDiligenceOptionsId",Order = 1)]
        public int DueDiligenceOptionsId { get; set; }

        [ForeignKey("DueDiligenceOptionsId")]
        public DueDiligenceOptions DueDiligenceOptions { get; set; }

        [Column("daysCount")]
        public int DaysCount { get; set; }
    }
}