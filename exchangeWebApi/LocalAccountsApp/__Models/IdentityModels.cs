﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using LocalAccountsApp.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace LocalAccountsApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
        public string firstName { get; set; }
        public  string lastName { get; set; }
        public  string company { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string fax { get; set; }
        public bool broker { get; set; }
        public string licence { get; set; }
        public bool bulk { get; set; }
        public bool single { get; set; }
        public virtual hearAboutUs hearaboutus{ get; set; }
        public virtual mailing mailing { get; set; }

        public virtual choiceLienPosition choiceLienPosition { get; set; }
        public virtual choicePropertyType choicePropertyType { get; set; }
  



        
        public virtual capitalAvailable capitaleAvailable { get; set; }

      
        public virtual  propertyValue propertyValue { get; set; }
        public virtual userLoanType userLoanType { get; set; }
        public virtual userPrpertyType userPrpertyType { get; set; }

        public int? stateID { get; set; }
    }
}