﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class userLoanType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int userLoanTypeId { get; set; }
        public bool performing { get; set; }
        public bool nonPerforming { get; set; }
        public bool newlyOriginated { get; set; }
       

        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }

    }
}