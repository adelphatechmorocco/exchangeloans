﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class optionByDate
    {
        [Key]
        [Column(Order = 0)]
        public int optionFeaturedID { get; set; }
        [Key]
        [Column(Order = 1)]
        public int loanNoteID { get; set; }
        [Key]
        [Column(Order = 2)]
        public DateTime modificationDate { get; set; }

        [ForeignKey("optionFeaturedID")]
        public optionFeatured optionFeatured { get; set; }

        [ForeignKey("loanNoteID")]
        public loanNote loanNote { get; set; }
    }
}