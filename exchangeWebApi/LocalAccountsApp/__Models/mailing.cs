﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class mailing
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int mailingId { get; set; }
        public bool featureListing { get; set; }
        public bool offersAndOther { get; set; }
        public bool newListing { get; set; }

        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }
    }
}