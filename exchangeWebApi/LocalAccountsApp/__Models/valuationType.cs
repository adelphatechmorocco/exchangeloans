﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class valuationType
    {
        [Column("valuationTypeID")]
        public int ID { get; set; }
        public string name { get; set; }
        
        
    }
}