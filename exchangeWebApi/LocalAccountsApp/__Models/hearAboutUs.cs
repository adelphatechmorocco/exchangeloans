﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class hearAboutUs
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int hearAboutUsId { get; set; }
        public bool internetSearch  { get; set; }
        public bool newsArticle { get; set; }
        public bool industryTradeGroup { get; set; }
        public bool refferalRecomendation { get; set; }
        public bool tvOrRadio { get; set; }
        public bool fciLenderServices { get; set; }
        public bool email { get; set; }
        public string other { get; set; }
        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }


    }
}