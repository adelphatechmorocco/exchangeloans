﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class closingType
    {
        [Column("closingTypeId")]
        public int ClosingTypeId { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}