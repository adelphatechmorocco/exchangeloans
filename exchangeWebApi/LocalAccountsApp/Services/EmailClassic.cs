﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Reflection;
using System.Xml;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using RestSharp;

namespace SignUp.Services
{
    public class EMail : IIdentityMessageService
    {

        #region Private Fields

        private static string FromAddress;
        private static string strSmtpClient;
        private static string UserID;
        private static string Password;
        private static string SMTPPort;
        private static bool bEnableSSL;

        #endregion

        #region Interface Implementation

        public async Task SendAsync(IdentityMessage message)
        {
            await configSendGridasync(message);
        }

        #endregion

        #region Send Email Method
        public async Task configSendGridasync(IdentityMessage message)
        {

           
                GetMailData();

            var body = "<p>Email From: {0} ({1})</p><p>"+message.Body+"</p><p>{2}</p>";
            var messagex = new MailMessage();
            messagex.To.Add(new MailAddress(message.Destination));  // replace with valid value 
            messagex.From = new MailAddress(FromAddress);  // replace with valid value
            messagex.Subject = message.Subject;
            messagex.Body = string.Format(body, "noreplay", FromAddress,"");
            messagex.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = FromAddress,  // replace with valid value
                    Password = Password  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = strSmtpClient;
                smtp.Port = Int32.Parse(SMTPPort);
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(messagex);
                
            }

 
        }
            
            

        #endregion

        #region Get Email provider data From Web.config file
        private static void GetMailData()
        {
            FromAddress = System.Configuration.ConfigurationManager.AppSettings.Get("FromAddress");
            strSmtpClient = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpClient");
            UserID = System.Configuration.ConfigurationManager.AppSettings.Get("UserID");
            Password = System.Configuration.ConfigurationManager.AppSettings.Get("Password");
            //ReplyTo = System.Configuration.ConfigurationManager.AppSettings.Get("ReplyTo");
            SMTPPort = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPPort");
            if ((System.Configuration.ConfigurationManager.AppSettings.Get("EnableSSL") == null))
            {
            }
            else {
                if ((System.Configuration.ConfigurationManager.AppSettings.Get("EnableSSL").ToUpper() == "YES"))
                {
                    bEnableSSL = true;
                }
                else {
                    bEnableSSL = false;
                }
            }
        }
        #endregion

    }
}