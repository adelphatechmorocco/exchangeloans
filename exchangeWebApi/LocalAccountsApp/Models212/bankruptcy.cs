﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class bankruptcy
    {
        [Column("bankruptcyID")]
        public int ID { get; set; }
        public DateTime? bkFillingDate { get; set; }
        public DateTime? bkDisChargeDate { get; set; }
        public DateTime? bkDismissalDate { get; set; }
        public bool? bankruptcyStatus { get; set; }

        [Key, ForeignKey("loanNote")]
        public int? loanNoteID { get; set; }
        [JsonIgnore]
        public virtual loanNote loanNote { get; set; }

        public int? bkChapterID { get; set; }
        [ForeignKey("bkChapterID")]
        public virtual bkChapter bkChapter { get; set; }
    }
}