﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class choiceLienPosition
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      
        public int choiceLienPositionId { get; set; }
        public bool firstLien { get; set; }
        public bool secondLien { get; set; }
        public bool thirdLien { get; set; }
        public bool other { get; set; }

        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }

    }
}