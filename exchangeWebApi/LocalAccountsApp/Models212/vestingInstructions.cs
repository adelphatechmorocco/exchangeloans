﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LocalAccountsApp.Models
{
    public class vestingInstructions
    {
        [Column("vestingInstructionID")]
        public int ID { get; set; }
        public string company { get; set; }
        public string contactPerson { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public int state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string other { get; set; }
        public int loanServicerId { get; set; }

        public int transactionID { get; set; }
        [ForeignKey("transactionID")]
        public virtual transaction transaction { get; set; }
    }
}