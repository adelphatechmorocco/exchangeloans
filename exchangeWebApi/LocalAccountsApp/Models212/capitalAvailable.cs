﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class capitalAvailable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int capitalAvailableId { get; set; }
        public bool under250K { get; set; }
        public bool between_250K500K { get; set; }
        public bool between_500K750K { get; set; }
        public bool moreThan1MM { get; set; }

        [Key, ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }
    }
}