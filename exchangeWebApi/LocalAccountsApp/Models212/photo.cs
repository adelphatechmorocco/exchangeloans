﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class photo
    {
        [Column("photoID")]
        public int ID { get; set; }
        public string title { get; set; }
        public string path { get; set; }

        public int loanNoteID { get; set; }
        [ForeignKey("loanNoteID")]
        public virtual loanNote loanNote { get; set; }



    }
}