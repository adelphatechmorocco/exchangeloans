﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class LoanOfferBuyer
    {
        [Column("loanOfferBuyyerId")]
        public int LoanOfferBuyerId { get; set; }

        [Column("loanNoteId")]
        public int LoanNoteId { get; set; }

        [Column("offerId")]
        public int OfferId { get; set; }

        [Column("userId")]
        public string UserId { get; set; }

        [ForeignKey("LoanNoteId")]
        public loanNote LoanNote { get; set; }

        [ForeignKey("OfferId")]
        public Offer Offer { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }
}