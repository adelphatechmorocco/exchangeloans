﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class propertyValue
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int propertyValueId { get; set; }
        public bool under100K { get; set; }
        public bool between_100K300K { get; set; }
        public bool between_301K750K { get; set; }
        public bool moreThan750K { get; set; }

        [Key,ForeignKey("user")]
        public string userid { get; set; }
        public ApplicationUser user { get; set; }
    }
}
