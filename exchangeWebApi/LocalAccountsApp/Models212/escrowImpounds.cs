﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.Models
{
    public class escrowImpounds
    {
        [Column("escrowImpounds")]
        public int ID { get; set; }
        public bool? escrowImpoundsStatus { get; set; }
        public float? notePaymentAmount { get; set; }
        public float? annualInsurancePremium { get; set; }
        public float? taxPortion { get; set; }
        public float? annualTaxes { get; set; }
        public float? insurancePortion { get; set; }
        public float? trustBalance { get; set; }

        [Key, ForeignKey("loanNote")]
        public int? loanNoteId { get; set; }
        [JsonIgnore]
        public virtual loanNote loanNote { get; set; }
    }
}