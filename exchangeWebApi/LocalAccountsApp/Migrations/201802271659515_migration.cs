namespace LocalAccountsApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.vestingInstructions", "role", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.vestingInstructions", "role");
        }
    }
}
